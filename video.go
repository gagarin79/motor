package motor

import (
	"github.com/gagarin79/motor/convar"
	"github.com/gagarin79/motor/gfx"
	"flag"
	"fmt"
	"log"
)

// videoMode
type videoMode struct {
	w, h  int
	valid bool
}

var modes = [...]videoMode{
	{640, 480, false},
	{720, 480, false},
	{720, 576, false},
	{800, 600, false},
	{1024, 768, false},
	{1280, 720, false},
	{1280, 768, false},
	{1152, 864, false},
	{1280, 800, false},
	{1360, 768, false},
	{1366, 768, false},
	{1280, 960, false},
	{1440, 900, false},
	{1280, 1024, false},
	{1600, 900, false},
	{1600, 1024, false},
	{1680, 1050, false},
	{1920, 1080, false},
	{2048, 1536, false},
	{2560, 1400, false},
}

// videoSys
type videoSys struct {
	mode int

	fullscreen bool

	x, y int
	w, h int
}

func (v videoSys) String() string {
	fs := "wd"
	if v.fullscreen {
		fs = "fs"
	}
	return fmt.Sprintf("%dx%d@%s", v.w, v.h, fs)
}

var video = videoSys{-1, false, -1, -1, 720, 480}

func (v *videoSys) init() {
	v.selectMode()
	v.createWindow()
	log.Printf("init video: %v\n", v)
	gfx.Init(video.w, video.h)
}

func (v *videoSys) shutdown() {
	gfx.Shutdown()
	log.Println("shutdown video: ok")
}

func (v *videoSys) selectMode() {
	if v.mode == -1 && v.w > 0 && v.h > 0 {
		return
	}

	enumModes()

	if v.mode < -1 || v.mode >= len(modes) {
		log.Printf("invalid video mode: %v\n", v.mode)
		v.mode = -1
	}
	var mode videoMode
	if v.mode == -1 || !modes[v.mode].valid {
		v.fullscreen = true
		for v.mode = len(modes) - 1; v.mode >= 0; v.mode-- {
			if modes[v.mode].valid {
				mode = modes[v.mode]
				return
			}
		}
		log.Fatalln("no available video mode found")
	} else {
		mode = modes[v.mode]
	}
	v.w, v.h = mode.w, mode.h
}

func init() {
	convar.Int(&video.mode, "video.mode", -1)
	convar.Bool(&video.fullscreen, "video.fullscreen", false)
	convar.Int(&video.x, "video.x", -1)
	convar.Int(&video.y, "video.y", -1)

	flag.IntVar(&video.mode, "mode", video.mode, "video mode number")
	flag.BoolVar(&video.fullscreen, "fs", video.fullscreen, "fullscreen mode")
	flag.IntVar(&video.w, "w", video.w, "window width")
	flag.IntVar(&video.h, "h", video.h, "window height")
}
