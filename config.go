package motor

import (
	"github.com/gagarin79/motor/convar"
	"github.com/gagarin79/motor/fs"
	"log"
	"os"
)

func readConfig() {
	f, err := fs.Open("config.json")
	if os.IsNotExist(err) {
		return
	} else if err != nil {
		log.Println(err)
		return
	}
	defer f.Close()
	if err := convar.Read(f); err != nil {
		log.Println(err)
	}
}

func writeConfig() {
	f, err := fs.Create(fs.Base, "config.json")
	if err != nil {
		log.Println(err)
		return
	}
	defer f.Close()
	if err := convar.Write(f); err != nil {
		log.Println(err)
	}
}
