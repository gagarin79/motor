// Package cmd provides interface to execution
// textual commands.
package cmd

import (
	"log"
	"sort"
	"strings"
)

// command call with args.
type call struct {
	f    func(a []string)
	args []string
}

var (
	cmds  = make(map[string]func(a []string))
	calls = make([]call, 0, 100)
)

// sortCmds returns the commands names as a slice in lexicographical order.
func sortCmds() []string {
	list := make(sort.StringSlice, len(cmds))
	i := 0
	for k := range cmds {
		list[i] = k
		i++
	}
	list.Sort()
	return list
}

// Visit visits the commands in lexicographical order,
// calling fn for each command name.
func Visit(fn func(string)) {
	for _, k := range sortCmds() {
		fn(k)
	}
}

// Parse text into call. Returning false if
// command not found.
func parseCall(text string) (call, bool) {
	args, c := strings.Fields(text), call{}
	if len(args) == 0 {
		return c, false
	}
	if f, ok := cmds[args[0]]; ok {
		c.f, c.args = f, args
		return c, true
	}
	return c, false
}

// Fetch last command call. Returning false
// if call queue is empty.
func fetchCall() (call, bool) {
	n := len(calls)
	if n == 0 {
		return call{}, false
	}
	c := calls[n-1]
	calls = calls[0 : n-1]
	return c, true
}

// Register registers command function with specified name.
func Register(name string, f func(a []string)) {
	if _, dup := cmds[name]; dup {
		log.Printf("command %q already exists\n", name)
		return
	}
	cmds[name] = f
}

// Execute parse text string and if command exists
// push call of command to call queue.
func Execute(text string) {
	if c, ok := parseCall(text); ok {
		calls = append(calls, c)
	}
}

// ExecuteNow parse text string and if command exists
// executes it.
func ExecuteNow(text string) {
	if c, ok := parseCall(text); ok {
		c.f(c.args)
	}
}

// ExecuteCalls executes queue of commands calls.
func ExecuteCalls() {
	for c, ok := fetchCall(); ok; c, ok = fetchCall() {
		c.f(c.args)
	}
}
