// Package math provides basic constants and mathematical functions.
// This is float32 version of the standard math package.
package math

import "math"

// Pi is math.Pi but as a float32 type
const Pi = float32(math.Pi)
const Sqrt2 = float32(math.Sqrt2)
const MaxFloat32 = math.MaxFloat32
const SmallestNonzeroFloat32 = math.SmallestNonzeroFloat32

// redefinition of math functions for float32

func Acos(x float32) float32         { return float32(math.Acos(float64(x))) }
func Atan2(x, y float32) float32     { return float32(math.Atan2(float64(x), float64(y))) }
func Ceil(x float32) float32         { return float32(math.Ceil(float64(x))) }
func Cos(x float32) float32          { return float32(math.Cos(float64(x))) }
func Floor(x float32) float32        { return float32(math.Floor(float64(x))) }
func Mod(x, y float32) float32       { return float32(math.Mod(float64(x), float64(y))) }
func Remainder(x, y float32) float32 { return float32(math.Remainder(float64(x), float64(y))) }
func Sin(x float32) float32          { return float32(math.Sin(float64(x))) }
func Sqrt(x float32) float32         { return float32(math.Sqrt(float64(x))) }
func Tan(x float32) float32          { return float32(math.Tan(float64(x))) }

func Signbit(x float32) bool {
	return math.Signbit(float64(x))
}

// it's more effective to convert to float64 and use the assembly
// code than to handle all the special cases in Go
func Min(x1, x2 float32) float32 { return float32(math.Min(float64(x1), float64(x2))) }
func Max(x1, x2 float32) float32 { return float32(math.Max(float64(x1), float64(x2))) }

// see Googles src/pkg/math for details

const (
	uvnan = 0x7fc00001
)

func NaN() float32                   { return math.Float32frombits(uvnan) }
func IsNaN(f float32) bool           { return f != f }
func Inf(sign int) float32           { return float32(math.Inf(sign)) }
func IsInf(f float32, sign int) bool { return math.IsInf(float64(f), sign) }

func Copysign(x, y float32) float32 {
	const sign = 1 << 31
	return math.Float32frombits(math.Float32bits(x)&^sign | math.Float32bits(y)&sign)
}

func Abs(x float32) float32 {
	const sign = 1 << 31
	return math.Float32frombits(math.Float32bits(x) &^ sign)
}

func Nextafter(x, y float32) (r float32) {
	switch {
	case IsNaN(x) || IsNaN(y): // special case
		r = NaN()
	case x == y:
		r = x
	case x == 0:
		r = Copysign(math.Float32frombits(1), y)
	case (y > x) == (x > 0):
		r = math.Float32frombits(math.Float32bits(x) + 1)
	default:
		r = math.Float32frombits(math.Float32bits(x) - 1)
	}
	return
}

func Clamp(p, p0, p1 float32) float32 {
	if p0 > p1 {
		return NaN()
	} else if p < p0 {
		return p0
	} else if p > p1 {
		return p1
	}
	return p
}
