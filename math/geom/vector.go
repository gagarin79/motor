// Package geom provides basic linear algebra types
// and functions.
package geom

import (
	"github.com/gagarin79/motor/math"
	"strconv"
)

// A Vector2 is an spatial vector in 2D space.
type Vector2 struct {
	X, Y float32
}

// String returns a string representation of v like "(3.14,1.59)".
func (v Vector2) String() string {
	return "(" + strconv.FormatFloat(float64(v.X), 'f', 6, 64) + "," +
		strconv.FormatFloat(float64(v.Y), 'f', 6, 64) + ")"
}

// Add returns the vector v + a.
func (v Vector2) Add(a Vector2) Vector2 {
	return Vector2{v.X + a.X, v.Y + a.Y}
}

// Sub returns the vector v - a.
func (v Vector2) Sub(a Vector2) Vector2 {
	return Vector2{v.X - a.X, v.Y - a.Y}
}

// Mul returns the vector k * p.
func (v Vector2) Mul(k float32) Vector2 {
	return Vector2{v.X * k, v.Y * k}
}

// Mul returns the vector v/k.
func (v Vector2) Div(k float32) Vector2 {
	return Vector2{v.X / k, v.Y / k}
}

// Dot returns the dot product v·a.
func (v Vector2) Dot(a Vector2) float32 {
	return v.X*a.X + v.Y*a.Y
}

// Neg returns the vector -p, or equivalently p rotated by 180 degrees.
func (v Vector2) Neg() Vector2 {
	return Vector2{-v.X, -v.Y}
}

// Len returns the length of the vector v.
func (v Vector2) Len() float32 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

// Norm returns the vector v normalized to the length of vector v, or the zero Vector2
// if v is degenerate.
func (v *Vector2) Norm() Vector2 {
	d := v.Len()
	if d == 0.0 {
		return ZV2
	}
	t := 1.0 / d
	return Vector2{v.X * t, v.Y * t}
}

// ZV2 is zero Vector2.
var ZV2 Vector2

// Vec2 is shorthand for Vector2{x, y}.
func Vec2(x, y float32) Vector2 {
	return Vector2{x, y}
}

// A Vector3 is an spatial vector in 3D space.
type Vector3 struct {
	X, Y, Z float32
}

// String returns a string representation of v like "(3.14,1.59,2.61)".
func (v Vector3) String() string {
	return "(" + strconv.FormatFloat(float64(v.X), 'f', 6, 64) + "," +
		strconv.FormatFloat(float64(v.Y), 'f', 6, 64) + "," +
		strconv.FormatFloat(float64(v.Z), 'f', 6, 64) + ")"
}

// Add returns the vector v + a.
func (v Vector3) Add(a Vector3) Vector3 {
	return Vector3{v.X + a.X, v.Y + a.Y, v.Z + a.Z}
}

// Sub returns the vector v - a.
func (v Vector3) Sub(a Vector3) Vector3 {
	return Vector3{v.X - a.X, v.Y - a.Y, v.Z - a.Z}
}

// Mul returns the vector k * p.
func (v Vector3) Mul(k float32) Vector3 {
	return Vector3{v.X * k, v.Y * k, v.Z * k}
}

// Mul returns the vector v/k.
func (v Vector3) Div(k float32) Vector3 {
	return Vector3{v.X / k, v.Y / k, v.Z / k}
}

// Dot returns the dot product v·a.
func (v Vector3) Dot(a Vector3) float32 {
	return v.X*a.X + v.Y*a.Y + v.Z*a.Z
}

// Neg returns the vector -p, or equivalently p rotated by 180 degrees.
func (v Vector3) Neg() Vector3 {
	return Vector3{-v.X, -v.Y, -v.Z}
}

// Len returns the length of the vector v.
func (v Vector3) Len() float32 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y + v.Z*v.Z)
}

// Norm returns the vector v normalized to the length of vector v, or the zero Vector3
// if v is degenerate.
func (v *Vector3) Norm() Vector3 {
	d := v.Len()
	if d == 0.0 {
		return ZV3
	}
	t := 1.0 / d
	return Vector3{v.X * t, v.Y * t, v.Z * t}
}

// ZV3 is zero Vector3.
var ZV3 Vector3

// Vec3 is shorthand for Vector3{x, y, z}.
func Vec3(x, y, z float32) Vector3 {
	return Vector3{x, y, z}
}
