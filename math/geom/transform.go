package geom

import "github.com/gagarin79/motor/math"

// Rotation.
type Rotation struct {
	S, C float32
}

func Rot(a float32) Rotation {
	return Rotation{S: math.Sin(a), C: math.Cos(a)}
}

func (q *Rotation) Reset() {
	q.S, q.C = 0, 1
}

func (q *Rotation) Angle() float32 {
	return math.Atan2(q.S, q.C)
}

func (q *Rotation) SetAngle(a float32) {
	q.S, q.C = math.Sin(a), math.Cos(a)
}

// Transform.
type Transform struct {
	P Vector2
	Q Rotation
}

func Xf(p Vector2, a float32) Transform {
	return Transform{p, Rot(a)}
}

func (t *Transform) Reset() {
	t.P = ZV2
	t.Q.Reset()
}

func (t *Transform) Set(p Vector2, a float32) {
	t.P = p
	t.Q.SetAngle(a)
}
