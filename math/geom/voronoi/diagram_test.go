package voronoi

import (
	"github.com/gagarin79/motor/geom"
	"math/rand"
	"testing"
)

func BenchmarkCompute(b *testing.B) {
	var (
		rnd    = rand.New(rand.NewSource(1))
		d      = NewDiagram()
		points = make([]*Point, 1000)
		dx     = 1920.0
		dy     = 1080.0
	)
	for i := 0; i < len(points); i++ {
		points[i] = Pt(rnd.Float64()*dx, rnd.Float64()*dy)
	}
	for i := 0; i < b.N; i++ {
		d.Compute(points)
		d.CloseRect(geom.Rect(0, 0, dx, dy))
	}
}
