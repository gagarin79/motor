package voronoi

// event.
// if circle != nil then is circle event otherwise
// is site event.
type event struct {
	site     *Site
	x, y, yc float64
	arc      *arc
}

func newEvent(s *Site, a *arc) *event {
	return &event{site: s, arc: a, x: s.X, y: s.Y}
}

// eventsList.
type eventList []*event

func (l eventList) find(e *event) int {
	for i := 0; i < len(l); i++ {
		if l[i] == e {
			return i
		}
	}
	return -1
}

func (l *eventList) remove(e *event) bool {
	i := l.find(e)
	if i == -1 {
		return false
	}
	a := *l
	*l = append(a[:i], a[i+1:]...)
	return true
}

// eventQueue attaches the methods of heap.Interface to []*event.
type eventQueue []*event

// Len returns length of event queue.
func (q eventQueue) Len() int {
	return len(q)
}

// Less check position of two points in event queue.
// first check greater Y position and then greater X position.
func (q eventQueue) Less(i, j int) bool {
	b, a := q[j], q[i]
	r := a.y - b.y
	if r != 0.0 {
		return r < 0.0
	}
	return a.x < b.x
}

// Swap swaps two elements in queue.
func (q eventQueue) Swap(i, j int) {
	q[i], q[j] = q[j], q[i]
}

// Push adds new element to back of queue.
func (q *eventQueue) Push(x interface{}) {
	a := *q
	n := len(a)
	if cap(a) < n+1 {
		c := make(eventQueue, n, (n+1)*2+1)
		copy(c, *q)
		a = c
	}
	a = a[0 : n+1]
	e := x.(*event)
	a[n] = e
	*q = a
}

// Pop returns and removes last element from queue.
func (q *eventQueue) Pop() interface{} {
	a := *q
	n := len(a)
	e := a[n-1]
	*q = a[0 : n-1]
	return e
}
