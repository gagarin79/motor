package voronoi

import "math"

type arc struct {
	site        *Site
	red         bool
	parent      *arc
	left, right *arc
	prev, next  *arc
	edge        *Edge
	ev          *event
}

func (a *arc) leftBP(dix float64) float64 {
	p := a.site
	rfocx, rfocy := p.X, p.Y
	prby2 := rfocy - dix
	if prby2 == 0.0 {
		return rfocx
	}
	if a.prev == nil {
		return math.Inf(-1)
	}
	l := a.prev
	p = l.site
	lfocx, lfocy := p.X, p.Y
	plby2 := lfocy - dix
	if plby2 == 0.0 {
		return lfocx
	}
	hl := lfocx - rfocx
	aby2 := 1.0/prby2 - 1.0/plby2
	b := hl / plby2
	if aby2 != 0.0 {
		return (-b+math.Sqrt(b*b-2.0*aby2*(hl*hl/(-2.0*plby2)-lfocy+plby2/2.0+rfocy-prby2/2.0)))/aby2 + rfocx
	}
	return (rfocx + lfocx) / 2.0
}

func (a *arc) rightBP(dix float64) float64 {
	if a.next != nil {
		return a.next.leftBP(dix)
	}
	if a.site.Y == dix {
		return a.site.X
	}
	return math.Inf(1)
}

func (n *arc) first() *arc {
	for n.left != nil {
		n = n.left
	}
	return n
}

func (n *arc) last() *arc {
	for n.right != nil {
		n = n.right
	}
	return n
}

func newArc(s *Site) *arc {
	return &arc{site: s}
}

type rbtree struct {
	root *arc
}

func (t *rbtree) insert(m, n *arc) {
	var p, q, r *arc
	if m != nil {
		n.prev, n.next = m, m.next
		if m.next != nil {
			m.next.prev = n
		}
		m.next = n
		if m.right != nil {
			m = m.right.first()
			m.left = n
		} else {
			m.right = n
		}
		p = m
	} else if t.root != nil {
		m = t.root.first()
		n.prev, n.next = nil, m
		m.prev, m.left = n, n
		p = m
	} else {
		n.prev, n.next = nil, nil
		t.root, p = n, nil
	}
	n.left, n.right = nil, nil
	n.parent, n.red = p, true
	m = n
	for p != nil && p.red {
		q = p.parent
		if p == q.left {
			r = q.right
			if r != nil && r.red {
				p.red, r.red = false, false
				q.red, m = true, q
			} else {
				if m == p.right {
					t.rotateLeft(p)
					m, p = p, p.parent
				}
				p.red, q.red = false, true
				t.rotateRight(q)
			}
		} else {
			r = q.left
			if r != nil && r.red {
				p.red, r.red = false, false
				q.red, m = true, q
			} else {
				if m == p.left {
					t.rotateRight(p)
					m, p = p, p.parent
				}
				p.red, q.red = false, true
				t.rotateLeft(q)
			}
		}
		p = m.parent
	}
	t.root.red = false
}

func (t *rbtree) remove(n *arc) {
	var (
		q, r *arc
		red  bool
	)
	if n.next != nil {
		n.next.prev = n.prev
	}
	if n.prev != nil {
		n.prev.next = n.next
	}
	n.next, n.prev = nil, nil
	p, l, r := n.parent, n.left, n.right
	if l == nil {
		q = r
	} else if r == nil {
		q = l
	} else {
		q = r.first()
	}
	if p != nil {
		if p.left == n {
			p.left = q
		} else {
			p.right = q
		}
	} else {
		t.root = q
	}
	if l != nil && r != nil {
		red, q.red = q.red, n.red
		q.left, l.parent = l, q
		if q != r {
			p, q.parent = q.parent, n.parent
			n, p.left = q.right, q.right
			q.right, r.parent = r, q
		} else {
			q.parent, p, n = p, q, q.right
		}
	} else {
		red, n = n.red, q
	}
	if n != nil {
		n.parent = p
	}
	if red {
		return
	}
	if n != nil && n.red {
		n.red = false
		return
	}
	first := true
	for first || !n.red {
		first = false
		if n == t.root {
			break
		}
		if n == p.left {
			r = p.right
			if r.red {
				r.red, p.red = false, true
				t.rotateLeft(p)
				r = p.right
			}
			if (r.left != nil && r.left.red) || (r.right != nil && r.right.red) {
				if r.right == nil || !r.right.red {
					r.left.red, r.red = false, true
					t.rotateRight(r)
					r = p.right
				}
				r.red, p.red, r.right.red = p.red, false, false
				t.rotateLeft(p)
				n = t.root
				break
			}
		} else {
			r = p.left
			if r.red {
				r.red, p.red = false, true
				t.rotateRight(p)
				r = p.left
			}
			if (r.left != nil && r.left.red) || (r.right != nil && r.right.red) {
				if r.left == nil || !r.left.red {
					r.right.red, r.red = false, true
					t.rotateLeft(r)
					r = p.left
				}
				r.red, p.red, r.left.red = p.red, false, false
				t.rotateRight(p)
				n = t.root
				break
			}
		}
		r.red, n, p = true, p, p.parent
	}
	if n != nil {
		n.red = false
	}
}

func (t *rbtree) rotateLeft(n *arc) {
	var (
		p, q = n, n.right
		r    = p.parent
	)
	if r != nil {
		if r.left == p {
			r.left = q
		} else {
			r.right = q
		}
	} else {
		t.root = q
	}
	q.parent, p.parent, p.right = r, q, q.left
	if p.right != nil {
		p.right.parent = p
	}
	q.left = p
}

func (t *rbtree) rotateRight(n *arc) {
	var (
		p, q = n, n.left
		r    = p.parent
	)
	if r != nil {
		if r.left == p {
			r.left = q
		} else {
			r.right = q
		}
	} else {
		t.root = q
	}
	q.parent, p.parent, p.left = r, q, q.right
	if p.left != nil {
		p.left.parent = p
	}
	q.right = p
}
