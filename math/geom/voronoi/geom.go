package voronoi

import (
	"math"
	"sort"
)

type Point struct {
	X, Y float64
}

func Pt(x float64, y float64) *Point {
	return &Point{x, y}
}

// Rectangle.
type Rectangle struct {
	Min, Max Point
}

func Rect(x0, y0, x1, y1 float64) Rectangle {
	if x0 > x1 {
		x0, x1 = x1, x0
	}
	if y0 > y1 {
		y0, y1 = y1, y0
	}
	return Rectangle{Point{x0, y0}, Point{x1, y1}}
}

type Site struct {
	*Point
	Halfedges []Halfedge
}

func (s *Site) addHE(e *Edge, l, r *Site) {
	h := Halfedge{site: l, edge: e}
	if r != nil {
		h.angle = math.Atan2(r.Y-l.Y, r.X-l.X)
	} else {
		a, b := e.A, e.B
		if e.L == l {
			h.angle = math.Atan2(b.X-a.X, a.Y-b.Y)
		} else {
			h.angle = math.Atan2(a.X-b.X, b.Y-a.Y)
		}
	}
	a := s.Halfedges
	n := len(a)
	if cap(a) < n+1 {
		b := make([]Halfedge, n+1, (n+1)*2+1)
		copy(b, a)
		a = b
	}
	a = a[:n+1]
	a[n] = h
	s.Halfedges = a
}

func (s *Site) fix() bool {
	for i := len(s.Halfedges) - 1; i >= 0; i-- {
		e := s.Halfedges[i].edge
		if e.A == nil || e.B == nil {
			s.Halfedges = s.Halfedges[:i+copy(s.Halfedges[i:], s.Halfedges[i+1:])]
		}
	}
	sort.Sort(halfedgeSlice(s.Halfedges))
	return len(s.Halfedges) > 0
}

type Edge struct {
	L, R *Site
	A, B *Point
}

func (e *Edge) setStart(l, r *Site, p *Point) {
	if e.A == nil && e.B == nil {
		e.A, e.L, e.R = p, l, r
	} else if e.L == r {
		e.B = p
	} else {
		e.A = p
	}
}

func (e *Edge) setEnd(l, r *Site, p *Point) {
	e.setStart(r, l, p)
}

func newEdge(l, r *Site, a, b *Point) *Edge {
	e := &Edge{L: l, R: r}
	if a != nil {
		e.setStart(l, r, a)
	}
	if b != nil {
		e.setEnd(l, r, b)
	}
	l.addHE(e, l, r)
	r.addHE(e, r, l)
	return e
}

func (e *Edge) connectRect(r *Rectangle) bool {
	if e.B != nil {
		return true
	}
	var (
		a, b   *Point = e.A, nil
		xl, xr        = r.Min.X, r.Max.X
		yt, yb        = r.Min.Y, r.Max.Y
		lp, rp        = e.L, e.R
		lx, ly        = lp.X, lp.Y
		rx, ry        = rp.X, rp.Y
		fx, fy        = (lx + rx) / 2.0, (ly + ry) / 2.0
	)
	if ry != ly {
		fm := (lx - rx) / (ry - ly)
		fb := fy - fm*fx
		if fm < -1 || fm > 1 {
			if lx > rx {
				if a == nil {
					a = Pt((yt-fb)/fm, yt)
				} else if a.Y >= yb {
					return false
				}
				b = Pt((yb-fb)/fm, yb)
			} else {
				if a == nil {
					a = Pt((yb-fb)/fm, yb)
				} else if a.Y < yt {
					return false
				}
				b = Pt((yt-fb)/fm, yt)
			}
		} else {
			if ly < ry {
				if a == nil {
					a = Pt(xl, fm*xl+fb)
				} else if a.X >= xr {
					return false
				}
				b = Pt(xr, fm*xr+fb)
			} else {
				if a == nil {
					a = Pt(xr, fm*xr+fb)
				} else if a.X < xl {
					return false
				}
				b = Pt(xl, fm*xl+fb)
			}
		}
	} else {
		if fx < xl || fx >= xr {
			return false
		}
		if lx > rx {
			if a == nil {
				a = Pt(fx, yt)
			} else if a.Y >= yb {
				return false
			}
			b = Pt(fx, yb)
		} else {
			if a == nil {
				a = Pt(fx, yb)
			} else if a.Y < yt {
				return false
			}
			b = Pt(fx, yt)
		}
	}
	e.A, e.B = a, b
	return true
}

func (e *Edge) clipRect(r *Rectangle) bool {
	if !e.connectRect(r) {
		return false
	}
	var (
		xl, xr         = r.Min.X, r.Max.X
		yt, yb         = r.Min.Y, r.Max.Y
		ax, ay         = e.A.X, e.A.Y
		bx, by         = e.B.X, e.B.Y
		dx, dy         = bx - ax, by - ay
		t0, t1 float64 = 0, 1
	)
	q := ax - xl
	if dx == 0 && q < 0 {
		return false
	}
	t := -q / dx
	if dx < 0 {
		if t < t0 {
			return false
		} else if t < t1 {
			t1 = t
		}
	} else if dx > 0 {
		if t > t1 {
			return false
		} else if t > t0 {
			t0 = t
		}
	}
	q = xr - ax
	if dx == 0 && q < 0 {
		return false
	}
	t = q / dx
	if dx < 0 {
		if t > t1 {
			return false
		} else if t > t0 {
			t0 = t
		}
	} else if dx > 0 {
		if t < t0 {
			return false
		} else if t < t1 {
			t1 = t
		}
	}
	q = ay - yt
	if dy == 0 && q < 0 {
		return false
	}
	t = -q / dy
	if dy < 0 {
		if t < t0 {
			return false
		} else if t < t1 {
			t1 = t
		}
	} else if dy > 0 {
		if t > t1 {
			return false
		} else if t > t0 {
			t0 = t
		}
	}
	q = yb - ay
	if dy == 0 && q < 0 {
		return false
	}
	t = q / dy
	if dy < 0 {
		if t > t1 {
			return false
		} else if t > t0 {
			t0 = t
		}
	} else if dy > 0 {
		if t < t0 {
			return false
		} else if t < t1 {
			t1 = t
		}
	}
	if t0 > 0 {
		e.A = Pt(ax+t0*dx, ay+t0*dy)
	}
	if t1 < 1 {
		e.B = Pt(ax+t1*dx, ay+t1*dy)
	}
	return math.Abs(e.A.X-e.B.X) >= 1e-9 && math.Abs(e.A.Y-e.B.Y) >= 1e-9
}

type Halfedge struct {
	site  *Site
	edge  *Edge
	angle float64
}

func (h Halfedge) Start() *Point {
	if h.site == h.edge.L {
		return h.edge.A
	}
	return h.edge.B
}

func (h Halfedge) End() *Point {
	if h.site == h.edge.L {
		return h.edge.B
	}
	return h.edge.A
}

type halfedgeSlice []Halfedge

func (p halfedgeSlice) Len() int {
	return len(p)
}

func (p halfedgeSlice) Less(i, j int) bool {
	return p[j].angle < p[i].angle
}

func (p halfedgeSlice) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}
