package voronoi

import (
	"container/heap"
	"fmt"
	"math"
)

type Diagram struct {
	Sites []*Site
	Edges []*Edge

	bl    rbtree
	queue eventQueue
	del   eventList
	darcs []*arc
}

func NewDiagram() *Diagram {
	return &Diagram{}
}

func (d Diagram) String() string {
	return fmt.Sprintf("Diagram{Sites: %d, Edges: %d}", len(d.Sites), len(d.Edges))
}

func (d *Diagram) Compute(points []*Point) {
	n, m := len(points), len(d.Sites)
	if m < n {
		d.Sites = make([]*Site, 0, n)
		d.Edges = make([]*Edge, 0, n)
		d.queue = make(eventQueue, 0, n+1)
		d.del = make(eventList, 0, 100)
		d.darcs = make([]*arc, 0, 20)
	}
	d.Sites, d.Edges = d.Sites[:n], d.Edges[:0]
	d.queue, d.del, d.darcs = d.queue[:0], d.del[:0], d.darcs[:0]
	d.bl.root = nil
	for i, p := range points {
		d.Sites[i] = &Site{Point: p, Halfedges: make([]Halfedge, 0, 10)}
		heap.Push(&d.queue, newEvent(d.Sites[i], nil))
	}
	for d.queue.Len() > 0 {
		e := heap.Pop(&d.queue).(*event)
		if d.del.remove(e) {
			continue
		}
		if e.arc == nil {
			d.addArc(e.site)
		} else {
			d.removeArc(e)
		}
	}
}

func (d *Diagram) CloseRect(r Rectangle) {
	for _, e := range d.Edges {
		if !e.clipRect(&r) {
			e.A, e.B = nil, nil
		}
	}
	d.closeRect(&r)
}

func (d *Diagram) addArc(s *Site) {
	var (
		x, dix = s.X, s.Y
		la, ra *arc
		n      = d.bl.root
		na     = newArc(s)
	)
	for n != nil {
		dxl := n.leftBP(dix) - x
		if dxl > 1e-9 {
			n = n.left
		} else {
			dxr := x - n.rightBP(dix)
			if dxr > 1e-9 {
				if n.right == nil {
					la = n
					break
				}
				n = n.right
			} else {
				if dxl > -1e-9 {
					la, ra = n.prev, n
				} else if dxr > -1e-9 {
					la, ra = n, n.next
				} else {
					la, ra = n, n
				}
				break
			}
		}
	}
	d.bl.insert(la, na)
	if la == nil && ra != nil {
		panic("d.addArc: la == nil && ra != nil.")
	}
	if la == nil && ra == nil {
		return
	}
	if la != nil && ra == nil {
		na.edge = newEdge(la.site, na.site, nil, nil)
		d.Edges = append(d.Edges, na.edge)
		return
	}
	if la == ra {
		d.removeCircle(la)
		ra, ed := newArc(la.site), newEdge(la.site, na.site, nil, nil)
		na.edge, ra.edge = ed, ed
		d.Edges = append(d.Edges, ed)
		d.bl.insert(na, ra)
		d.checkCircle(la)
		d.checkCircle(ra)
		return
	}
	d.removeCircle(la)
	d.removeCircle(ra)
	var (
		ls, rs = la.site, ra.site
		ax, ay = ls.X, ls.Y
		bx, by = s.X - ax, s.Y - ay
		cx, cy = rs.X - ax, rs.Y - ay
		dp     = 2 * (bx*cy - by*cx)
		hb, hc = bx*bx + by*by, cx*cx + cy*cy
		p      = Pt((cy*hb-by*hc)/dp+ax, (bx*hc-cx*hb)/dp+ay)
	)
	ra.edge.setStart(ls, rs, p)
	na.edge, ra.edge = newEdge(ls, s, nil, p), newEdge(s, rs, nil, p)
	d.Edges = append(d.Edges, na.edge)
	d.Edges = append(d.Edges, ra.edge)
	d.checkCircle(la)
	d.checkCircle(ra)
}

func (d *Diagram) removeArc(e *event) {
	var (
		x, y  = e.x, e.yc
		p     = Pt(x, y)
		darcs = append(d.darcs[:0], e.arc)
	)

	la := e.arc.prev
	for ; la.ev != nil && math.Abs(x-la.ev.x) < 1e-9 && math.Abs(y-la.ev.yc) < 1e-9; la = la.prev {
		darcs = append([]*arc{la}, darcs...)
		d.detachArc(la)
	}
	darcs = append([]*arc{la}, darcs...)
	d.removeCircle(la)

	ra := e.arc.next
	for ; ra.ev != nil && math.Abs(x-ra.ev.x) < 1e-9 && math.Abs(y-ra.ev.yc) < 1e-9; ra = ra.next {
		darcs = append(darcs, ra)
		d.detachArc(ra)
	}
	darcs = append(darcs, ra)
	d.removeCircle(ra)

	d.detachArc(e.arc)

	for i := 1; i < len(darcs); i++ {
		la, ra := darcs[i-1], darcs[i]
		ra.edge.setStart(la.site, ra.site, p)
	}

	la, ra = darcs[0], darcs[len(darcs)-1]
	ra.edge = newEdge(la.site, ra.site, nil, p)
	d.Edges = append(d.Edges, ra.edge)

	for i := range darcs {
		darcs[i] = nil
	}

	d.darcs = darcs[:0]

	d.checkCircle(la)
	d.checkCircle(ra)
}

func (d *Diagram) detachArc(a *arc) {
	d.bl.remove(a)
	d.removeCircle(a)
}

func (d *Diagram) checkCircle(a *arc) {
	if a.prev == nil || a.next == nil {
		return
	}
	l, c, r := a.prev.site, a.site, a.next.site
	if l == r {
		return
	}
	var (
		bx, by = c.X, c.Y
		ax, ay = l.X - bx, l.Y - by
		cx, cy = r.X - bx, r.Y - by
		dp     = 2 * (ax*cy - ay*cx)
	)
	if dp >= -2e-12 {
		return
	}
	var (
		ha, hc = ax*ax + ay*ay, cx*cx + cy*cy
		x, y   = (cy*ha - ay*hc) / dp, (ax*hc - cx*ha) / dp
		yc     = y + by
	)
	e := newEvent(c, a)
	e.x, e.y, e.yc = x+bx, yc+math.Sqrt(x*x+y*y), yc
	a.ev = e
	heap.Push(&d.queue, e)
}

func (d *Diagram) removeCircle(a *arc) {
	if a.ev != nil {
		d.del, a.ev = append(d.del, a.ev), nil
	}
}

func (d *Diagram) closeRect(r *Rectangle) {
	xl, xr := r.Min.X, r.Max.X
	yt, yb := r.Min.Y, r.Max.Y

	for _, s := range d.Sites {
		if !s.fix() {
			continue
		}
		for j, n := 0, len(s.Halfedges); j < n; j++ {
			end, start := s.Halfedges[j].End(), s.Halfedges[(j+1)%n].Start()
			if math.Abs(end.X-start.X) >= 1e-9 || math.Abs(end.Y-start.Y) >= 1e-9 {
				var b *Point
				if eqEps(end.X, xl) && ltEps(end.Y, yb) {
					if eqEps(start.X, xl) {
						b = Pt(xl, start.Y)
					} else {
						b = Pt(xl, yb)
					}
				} else if eqEps(end.Y, yb) && ltEps(end.X, xr) {
					if eqEps(start.Y, yb) {
						b = Pt(start.X, yb)
					} else {
						b = Pt(xr, yb)
					}
				} else if eqEps(end.X, xr) && gtEps(end.Y, yt) {
					if eqEps(start.X, xr) {
						b = Pt(xr, start.Y)
					} else {
						b = Pt(xr, yt)
					}
				} else if eqEps(end.Y, yt) && gtEps(end.X, xl) {
					if eqEps(start.Y, yt) {
						b = Pt(start.X, yt)
					} else {
						b = Pt(xl, yt)
					}
				} else {
					panic(fmt.Sprintf("not found border point: end: %v, start: %v, j: %v, n: %n", end, start, j, n))
				}
				edge := &Edge{L: s, A: end, B: b}
				d.Edges = append(d.Edges, edge)
				h, k := Halfedge{site: s, edge: edge}, j+1
				s.Halfedges = append(s.Halfedges, h)
				if k < n {
					copy(s.Halfedges[k+1:], s.Halfedges[k:])
					s.Halfedges[k] = h
				}
				n = len(s.Halfedges)
			}
		}
	}
}

func eqEps(x, y float64) bool {
	return math.Abs(x-y) < 1e-9
}

func ltEps(x, y float64) bool {
	return y-x > 1e-9
}

func gtEps(x, y float64) bool {
	return x-y > 1e-9
}
