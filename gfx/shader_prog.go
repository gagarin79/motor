package gfx

import (
	"errors"
	"fmt"
	"github.com/gagarin79/motor/gfx/gl"
)

type shaderSourceType int

const (
	ssVert shaderSourceType = iota
	ssFrag
)

type shaderSource struct {
	id  gl.Uint
	typ shaderSourceType
	src **gl.Char
}

func newShaderSource(typ shaderSourceType, src string) *shaderSource {
	glsrc := gl.GLString(src)
	return &shaderSource{typ: typ, src: &glsrc}
}

func (s *shaderSource) compile() error {
	if s.src == nil {
		return errors.New("invalid shader source")
	}

	if s.id == 0 {
		if s.typ == ssVert {
			s.id = gl.CreateShader(gl.VERTEX_SHADER)
		} else {
			s.id = gl.CreateShader(gl.FRAGMENT_SHADER)
		}
	}
	gl.ShaderSource(s.id, 1, s.src, nil)
	gl.CompileShader(s.id)

	var status gl.Int
	gl.GetShaderiv(s.id, gl.COMPILE_STATUS, &status)
	if status != gl.TRUE {
		var (
			log    = gl.GLStringAlloc(256)
			logLen gl.Sizei
		)
		defer gl.GLStringFree(log)
		gl.GetShaderInfoLog(s.id, 256, &logLen, log)
		gl.DeleteShader(s.id)
		s.id = 0
		return errors.New(gl.GoString(log))
	}

	return nil
}

func (s *shaderSource) unload() {
	if s.id != 0 {
		gl.DeleteShader(s.id)
	}
}

type shaderProgram struct {
	id    gl.Uint
	name  string
	vert  *shaderSource
	frag  *shaderSource
	parms []shaderParm
}

var currentShaderProg *shaderProgram

func newShaderProgram(name, vsrc, fsrc string, parms []shaderParm) *shaderProgram {
	return &shaderProgram{
		name:  name,
		vert:  newShaderSource(ssVert, vsrc),
		frag:  newShaderSource(ssFrag, fsrc),
		parms: parms,
	}
}

func (p shaderProgram) String() string {
	return fmt.Sprintf("shader{%q}", p.name)
}

func (p *shaderProgram) bind() {
	for _, par := range p.parms {
		par.bind()
	}
}

func (p *shaderProgram) load() error {
	if err := p.vert.compile(); err != nil {
		return err
	}
	if err := p.frag.compile(); err != nil {
		return err
	}

	if p.id == 0 {
		p.id = gl.CreateProgram()
	}
	gl.AttachShader(p.id, p.vert.id)
	gl.AttachShader(p.id, p.frag.id)
	gl.LinkProgram(p.id)
	gl.ValidateProgram(p.id)

	if glerr := gl.GetError(); glerr != gl.NO_ERROR {
		gl.DeleteProgram(p.id)
		return fmt.Errorf("validate shader program failed: %d", glerr)
	}

	for _, par := range p.parms {
		if err := par.init(p); err != nil {
			return err
		}
	}

	return nil
}

func (p *shaderProgram) unload() error {
	p.vert.unload()
	p.frag.unload()
	if p.id != 0 {
		gl.DeleteProgram(p.id)
		p.id = 0
	}
	return nil
}

func (p *shaderProgram) getUniformLoc(name string) int {
	if p.id == 0 {
		return -1
	}
	cname := gl.GLString(name)
	defer gl.GLStringFree(cname)
	return int(gl.GetUniformLocation(p.id, cname))
}
