// Automatically generated OpenGL binding.
// gl2.1-core
package gl

// #cgo darwin  LDFLAGS: -framework OpenGL
// #cgo linux   LDFLAGS: -lGL
// #cgo windows LDFLAGS: -lopengl32
//
// #include <stdlib.h>
// #if defined(__APPLE__)
// #include <dlfcn.h>
// #elif defined(_WIN32)
// #define WIN32_LEAN_AND_MEAN 1
// #include <windows.h>
// #else
// #include <X11/Xlib.h>
// #include <GL/glx.h>
// #endif
//
// #ifndef APIENTRY
// #define APIENTRY
// #endif
// #ifndef APIENTRYP
// #define APIENTRYP APIENTRY *
// #endif
// #ifndef GLAPI
// #define GLAPI extern
// #endif
//
// #ifdef _WIN32
// static HMODULE hOpengl32Lib = NULL;
// #endif
//
// static void* GetGLProcAddress(const char* name) {
// #ifdef __APPLE__
//     return dlsym(RTLD_DEFAULT, name);
// #elif _WIN32
//     void* pf = wglGetProcAddress((LPCSTR)name);
// 	   if(pf) {
//         return pf;
//     }
//     if(hOpengl32Lib == NULL) {
//         hOpengl32Lib = LoadLibraryA("opengl32.dll");
//     }
//     return GetProcAddress(hOpengl32Lib, (LPCSTR)name);
// #else
//     return glXGetProcAddress((const GLubyte*)name);
// #endif
// }
// 
// #ifndef GL_VERSION_1_0
// #define GL_VERSION_1_0 1
// typedef unsigned int GLenum;
// typedef float GLfloat;
// typedef int GLint;
// typedef int GLsizei;
// typedef void GLvoid;
// typedef unsigned int GLbitfield;
// typedef double GLdouble;
// typedef unsigned int GLuint;
// typedef unsigned char GLboolean;
// typedef unsigned char GLubyte;
// typedef signed char GLbyte;
// typedef short GLshort;
// typedef unsigned short GLushort;
// 
// void (APIENTRYP glCullFaceProc)(GLenum mode);
// void (APIENTRYP glFrontFaceProc)(GLenum mode);
// void (APIENTRYP glHintProc)(GLenum target, GLenum mode);
// void (APIENTRYP glLineWidthProc)(GLfloat width);
// void (APIENTRYP glPointSizeProc)(GLfloat size);
// void (APIENTRYP glPolygonModeProc)(GLenum face, GLenum mode);
// void (APIENTRYP glScissorProc)(GLint x, GLint y, GLsizei width, GLsizei height);
// void (APIENTRYP glTexParameterfProc)(GLenum target, GLenum pname, GLfloat param);
// void (APIENTRYP glTexParameterfvProc)(GLenum target, GLenum pname, GLfloat* params);
// void (APIENTRYP glTexParameteriProc)(GLenum target, GLenum pname, GLint param);
// void (APIENTRYP glTexParameterivProc)(GLenum target, GLenum pname, GLint* params);
// void (APIENTRYP glTexImage1DProc)(GLenum target, GLint level, GLint internalformat, GLsizei width, GLint border, GLenum format, GLenum type_, GLvoid* pixels);
// void (APIENTRYP glTexImage2DProc)(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type_, GLvoid* pixels);
// void (APIENTRYP glDrawBufferProc)(GLenum mode);
// void (APIENTRYP glClearProc)(GLbitfield mask);
// void (APIENTRYP glClearColorProc)(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha);
// void (APIENTRYP glClearStencilProc)(GLint s);
// void (APIENTRYP glClearDepthProc)(GLdouble depth);
// void (APIENTRYP glStencilMaskProc)(GLuint mask);
// void (APIENTRYP glColorMaskProc)(GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha);
// void (APIENTRYP glDepthMaskProc)(GLboolean flag);
// void (APIENTRYP glDisableProc)(GLenum cap);
// void (APIENTRYP glEnableProc)(GLenum cap);
// void (APIENTRYP glFinishProc)();
// void (APIENTRYP glFlushProc)();
// void (APIENTRYP glBlendFuncProc)(GLenum sfactor, GLenum dfactor);
// void (APIENTRYP glLogicOpProc)(GLenum opcode);
// void (APIENTRYP glStencilFuncProc)(GLenum func_, GLint ref, GLuint mask);
// void (APIENTRYP glStencilOpProc)(GLenum fail, GLenum zfail, GLenum zpass);
// void (APIENTRYP glDepthFuncProc)(GLenum func_);
// void (APIENTRYP glPixelStorefProc)(GLenum pname, GLfloat param);
// void (APIENTRYP glPixelStoreiProc)(GLenum pname, GLint param);
// void (APIENTRYP glReadBufferProc)(GLenum mode);
// void (APIENTRYP glReadPixelsProc)(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type_, GLvoid* pixels);
// void (APIENTRYP glGetBooleanvProc)(GLenum pname, GLboolean* params);
// void (APIENTRYP glGetDoublevProc)(GLenum pname, GLdouble* params);
// GLenum (APIENTRYP glGetErrorProc)();
// void (APIENTRYP glGetFloatvProc)(GLenum pname, GLfloat* params);
// void (APIENTRYP glGetIntegervProc)(GLenum pname, GLint* params);
// GLubyte* (APIENTRYP glGetStringProc)(GLenum name);
// void (APIENTRYP glGetTexImageProc)(GLenum target, GLint level, GLenum format, GLenum type_, GLvoid* pixels);
// void (APIENTRYP glGetTexParameterfvProc)(GLenum target, GLenum pname, GLfloat* params);
// void (APIENTRYP glGetTexParameterivProc)(GLenum target, GLenum pname, GLint* params);
// void (APIENTRYP glGetTexLevelParameterfvProc)(GLenum target, GLint level, GLenum pname, GLfloat* params);
// void (APIENTRYP glGetTexLevelParameterivProc)(GLenum target, GLint level, GLenum pname, GLint* params);
// GLboolean (APIENTRYP glIsEnabledProc)(GLenum cap);
// void (APIENTRYP glDepthRangeProc)(GLdouble near_, GLdouble far_);
// void (APIENTRYP glViewportProc)(GLint x, GLint y, GLsizei width, GLsizei height);
// void (APIENTRYP glNewListProc)(GLuint list, GLenum mode);
// void (APIENTRYP glEndListProc)();
// void (APIENTRYP glCallListProc)(GLuint list);
// void (APIENTRYP glCallListsProc)(GLsizei n, GLenum type_, GLvoid* lists);
// void (APIENTRYP glDeleteListsProc)(GLuint list, GLsizei range_);
// GLuint (APIENTRYP glGenListsProc)(GLsizei range_);
// void (APIENTRYP glListBaseProc)(GLuint base);
// void (APIENTRYP glBeginProc)(GLenum mode);
// void (APIENTRYP glBitmapProc)(GLsizei width, GLsizei height, GLfloat xorig, GLfloat yorig, GLfloat xmove, GLfloat ymove, GLubyte* bitmap);
// void (APIENTRYP glColor3bProc)(GLbyte red, GLbyte green, GLbyte blue);
// void (APIENTRYP glColor3bvProc)(GLbyte* v);
// void (APIENTRYP glColor3dProc)(GLdouble red, GLdouble green, GLdouble blue);
// void (APIENTRYP glColor3dvProc)(GLdouble* v);
// void (APIENTRYP glColor3fProc)(GLfloat red, GLfloat green, GLfloat blue);
// void (APIENTRYP glColor3fvProc)(GLfloat* v);
// void (APIENTRYP glColor3iProc)(GLint red, GLint green, GLint blue);
// void (APIENTRYP glColor3ivProc)(GLint* v);
// void (APIENTRYP glColor3sProc)(GLshort red, GLshort green, GLshort blue);
// void (APIENTRYP glColor3svProc)(GLshort* v);
// void (APIENTRYP glColor3ubProc)(GLubyte red, GLubyte green, GLubyte blue);
// void (APIENTRYP glColor3ubvProc)(GLubyte* v);
// void (APIENTRYP glColor3uiProc)(GLuint red, GLuint green, GLuint blue);
// void (APIENTRYP glColor3uivProc)(GLuint* v);
// void (APIENTRYP glColor3usProc)(GLushort red, GLushort green, GLushort blue);
// void (APIENTRYP glColor3usvProc)(GLushort* v);
// void (APIENTRYP glColor4bProc)(GLbyte red, GLbyte green, GLbyte blue, GLbyte alpha);
// void (APIENTRYP glColor4bvProc)(GLbyte* v);
// void (APIENTRYP glColor4dProc)(GLdouble red, GLdouble green, GLdouble blue, GLdouble alpha);
// void (APIENTRYP glColor4dvProc)(GLdouble* v);
// void (APIENTRYP glColor4fProc)(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha);
// void (APIENTRYP glColor4fvProc)(GLfloat* v);
// void (APIENTRYP glColor4iProc)(GLint red, GLint green, GLint blue, GLint alpha);
// void (APIENTRYP glColor4ivProc)(GLint* v);
// void (APIENTRYP glColor4sProc)(GLshort red, GLshort green, GLshort blue, GLshort alpha);
// void (APIENTRYP glColor4svProc)(GLshort* v);
// void (APIENTRYP glColor4ubProc)(GLubyte red, GLubyte green, GLubyte blue, GLubyte alpha);
// void (APIENTRYP glColor4ubvProc)(GLubyte* v);
// void (APIENTRYP glColor4uiProc)(GLuint red, GLuint green, GLuint blue, GLuint alpha);
// void (APIENTRYP glColor4uivProc)(GLuint* v);
// void (APIENTRYP glColor4usProc)(GLushort red, GLushort green, GLushort blue, GLushort alpha);
// void (APIENTRYP glColor4usvProc)(GLushort* v);
// void (APIENTRYP glEdgeFlagProc)(GLboolean flag);
// void (APIENTRYP glEdgeFlagvProc)(GLboolean* flag);
// void (APIENTRYP glEndProc)();
// void (APIENTRYP glIndexdProc)(GLdouble c);
// void (APIENTRYP glIndexdvProc)(GLdouble* c);
// void (APIENTRYP glIndexfProc)(GLfloat c);
// void (APIENTRYP glIndexfvProc)(GLfloat* c);
// void (APIENTRYP glIndexiProc)(GLint c);
// void (APIENTRYP glIndexivProc)(GLint* c);
// void (APIENTRYP glIndexsProc)(GLshort c);
// void (APIENTRYP glIndexsvProc)(GLshort* c);
// void (APIENTRYP glNormal3bProc)(GLbyte nx, GLbyte ny, GLbyte nz);
// void (APIENTRYP glNormal3bvProc)(GLbyte* v);
// void (APIENTRYP glNormal3dProc)(GLdouble nx, GLdouble ny, GLdouble nz);
// void (APIENTRYP glNormal3dvProc)(GLdouble* v);
// void (APIENTRYP glNormal3fProc)(GLfloat nx, GLfloat ny, GLfloat nz);
// void (APIENTRYP glNormal3fvProc)(GLfloat* v);
// void (APIENTRYP glNormal3iProc)(GLint nx, GLint ny, GLint nz);
// void (APIENTRYP glNormal3ivProc)(GLint* v);
// void (APIENTRYP glNormal3sProc)(GLshort nx, GLshort ny, GLshort nz);
// void (APIENTRYP glNormal3svProc)(GLshort* v);
// void (APIENTRYP glRasterPos2dProc)(GLdouble x, GLdouble y);
// void (APIENTRYP glRasterPos2dvProc)(GLdouble* v);
// void (APIENTRYP glRasterPos2fProc)(GLfloat x, GLfloat y);
// void (APIENTRYP glRasterPos2fvProc)(GLfloat* v);
// void (APIENTRYP glRasterPos2iProc)(GLint x, GLint y);
// void (APIENTRYP glRasterPos2ivProc)(GLint* v);
// void (APIENTRYP glRasterPos2sProc)(GLshort x, GLshort y);
// void (APIENTRYP glRasterPos2svProc)(GLshort* v);
// void (APIENTRYP glRasterPos3dProc)(GLdouble x, GLdouble y, GLdouble z);
// void (APIENTRYP glRasterPos3dvProc)(GLdouble* v);
// void (APIENTRYP glRasterPos3fProc)(GLfloat x, GLfloat y, GLfloat z);
// void (APIENTRYP glRasterPos3fvProc)(GLfloat* v);
// void (APIENTRYP glRasterPos3iProc)(GLint x, GLint y, GLint z);
// void (APIENTRYP glRasterPos3ivProc)(GLint* v);
// void (APIENTRYP glRasterPos3sProc)(GLshort x, GLshort y, GLshort z);
// void (APIENTRYP glRasterPos3svProc)(GLshort* v);
// void (APIENTRYP glRasterPos4dProc)(GLdouble x, GLdouble y, GLdouble z, GLdouble w);
// void (APIENTRYP glRasterPos4dvProc)(GLdouble* v);
// void (APIENTRYP glRasterPos4fProc)(GLfloat x, GLfloat y, GLfloat z, GLfloat w);
// void (APIENTRYP glRasterPos4fvProc)(GLfloat* v);
// void (APIENTRYP glRasterPos4iProc)(GLint x, GLint y, GLint z, GLint w);
// void (APIENTRYP glRasterPos4ivProc)(GLint* v);
// void (APIENTRYP glRasterPos4sProc)(GLshort x, GLshort y, GLshort z, GLshort w);
// void (APIENTRYP glRasterPos4svProc)(GLshort* v);
// void (APIENTRYP glRectdProc)(GLdouble x1, GLdouble y1, GLdouble x2, GLdouble y2);
// void (APIENTRYP glRectdvProc)(GLdouble* v1, GLdouble* v2);
// void (APIENTRYP glRectfProc)(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2);
// void (APIENTRYP glRectfvProc)(GLfloat* v1, GLfloat* v2);
// void (APIENTRYP glRectiProc)(GLint x1, GLint y1, GLint x2, GLint y2);
// void (APIENTRYP glRectivProc)(GLint* v1, GLint* v2);
// void (APIENTRYP glRectsProc)(GLshort x1, GLshort y1, GLshort x2, GLshort y2);
// void (APIENTRYP glRectsvProc)(GLshort* v1, GLshort* v2);
// void (APIENTRYP glTexCoord1dProc)(GLdouble s);
// void (APIENTRYP glTexCoord1dvProc)(GLdouble* v);
// void (APIENTRYP glTexCoord1fProc)(GLfloat s);
// void (APIENTRYP glTexCoord1fvProc)(GLfloat* v);
// void (APIENTRYP glTexCoord1iProc)(GLint s);
// void (APIENTRYP glTexCoord1ivProc)(GLint* v);
// void (APIENTRYP glTexCoord1sProc)(GLshort s);
// void (APIENTRYP glTexCoord1svProc)(GLshort* v);
// void (APIENTRYP glTexCoord2dProc)(GLdouble s, GLdouble t);
// void (APIENTRYP glTexCoord2dvProc)(GLdouble* v);
// void (APIENTRYP glTexCoord2fProc)(GLfloat s, GLfloat t);
// void (APIENTRYP glTexCoord2fvProc)(GLfloat* v);
// void (APIENTRYP glTexCoord2iProc)(GLint s, GLint t);
// void (APIENTRYP glTexCoord2ivProc)(GLint* v);
// void (APIENTRYP glTexCoord2sProc)(GLshort s, GLshort t);
// void (APIENTRYP glTexCoord2svProc)(GLshort* v);
// void (APIENTRYP glTexCoord3dProc)(GLdouble s, GLdouble t, GLdouble r);
// void (APIENTRYP glTexCoord3dvProc)(GLdouble* v);
// void (APIENTRYP glTexCoord3fProc)(GLfloat s, GLfloat t, GLfloat r);
// void (APIENTRYP glTexCoord3fvProc)(GLfloat* v);
// void (APIENTRYP glTexCoord3iProc)(GLint s, GLint t, GLint r);
// void (APIENTRYP glTexCoord3ivProc)(GLint* v);
// void (APIENTRYP glTexCoord3sProc)(GLshort s, GLshort t, GLshort r);
// void (APIENTRYP glTexCoord3svProc)(GLshort* v);
// void (APIENTRYP glTexCoord4dProc)(GLdouble s, GLdouble t, GLdouble r, GLdouble q);
// void (APIENTRYP glTexCoord4dvProc)(GLdouble* v);
// void (APIENTRYP glTexCoord4fProc)(GLfloat s, GLfloat t, GLfloat r, GLfloat q);
// void (APIENTRYP glTexCoord4fvProc)(GLfloat* v);
// void (APIENTRYP glTexCoord4iProc)(GLint s, GLint t, GLint r, GLint q);
// void (APIENTRYP glTexCoord4ivProc)(GLint* v);
// void (APIENTRYP glTexCoord4sProc)(GLshort s, GLshort t, GLshort r, GLshort q);
// void (APIENTRYP glTexCoord4svProc)(GLshort* v);
// void (APIENTRYP glVertex2dProc)(GLdouble x, GLdouble y);
// void (APIENTRYP glVertex2dvProc)(GLdouble* v);
// void (APIENTRYP glVertex2fProc)(GLfloat x, GLfloat y);
// void (APIENTRYP glVertex2fvProc)(GLfloat* v);
// void (APIENTRYP glVertex2iProc)(GLint x, GLint y);
// void (APIENTRYP glVertex2ivProc)(GLint* v);
// void (APIENTRYP glVertex2sProc)(GLshort x, GLshort y);
// void (APIENTRYP glVertex2svProc)(GLshort* v);
// void (APIENTRYP glVertex3dProc)(GLdouble x, GLdouble y, GLdouble z);
// void (APIENTRYP glVertex3dvProc)(GLdouble* v);
// void (APIENTRYP glVertex3fProc)(GLfloat x, GLfloat y, GLfloat z);
// void (APIENTRYP glVertex3fvProc)(GLfloat* v);
// void (APIENTRYP glVertex3iProc)(GLint x, GLint y, GLint z);
// void (APIENTRYP glVertex3ivProc)(GLint* v);
// void (APIENTRYP glVertex3sProc)(GLshort x, GLshort y, GLshort z);
// void (APIENTRYP glVertex3svProc)(GLshort* v);
// void (APIENTRYP glVertex4dProc)(GLdouble x, GLdouble y, GLdouble z, GLdouble w);
// void (APIENTRYP glVertex4dvProc)(GLdouble* v);
// void (APIENTRYP glVertex4fProc)(GLfloat x, GLfloat y, GLfloat z, GLfloat w);
// void (APIENTRYP glVertex4fvProc)(GLfloat* v);
// void (APIENTRYP glVertex4iProc)(GLint x, GLint y, GLint z, GLint w);
// void (APIENTRYP glVertex4ivProc)(GLint* v);
// void (APIENTRYP glVertex4sProc)(GLshort x, GLshort y, GLshort z, GLshort w);
// void (APIENTRYP glVertex4svProc)(GLshort* v);
// void (APIENTRYP glClipPlaneProc)(GLenum plane, GLdouble* equation);
// void (APIENTRYP glColorMaterialProc)(GLenum face, GLenum mode);
// void (APIENTRYP glFogfProc)(GLenum pname, GLfloat param);
// void (APIENTRYP glFogfvProc)(GLenum pname, GLfloat* params);
// void (APIENTRYP glFogiProc)(GLenum pname, GLint param);
// void (APIENTRYP glFogivProc)(GLenum pname, GLint* params);
// void (APIENTRYP glLightfProc)(GLenum light, GLenum pname, GLfloat param);
// void (APIENTRYP glLightfvProc)(GLenum light, GLenum pname, GLfloat* params);
// void (APIENTRYP glLightiProc)(GLenum light, GLenum pname, GLint param);
// void (APIENTRYP glLightivProc)(GLenum light, GLenum pname, GLint* params);
// void (APIENTRYP glLightModelfProc)(GLenum pname, GLfloat param);
// void (APIENTRYP glLightModelfvProc)(GLenum pname, GLfloat* params);
// void (APIENTRYP glLightModeliProc)(GLenum pname, GLint param);
// void (APIENTRYP glLightModelivProc)(GLenum pname, GLint* params);
// void (APIENTRYP glLineStippleProc)(GLint factor, GLushort pattern);
// void (APIENTRYP glMaterialfProc)(GLenum face, GLenum pname, GLfloat param);
// void (APIENTRYP glMaterialfvProc)(GLenum face, GLenum pname, GLfloat* params);
// void (APIENTRYP glMaterialiProc)(GLenum face, GLenum pname, GLint param);
// void (APIENTRYP glMaterialivProc)(GLenum face, GLenum pname, GLint* params);
// void (APIENTRYP glPolygonStippleProc)(GLubyte* mask);
// void (APIENTRYP glShadeModelProc)(GLenum mode);
// void (APIENTRYP glTexEnvfProc)(GLenum target, GLenum pname, GLfloat param);
// void (APIENTRYP glTexEnvfvProc)(GLenum target, GLenum pname, GLfloat* params);
// void (APIENTRYP glTexEnviProc)(GLenum target, GLenum pname, GLint param);
// void (APIENTRYP glTexEnvivProc)(GLenum target, GLenum pname, GLint* params);
// void (APIENTRYP glTexGendProc)(GLenum coord, GLenum pname, GLdouble param);
// void (APIENTRYP glTexGendvProc)(GLenum coord, GLenum pname, GLdouble* params);
// void (APIENTRYP glTexGenfProc)(GLenum coord, GLenum pname, GLfloat param);
// void (APIENTRYP glTexGenfvProc)(GLenum coord, GLenum pname, GLfloat* params);
// void (APIENTRYP glTexGeniProc)(GLenum coord, GLenum pname, GLint param);
// void (APIENTRYP glTexGenivProc)(GLenum coord, GLenum pname, GLint* params);
// void (APIENTRYP glFeedbackBufferProc)(GLsizei size, GLenum type_, GLfloat* buffer);
// void (APIENTRYP glSelectBufferProc)(GLsizei size, GLuint* buffer);
// GLint (APIENTRYP glRenderModeProc)(GLenum mode);
// void (APIENTRYP glInitNamesProc)();
// void (APIENTRYP glLoadNameProc)(GLuint name);
// void (APIENTRYP glPassThroughProc)(GLfloat token);
// void (APIENTRYP glPopNameProc)();
// void (APIENTRYP glPushNameProc)(GLuint name);
// void (APIENTRYP glClearAccumProc)(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha);
// void (APIENTRYP glClearIndexProc)(GLfloat c);
// void (APIENTRYP glIndexMaskProc)(GLuint mask);
// void (APIENTRYP glAccumProc)(GLenum op, GLfloat value);
// void (APIENTRYP glPopAttribProc)();
// void (APIENTRYP glPushAttribProc)(GLbitfield mask);
// void (APIENTRYP glMap1dProc)(GLenum target, GLdouble u1, GLdouble u2, GLint stride, GLint order, GLdouble* points);
// void (APIENTRYP glMap1fProc)(GLenum target, GLfloat u1, GLfloat u2, GLint stride, GLint order, GLfloat* points);
// void (APIENTRYP glMap2dProc)(GLenum target, GLdouble u1, GLdouble u2, GLint ustride, GLint uorder, GLdouble v1, GLdouble v2, GLint vstride, GLint vorder, GLdouble* points);
// void (APIENTRYP glMap2fProc)(GLenum target, GLfloat u1, GLfloat u2, GLint ustride, GLint uorder, GLfloat v1, GLfloat v2, GLint vstride, GLint vorder, GLfloat* points);
// void (APIENTRYP glMapGrid1dProc)(GLint un, GLdouble u1, GLdouble u2);
// void (APIENTRYP glMapGrid1fProc)(GLint un, GLfloat u1, GLfloat u2);
// void (APIENTRYP glMapGrid2dProc)(GLint un, GLdouble u1, GLdouble u2, GLint vn, GLdouble v1, GLdouble v2);
// void (APIENTRYP glMapGrid2fProc)(GLint un, GLfloat u1, GLfloat u2, GLint vn, GLfloat v1, GLfloat v2);
// void (APIENTRYP glEvalCoord1dProc)(GLdouble u);
// void (APIENTRYP glEvalCoord1dvProc)(GLdouble* u);
// void (APIENTRYP glEvalCoord1fProc)(GLfloat u);
// void (APIENTRYP glEvalCoord1fvProc)(GLfloat* u);
// void (APIENTRYP glEvalCoord2dProc)(GLdouble u, GLdouble v);
// void (APIENTRYP glEvalCoord2dvProc)(GLdouble* u);
// void (APIENTRYP glEvalCoord2fProc)(GLfloat u, GLfloat v);
// void (APIENTRYP glEvalCoord2fvProc)(GLfloat* u);
// void (APIENTRYP glEvalMesh1Proc)(GLenum mode, GLint i1, GLint i2);
// void (APIENTRYP glEvalPoint1Proc)(GLint i);
// void (APIENTRYP glEvalMesh2Proc)(GLenum mode, GLint i1, GLint i2, GLint j1, GLint j2);
// void (APIENTRYP glEvalPoint2Proc)(GLint i, GLint j);
// void (APIENTRYP glAlphaFuncProc)(GLenum func_, GLfloat ref);
// void (APIENTRYP glPixelZoomProc)(GLfloat xfactor, GLfloat yfactor);
// void (APIENTRYP glPixelTransferfProc)(GLenum pname, GLfloat param);
// void (APIENTRYP glPixelTransferiProc)(GLenum pname, GLint param);
// void (APIENTRYP glPixelMapfvProc)(GLenum map_, GLsizei mapsize, GLfloat* values);
// void (APIENTRYP glPixelMapuivProc)(GLenum map_, GLsizei mapsize, GLuint* values);
// void (APIENTRYP glPixelMapusvProc)(GLenum map_, GLsizei mapsize, GLushort* values);
// void (APIENTRYP glCopyPixelsProc)(GLint x, GLint y, GLsizei width, GLsizei height, GLenum type_);
// void (APIENTRYP glDrawPixelsProc)(GLsizei width, GLsizei height, GLenum format, GLenum type_, GLvoid* pixels);
// void (APIENTRYP glGetClipPlaneProc)(GLenum plane, GLdouble* equation);
// void (APIENTRYP glGetLightfvProc)(GLenum light, GLenum pname, GLfloat* params);
// void (APIENTRYP glGetLightivProc)(GLenum light, GLenum pname, GLint* params);
// void (APIENTRYP glGetMapdvProc)(GLenum target, GLenum query, GLdouble* v);
// void (APIENTRYP glGetMapfvProc)(GLenum target, GLenum query, GLfloat* v);
// void (APIENTRYP glGetMapivProc)(GLenum target, GLenum query, GLint* v);
// void (APIENTRYP glGetMaterialfvProc)(GLenum face, GLenum pname, GLfloat* params);
// void (APIENTRYP glGetMaterialivProc)(GLenum face, GLenum pname, GLint* params);
// void (APIENTRYP glGetPixelMapfvProc)(GLenum map_, GLfloat* values);
// void (APIENTRYP glGetPixelMapuivProc)(GLenum map_, GLuint* values);
// void (APIENTRYP glGetPixelMapusvProc)(GLenum map_, GLushort* values);
// void (APIENTRYP glGetPolygonStippleProc)(GLubyte* mask);
// void (APIENTRYP glGetTexEnvfvProc)(GLenum target, GLenum pname, GLfloat* params);
// void (APIENTRYP glGetTexEnvivProc)(GLenum target, GLenum pname, GLint* params);
// void (APIENTRYP glGetTexGendvProc)(GLenum coord, GLenum pname, GLdouble* params);
// void (APIENTRYP glGetTexGenfvProc)(GLenum coord, GLenum pname, GLfloat* params);
// void (APIENTRYP glGetTexGenivProc)(GLenum coord, GLenum pname, GLint* params);
// GLboolean (APIENTRYP glIsListProc)(GLuint list);
// void (APIENTRYP glFrustumProc)(GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble zNear, GLdouble zFar);
// void (APIENTRYP glLoadIdentityProc)();
// void (APIENTRYP glLoadMatrixfProc)(GLfloat* m);
// void (APIENTRYP glLoadMatrixdProc)(GLdouble* m);
// void (APIENTRYP glMatrixModeProc)(GLenum mode);
// void (APIENTRYP glMultMatrixfProc)(GLfloat* m);
// void (APIENTRYP glMultMatrixdProc)(GLdouble* m);
// void (APIENTRYP glOrthoProc)(GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble zNear, GLdouble zFar);
// void (APIENTRYP glPopMatrixProc)();
// void (APIENTRYP glPushMatrixProc)();
// void (APIENTRYP glRotatedProc)(GLdouble angle, GLdouble x, GLdouble y, GLdouble z);
// void (APIENTRYP glRotatefProc)(GLfloat angle, GLfloat x, GLfloat y, GLfloat z);
// void (APIENTRYP glScaledProc)(GLdouble x, GLdouble y, GLdouble z);
// void (APIENTRYP glScalefProc)(GLfloat x, GLfloat y, GLfloat z);
// void (APIENTRYP glTranslatedProc)(GLdouble x, GLdouble y, GLdouble z);
// void (APIENTRYP glTranslatefProc)(GLfloat x, GLfloat y, GLfloat z);
// 
// inline void glCullFace(GLenum mode){  glCullFaceProc(mode); }
// inline void glFrontFace(GLenum mode){  glFrontFaceProc(mode); }
// inline void glHint(GLenum target, GLenum mode){  glHintProc(target, mode); }
// inline void glLineWidth(GLfloat width){  glLineWidthProc(width); }
// inline void glPointSize(GLfloat size){  glPointSizeProc(size); }
// inline void glPolygonMode(GLenum face, GLenum mode){  glPolygonModeProc(face, mode); }
// inline void glScissor(GLint x, GLint y, GLsizei width, GLsizei height){  glScissorProc(x, y, width, height); }
// inline void glTexParameterf(GLenum target, GLenum pname, GLfloat param){  glTexParameterfProc(target, pname, param); }
// inline void glTexParameterfv(GLenum target, GLenum pname, GLfloat* params){  glTexParameterfvProc(target, pname, params); }
// inline void glTexParameteri(GLenum target, GLenum pname, GLint param){  glTexParameteriProc(target, pname, param); }
// inline void glTexParameteriv(GLenum target, GLenum pname, GLint* params){  glTexParameterivProc(target, pname, params); }
// inline void glTexImage1D(GLenum target, GLint level, GLint internalformat, GLsizei width, GLint border, GLenum format, GLenum type_, GLvoid* pixels){  glTexImage1DProc(target, level, internalformat, width, border, format, type_, pixels); }
// inline void glTexImage2D(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLint border, GLenum format, GLenum type_, GLvoid* pixels){  glTexImage2DProc(target, level, internalformat, width, height, border, format, type_, pixels); }
// inline void glDrawBuffer(GLenum mode){  glDrawBufferProc(mode); }
// inline void glClear(GLbitfield mask){  glClearProc(mask); }
// inline void glClearColor(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha){  glClearColorProc(red, green, blue, alpha); }
// inline void glClearStencil(GLint s){  glClearStencilProc(s); }
// inline void glClearDepth(GLdouble depth){  glClearDepthProc(depth); }
// inline void glStencilMask(GLuint mask){  glStencilMaskProc(mask); }
// inline void glColorMask(GLboolean red, GLboolean green, GLboolean blue, GLboolean alpha){  glColorMaskProc(red, green, blue, alpha); }
// inline void glDepthMask(GLboolean flag){  glDepthMaskProc(flag); }
// inline void glDisable(GLenum cap){  glDisableProc(cap); }
// inline void glEnable(GLenum cap){  glEnableProc(cap); }
// inline void glFinish(){  glFinishProc(); }
// inline void glFlush(){  glFlushProc(); }
// inline void glBlendFunc(GLenum sfactor, GLenum dfactor){  glBlendFuncProc(sfactor, dfactor); }
// inline void glLogicOp(GLenum opcode){  glLogicOpProc(opcode); }
// inline void glStencilFunc(GLenum func_, GLint ref, GLuint mask){  glStencilFuncProc(func_, ref, mask); }
// inline void glStencilOp(GLenum fail, GLenum zfail, GLenum zpass){  glStencilOpProc(fail, zfail, zpass); }
// inline void glDepthFunc(GLenum func_){  glDepthFuncProc(func_); }
// inline void glPixelStoref(GLenum pname, GLfloat param){  glPixelStorefProc(pname, param); }
// inline void glPixelStorei(GLenum pname, GLint param){  glPixelStoreiProc(pname, param); }
// inline void glReadBuffer(GLenum mode){  glReadBufferProc(mode); }
// inline void glReadPixels(GLint x, GLint y, GLsizei width, GLsizei height, GLenum format, GLenum type_, GLvoid* pixels){  glReadPixelsProc(x, y, width, height, format, type_, pixels); }
// inline void glGetBooleanv(GLenum pname, GLboolean* params){  glGetBooleanvProc(pname, params); }
// inline void glGetDoublev(GLenum pname, GLdouble* params){  glGetDoublevProc(pname, params); }
// inline GLenum glGetError(){ return glGetErrorProc(); }
// inline void glGetFloatv(GLenum pname, GLfloat* params){  glGetFloatvProc(pname, params); }
// inline void glGetIntegerv(GLenum pname, GLint* params){  glGetIntegervProc(pname, params); }
// inline GLubyte* glGetString(GLenum name){ return glGetStringProc(name); }
// inline void glGetTexImage(GLenum target, GLint level, GLenum format, GLenum type_, GLvoid* pixels){  glGetTexImageProc(target, level, format, type_, pixels); }
// inline void glGetTexParameterfv(GLenum target, GLenum pname, GLfloat* params){  glGetTexParameterfvProc(target, pname, params); }
// inline void glGetTexParameteriv(GLenum target, GLenum pname, GLint* params){  glGetTexParameterivProc(target, pname, params); }
// inline void glGetTexLevelParameterfv(GLenum target, GLint level, GLenum pname, GLfloat* params){  glGetTexLevelParameterfvProc(target, level, pname, params); }
// inline void glGetTexLevelParameteriv(GLenum target, GLint level, GLenum pname, GLint* params){  glGetTexLevelParameterivProc(target, level, pname, params); }
// inline GLboolean glIsEnabled(GLenum cap){ return glIsEnabledProc(cap); }
// inline void glDepthRange(GLdouble near_, GLdouble far_){  glDepthRangeProc(near_, far_); }
// inline void glViewport(GLint x, GLint y, GLsizei width, GLsizei height){  glViewportProc(x, y, width, height); }
// inline void glNewList(GLuint list, GLenum mode){  glNewListProc(list, mode); }
// inline void glEndList(){  glEndListProc(); }
// inline void glCallList(GLuint list){  glCallListProc(list); }
// inline void glCallLists(GLsizei n, GLenum type_, GLvoid* lists){  glCallListsProc(n, type_, lists); }
// inline void glDeleteLists(GLuint list, GLsizei range_){  glDeleteListsProc(list, range_); }
// inline GLuint glGenLists(GLsizei range_){ return glGenListsProc(range_); }
// inline void glListBase(GLuint base){  glListBaseProc(base); }
// inline void glBegin(GLenum mode){  glBeginProc(mode); }
// inline void glBitmap(GLsizei width, GLsizei height, GLfloat xorig, GLfloat yorig, GLfloat xmove, GLfloat ymove, GLubyte* bitmap){  glBitmapProc(width, height, xorig, yorig, xmove, ymove, bitmap); }
// inline void glColor3b(GLbyte red, GLbyte green, GLbyte blue){  glColor3bProc(red, green, blue); }
// inline void glColor3bv(GLbyte* v){  glColor3bvProc(v); }
// inline void glColor3d(GLdouble red, GLdouble green, GLdouble blue){  glColor3dProc(red, green, blue); }
// inline void glColor3dv(GLdouble* v){  glColor3dvProc(v); }
// inline void glColor3f(GLfloat red, GLfloat green, GLfloat blue){  glColor3fProc(red, green, blue); }
// inline void glColor3fv(GLfloat* v){  glColor3fvProc(v); }
// inline void glColor3i(GLint red, GLint green, GLint blue){  glColor3iProc(red, green, blue); }
// inline void glColor3iv(GLint* v){  glColor3ivProc(v); }
// inline void glColor3s(GLshort red, GLshort green, GLshort blue){  glColor3sProc(red, green, blue); }
// inline void glColor3sv(GLshort* v){  glColor3svProc(v); }
// inline void glColor3ub(GLubyte red, GLubyte green, GLubyte blue){  glColor3ubProc(red, green, blue); }
// inline void glColor3ubv(GLubyte* v){  glColor3ubvProc(v); }
// inline void glColor3ui(GLuint red, GLuint green, GLuint blue){  glColor3uiProc(red, green, blue); }
// inline void glColor3uiv(GLuint* v){  glColor3uivProc(v); }
// inline void glColor3us(GLushort red, GLushort green, GLushort blue){  glColor3usProc(red, green, blue); }
// inline void glColor3usv(GLushort* v){  glColor3usvProc(v); }
// inline void glColor4b(GLbyte red, GLbyte green, GLbyte blue, GLbyte alpha){  glColor4bProc(red, green, blue, alpha); }
// inline void glColor4bv(GLbyte* v){  glColor4bvProc(v); }
// inline void glColor4d(GLdouble red, GLdouble green, GLdouble blue, GLdouble alpha){  glColor4dProc(red, green, blue, alpha); }
// inline void glColor4dv(GLdouble* v){  glColor4dvProc(v); }
// inline void glColor4f(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha){  glColor4fProc(red, green, blue, alpha); }
// inline void glColor4fv(GLfloat* v){  glColor4fvProc(v); }
// inline void glColor4i(GLint red, GLint green, GLint blue, GLint alpha){  glColor4iProc(red, green, blue, alpha); }
// inline void glColor4iv(GLint* v){  glColor4ivProc(v); }
// inline void glColor4s(GLshort red, GLshort green, GLshort blue, GLshort alpha){  glColor4sProc(red, green, blue, alpha); }
// inline void glColor4sv(GLshort* v){  glColor4svProc(v); }
// inline void glColor4ub(GLubyte red, GLubyte green, GLubyte blue, GLubyte alpha){  glColor4ubProc(red, green, blue, alpha); }
// inline void glColor4ubv(GLubyte* v){  glColor4ubvProc(v); }
// inline void glColor4ui(GLuint red, GLuint green, GLuint blue, GLuint alpha){  glColor4uiProc(red, green, blue, alpha); }
// inline void glColor4uiv(GLuint* v){  glColor4uivProc(v); }
// inline void glColor4us(GLushort red, GLushort green, GLushort blue, GLushort alpha){  glColor4usProc(red, green, blue, alpha); }
// inline void glColor4usv(GLushort* v){  glColor4usvProc(v); }
// inline void glEdgeFlag(GLboolean flag){  glEdgeFlagProc(flag); }
// inline void glEdgeFlagv(GLboolean* flag){  glEdgeFlagvProc(flag); }
// inline void glEnd(){  glEndProc(); }
// inline void glIndexd(GLdouble c){  glIndexdProc(c); }
// inline void glIndexdv(GLdouble* c){  glIndexdvProc(c); }
// inline void glIndexf(GLfloat c){  glIndexfProc(c); }
// inline void glIndexfv(GLfloat* c){  glIndexfvProc(c); }
// inline void glIndexi(GLint c){  glIndexiProc(c); }
// inline void glIndexiv(GLint* c){  glIndexivProc(c); }
// inline void glIndexs(GLshort c){  glIndexsProc(c); }
// inline void glIndexsv(GLshort* c){  glIndexsvProc(c); }
// inline void glNormal3b(GLbyte nx, GLbyte ny, GLbyte nz){  glNormal3bProc(nx, ny, nz); }
// inline void glNormal3bv(GLbyte* v){  glNormal3bvProc(v); }
// inline void glNormal3d(GLdouble nx, GLdouble ny, GLdouble nz){  glNormal3dProc(nx, ny, nz); }
// inline void glNormal3dv(GLdouble* v){  glNormal3dvProc(v); }
// inline void glNormal3f(GLfloat nx, GLfloat ny, GLfloat nz){  glNormal3fProc(nx, ny, nz); }
// inline void glNormal3fv(GLfloat* v){  glNormal3fvProc(v); }
// inline void glNormal3i(GLint nx, GLint ny, GLint nz){  glNormal3iProc(nx, ny, nz); }
// inline void glNormal3iv(GLint* v){  glNormal3ivProc(v); }
// inline void glNormal3s(GLshort nx, GLshort ny, GLshort nz){  glNormal3sProc(nx, ny, nz); }
// inline void glNormal3sv(GLshort* v){  glNormal3svProc(v); }
// inline void glRasterPos2d(GLdouble x, GLdouble y){  glRasterPos2dProc(x, y); }
// inline void glRasterPos2dv(GLdouble* v){  glRasterPos2dvProc(v); }
// inline void glRasterPos2f(GLfloat x, GLfloat y){  glRasterPos2fProc(x, y); }
// inline void glRasterPos2fv(GLfloat* v){  glRasterPos2fvProc(v); }
// inline void glRasterPos2i(GLint x, GLint y){  glRasterPos2iProc(x, y); }
// inline void glRasterPos2iv(GLint* v){  glRasterPos2ivProc(v); }
// inline void glRasterPos2s(GLshort x, GLshort y){  glRasterPos2sProc(x, y); }
// inline void glRasterPos2sv(GLshort* v){  glRasterPos2svProc(v); }
// inline void glRasterPos3d(GLdouble x, GLdouble y, GLdouble z){  glRasterPos3dProc(x, y, z); }
// inline void glRasterPos3dv(GLdouble* v){  glRasterPos3dvProc(v); }
// inline void glRasterPos3f(GLfloat x, GLfloat y, GLfloat z){  glRasterPos3fProc(x, y, z); }
// inline void glRasterPos3fv(GLfloat* v){  glRasterPos3fvProc(v); }
// inline void glRasterPos3i(GLint x, GLint y, GLint z){  glRasterPos3iProc(x, y, z); }
// inline void glRasterPos3iv(GLint* v){  glRasterPos3ivProc(v); }
// inline void glRasterPos3s(GLshort x, GLshort y, GLshort z){  glRasterPos3sProc(x, y, z); }
// inline void glRasterPos3sv(GLshort* v){  glRasterPos3svProc(v); }
// inline void glRasterPos4d(GLdouble x, GLdouble y, GLdouble z, GLdouble w){  glRasterPos4dProc(x, y, z, w); }
// inline void glRasterPos4dv(GLdouble* v){  glRasterPos4dvProc(v); }
// inline void glRasterPos4f(GLfloat x, GLfloat y, GLfloat z, GLfloat w){  glRasterPos4fProc(x, y, z, w); }
// inline void glRasterPos4fv(GLfloat* v){  glRasterPos4fvProc(v); }
// inline void glRasterPos4i(GLint x, GLint y, GLint z, GLint w){  glRasterPos4iProc(x, y, z, w); }
// inline void glRasterPos4iv(GLint* v){  glRasterPos4ivProc(v); }
// inline void glRasterPos4s(GLshort x, GLshort y, GLshort z, GLshort w){  glRasterPos4sProc(x, y, z, w); }
// inline void glRasterPos4sv(GLshort* v){  glRasterPos4svProc(v); }
// inline void glRectd(GLdouble x1, GLdouble y1, GLdouble x2, GLdouble y2){  glRectdProc(x1, y1, x2, y2); }
// inline void glRectdv(GLdouble* v1, GLdouble* v2){  glRectdvProc(v1, v2); }
// inline void glRectf(GLfloat x1, GLfloat y1, GLfloat x2, GLfloat y2){  glRectfProc(x1, y1, x2, y2); }
// inline void glRectfv(GLfloat* v1, GLfloat* v2){  glRectfvProc(v1, v2); }
// inline void glRecti(GLint x1, GLint y1, GLint x2, GLint y2){  glRectiProc(x1, y1, x2, y2); }
// inline void glRectiv(GLint* v1, GLint* v2){  glRectivProc(v1, v2); }
// inline void glRects(GLshort x1, GLshort y1, GLshort x2, GLshort y2){  glRectsProc(x1, y1, x2, y2); }
// inline void glRectsv(GLshort* v1, GLshort* v2){  glRectsvProc(v1, v2); }
// inline void glTexCoord1d(GLdouble s){  glTexCoord1dProc(s); }
// inline void glTexCoord1dv(GLdouble* v){  glTexCoord1dvProc(v); }
// inline void glTexCoord1f(GLfloat s){  glTexCoord1fProc(s); }
// inline void glTexCoord1fv(GLfloat* v){  glTexCoord1fvProc(v); }
// inline void glTexCoord1i(GLint s){  glTexCoord1iProc(s); }
// inline void glTexCoord1iv(GLint* v){  glTexCoord1ivProc(v); }
// inline void glTexCoord1s(GLshort s){  glTexCoord1sProc(s); }
// inline void glTexCoord1sv(GLshort* v){  glTexCoord1svProc(v); }
// inline void glTexCoord2d(GLdouble s, GLdouble t){  glTexCoord2dProc(s, t); }
// inline void glTexCoord2dv(GLdouble* v){  glTexCoord2dvProc(v); }
// inline void glTexCoord2f(GLfloat s, GLfloat t){  glTexCoord2fProc(s, t); }
// inline void glTexCoord2fv(GLfloat* v){  glTexCoord2fvProc(v); }
// inline void glTexCoord2i(GLint s, GLint t){  glTexCoord2iProc(s, t); }
// inline void glTexCoord2iv(GLint* v){  glTexCoord2ivProc(v); }
// inline void glTexCoord2s(GLshort s, GLshort t){  glTexCoord2sProc(s, t); }
// inline void glTexCoord2sv(GLshort* v){  glTexCoord2svProc(v); }
// inline void glTexCoord3d(GLdouble s, GLdouble t, GLdouble r){  glTexCoord3dProc(s, t, r); }
// inline void glTexCoord3dv(GLdouble* v){  glTexCoord3dvProc(v); }
// inline void glTexCoord3f(GLfloat s, GLfloat t, GLfloat r){  glTexCoord3fProc(s, t, r); }
// inline void glTexCoord3fv(GLfloat* v){  glTexCoord3fvProc(v); }
// inline void glTexCoord3i(GLint s, GLint t, GLint r){  glTexCoord3iProc(s, t, r); }
// inline void glTexCoord3iv(GLint* v){  glTexCoord3ivProc(v); }
// inline void glTexCoord3s(GLshort s, GLshort t, GLshort r){  glTexCoord3sProc(s, t, r); }
// inline void glTexCoord3sv(GLshort* v){  glTexCoord3svProc(v); }
// inline void glTexCoord4d(GLdouble s, GLdouble t, GLdouble r, GLdouble q){  glTexCoord4dProc(s, t, r, q); }
// inline void glTexCoord4dv(GLdouble* v){  glTexCoord4dvProc(v); }
// inline void glTexCoord4f(GLfloat s, GLfloat t, GLfloat r, GLfloat q){  glTexCoord4fProc(s, t, r, q); }
// inline void glTexCoord4fv(GLfloat* v){  glTexCoord4fvProc(v); }
// inline void glTexCoord4i(GLint s, GLint t, GLint r, GLint q){  glTexCoord4iProc(s, t, r, q); }
// inline void glTexCoord4iv(GLint* v){  glTexCoord4ivProc(v); }
// inline void glTexCoord4s(GLshort s, GLshort t, GLshort r, GLshort q){  glTexCoord4sProc(s, t, r, q); }
// inline void glTexCoord4sv(GLshort* v){  glTexCoord4svProc(v); }
// inline void glVertex2d(GLdouble x, GLdouble y){  glVertex2dProc(x, y); }
// inline void glVertex2dv(GLdouble* v){  glVertex2dvProc(v); }
// inline void glVertex2f(GLfloat x, GLfloat y){  glVertex2fProc(x, y); }
// inline void glVertex2fv(GLfloat* v){  glVertex2fvProc(v); }
// inline void glVertex2i(GLint x, GLint y){  glVertex2iProc(x, y); }
// inline void glVertex2iv(GLint* v){  glVertex2ivProc(v); }
// inline void glVertex2s(GLshort x, GLshort y){  glVertex2sProc(x, y); }
// inline void glVertex2sv(GLshort* v){  glVertex2svProc(v); }
// inline void glVertex3d(GLdouble x, GLdouble y, GLdouble z){  glVertex3dProc(x, y, z); }
// inline void glVertex3dv(GLdouble* v){  glVertex3dvProc(v); }
// inline void glVertex3f(GLfloat x, GLfloat y, GLfloat z){  glVertex3fProc(x, y, z); }
// inline void glVertex3fv(GLfloat* v){  glVertex3fvProc(v); }
// inline void glVertex3i(GLint x, GLint y, GLint z){  glVertex3iProc(x, y, z); }
// inline void glVertex3iv(GLint* v){  glVertex3ivProc(v); }
// inline void glVertex3s(GLshort x, GLshort y, GLshort z){  glVertex3sProc(x, y, z); }
// inline void glVertex3sv(GLshort* v){  glVertex3svProc(v); }
// inline void glVertex4d(GLdouble x, GLdouble y, GLdouble z, GLdouble w){  glVertex4dProc(x, y, z, w); }
// inline void glVertex4dv(GLdouble* v){  glVertex4dvProc(v); }
// inline void glVertex4f(GLfloat x, GLfloat y, GLfloat z, GLfloat w){  glVertex4fProc(x, y, z, w); }
// inline void glVertex4fv(GLfloat* v){  glVertex4fvProc(v); }
// inline void glVertex4i(GLint x, GLint y, GLint z, GLint w){  glVertex4iProc(x, y, z, w); }
// inline void glVertex4iv(GLint* v){  glVertex4ivProc(v); }
// inline void glVertex4s(GLshort x, GLshort y, GLshort z, GLshort w){  glVertex4sProc(x, y, z, w); }
// inline void glVertex4sv(GLshort* v){  glVertex4svProc(v); }
// inline void glClipPlane(GLenum plane, GLdouble* equation){  glClipPlaneProc(plane, equation); }
// inline void glColorMaterial(GLenum face, GLenum mode){  glColorMaterialProc(face, mode); }
// inline void glFogf(GLenum pname, GLfloat param){  glFogfProc(pname, param); }
// inline void glFogfv(GLenum pname, GLfloat* params){  glFogfvProc(pname, params); }
// inline void glFogi(GLenum pname, GLint param){  glFogiProc(pname, param); }
// inline void glFogiv(GLenum pname, GLint* params){  glFogivProc(pname, params); }
// inline void glLightf(GLenum light, GLenum pname, GLfloat param){  glLightfProc(light, pname, param); }
// inline void glLightfv(GLenum light, GLenum pname, GLfloat* params){  glLightfvProc(light, pname, params); }
// inline void glLighti(GLenum light, GLenum pname, GLint param){  glLightiProc(light, pname, param); }
// inline void glLightiv(GLenum light, GLenum pname, GLint* params){  glLightivProc(light, pname, params); }
// inline void glLightModelf(GLenum pname, GLfloat param){  glLightModelfProc(pname, param); }
// inline void glLightModelfv(GLenum pname, GLfloat* params){  glLightModelfvProc(pname, params); }
// inline void glLightModeli(GLenum pname, GLint param){  glLightModeliProc(pname, param); }
// inline void glLightModeliv(GLenum pname, GLint* params){  glLightModelivProc(pname, params); }
// inline void glLineStipple(GLint factor, GLushort pattern){  glLineStippleProc(factor, pattern); }
// inline void glMaterialf(GLenum face, GLenum pname, GLfloat param){  glMaterialfProc(face, pname, param); }
// inline void glMaterialfv(GLenum face, GLenum pname, GLfloat* params){  glMaterialfvProc(face, pname, params); }
// inline void glMateriali(GLenum face, GLenum pname, GLint param){  glMaterialiProc(face, pname, param); }
// inline void glMaterialiv(GLenum face, GLenum pname, GLint* params){  glMaterialivProc(face, pname, params); }
// inline void glPolygonStipple(GLubyte* mask){  glPolygonStippleProc(mask); }
// inline void glShadeModel(GLenum mode){  glShadeModelProc(mode); }
// inline void glTexEnvf(GLenum target, GLenum pname, GLfloat param){  glTexEnvfProc(target, pname, param); }
// inline void glTexEnvfv(GLenum target, GLenum pname, GLfloat* params){  glTexEnvfvProc(target, pname, params); }
// inline void glTexEnvi(GLenum target, GLenum pname, GLint param){  glTexEnviProc(target, pname, param); }
// inline void glTexEnviv(GLenum target, GLenum pname, GLint* params){  glTexEnvivProc(target, pname, params); }
// inline void glTexGend(GLenum coord, GLenum pname, GLdouble param){  glTexGendProc(coord, pname, param); }
// inline void glTexGendv(GLenum coord, GLenum pname, GLdouble* params){  glTexGendvProc(coord, pname, params); }
// inline void glTexGenf(GLenum coord, GLenum pname, GLfloat param){  glTexGenfProc(coord, pname, param); }
// inline void glTexGenfv(GLenum coord, GLenum pname, GLfloat* params){  glTexGenfvProc(coord, pname, params); }
// inline void glTexGeni(GLenum coord, GLenum pname, GLint param){  glTexGeniProc(coord, pname, param); }
// inline void glTexGeniv(GLenum coord, GLenum pname, GLint* params){  glTexGenivProc(coord, pname, params); }
// inline void glFeedbackBuffer(GLsizei size, GLenum type_, GLfloat* buffer){  glFeedbackBufferProc(size, type_, buffer); }
// inline void glSelectBuffer(GLsizei size, GLuint* buffer){  glSelectBufferProc(size, buffer); }
// inline GLint glRenderMode(GLenum mode){ return glRenderModeProc(mode); }
// inline void glInitNames(){  glInitNamesProc(); }
// inline void glLoadName(GLuint name){  glLoadNameProc(name); }
// inline void glPassThrough(GLfloat token){  glPassThroughProc(token); }
// inline void glPopName(){  glPopNameProc(); }
// inline void glPushName(GLuint name){  glPushNameProc(name); }
// inline void glClearAccum(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha){  glClearAccumProc(red, green, blue, alpha); }
// inline void glClearIndex(GLfloat c){  glClearIndexProc(c); }
// inline void glIndexMask(GLuint mask){  glIndexMaskProc(mask); }
// inline void glAccum(GLenum op, GLfloat value){  glAccumProc(op, value); }
// inline void glPopAttrib(){  glPopAttribProc(); }
// inline void glPushAttrib(GLbitfield mask){  glPushAttribProc(mask); }
// inline void glMap1d(GLenum target, GLdouble u1, GLdouble u2, GLint stride, GLint order, GLdouble* points){  glMap1dProc(target, u1, u2, stride, order, points); }
// inline void glMap1f(GLenum target, GLfloat u1, GLfloat u2, GLint stride, GLint order, GLfloat* points){  glMap1fProc(target, u1, u2, stride, order, points); }
// inline void glMap2d(GLenum target, GLdouble u1, GLdouble u2, GLint ustride, GLint uorder, GLdouble v1, GLdouble v2, GLint vstride, GLint vorder, GLdouble* points){  glMap2dProc(target, u1, u2, ustride, uorder, v1, v2, vstride, vorder, points); }
// inline void glMap2f(GLenum target, GLfloat u1, GLfloat u2, GLint ustride, GLint uorder, GLfloat v1, GLfloat v2, GLint vstride, GLint vorder, GLfloat* points){  glMap2fProc(target, u1, u2, ustride, uorder, v1, v2, vstride, vorder, points); }
// inline void glMapGrid1d(GLint un, GLdouble u1, GLdouble u2){  glMapGrid1dProc(un, u1, u2); }
// inline void glMapGrid1f(GLint un, GLfloat u1, GLfloat u2){  glMapGrid1fProc(un, u1, u2); }
// inline void glMapGrid2d(GLint un, GLdouble u1, GLdouble u2, GLint vn, GLdouble v1, GLdouble v2){  glMapGrid2dProc(un, u1, u2, vn, v1, v2); }
// inline void glMapGrid2f(GLint un, GLfloat u1, GLfloat u2, GLint vn, GLfloat v1, GLfloat v2){  glMapGrid2fProc(un, u1, u2, vn, v1, v2); }
// inline void glEvalCoord1d(GLdouble u){  glEvalCoord1dProc(u); }
// inline void glEvalCoord1dv(GLdouble* u){  glEvalCoord1dvProc(u); }
// inline void glEvalCoord1f(GLfloat u){  glEvalCoord1fProc(u); }
// inline void glEvalCoord1fv(GLfloat* u){  glEvalCoord1fvProc(u); }
// inline void glEvalCoord2d(GLdouble u, GLdouble v){  glEvalCoord2dProc(u, v); }
// inline void glEvalCoord2dv(GLdouble* u){  glEvalCoord2dvProc(u); }
// inline void glEvalCoord2f(GLfloat u, GLfloat v){  glEvalCoord2fProc(u, v); }
// inline void glEvalCoord2fv(GLfloat* u){  glEvalCoord2fvProc(u); }
// inline void glEvalMesh1(GLenum mode, GLint i1, GLint i2){  glEvalMesh1Proc(mode, i1, i2); }
// inline void glEvalPoint1(GLint i){  glEvalPoint1Proc(i); }
// inline void glEvalMesh2(GLenum mode, GLint i1, GLint i2, GLint j1, GLint j2){  glEvalMesh2Proc(mode, i1, i2, j1, j2); }
// inline void glEvalPoint2(GLint i, GLint j){  glEvalPoint2Proc(i, j); }
// inline void glAlphaFunc(GLenum func_, GLfloat ref){  glAlphaFuncProc(func_, ref); }
// inline void glPixelZoom(GLfloat xfactor, GLfloat yfactor){  glPixelZoomProc(xfactor, yfactor); }
// inline void glPixelTransferf(GLenum pname, GLfloat param){  glPixelTransferfProc(pname, param); }
// inline void glPixelTransferi(GLenum pname, GLint param){  glPixelTransferiProc(pname, param); }
// inline void glPixelMapfv(GLenum map_, GLsizei mapsize, GLfloat* values){  glPixelMapfvProc(map_, mapsize, values); }
// inline void glPixelMapuiv(GLenum map_, GLsizei mapsize, GLuint* values){  glPixelMapuivProc(map_, mapsize, values); }
// inline void glPixelMapusv(GLenum map_, GLsizei mapsize, GLushort* values){  glPixelMapusvProc(map_, mapsize, values); }
// inline void glCopyPixels(GLint x, GLint y, GLsizei width, GLsizei height, GLenum type_){  glCopyPixelsProc(x, y, width, height, type_); }
// inline void glDrawPixels(GLsizei width, GLsizei height, GLenum format, GLenum type_, GLvoid* pixels){  glDrawPixelsProc(width, height, format, type_, pixels); }
// inline void glGetClipPlane(GLenum plane, GLdouble* equation){  glGetClipPlaneProc(plane, equation); }
// inline void glGetLightfv(GLenum light, GLenum pname, GLfloat* params){  glGetLightfvProc(light, pname, params); }
// inline void glGetLightiv(GLenum light, GLenum pname, GLint* params){  glGetLightivProc(light, pname, params); }
// inline void glGetMapdv(GLenum target, GLenum query, GLdouble* v){  glGetMapdvProc(target, query, v); }
// inline void glGetMapfv(GLenum target, GLenum query, GLfloat* v){  glGetMapfvProc(target, query, v); }
// inline void glGetMapiv(GLenum target, GLenum query, GLint* v){  glGetMapivProc(target, query, v); }
// inline void glGetMaterialfv(GLenum face, GLenum pname, GLfloat* params){  glGetMaterialfvProc(face, pname, params); }
// inline void glGetMaterialiv(GLenum face, GLenum pname, GLint* params){  glGetMaterialivProc(face, pname, params); }
// inline void glGetPixelMapfv(GLenum map_, GLfloat* values){  glGetPixelMapfvProc(map_, values); }
// inline void glGetPixelMapuiv(GLenum map_, GLuint* values){  glGetPixelMapuivProc(map_, values); }
// inline void glGetPixelMapusv(GLenum map_, GLushort* values){  glGetPixelMapusvProc(map_, values); }
// inline void glGetPolygonStipple(GLubyte* mask){  glGetPolygonStippleProc(mask); }
// inline void glGetTexEnvfv(GLenum target, GLenum pname, GLfloat* params){  glGetTexEnvfvProc(target, pname, params); }
// inline void glGetTexEnviv(GLenum target, GLenum pname, GLint* params){  glGetTexEnvivProc(target, pname, params); }
// inline void glGetTexGendv(GLenum coord, GLenum pname, GLdouble* params){  glGetTexGendvProc(coord, pname, params); }
// inline void glGetTexGenfv(GLenum coord, GLenum pname, GLfloat* params){  glGetTexGenfvProc(coord, pname, params); }
// inline void glGetTexGeniv(GLenum coord, GLenum pname, GLint* params){  glGetTexGenivProc(coord, pname, params); }
// inline GLboolean glIsList(GLuint list){ return glIsListProc(list); }
// inline void glFrustum(GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble zNear, GLdouble zFar){  glFrustumProc(left, right, bottom, top, zNear, zFar); }
// inline void glLoadIdentity(){  glLoadIdentityProc(); }
// inline void glLoadMatrixf(GLfloat* m){  glLoadMatrixfProc(m); }
// inline void glLoadMatrixd(GLdouble* m){  glLoadMatrixdProc(m); }
// inline void glMatrixMode(GLenum mode){  glMatrixModeProc(mode); }
// inline void glMultMatrixf(GLfloat* m){  glMultMatrixfProc(m); }
// inline void glMultMatrixd(GLdouble* m){  glMultMatrixdProc(m); }
// inline void glOrtho(GLdouble left, GLdouble right, GLdouble bottom, GLdouble top, GLdouble zNear, GLdouble zFar){  glOrthoProc(left, right, bottom, top, zNear, zFar); }
// inline void glPopMatrix(){  glPopMatrixProc(); }
// inline void glPushMatrix(){  glPushMatrixProc(); }
// inline void glRotated(GLdouble angle, GLdouble x, GLdouble y, GLdouble z){  glRotatedProc(angle, x, y, z); }
// inline void glRotatef(GLfloat angle, GLfloat x, GLfloat y, GLfloat z){  glRotatefProc(angle, x, y, z); }
// inline void glScaled(GLdouble x, GLdouble y, GLdouble z){  glScaledProc(x, y, z); }
// inline void glScalef(GLfloat x, GLfloat y, GLfloat z){  glScalefProc(x, y, z); }
// inline void glTranslated(GLdouble x, GLdouble y, GLdouble z){  glTranslatedProc(x, y, z); }
// inline void glTranslatef(GLfloat x, GLfloat y, GLfloat z){  glTranslatefProc(x, y, z); }
// 
// int initGL_VERSION_1_0() { 
//     if ((glCullFaceProc = GetGLProcAddress("glCullFace")) == NULL) return 0;
//     if ((glFrontFaceProc = GetGLProcAddress("glFrontFace")) == NULL) return 0;
//     if ((glHintProc = GetGLProcAddress("glHint")) == NULL) return 0;
//     if ((glLineWidthProc = GetGLProcAddress("glLineWidth")) == NULL) return 0;
//     if ((glPointSizeProc = GetGLProcAddress("glPointSize")) == NULL) return 0;
//     if ((glPolygonModeProc = GetGLProcAddress("glPolygonMode")) == NULL) return 0;
//     if ((glScissorProc = GetGLProcAddress("glScissor")) == NULL) return 0;
//     if ((glTexParameterfProc = GetGLProcAddress("glTexParameterf")) == NULL) return 0;
//     if ((glTexParameterfvProc = GetGLProcAddress("glTexParameterfv")) == NULL) return 0;
//     if ((glTexParameteriProc = GetGLProcAddress("glTexParameteri")) == NULL) return 0;
//     if ((glTexParameterivProc = GetGLProcAddress("glTexParameteriv")) == NULL) return 0;
//     if ((glTexImage1DProc = GetGLProcAddress("glTexImage1D")) == NULL) return 0;
//     if ((glTexImage2DProc = GetGLProcAddress("glTexImage2D")) == NULL) return 0;
//     if ((glDrawBufferProc = GetGLProcAddress("glDrawBuffer")) == NULL) return 0;
//     if ((glClearProc = GetGLProcAddress("glClear")) == NULL) return 0;
//     if ((glClearColorProc = GetGLProcAddress("glClearColor")) == NULL) return 0;
//     if ((glClearStencilProc = GetGLProcAddress("glClearStencil")) == NULL) return 0;
//     if ((glClearDepthProc = GetGLProcAddress("glClearDepth")) == NULL) return 0;
//     if ((glStencilMaskProc = GetGLProcAddress("glStencilMask")) == NULL) return 0;
//     if ((glColorMaskProc = GetGLProcAddress("glColorMask")) == NULL) return 0;
//     if ((glDepthMaskProc = GetGLProcAddress("glDepthMask")) == NULL) return 0;
//     if ((glDisableProc = GetGLProcAddress("glDisable")) == NULL) return 0;
//     if ((glEnableProc = GetGLProcAddress("glEnable")) == NULL) return 0;
//     if ((glFinishProc = GetGLProcAddress("glFinish")) == NULL) return 0;
//     if ((glFlushProc = GetGLProcAddress("glFlush")) == NULL) return 0;
//     if ((glBlendFuncProc = GetGLProcAddress("glBlendFunc")) == NULL) return 0;
//     if ((glLogicOpProc = GetGLProcAddress("glLogicOp")) == NULL) return 0;
//     if ((glStencilFuncProc = GetGLProcAddress("glStencilFunc")) == NULL) return 0;
//     if ((glStencilOpProc = GetGLProcAddress("glStencilOp")) == NULL) return 0;
//     if ((glDepthFuncProc = GetGLProcAddress("glDepthFunc")) == NULL) return 0;
//     if ((glPixelStorefProc = GetGLProcAddress("glPixelStoref")) == NULL) return 0;
//     if ((glPixelStoreiProc = GetGLProcAddress("glPixelStorei")) == NULL) return 0;
//     if ((glReadBufferProc = GetGLProcAddress("glReadBuffer")) == NULL) return 0;
//     if ((glReadPixelsProc = GetGLProcAddress("glReadPixels")) == NULL) return 0;
//     if ((glGetBooleanvProc = GetGLProcAddress("glGetBooleanv")) == NULL) return 0;
//     if ((glGetDoublevProc = GetGLProcAddress("glGetDoublev")) == NULL) return 0;
//     if ((glGetErrorProc = GetGLProcAddress("glGetError")) == NULL) return 0;
//     if ((glGetFloatvProc = GetGLProcAddress("glGetFloatv")) == NULL) return 0;
//     if ((glGetIntegervProc = GetGLProcAddress("glGetIntegerv")) == NULL) return 0;
//     if ((glGetStringProc = GetGLProcAddress("glGetString")) == NULL) return 0;
//     if ((glGetTexImageProc = GetGLProcAddress("glGetTexImage")) == NULL) return 0;
//     if ((glGetTexParameterfvProc = GetGLProcAddress("glGetTexParameterfv")) == NULL) return 0;
//     if ((glGetTexParameterivProc = GetGLProcAddress("glGetTexParameteriv")) == NULL) return 0;
//     if ((glGetTexLevelParameterfvProc = GetGLProcAddress("glGetTexLevelParameterfv")) == NULL) return 0;
//     if ((glGetTexLevelParameterivProc = GetGLProcAddress("glGetTexLevelParameteriv")) == NULL) return 0;
//     if ((glIsEnabledProc = GetGLProcAddress("glIsEnabled")) == NULL) return 0;
//     if ((glDepthRangeProc = GetGLProcAddress("glDepthRange")) == NULL) return 0;
//     if ((glViewportProc = GetGLProcAddress("glViewport")) == NULL) return 0;
//     if ((glNewListProc = GetGLProcAddress("glNewList")) == NULL) return 0;
//     if ((glEndListProc = GetGLProcAddress("glEndList")) == NULL) return 0;
//     if ((glCallListProc = GetGLProcAddress("glCallList")) == NULL) return 0;
//     if ((glCallListsProc = GetGLProcAddress("glCallLists")) == NULL) return 0;
//     if ((glDeleteListsProc = GetGLProcAddress("glDeleteLists")) == NULL) return 0;
//     if ((glGenListsProc = GetGLProcAddress("glGenLists")) == NULL) return 0;
//     if ((glListBaseProc = GetGLProcAddress("glListBase")) == NULL) return 0;
//     if ((glBeginProc = GetGLProcAddress("glBegin")) == NULL) return 0;
//     if ((glBitmapProc = GetGLProcAddress("glBitmap")) == NULL) return 0;
//     if ((glColor3bProc = GetGLProcAddress("glColor3b")) == NULL) return 0;
//     if ((glColor3bvProc = GetGLProcAddress("glColor3bv")) == NULL) return 0;
//     if ((glColor3dProc = GetGLProcAddress("glColor3d")) == NULL) return 0;
//     if ((glColor3dvProc = GetGLProcAddress("glColor3dv")) == NULL) return 0;
//     if ((glColor3fProc = GetGLProcAddress("glColor3f")) == NULL) return 0;
//     if ((glColor3fvProc = GetGLProcAddress("glColor3fv")) == NULL) return 0;
//     if ((glColor3iProc = GetGLProcAddress("glColor3i")) == NULL) return 0;
//     if ((glColor3ivProc = GetGLProcAddress("glColor3iv")) == NULL) return 0;
//     if ((glColor3sProc = GetGLProcAddress("glColor3s")) == NULL) return 0;
//     if ((glColor3svProc = GetGLProcAddress("glColor3sv")) == NULL) return 0;
//     if ((glColor3ubProc = GetGLProcAddress("glColor3ub")) == NULL) return 0;
//     if ((glColor3ubvProc = GetGLProcAddress("glColor3ubv")) == NULL) return 0;
//     if ((glColor3uiProc = GetGLProcAddress("glColor3ui")) == NULL) return 0;
//     if ((glColor3uivProc = GetGLProcAddress("glColor3uiv")) == NULL) return 0;
//     if ((glColor3usProc = GetGLProcAddress("glColor3us")) == NULL) return 0;
//     if ((glColor3usvProc = GetGLProcAddress("glColor3usv")) == NULL) return 0;
//     if ((glColor4bProc = GetGLProcAddress("glColor4b")) == NULL) return 0;
//     if ((glColor4bvProc = GetGLProcAddress("glColor4bv")) == NULL) return 0;
//     if ((glColor4dProc = GetGLProcAddress("glColor4d")) == NULL) return 0;
//     if ((glColor4dvProc = GetGLProcAddress("glColor4dv")) == NULL) return 0;
//     if ((glColor4fProc = GetGLProcAddress("glColor4f")) == NULL) return 0;
//     if ((glColor4fvProc = GetGLProcAddress("glColor4fv")) == NULL) return 0;
//     if ((glColor4iProc = GetGLProcAddress("glColor4i")) == NULL) return 0;
//     if ((glColor4ivProc = GetGLProcAddress("glColor4iv")) == NULL) return 0;
//     if ((glColor4sProc = GetGLProcAddress("glColor4s")) == NULL) return 0;
//     if ((glColor4svProc = GetGLProcAddress("glColor4sv")) == NULL) return 0;
//     if ((glColor4ubProc = GetGLProcAddress("glColor4ub")) == NULL) return 0;
//     if ((glColor4ubvProc = GetGLProcAddress("glColor4ubv")) == NULL) return 0;
//     if ((glColor4uiProc = GetGLProcAddress("glColor4ui")) == NULL) return 0;
//     if ((glColor4uivProc = GetGLProcAddress("glColor4uiv")) == NULL) return 0;
//     if ((glColor4usProc = GetGLProcAddress("glColor4us")) == NULL) return 0;
//     if ((glColor4usvProc = GetGLProcAddress("glColor4usv")) == NULL) return 0;
//     if ((glEdgeFlagProc = GetGLProcAddress("glEdgeFlag")) == NULL) return 0;
//     if ((glEdgeFlagvProc = GetGLProcAddress("glEdgeFlagv")) == NULL) return 0;
//     if ((glEndProc = GetGLProcAddress("glEnd")) == NULL) return 0;
//     if ((glIndexdProc = GetGLProcAddress("glIndexd")) == NULL) return 0;
//     if ((glIndexdvProc = GetGLProcAddress("glIndexdv")) == NULL) return 0;
//     if ((glIndexfProc = GetGLProcAddress("glIndexf")) == NULL) return 0;
//     if ((glIndexfvProc = GetGLProcAddress("glIndexfv")) == NULL) return 0;
//     if ((glIndexiProc = GetGLProcAddress("glIndexi")) == NULL) return 0;
//     if ((glIndexivProc = GetGLProcAddress("glIndexiv")) == NULL) return 0;
//     if ((glIndexsProc = GetGLProcAddress("glIndexs")) == NULL) return 0;
//     if ((glIndexsvProc = GetGLProcAddress("glIndexsv")) == NULL) return 0;
//     if ((glNormal3bProc = GetGLProcAddress("glNormal3b")) == NULL) return 0;
//     if ((glNormal3bvProc = GetGLProcAddress("glNormal3bv")) == NULL) return 0;
//     if ((glNormal3dProc = GetGLProcAddress("glNormal3d")) == NULL) return 0;
//     if ((glNormal3dvProc = GetGLProcAddress("glNormal3dv")) == NULL) return 0;
//     if ((glNormal3fProc = GetGLProcAddress("glNormal3f")) == NULL) return 0;
//     if ((glNormal3fvProc = GetGLProcAddress("glNormal3fv")) == NULL) return 0;
//     if ((glNormal3iProc = GetGLProcAddress("glNormal3i")) == NULL) return 0;
//     if ((glNormal3ivProc = GetGLProcAddress("glNormal3iv")) == NULL) return 0;
//     if ((glNormal3sProc = GetGLProcAddress("glNormal3s")) == NULL) return 0;
//     if ((glNormal3svProc = GetGLProcAddress("glNormal3sv")) == NULL) return 0;
//     if ((glRasterPos2dProc = GetGLProcAddress("glRasterPos2d")) == NULL) return 0;
//     if ((glRasterPos2dvProc = GetGLProcAddress("glRasterPos2dv")) == NULL) return 0;
//     if ((glRasterPos2fProc = GetGLProcAddress("glRasterPos2f")) == NULL) return 0;
//     if ((glRasterPos2fvProc = GetGLProcAddress("glRasterPos2fv")) == NULL) return 0;
//     if ((glRasterPos2iProc = GetGLProcAddress("glRasterPos2i")) == NULL) return 0;
//     if ((glRasterPos2ivProc = GetGLProcAddress("glRasterPos2iv")) == NULL) return 0;
//     if ((glRasterPos2sProc = GetGLProcAddress("glRasterPos2s")) == NULL) return 0;
//     if ((glRasterPos2svProc = GetGLProcAddress("glRasterPos2sv")) == NULL) return 0;
//     if ((glRasterPos3dProc = GetGLProcAddress("glRasterPos3d")) == NULL) return 0;
//     if ((glRasterPos3dvProc = GetGLProcAddress("glRasterPos3dv")) == NULL) return 0;
//     if ((glRasterPos3fProc = GetGLProcAddress("glRasterPos3f")) == NULL) return 0;
//     if ((glRasterPos3fvProc = GetGLProcAddress("glRasterPos3fv")) == NULL) return 0;
//     if ((glRasterPos3iProc = GetGLProcAddress("glRasterPos3i")) == NULL) return 0;
//     if ((glRasterPos3ivProc = GetGLProcAddress("glRasterPos3iv")) == NULL) return 0;
//     if ((glRasterPos3sProc = GetGLProcAddress("glRasterPos3s")) == NULL) return 0;
//     if ((glRasterPos3svProc = GetGLProcAddress("glRasterPos3sv")) == NULL) return 0;
//     if ((glRasterPos4dProc = GetGLProcAddress("glRasterPos4d")) == NULL) return 0;
//     if ((glRasterPos4dvProc = GetGLProcAddress("glRasterPos4dv")) == NULL) return 0;
//     if ((glRasterPos4fProc = GetGLProcAddress("glRasterPos4f")) == NULL) return 0;
//     if ((glRasterPos4fvProc = GetGLProcAddress("glRasterPos4fv")) == NULL) return 0;
//     if ((glRasterPos4iProc = GetGLProcAddress("glRasterPos4i")) == NULL) return 0;
//     if ((glRasterPos4ivProc = GetGLProcAddress("glRasterPos4iv")) == NULL) return 0;
//     if ((glRasterPos4sProc = GetGLProcAddress("glRasterPos4s")) == NULL) return 0;
//     if ((glRasterPos4svProc = GetGLProcAddress("glRasterPos4sv")) == NULL) return 0;
//     if ((glRectdProc = GetGLProcAddress("glRectd")) == NULL) return 0;
//     if ((glRectdvProc = GetGLProcAddress("glRectdv")) == NULL) return 0;
//     if ((glRectfProc = GetGLProcAddress("glRectf")) == NULL) return 0;
//     if ((glRectfvProc = GetGLProcAddress("glRectfv")) == NULL) return 0;
//     if ((glRectiProc = GetGLProcAddress("glRecti")) == NULL) return 0;
//     if ((glRectivProc = GetGLProcAddress("glRectiv")) == NULL) return 0;
//     if ((glRectsProc = GetGLProcAddress("glRects")) == NULL) return 0;
//     if ((glRectsvProc = GetGLProcAddress("glRectsv")) == NULL) return 0;
//     if ((glTexCoord1dProc = GetGLProcAddress("glTexCoord1d")) == NULL) return 0;
//     if ((glTexCoord1dvProc = GetGLProcAddress("glTexCoord1dv")) == NULL) return 0;
//     if ((glTexCoord1fProc = GetGLProcAddress("glTexCoord1f")) == NULL) return 0;
//     if ((glTexCoord1fvProc = GetGLProcAddress("glTexCoord1fv")) == NULL) return 0;
//     if ((glTexCoord1iProc = GetGLProcAddress("glTexCoord1i")) == NULL) return 0;
//     if ((glTexCoord1ivProc = GetGLProcAddress("glTexCoord1iv")) == NULL) return 0;
//     if ((glTexCoord1sProc = GetGLProcAddress("glTexCoord1s")) == NULL) return 0;
//     if ((glTexCoord1svProc = GetGLProcAddress("glTexCoord1sv")) == NULL) return 0;
//     if ((glTexCoord2dProc = GetGLProcAddress("glTexCoord2d")) == NULL) return 0;
//     if ((glTexCoord2dvProc = GetGLProcAddress("glTexCoord2dv")) == NULL) return 0;
//     if ((glTexCoord2fProc = GetGLProcAddress("glTexCoord2f")) == NULL) return 0;
//     if ((glTexCoord2fvProc = GetGLProcAddress("glTexCoord2fv")) == NULL) return 0;
//     if ((glTexCoord2iProc = GetGLProcAddress("glTexCoord2i")) == NULL) return 0;
//     if ((glTexCoord2ivProc = GetGLProcAddress("glTexCoord2iv")) == NULL) return 0;
//     if ((glTexCoord2sProc = GetGLProcAddress("glTexCoord2s")) == NULL) return 0;
//     if ((glTexCoord2svProc = GetGLProcAddress("glTexCoord2sv")) == NULL) return 0;
//     if ((glTexCoord3dProc = GetGLProcAddress("glTexCoord3d")) == NULL) return 0;
//     if ((glTexCoord3dvProc = GetGLProcAddress("glTexCoord3dv")) == NULL) return 0;
//     if ((glTexCoord3fProc = GetGLProcAddress("glTexCoord3f")) == NULL) return 0;
//     if ((glTexCoord3fvProc = GetGLProcAddress("glTexCoord3fv")) == NULL) return 0;
//     if ((glTexCoord3iProc = GetGLProcAddress("glTexCoord3i")) == NULL) return 0;
//     if ((glTexCoord3ivProc = GetGLProcAddress("glTexCoord3iv")) == NULL) return 0;
//     if ((glTexCoord3sProc = GetGLProcAddress("glTexCoord3s")) == NULL) return 0;
//     if ((glTexCoord3svProc = GetGLProcAddress("glTexCoord3sv")) == NULL) return 0;
//     if ((glTexCoord4dProc = GetGLProcAddress("glTexCoord4d")) == NULL) return 0;
//     if ((glTexCoord4dvProc = GetGLProcAddress("glTexCoord4dv")) == NULL) return 0;
//     if ((glTexCoord4fProc = GetGLProcAddress("glTexCoord4f")) == NULL) return 0;
//     if ((glTexCoord4fvProc = GetGLProcAddress("glTexCoord4fv")) == NULL) return 0;
//     if ((glTexCoord4iProc = GetGLProcAddress("glTexCoord4i")) == NULL) return 0;
//     if ((glTexCoord4ivProc = GetGLProcAddress("glTexCoord4iv")) == NULL) return 0;
//     if ((glTexCoord4sProc = GetGLProcAddress("glTexCoord4s")) == NULL) return 0;
//     if ((glTexCoord4svProc = GetGLProcAddress("glTexCoord4sv")) == NULL) return 0;
//     if ((glVertex2dProc = GetGLProcAddress("glVertex2d")) == NULL) return 0;
//     if ((glVertex2dvProc = GetGLProcAddress("glVertex2dv")) == NULL) return 0;
//     if ((glVertex2fProc = GetGLProcAddress("glVertex2f")) == NULL) return 0;
//     if ((glVertex2fvProc = GetGLProcAddress("glVertex2fv")) == NULL) return 0;
//     if ((glVertex2iProc = GetGLProcAddress("glVertex2i")) == NULL) return 0;
//     if ((glVertex2ivProc = GetGLProcAddress("glVertex2iv")) == NULL) return 0;
//     if ((glVertex2sProc = GetGLProcAddress("glVertex2s")) == NULL) return 0;
//     if ((glVertex2svProc = GetGLProcAddress("glVertex2sv")) == NULL) return 0;
//     if ((glVertex3dProc = GetGLProcAddress("glVertex3d")) == NULL) return 0;
//     if ((glVertex3dvProc = GetGLProcAddress("glVertex3dv")) == NULL) return 0;
//     if ((glVertex3fProc = GetGLProcAddress("glVertex3f")) == NULL) return 0;
//     if ((glVertex3fvProc = GetGLProcAddress("glVertex3fv")) == NULL) return 0;
//     if ((glVertex3iProc = GetGLProcAddress("glVertex3i")) == NULL) return 0;
//     if ((glVertex3ivProc = GetGLProcAddress("glVertex3iv")) == NULL) return 0;
//     if ((glVertex3sProc = GetGLProcAddress("glVertex3s")) == NULL) return 0;
//     if ((glVertex3svProc = GetGLProcAddress("glVertex3sv")) == NULL) return 0;
//     if ((glVertex4dProc = GetGLProcAddress("glVertex4d")) == NULL) return 0;
//     if ((glVertex4dvProc = GetGLProcAddress("glVertex4dv")) == NULL) return 0;
//     if ((glVertex4fProc = GetGLProcAddress("glVertex4f")) == NULL) return 0;
//     if ((glVertex4fvProc = GetGLProcAddress("glVertex4fv")) == NULL) return 0;
//     if ((glVertex4iProc = GetGLProcAddress("glVertex4i")) == NULL) return 0;
//     if ((glVertex4ivProc = GetGLProcAddress("glVertex4iv")) == NULL) return 0;
//     if ((glVertex4sProc = GetGLProcAddress("glVertex4s")) == NULL) return 0;
//     if ((glVertex4svProc = GetGLProcAddress("glVertex4sv")) == NULL) return 0;
//     if ((glClipPlaneProc = GetGLProcAddress("glClipPlane")) == NULL) return 0;
//     if ((glColorMaterialProc = GetGLProcAddress("glColorMaterial")) == NULL) return 0;
//     if ((glFogfProc = GetGLProcAddress("glFogf")) == NULL) return 0;
//     if ((glFogfvProc = GetGLProcAddress("glFogfv")) == NULL) return 0;
//     if ((glFogiProc = GetGLProcAddress("glFogi")) == NULL) return 0;
//     if ((glFogivProc = GetGLProcAddress("glFogiv")) == NULL) return 0;
//     if ((glLightfProc = GetGLProcAddress("glLightf")) == NULL) return 0;
//     if ((glLightfvProc = GetGLProcAddress("glLightfv")) == NULL) return 0;
//     if ((glLightiProc = GetGLProcAddress("glLighti")) == NULL) return 0;
//     if ((glLightivProc = GetGLProcAddress("glLightiv")) == NULL) return 0;
//     if ((glLightModelfProc = GetGLProcAddress("glLightModelf")) == NULL) return 0;
//     if ((glLightModelfvProc = GetGLProcAddress("glLightModelfv")) == NULL) return 0;
//     if ((glLightModeliProc = GetGLProcAddress("glLightModeli")) == NULL) return 0;
//     if ((glLightModelivProc = GetGLProcAddress("glLightModeliv")) == NULL) return 0;
//     if ((glLineStippleProc = GetGLProcAddress("glLineStipple")) == NULL) return 0;
//     if ((glMaterialfProc = GetGLProcAddress("glMaterialf")) == NULL) return 0;
//     if ((glMaterialfvProc = GetGLProcAddress("glMaterialfv")) == NULL) return 0;
//     if ((glMaterialiProc = GetGLProcAddress("glMateriali")) == NULL) return 0;
//     if ((glMaterialivProc = GetGLProcAddress("glMaterialiv")) == NULL) return 0;
//     if ((glPolygonStippleProc = GetGLProcAddress("glPolygonStipple")) == NULL) return 0;
//     if ((glShadeModelProc = GetGLProcAddress("glShadeModel")) == NULL) return 0;
//     if ((glTexEnvfProc = GetGLProcAddress("glTexEnvf")) == NULL) return 0;
//     if ((glTexEnvfvProc = GetGLProcAddress("glTexEnvfv")) == NULL) return 0;
//     if ((glTexEnviProc = GetGLProcAddress("glTexEnvi")) == NULL) return 0;
//     if ((glTexEnvivProc = GetGLProcAddress("glTexEnviv")) == NULL) return 0;
//     if ((glTexGendProc = GetGLProcAddress("glTexGend")) == NULL) return 0;
//     if ((glTexGendvProc = GetGLProcAddress("glTexGendv")) == NULL) return 0;
//     if ((glTexGenfProc = GetGLProcAddress("glTexGenf")) == NULL) return 0;
//     if ((glTexGenfvProc = GetGLProcAddress("glTexGenfv")) == NULL) return 0;
//     if ((glTexGeniProc = GetGLProcAddress("glTexGeni")) == NULL) return 0;
//     if ((glTexGenivProc = GetGLProcAddress("glTexGeniv")) == NULL) return 0;
//     if ((glFeedbackBufferProc = GetGLProcAddress("glFeedbackBuffer")) == NULL) return 0;
//     if ((glSelectBufferProc = GetGLProcAddress("glSelectBuffer")) == NULL) return 0;
//     if ((glRenderModeProc = GetGLProcAddress("glRenderMode")) == NULL) return 0;
//     if ((glInitNamesProc = GetGLProcAddress("glInitNames")) == NULL) return 0;
//     if ((glLoadNameProc = GetGLProcAddress("glLoadName")) == NULL) return 0;
//     if ((glPassThroughProc = GetGLProcAddress("glPassThrough")) == NULL) return 0;
//     if ((glPopNameProc = GetGLProcAddress("glPopName")) == NULL) return 0;
//     if ((glPushNameProc = GetGLProcAddress("glPushName")) == NULL) return 0;
//     if ((glClearAccumProc = GetGLProcAddress("glClearAccum")) == NULL) return 0;
//     if ((glClearIndexProc = GetGLProcAddress("glClearIndex")) == NULL) return 0;
//     if ((glIndexMaskProc = GetGLProcAddress("glIndexMask")) == NULL) return 0;
//     if ((glAccumProc = GetGLProcAddress("glAccum")) == NULL) return 0;
//     if ((glPopAttribProc = GetGLProcAddress("glPopAttrib")) == NULL) return 0;
//     if ((glPushAttribProc = GetGLProcAddress("glPushAttrib")) == NULL) return 0;
//     if ((glMap1dProc = GetGLProcAddress("glMap1d")) == NULL) return 0;
//     if ((glMap1fProc = GetGLProcAddress("glMap1f")) == NULL) return 0;
//     if ((glMap2dProc = GetGLProcAddress("glMap2d")) == NULL) return 0;
//     if ((glMap2fProc = GetGLProcAddress("glMap2f")) == NULL) return 0;
//     if ((glMapGrid1dProc = GetGLProcAddress("glMapGrid1d")) == NULL) return 0;
//     if ((glMapGrid1fProc = GetGLProcAddress("glMapGrid1f")) == NULL) return 0;
//     if ((glMapGrid2dProc = GetGLProcAddress("glMapGrid2d")) == NULL) return 0;
//     if ((glMapGrid2fProc = GetGLProcAddress("glMapGrid2f")) == NULL) return 0;
//     if ((glEvalCoord1dProc = GetGLProcAddress("glEvalCoord1d")) == NULL) return 0;
//     if ((glEvalCoord1dvProc = GetGLProcAddress("glEvalCoord1dv")) == NULL) return 0;
//     if ((glEvalCoord1fProc = GetGLProcAddress("glEvalCoord1f")) == NULL) return 0;
//     if ((glEvalCoord1fvProc = GetGLProcAddress("glEvalCoord1fv")) == NULL) return 0;
//     if ((glEvalCoord2dProc = GetGLProcAddress("glEvalCoord2d")) == NULL) return 0;
//     if ((glEvalCoord2dvProc = GetGLProcAddress("glEvalCoord2dv")) == NULL) return 0;
//     if ((glEvalCoord2fProc = GetGLProcAddress("glEvalCoord2f")) == NULL) return 0;
//     if ((glEvalCoord2fvProc = GetGLProcAddress("glEvalCoord2fv")) == NULL) return 0;
//     if ((glEvalMesh1Proc = GetGLProcAddress("glEvalMesh1")) == NULL) return 0;
//     if ((glEvalPoint1Proc = GetGLProcAddress("glEvalPoint1")) == NULL) return 0;
//     if ((glEvalMesh2Proc = GetGLProcAddress("glEvalMesh2")) == NULL) return 0;
//     if ((glEvalPoint2Proc = GetGLProcAddress("glEvalPoint2")) == NULL) return 0;
//     if ((glAlphaFuncProc = GetGLProcAddress("glAlphaFunc")) == NULL) return 0;
//     if ((glPixelZoomProc = GetGLProcAddress("glPixelZoom")) == NULL) return 0;
//     if ((glPixelTransferfProc = GetGLProcAddress("glPixelTransferf")) == NULL) return 0;
//     if ((glPixelTransferiProc = GetGLProcAddress("glPixelTransferi")) == NULL) return 0;
//     if ((glPixelMapfvProc = GetGLProcAddress("glPixelMapfv")) == NULL) return 0;
//     if ((glPixelMapuivProc = GetGLProcAddress("glPixelMapuiv")) == NULL) return 0;
//     if ((glPixelMapusvProc = GetGLProcAddress("glPixelMapusv")) == NULL) return 0;
//     if ((glCopyPixelsProc = GetGLProcAddress("glCopyPixels")) == NULL) return 0;
//     if ((glDrawPixelsProc = GetGLProcAddress("glDrawPixels")) == NULL) return 0;
//     if ((glGetClipPlaneProc = GetGLProcAddress("glGetClipPlane")) == NULL) return 0;
//     if ((glGetLightfvProc = GetGLProcAddress("glGetLightfv")) == NULL) return 0;
//     if ((glGetLightivProc = GetGLProcAddress("glGetLightiv")) == NULL) return 0;
//     if ((glGetMapdvProc = GetGLProcAddress("glGetMapdv")) == NULL) return 0;
//     if ((glGetMapfvProc = GetGLProcAddress("glGetMapfv")) == NULL) return 0;
//     if ((glGetMapivProc = GetGLProcAddress("glGetMapiv")) == NULL) return 0;
//     if ((glGetMaterialfvProc = GetGLProcAddress("glGetMaterialfv")) == NULL) return 0;
//     if ((glGetMaterialivProc = GetGLProcAddress("glGetMaterialiv")) == NULL) return 0;
//     if ((glGetPixelMapfvProc = GetGLProcAddress("glGetPixelMapfv")) == NULL) return 0;
//     if ((glGetPixelMapuivProc = GetGLProcAddress("glGetPixelMapuiv")) == NULL) return 0;
//     if ((glGetPixelMapusvProc = GetGLProcAddress("glGetPixelMapusv")) == NULL) return 0;
//     if ((glGetPolygonStippleProc = GetGLProcAddress("glGetPolygonStipple")) == NULL) return 0;
//     if ((glGetTexEnvfvProc = GetGLProcAddress("glGetTexEnvfv")) == NULL) return 0;
//     if ((glGetTexEnvivProc = GetGLProcAddress("glGetTexEnviv")) == NULL) return 0;
//     if ((glGetTexGendvProc = GetGLProcAddress("glGetTexGendv")) == NULL) return 0;
//     if ((glGetTexGenfvProc = GetGLProcAddress("glGetTexGenfv")) == NULL) return 0;
//     if ((glGetTexGenivProc = GetGLProcAddress("glGetTexGeniv")) == NULL) return 0;
//     if ((glIsListProc = GetGLProcAddress("glIsList")) == NULL) return 0;
//     if ((glFrustumProc = GetGLProcAddress("glFrustum")) == NULL) return 0;
//     if ((glLoadIdentityProc = GetGLProcAddress("glLoadIdentity")) == NULL) return 0;
//     if ((glLoadMatrixfProc = GetGLProcAddress("glLoadMatrixf")) == NULL) return 0;
//     if ((glLoadMatrixdProc = GetGLProcAddress("glLoadMatrixd")) == NULL) return 0;
//     if ((glMatrixModeProc = GetGLProcAddress("glMatrixMode")) == NULL) return 0;
//     if ((glMultMatrixfProc = GetGLProcAddress("glMultMatrixf")) == NULL) return 0;
//     if ((glMultMatrixdProc = GetGLProcAddress("glMultMatrixd")) == NULL) return 0;
//     if ((glOrthoProc = GetGLProcAddress("glOrtho")) == NULL) return 0;
//     if ((glPopMatrixProc = GetGLProcAddress("glPopMatrix")) == NULL) return 0;
//     if ((glPushMatrixProc = GetGLProcAddress("glPushMatrix")) == NULL) return 0;
//     if ((glRotatedProc = GetGLProcAddress("glRotated")) == NULL) return 0;
//     if ((glRotatefProc = GetGLProcAddress("glRotatef")) == NULL) return 0;
//     if ((glScaledProc = GetGLProcAddress("glScaled")) == NULL) return 0;
//     if ((glScalefProc = GetGLProcAddress("glScalef")) == NULL) return 0;
//     if ((glTranslatedProc = GetGLProcAddress("glTranslated")) == NULL) return 0;
//     if ((glTranslatefProc = GetGLProcAddress("glTranslatef")) == NULL) return 0;
//     return 1;
// }
// #endif // GL_VERSION_1_0
// #ifndef GL_VERSION_1_1
// #define GL_VERSION_1_1 1
// typedef float GLclampf;
// typedef double GLclampd;
// 
// void (APIENTRYP glDrawArraysProc)(GLenum mode, GLint first, GLsizei count);
// void (APIENTRYP glDrawElementsProc)(GLenum mode, GLsizei count, GLenum type_, GLvoid* indices);
// void (APIENTRYP glGetPointervProc)(GLenum pname, GLvoid** params);
// void (APIENTRYP glPolygonOffsetProc)(GLfloat factor, GLfloat units);
// void (APIENTRYP glCopyTexImage1DProc)(GLenum target, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLint border);
// void (APIENTRYP glCopyTexImage2DProc)(GLenum target, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLsizei height, GLint border);
// void (APIENTRYP glCopyTexSubImage1DProc)(GLenum target, GLint level, GLint xoffset, GLint x, GLint y, GLsizei width);
// void (APIENTRYP glCopyTexSubImage2DProc)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height);
// void (APIENTRYP glTexSubImage1DProc)(GLenum target, GLint level, GLint xoffset, GLsizei width, GLenum format, GLenum type_, GLvoid* pixels);
// void (APIENTRYP glTexSubImage2DProc)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type_, GLvoid* pixels);
// void (APIENTRYP glBindTextureProc)(GLenum target, GLuint texture);
// void (APIENTRYP glDeleteTexturesProc)(GLsizei n, GLuint* textures);
// void (APIENTRYP glGenTexturesProc)(GLsizei n, GLuint* textures);
// GLboolean (APIENTRYP glIsTextureProc)(GLuint texture);
// void (APIENTRYP glArrayElementProc)(GLint i);
// void (APIENTRYP glColorPointerProc)(GLint size, GLenum type_, GLsizei stride, GLvoid* pointer);
// void (APIENTRYP glDisableClientStateProc)(GLenum array);
// void (APIENTRYP glEdgeFlagPointerProc)(GLsizei stride, GLvoid* pointer);
// void (APIENTRYP glEnableClientStateProc)(GLenum array);
// void (APIENTRYP glIndexPointerProc)(GLenum type_, GLsizei stride, GLvoid* pointer);
// void (APIENTRYP glInterleavedArraysProc)(GLenum format, GLsizei stride, GLvoid* pointer);
// void (APIENTRYP glNormalPointerProc)(GLenum type_, GLsizei stride, GLvoid* pointer);
// void (APIENTRYP glTexCoordPointerProc)(GLint size, GLenum type_, GLsizei stride, GLvoid* pointer);
// void (APIENTRYP glVertexPointerProc)(GLint size, GLenum type_, GLsizei stride, GLvoid* pointer);
// GLboolean (APIENTRYP glAreTexturesResidentProc)(GLsizei n, GLuint* textures, GLboolean* residences);
// void (APIENTRYP glPrioritizeTexturesProc)(GLsizei n, GLuint* textures, GLfloat* priorities);
// void (APIENTRYP glIndexubProc)(GLubyte c);
// void (APIENTRYP glIndexubvProc)(GLubyte* c);
// void (APIENTRYP glPopClientAttribProc)();
// void (APIENTRYP glPushClientAttribProc)(GLbitfield mask);
// 
// inline void glDrawArrays(GLenum mode, GLint first, GLsizei count){  glDrawArraysProc(mode, first, count); }
// inline void glDrawElements(GLenum mode, GLsizei count, GLenum type_, GLvoid* indices){  glDrawElementsProc(mode, count, type_, indices); }
// inline void glGetPointerv(GLenum pname, GLvoid** params){  glGetPointervProc(pname, params); }
// inline void glPolygonOffset(GLfloat factor, GLfloat units){  glPolygonOffsetProc(factor, units); }
// inline void glCopyTexImage1D(GLenum target, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLint border){  glCopyTexImage1DProc(target, level, internalformat, x, y, width, border); }
// inline void glCopyTexImage2D(GLenum target, GLint level, GLenum internalformat, GLint x, GLint y, GLsizei width, GLsizei height, GLint border){  glCopyTexImage2DProc(target, level, internalformat, x, y, width, height, border); }
// inline void glCopyTexSubImage1D(GLenum target, GLint level, GLint xoffset, GLint x, GLint y, GLsizei width){  glCopyTexSubImage1DProc(target, level, xoffset, x, y, width); }
// inline void glCopyTexSubImage2D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint x, GLint y, GLsizei width, GLsizei height){  glCopyTexSubImage2DProc(target, level, xoffset, yoffset, x, y, width, height); }
// inline void glTexSubImage1D(GLenum target, GLint level, GLint xoffset, GLsizei width, GLenum format, GLenum type_, GLvoid* pixels){  glTexSubImage1DProc(target, level, xoffset, width, format, type_, pixels); }
// inline void glTexSubImage2D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLenum type_, GLvoid* pixels){  glTexSubImage2DProc(target, level, xoffset, yoffset, width, height, format, type_, pixels); }
// inline void glBindTexture(GLenum target, GLuint texture){  glBindTextureProc(target, texture); }
// inline void glDeleteTextures(GLsizei n, GLuint* textures){  glDeleteTexturesProc(n, textures); }
// inline void glGenTextures(GLsizei n, GLuint* textures){  glGenTexturesProc(n, textures); }
// inline GLboolean glIsTexture(GLuint texture){ return glIsTextureProc(texture); }
// inline void glArrayElement(GLint i){  glArrayElementProc(i); }
// inline void glColorPointer(GLint size, GLenum type_, GLsizei stride, GLvoid* pointer){  glColorPointerProc(size, type_, stride, pointer); }
// inline void glDisableClientState(GLenum array){  glDisableClientStateProc(array); }
// inline void glEdgeFlagPointer(GLsizei stride, GLvoid* pointer){  glEdgeFlagPointerProc(stride, pointer); }
// inline void glEnableClientState(GLenum array){  glEnableClientStateProc(array); }
// inline void glIndexPointer(GLenum type_, GLsizei stride, GLvoid* pointer){  glIndexPointerProc(type_, stride, pointer); }
// inline void glInterleavedArrays(GLenum format, GLsizei stride, GLvoid* pointer){  glInterleavedArraysProc(format, stride, pointer); }
// inline void glNormalPointer(GLenum type_, GLsizei stride, GLvoid* pointer){  glNormalPointerProc(type_, stride, pointer); }
// inline void glTexCoordPointer(GLint size, GLenum type_, GLsizei stride, GLvoid* pointer){  glTexCoordPointerProc(size, type_, stride, pointer); }
// inline void glVertexPointer(GLint size, GLenum type_, GLsizei stride, GLvoid* pointer){  glVertexPointerProc(size, type_, stride, pointer); }
// inline GLboolean glAreTexturesResident(GLsizei n, GLuint* textures, GLboolean* residences){ return glAreTexturesResidentProc(n, textures, residences); }
// inline void glPrioritizeTextures(GLsizei n, GLuint* textures, GLfloat* priorities){  glPrioritizeTexturesProc(n, textures, priorities); }
// inline void glIndexub(GLubyte c){  glIndexubProc(c); }
// inline void glIndexubv(GLubyte* c){  glIndexubvProc(c); }
// inline void glPopClientAttrib(){  glPopClientAttribProc(); }
// inline void glPushClientAttrib(GLbitfield mask){  glPushClientAttribProc(mask); }
// 
// int initGL_VERSION_1_1() { 
//     if ((glDrawArraysProc = GetGLProcAddress("glDrawArrays")) == NULL) return 0;
//     if ((glDrawElementsProc = GetGLProcAddress("glDrawElements")) == NULL) return 0;
//     if ((glGetPointervProc = GetGLProcAddress("glGetPointerv")) == NULL) return 0;
//     if ((glPolygonOffsetProc = GetGLProcAddress("glPolygonOffset")) == NULL) return 0;
//     if ((glCopyTexImage1DProc = GetGLProcAddress("glCopyTexImage1D")) == NULL) return 0;
//     if ((glCopyTexImage2DProc = GetGLProcAddress("glCopyTexImage2D")) == NULL) return 0;
//     if ((glCopyTexSubImage1DProc = GetGLProcAddress("glCopyTexSubImage1D")) == NULL) return 0;
//     if ((glCopyTexSubImage2DProc = GetGLProcAddress("glCopyTexSubImage2D")) == NULL) return 0;
//     if ((glTexSubImage1DProc = GetGLProcAddress("glTexSubImage1D")) == NULL) return 0;
//     if ((glTexSubImage2DProc = GetGLProcAddress("glTexSubImage2D")) == NULL) return 0;
//     if ((glBindTextureProc = GetGLProcAddress("glBindTexture")) == NULL) return 0;
//     if ((glDeleteTexturesProc = GetGLProcAddress("glDeleteTextures")) == NULL) return 0;
//     if ((glGenTexturesProc = GetGLProcAddress("glGenTextures")) == NULL) return 0;
//     if ((glIsTextureProc = GetGLProcAddress("glIsTexture")) == NULL) return 0;
//     if ((glArrayElementProc = GetGLProcAddress("glArrayElement")) == NULL) return 0;
//     if ((glColorPointerProc = GetGLProcAddress("glColorPointer")) == NULL) return 0;
//     if ((glDisableClientStateProc = GetGLProcAddress("glDisableClientState")) == NULL) return 0;
//     if ((glEdgeFlagPointerProc = GetGLProcAddress("glEdgeFlagPointer")) == NULL) return 0;
//     if ((glEnableClientStateProc = GetGLProcAddress("glEnableClientState")) == NULL) return 0;
//     if ((glIndexPointerProc = GetGLProcAddress("glIndexPointer")) == NULL) return 0;
//     if ((glInterleavedArraysProc = GetGLProcAddress("glInterleavedArrays")) == NULL) return 0;
//     if ((glNormalPointerProc = GetGLProcAddress("glNormalPointer")) == NULL) return 0;
//     if ((glTexCoordPointerProc = GetGLProcAddress("glTexCoordPointer")) == NULL) return 0;
//     if ((glVertexPointerProc = GetGLProcAddress("glVertexPointer")) == NULL) return 0;
//     if ((glAreTexturesResidentProc = GetGLProcAddress("glAreTexturesResident")) == NULL) return 0;
//     if ((glPrioritizeTexturesProc = GetGLProcAddress("glPrioritizeTextures")) == NULL) return 0;
//     if ((glIndexubProc = GetGLProcAddress("glIndexub")) == NULL) return 0;
//     if ((glIndexubvProc = GetGLProcAddress("glIndexubv")) == NULL) return 0;
//     if ((glPopClientAttribProc = GetGLProcAddress("glPopClientAttrib")) == NULL) return 0;
//     if ((glPushClientAttribProc = GetGLProcAddress("glPushClientAttrib")) == NULL) return 0;
//     return 1;
// }
// #endif // GL_VERSION_1_1
// #ifndef GL_VERSION_1_2
// #define GL_VERSION_1_2 1
// 
// void (APIENTRYP glBlendColorProc)(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha);
// void (APIENTRYP glBlendEquationProc)(GLenum mode);
// void (APIENTRYP glDrawRangeElementsProc)(GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type_, GLvoid* indices);
// void (APIENTRYP glTexImage3DProc)(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type_, GLvoid* pixels);
// void (APIENTRYP glTexSubImage3DProc)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type_, GLvoid* pixels);
// void (APIENTRYP glCopyTexSubImage3DProc)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLint x, GLint y, GLsizei width, GLsizei height);
// 
// inline void glBlendColor(GLfloat red, GLfloat green, GLfloat blue, GLfloat alpha){  glBlendColorProc(red, green, blue, alpha); }
// inline void glBlendEquation(GLenum mode){  glBlendEquationProc(mode); }
// inline void glDrawRangeElements(GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type_, GLvoid* indices){  glDrawRangeElementsProc(mode, start, end, count, type_, indices); }
// inline void glTexImage3D(GLenum target, GLint level, GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type_, GLvoid* pixels){  glTexImage3DProc(target, level, internalformat, width, height, depth, border, format, type_, pixels); }
// inline void glTexSubImage3D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLenum type_, GLvoid* pixels){  glTexSubImage3DProc(target, level, xoffset, yoffset, zoffset, width, height, depth, format, type_, pixels); }
// inline void glCopyTexSubImage3D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLint x, GLint y, GLsizei width, GLsizei height){  glCopyTexSubImage3DProc(target, level, xoffset, yoffset, zoffset, x, y, width, height); }
// 
// int initGL_VERSION_1_2() { 
//     if ((glBlendColorProc = GetGLProcAddress("glBlendColor")) == NULL) return 0;
//     if ((glBlendEquationProc = GetGLProcAddress("glBlendEquation")) == NULL) return 0;
//     if ((glDrawRangeElementsProc = GetGLProcAddress("glDrawRangeElements")) == NULL) return 0;
//     if ((glTexImage3DProc = GetGLProcAddress("glTexImage3D")) == NULL) return 0;
//     if ((glTexSubImage3DProc = GetGLProcAddress("glTexSubImage3D")) == NULL) return 0;
//     if ((glCopyTexSubImage3DProc = GetGLProcAddress("glCopyTexSubImage3D")) == NULL) return 0;
//     return 1;
// }
// #endif // GL_VERSION_1_2
// #ifndef GL_VERSION_1_3
// #define GL_VERSION_1_3 1
// 
// void (APIENTRYP glActiveTextureProc)(GLenum texture);
// void (APIENTRYP glSampleCoverageProc)(GLfloat value, GLboolean invert);
// void (APIENTRYP glCompressedTexImage3DProc)(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLsizei imageSize, GLvoid* data);
// void (APIENTRYP glCompressedTexImage2DProc)(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, GLvoid* data);
// void (APIENTRYP glCompressedTexImage1DProc)(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLint border, GLsizei imageSize, GLvoid* data);
// void (APIENTRYP glCompressedTexSubImage3DProc)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLsizei imageSize, GLvoid* data);
// void (APIENTRYP glCompressedTexSubImage2DProc)(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLsizei imageSize, GLvoid* data);
// void (APIENTRYP glCompressedTexSubImage1DProc)(GLenum target, GLint level, GLint xoffset, GLsizei width, GLenum format, GLsizei imageSize, GLvoid* data);
// void (APIENTRYP glGetCompressedTexImageProc)(GLenum target, GLint level, GLvoid* img);
// void (APIENTRYP glClientActiveTextureProc)(GLenum texture);
// void (APIENTRYP glMultiTexCoord1dProc)(GLenum target, GLdouble s);
// void (APIENTRYP glMultiTexCoord1dvProc)(GLenum target, GLdouble* v);
// void (APIENTRYP glMultiTexCoord1fProc)(GLenum target, GLfloat s);
// void (APIENTRYP glMultiTexCoord1fvProc)(GLenum target, GLfloat* v);
// void (APIENTRYP glMultiTexCoord1iProc)(GLenum target, GLint s);
// void (APIENTRYP glMultiTexCoord1ivProc)(GLenum target, GLint* v);
// void (APIENTRYP glMultiTexCoord1sProc)(GLenum target, GLshort s);
// void (APIENTRYP glMultiTexCoord1svProc)(GLenum target, GLshort* v);
// void (APIENTRYP glMultiTexCoord2dProc)(GLenum target, GLdouble s, GLdouble t);
// void (APIENTRYP glMultiTexCoord2dvProc)(GLenum target, GLdouble* v);
// void (APIENTRYP glMultiTexCoord2fProc)(GLenum target, GLfloat s, GLfloat t);
// void (APIENTRYP glMultiTexCoord2fvProc)(GLenum target, GLfloat* v);
// void (APIENTRYP glMultiTexCoord2iProc)(GLenum target, GLint s, GLint t);
// void (APIENTRYP glMultiTexCoord2ivProc)(GLenum target, GLint* v);
// void (APIENTRYP glMultiTexCoord2sProc)(GLenum target, GLshort s, GLshort t);
// void (APIENTRYP glMultiTexCoord2svProc)(GLenum target, GLshort* v);
// void (APIENTRYP glMultiTexCoord3dProc)(GLenum target, GLdouble s, GLdouble t, GLdouble r);
// void (APIENTRYP glMultiTexCoord3dvProc)(GLenum target, GLdouble* v);
// void (APIENTRYP glMultiTexCoord3fProc)(GLenum target, GLfloat s, GLfloat t, GLfloat r);
// void (APIENTRYP glMultiTexCoord3fvProc)(GLenum target, GLfloat* v);
// void (APIENTRYP glMultiTexCoord3iProc)(GLenum target, GLint s, GLint t, GLint r);
// void (APIENTRYP glMultiTexCoord3ivProc)(GLenum target, GLint* v);
// void (APIENTRYP glMultiTexCoord3sProc)(GLenum target, GLshort s, GLshort t, GLshort r);
// void (APIENTRYP glMultiTexCoord3svProc)(GLenum target, GLshort* v);
// void (APIENTRYP glMultiTexCoord4dProc)(GLenum target, GLdouble s, GLdouble t, GLdouble r, GLdouble q);
// void (APIENTRYP glMultiTexCoord4dvProc)(GLenum target, GLdouble* v);
// void (APIENTRYP glMultiTexCoord4fProc)(GLenum target, GLfloat s, GLfloat t, GLfloat r, GLfloat q);
// void (APIENTRYP glMultiTexCoord4fvProc)(GLenum target, GLfloat* v);
// void (APIENTRYP glMultiTexCoord4iProc)(GLenum target, GLint s, GLint t, GLint r, GLint q);
// void (APIENTRYP glMultiTexCoord4ivProc)(GLenum target, GLint* v);
// void (APIENTRYP glMultiTexCoord4sProc)(GLenum target, GLshort s, GLshort t, GLshort r, GLshort q);
// void (APIENTRYP glMultiTexCoord4svProc)(GLenum target, GLshort* v);
// void (APIENTRYP glLoadTransposeMatrixfProc)(GLfloat* m);
// void (APIENTRYP glLoadTransposeMatrixdProc)(GLdouble* m);
// void (APIENTRYP glMultTransposeMatrixfProc)(GLfloat* m);
// void (APIENTRYP glMultTransposeMatrixdProc)(GLdouble* m);
// 
// inline void glActiveTexture(GLenum texture){  glActiveTextureProc(texture); }
// inline void glSampleCoverage(GLfloat value, GLboolean invert){  glSampleCoverageProc(value, invert); }
// inline void glCompressedTexImage3D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLsizei imageSize, GLvoid* data){  glCompressedTexImage3DProc(target, level, internalformat, width, height, depth, border, imageSize, data); }
// inline void glCompressedTexImage2D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLsizei height, GLint border, GLsizei imageSize, GLvoid* data){  glCompressedTexImage2DProc(target, level, internalformat, width, height, border, imageSize, data); }
// inline void glCompressedTexImage1D(GLenum target, GLint level, GLenum internalformat, GLsizei width, GLint border, GLsizei imageSize, GLvoid* data){  glCompressedTexImage1DProc(target, level, internalformat, width, border, imageSize, data); }
// inline void glCompressedTexSubImage3D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLint zoffset, GLsizei width, GLsizei height, GLsizei depth, GLenum format, GLsizei imageSize, GLvoid* data){  glCompressedTexSubImage3DProc(target, level, xoffset, yoffset, zoffset, width, height, depth, format, imageSize, data); }
// inline void glCompressedTexSubImage2D(GLenum target, GLint level, GLint xoffset, GLint yoffset, GLsizei width, GLsizei height, GLenum format, GLsizei imageSize, GLvoid* data){  glCompressedTexSubImage2DProc(target, level, xoffset, yoffset, width, height, format, imageSize, data); }
// inline void glCompressedTexSubImage1D(GLenum target, GLint level, GLint xoffset, GLsizei width, GLenum format, GLsizei imageSize, GLvoid* data){  glCompressedTexSubImage1DProc(target, level, xoffset, width, format, imageSize, data); }
// inline void glGetCompressedTexImage(GLenum target, GLint level, GLvoid* img){  glGetCompressedTexImageProc(target, level, img); }
// inline void glClientActiveTexture(GLenum texture){  glClientActiveTextureProc(texture); }
// inline void glMultiTexCoord1d(GLenum target, GLdouble s){  glMultiTexCoord1dProc(target, s); }
// inline void glMultiTexCoord1dv(GLenum target, GLdouble* v){  glMultiTexCoord1dvProc(target, v); }
// inline void glMultiTexCoord1f(GLenum target, GLfloat s){  glMultiTexCoord1fProc(target, s); }
// inline void glMultiTexCoord1fv(GLenum target, GLfloat* v){  glMultiTexCoord1fvProc(target, v); }
// inline void glMultiTexCoord1i(GLenum target, GLint s){  glMultiTexCoord1iProc(target, s); }
// inline void glMultiTexCoord1iv(GLenum target, GLint* v){  glMultiTexCoord1ivProc(target, v); }
// inline void glMultiTexCoord1s(GLenum target, GLshort s){  glMultiTexCoord1sProc(target, s); }
// inline void glMultiTexCoord1sv(GLenum target, GLshort* v){  glMultiTexCoord1svProc(target, v); }
// inline void glMultiTexCoord2d(GLenum target, GLdouble s, GLdouble t){  glMultiTexCoord2dProc(target, s, t); }
// inline void glMultiTexCoord2dv(GLenum target, GLdouble* v){  glMultiTexCoord2dvProc(target, v); }
// inline void glMultiTexCoord2f(GLenum target, GLfloat s, GLfloat t){  glMultiTexCoord2fProc(target, s, t); }
// inline void glMultiTexCoord2fv(GLenum target, GLfloat* v){  glMultiTexCoord2fvProc(target, v); }
// inline void glMultiTexCoord2i(GLenum target, GLint s, GLint t){  glMultiTexCoord2iProc(target, s, t); }
// inline void glMultiTexCoord2iv(GLenum target, GLint* v){  glMultiTexCoord2ivProc(target, v); }
// inline void glMultiTexCoord2s(GLenum target, GLshort s, GLshort t){  glMultiTexCoord2sProc(target, s, t); }
// inline void glMultiTexCoord2sv(GLenum target, GLshort* v){  glMultiTexCoord2svProc(target, v); }
// inline void glMultiTexCoord3d(GLenum target, GLdouble s, GLdouble t, GLdouble r){  glMultiTexCoord3dProc(target, s, t, r); }
// inline void glMultiTexCoord3dv(GLenum target, GLdouble* v){  glMultiTexCoord3dvProc(target, v); }
// inline void glMultiTexCoord3f(GLenum target, GLfloat s, GLfloat t, GLfloat r){  glMultiTexCoord3fProc(target, s, t, r); }
// inline void glMultiTexCoord3fv(GLenum target, GLfloat* v){  glMultiTexCoord3fvProc(target, v); }
// inline void glMultiTexCoord3i(GLenum target, GLint s, GLint t, GLint r){  glMultiTexCoord3iProc(target, s, t, r); }
// inline void glMultiTexCoord3iv(GLenum target, GLint* v){  glMultiTexCoord3ivProc(target, v); }
// inline void glMultiTexCoord3s(GLenum target, GLshort s, GLshort t, GLshort r){  glMultiTexCoord3sProc(target, s, t, r); }
// inline void glMultiTexCoord3sv(GLenum target, GLshort* v){  glMultiTexCoord3svProc(target, v); }
// inline void glMultiTexCoord4d(GLenum target, GLdouble s, GLdouble t, GLdouble r, GLdouble q){  glMultiTexCoord4dProc(target, s, t, r, q); }
// inline void glMultiTexCoord4dv(GLenum target, GLdouble* v){  glMultiTexCoord4dvProc(target, v); }
// inline void glMultiTexCoord4f(GLenum target, GLfloat s, GLfloat t, GLfloat r, GLfloat q){  glMultiTexCoord4fProc(target, s, t, r, q); }
// inline void glMultiTexCoord4fv(GLenum target, GLfloat* v){  glMultiTexCoord4fvProc(target, v); }
// inline void glMultiTexCoord4i(GLenum target, GLint s, GLint t, GLint r, GLint q){  glMultiTexCoord4iProc(target, s, t, r, q); }
// inline void glMultiTexCoord4iv(GLenum target, GLint* v){  glMultiTexCoord4ivProc(target, v); }
// inline void glMultiTexCoord4s(GLenum target, GLshort s, GLshort t, GLshort r, GLshort q){  glMultiTexCoord4sProc(target, s, t, r, q); }
// inline void glMultiTexCoord4sv(GLenum target, GLshort* v){  glMultiTexCoord4svProc(target, v); }
// inline void glLoadTransposeMatrixf(GLfloat* m){  glLoadTransposeMatrixfProc(m); }
// inline void glLoadTransposeMatrixd(GLdouble* m){  glLoadTransposeMatrixdProc(m); }
// inline void glMultTransposeMatrixf(GLfloat* m){  glMultTransposeMatrixfProc(m); }
// inline void glMultTransposeMatrixd(GLdouble* m){  glMultTransposeMatrixdProc(m); }
// 
// int initGL_VERSION_1_3() { 
//     if ((glActiveTextureProc = GetGLProcAddress("glActiveTexture")) == NULL) return 0;
//     if ((glSampleCoverageProc = GetGLProcAddress("glSampleCoverage")) == NULL) return 0;
//     if ((glCompressedTexImage3DProc = GetGLProcAddress("glCompressedTexImage3D")) == NULL) return 0;
//     if ((glCompressedTexImage2DProc = GetGLProcAddress("glCompressedTexImage2D")) == NULL) return 0;
//     if ((glCompressedTexImage1DProc = GetGLProcAddress("glCompressedTexImage1D")) == NULL) return 0;
//     if ((glCompressedTexSubImage3DProc = GetGLProcAddress("glCompressedTexSubImage3D")) == NULL) return 0;
//     if ((glCompressedTexSubImage2DProc = GetGLProcAddress("glCompressedTexSubImage2D")) == NULL) return 0;
//     if ((glCompressedTexSubImage1DProc = GetGLProcAddress("glCompressedTexSubImage1D")) == NULL) return 0;
//     if ((glGetCompressedTexImageProc = GetGLProcAddress("glGetCompressedTexImage")) == NULL) return 0;
//     if ((glClientActiveTextureProc = GetGLProcAddress("glClientActiveTexture")) == NULL) return 0;
//     if ((glMultiTexCoord1dProc = GetGLProcAddress("glMultiTexCoord1d")) == NULL) return 0;
//     if ((glMultiTexCoord1dvProc = GetGLProcAddress("glMultiTexCoord1dv")) == NULL) return 0;
//     if ((glMultiTexCoord1fProc = GetGLProcAddress("glMultiTexCoord1f")) == NULL) return 0;
//     if ((glMultiTexCoord1fvProc = GetGLProcAddress("glMultiTexCoord1fv")) == NULL) return 0;
//     if ((glMultiTexCoord1iProc = GetGLProcAddress("glMultiTexCoord1i")) == NULL) return 0;
//     if ((glMultiTexCoord1ivProc = GetGLProcAddress("glMultiTexCoord1iv")) == NULL) return 0;
//     if ((glMultiTexCoord1sProc = GetGLProcAddress("glMultiTexCoord1s")) == NULL) return 0;
//     if ((glMultiTexCoord1svProc = GetGLProcAddress("glMultiTexCoord1sv")) == NULL) return 0;
//     if ((glMultiTexCoord2dProc = GetGLProcAddress("glMultiTexCoord2d")) == NULL) return 0;
//     if ((glMultiTexCoord2dvProc = GetGLProcAddress("glMultiTexCoord2dv")) == NULL) return 0;
//     if ((glMultiTexCoord2fProc = GetGLProcAddress("glMultiTexCoord2f")) == NULL) return 0;
//     if ((glMultiTexCoord2fvProc = GetGLProcAddress("glMultiTexCoord2fv")) == NULL) return 0;
//     if ((glMultiTexCoord2iProc = GetGLProcAddress("glMultiTexCoord2i")) == NULL) return 0;
//     if ((glMultiTexCoord2ivProc = GetGLProcAddress("glMultiTexCoord2iv")) == NULL) return 0;
//     if ((glMultiTexCoord2sProc = GetGLProcAddress("glMultiTexCoord2s")) == NULL) return 0;
//     if ((glMultiTexCoord2svProc = GetGLProcAddress("glMultiTexCoord2sv")) == NULL) return 0;
//     if ((glMultiTexCoord3dProc = GetGLProcAddress("glMultiTexCoord3d")) == NULL) return 0;
//     if ((glMultiTexCoord3dvProc = GetGLProcAddress("glMultiTexCoord3dv")) == NULL) return 0;
//     if ((glMultiTexCoord3fProc = GetGLProcAddress("glMultiTexCoord3f")) == NULL) return 0;
//     if ((glMultiTexCoord3fvProc = GetGLProcAddress("glMultiTexCoord3fv")) == NULL) return 0;
//     if ((glMultiTexCoord3iProc = GetGLProcAddress("glMultiTexCoord3i")) == NULL) return 0;
//     if ((glMultiTexCoord3ivProc = GetGLProcAddress("glMultiTexCoord3iv")) == NULL) return 0;
//     if ((glMultiTexCoord3sProc = GetGLProcAddress("glMultiTexCoord3s")) == NULL) return 0;
//     if ((glMultiTexCoord3svProc = GetGLProcAddress("glMultiTexCoord3sv")) == NULL) return 0;
//     if ((glMultiTexCoord4dProc = GetGLProcAddress("glMultiTexCoord4d")) == NULL) return 0;
//     if ((glMultiTexCoord4dvProc = GetGLProcAddress("glMultiTexCoord4dv")) == NULL) return 0;
//     if ((glMultiTexCoord4fProc = GetGLProcAddress("glMultiTexCoord4f")) == NULL) return 0;
//     if ((glMultiTexCoord4fvProc = GetGLProcAddress("glMultiTexCoord4fv")) == NULL) return 0;
//     if ((glMultiTexCoord4iProc = GetGLProcAddress("glMultiTexCoord4i")) == NULL) return 0;
//     if ((glMultiTexCoord4ivProc = GetGLProcAddress("glMultiTexCoord4iv")) == NULL) return 0;
//     if ((glMultiTexCoord4sProc = GetGLProcAddress("glMultiTexCoord4s")) == NULL) return 0;
//     if ((glMultiTexCoord4svProc = GetGLProcAddress("glMultiTexCoord4sv")) == NULL) return 0;
//     if ((glLoadTransposeMatrixfProc = GetGLProcAddress("glLoadTransposeMatrixf")) == NULL) return 0;
//     if ((glLoadTransposeMatrixdProc = GetGLProcAddress("glLoadTransposeMatrixd")) == NULL) return 0;
//     if ((glMultTransposeMatrixfProc = GetGLProcAddress("glMultTransposeMatrixf")) == NULL) return 0;
//     if ((glMultTransposeMatrixdProc = GetGLProcAddress("glMultTransposeMatrixd")) == NULL) return 0;
//     return 1;
// }
// #endif // GL_VERSION_1_3
// #ifndef GL_VERSION_1_4
// #define GL_VERSION_1_4 1
// 
// void (APIENTRYP glBlendFuncSeparateProc)(GLenum sfactorRGB, GLenum dfactorRGB, GLenum sfactorAlpha, GLenum dfactorAlpha);
// void (APIENTRYP glMultiDrawArraysProc)(GLenum mode, GLint* first, GLsizei* count, GLsizei drawcount);
// void (APIENTRYP glMultiDrawElementsProc)(GLenum mode, GLsizei* count, GLenum type_, GLvoid** indices, GLsizei drawcount);
// void (APIENTRYP glPointParameterfProc)(GLenum pname, GLfloat param);
// void (APIENTRYP glPointParameterfvProc)(GLenum pname, GLfloat* params);
// void (APIENTRYP glPointParameteriProc)(GLenum pname, GLint param);
// void (APIENTRYP glPointParameterivProc)(GLenum pname, GLint* params);
// void (APIENTRYP glFogCoordfProc)(GLfloat coord);
// void (APIENTRYP glFogCoordfvProc)(GLfloat* coord);
// void (APIENTRYP glFogCoorddProc)(GLdouble coord);
// void (APIENTRYP glFogCoorddvProc)(GLdouble* coord);
// void (APIENTRYP glFogCoordPointerProc)(GLenum type_, GLsizei stride, GLvoid* pointer);
// void (APIENTRYP glSecondaryColor3bProc)(GLbyte red, GLbyte green, GLbyte blue);
// void (APIENTRYP glSecondaryColor3bvProc)(GLbyte* v);
// void (APIENTRYP glSecondaryColor3dProc)(GLdouble red, GLdouble green, GLdouble blue);
// void (APIENTRYP glSecondaryColor3dvProc)(GLdouble* v);
// void (APIENTRYP glSecondaryColor3fProc)(GLfloat red, GLfloat green, GLfloat blue);
// void (APIENTRYP glSecondaryColor3fvProc)(GLfloat* v);
// void (APIENTRYP glSecondaryColor3iProc)(GLint red, GLint green, GLint blue);
// void (APIENTRYP glSecondaryColor3ivProc)(GLint* v);
// void (APIENTRYP glSecondaryColor3sProc)(GLshort red, GLshort green, GLshort blue);
// void (APIENTRYP glSecondaryColor3svProc)(GLshort* v);
// void (APIENTRYP glSecondaryColor3ubProc)(GLubyte red, GLubyte green, GLubyte blue);
// void (APIENTRYP glSecondaryColor3ubvProc)(GLubyte* v);
// void (APIENTRYP glSecondaryColor3uiProc)(GLuint red, GLuint green, GLuint blue);
// void (APIENTRYP glSecondaryColor3uivProc)(GLuint* v);
// void (APIENTRYP glSecondaryColor3usProc)(GLushort red, GLushort green, GLushort blue);
// void (APIENTRYP glSecondaryColor3usvProc)(GLushort* v);
// void (APIENTRYP glSecondaryColorPointerProc)(GLint size, GLenum type_, GLsizei stride, GLvoid* pointer);
// void (APIENTRYP glWindowPos2dProc)(GLdouble x, GLdouble y);
// void (APIENTRYP glWindowPos2dvProc)(GLdouble* v);
// void (APIENTRYP glWindowPos2fProc)(GLfloat x, GLfloat y);
// void (APIENTRYP glWindowPos2fvProc)(GLfloat* v);
// void (APIENTRYP glWindowPos2iProc)(GLint x, GLint y);
// void (APIENTRYP glWindowPos2ivProc)(GLint* v);
// void (APIENTRYP glWindowPos2sProc)(GLshort x, GLshort y);
// void (APIENTRYP glWindowPos2svProc)(GLshort* v);
// void (APIENTRYP glWindowPos3dProc)(GLdouble x, GLdouble y, GLdouble z);
// void (APIENTRYP glWindowPos3dvProc)(GLdouble* v);
// void (APIENTRYP glWindowPos3fProc)(GLfloat x, GLfloat y, GLfloat z);
// void (APIENTRYP glWindowPos3fvProc)(GLfloat* v);
// void (APIENTRYP glWindowPos3iProc)(GLint x, GLint y, GLint z);
// void (APIENTRYP glWindowPos3ivProc)(GLint* v);
// void (APIENTRYP glWindowPos3sProc)(GLshort x, GLshort y, GLshort z);
// void (APIENTRYP glWindowPos3svProc)(GLshort* v);
// 
// inline void glBlendFuncSeparate(GLenum sfactorRGB, GLenum dfactorRGB, GLenum sfactorAlpha, GLenum dfactorAlpha){  glBlendFuncSeparateProc(sfactorRGB, dfactorRGB, sfactorAlpha, dfactorAlpha); }
// inline void glMultiDrawArrays(GLenum mode, GLint* first, GLsizei* count, GLsizei drawcount){  glMultiDrawArraysProc(mode, first, count, drawcount); }
// inline void glMultiDrawElements(GLenum mode, GLsizei* count, GLenum type_, GLvoid** indices, GLsizei drawcount){  glMultiDrawElementsProc(mode, count, type_, indices, drawcount); }
// inline void glPointParameterf(GLenum pname, GLfloat param){  glPointParameterfProc(pname, param); }
// inline void glPointParameterfv(GLenum pname, GLfloat* params){  glPointParameterfvProc(pname, params); }
// inline void glPointParameteri(GLenum pname, GLint param){  glPointParameteriProc(pname, param); }
// inline void glPointParameteriv(GLenum pname, GLint* params){  glPointParameterivProc(pname, params); }
// inline void glFogCoordf(GLfloat coord){  glFogCoordfProc(coord); }
// inline void glFogCoordfv(GLfloat* coord){  glFogCoordfvProc(coord); }
// inline void glFogCoordd(GLdouble coord){  glFogCoorddProc(coord); }
// inline void glFogCoorddv(GLdouble* coord){  glFogCoorddvProc(coord); }
// inline void glFogCoordPointer(GLenum type_, GLsizei stride, GLvoid* pointer){  glFogCoordPointerProc(type_, stride, pointer); }
// inline void glSecondaryColor3b(GLbyte red, GLbyte green, GLbyte blue){  glSecondaryColor3bProc(red, green, blue); }
// inline void glSecondaryColor3bv(GLbyte* v){  glSecondaryColor3bvProc(v); }
// inline void glSecondaryColor3d(GLdouble red, GLdouble green, GLdouble blue){  glSecondaryColor3dProc(red, green, blue); }
// inline void glSecondaryColor3dv(GLdouble* v){  glSecondaryColor3dvProc(v); }
// inline void glSecondaryColor3f(GLfloat red, GLfloat green, GLfloat blue){  glSecondaryColor3fProc(red, green, blue); }
// inline void glSecondaryColor3fv(GLfloat* v){  glSecondaryColor3fvProc(v); }
// inline void glSecondaryColor3i(GLint red, GLint green, GLint blue){  glSecondaryColor3iProc(red, green, blue); }
// inline void glSecondaryColor3iv(GLint* v){  glSecondaryColor3ivProc(v); }
// inline void glSecondaryColor3s(GLshort red, GLshort green, GLshort blue){  glSecondaryColor3sProc(red, green, blue); }
// inline void glSecondaryColor3sv(GLshort* v){  glSecondaryColor3svProc(v); }
// inline void glSecondaryColor3ub(GLubyte red, GLubyte green, GLubyte blue){  glSecondaryColor3ubProc(red, green, blue); }
// inline void glSecondaryColor3ubv(GLubyte* v){  glSecondaryColor3ubvProc(v); }
// inline void glSecondaryColor3ui(GLuint red, GLuint green, GLuint blue){  glSecondaryColor3uiProc(red, green, blue); }
// inline void glSecondaryColor3uiv(GLuint* v){  glSecondaryColor3uivProc(v); }
// inline void glSecondaryColor3us(GLushort red, GLushort green, GLushort blue){  glSecondaryColor3usProc(red, green, blue); }
// inline void glSecondaryColor3usv(GLushort* v){  glSecondaryColor3usvProc(v); }
// inline void glSecondaryColorPointer(GLint size, GLenum type_, GLsizei stride, GLvoid* pointer){  glSecondaryColorPointerProc(size, type_, stride, pointer); }
// inline void glWindowPos2d(GLdouble x, GLdouble y){  glWindowPos2dProc(x, y); }
// inline void glWindowPos2dv(GLdouble* v){  glWindowPos2dvProc(v); }
// inline void glWindowPos2f(GLfloat x, GLfloat y){  glWindowPos2fProc(x, y); }
// inline void glWindowPos2fv(GLfloat* v){  glWindowPos2fvProc(v); }
// inline void glWindowPos2i(GLint x, GLint y){  glWindowPos2iProc(x, y); }
// inline void glWindowPos2iv(GLint* v){  glWindowPos2ivProc(v); }
// inline void glWindowPos2s(GLshort x, GLshort y){  glWindowPos2sProc(x, y); }
// inline void glWindowPos2sv(GLshort* v){  glWindowPos2svProc(v); }
// inline void glWindowPos3d(GLdouble x, GLdouble y, GLdouble z){  glWindowPos3dProc(x, y, z); }
// inline void glWindowPos3dv(GLdouble* v){  glWindowPos3dvProc(v); }
// inline void glWindowPos3f(GLfloat x, GLfloat y, GLfloat z){  glWindowPos3fProc(x, y, z); }
// inline void glWindowPos3fv(GLfloat* v){  glWindowPos3fvProc(v); }
// inline void glWindowPos3i(GLint x, GLint y, GLint z){  glWindowPos3iProc(x, y, z); }
// inline void glWindowPos3iv(GLint* v){  glWindowPos3ivProc(v); }
// inline void glWindowPos3s(GLshort x, GLshort y, GLshort z){  glWindowPos3sProc(x, y, z); }
// inline void glWindowPos3sv(GLshort* v){  glWindowPos3svProc(v); }
// 
// int initGL_VERSION_1_4() { 
//     if ((glBlendFuncSeparateProc = GetGLProcAddress("glBlendFuncSeparate")) == NULL) return 0;
//     if ((glMultiDrawArraysProc = GetGLProcAddress("glMultiDrawArrays")) == NULL) return 0;
//     if ((glMultiDrawElementsProc = GetGLProcAddress("glMultiDrawElements")) == NULL) return 0;
//     if ((glPointParameterfProc = GetGLProcAddress("glPointParameterf")) == NULL) return 0;
//     if ((glPointParameterfvProc = GetGLProcAddress("glPointParameterfv")) == NULL) return 0;
//     if ((glPointParameteriProc = GetGLProcAddress("glPointParameteri")) == NULL) return 0;
//     if ((glPointParameterivProc = GetGLProcAddress("glPointParameteriv")) == NULL) return 0;
//     if ((glFogCoordfProc = GetGLProcAddress("glFogCoordf")) == NULL) return 0;
//     if ((glFogCoordfvProc = GetGLProcAddress("glFogCoordfv")) == NULL) return 0;
//     if ((glFogCoorddProc = GetGLProcAddress("glFogCoordd")) == NULL) return 0;
//     if ((glFogCoorddvProc = GetGLProcAddress("glFogCoorddv")) == NULL) return 0;
//     if ((glFogCoordPointerProc = GetGLProcAddress("glFogCoordPointer")) == NULL) return 0;
//     if ((glSecondaryColor3bProc = GetGLProcAddress("glSecondaryColor3b")) == NULL) return 0;
//     if ((glSecondaryColor3bvProc = GetGLProcAddress("glSecondaryColor3bv")) == NULL) return 0;
//     if ((glSecondaryColor3dProc = GetGLProcAddress("glSecondaryColor3d")) == NULL) return 0;
//     if ((glSecondaryColor3dvProc = GetGLProcAddress("glSecondaryColor3dv")) == NULL) return 0;
//     if ((glSecondaryColor3fProc = GetGLProcAddress("glSecondaryColor3f")) == NULL) return 0;
//     if ((glSecondaryColor3fvProc = GetGLProcAddress("glSecondaryColor3fv")) == NULL) return 0;
//     if ((glSecondaryColor3iProc = GetGLProcAddress("glSecondaryColor3i")) == NULL) return 0;
//     if ((glSecondaryColor3ivProc = GetGLProcAddress("glSecondaryColor3iv")) == NULL) return 0;
//     if ((glSecondaryColor3sProc = GetGLProcAddress("glSecondaryColor3s")) == NULL) return 0;
//     if ((glSecondaryColor3svProc = GetGLProcAddress("glSecondaryColor3sv")) == NULL) return 0;
//     if ((glSecondaryColor3ubProc = GetGLProcAddress("glSecondaryColor3ub")) == NULL) return 0;
//     if ((glSecondaryColor3ubvProc = GetGLProcAddress("glSecondaryColor3ubv")) == NULL) return 0;
//     if ((glSecondaryColor3uiProc = GetGLProcAddress("glSecondaryColor3ui")) == NULL) return 0;
//     if ((glSecondaryColor3uivProc = GetGLProcAddress("glSecondaryColor3uiv")) == NULL) return 0;
//     if ((glSecondaryColor3usProc = GetGLProcAddress("glSecondaryColor3us")) == NULL) return 0;
//     if ((glSecondaryColor3usvProc = GetGLProcAddress("glSecondaryColor3usv")) == NULL) return 0;
//     if ((glSecondaryColorPointerProc = GetGLProcAddress("glSecondaryColorPointer")) == NULL) return 0;
//     if ((glWindowPos2dProc = GetGLProcAddress("glWindowPos2d")) == NULL) return 0;
//     if ((glWindowPos2dvProc = GetGLProcAddress("glWindowPos2dv")) == NULL) return 0;
//     if ((glWindowPos2fProc = GetGLProcAddress("glWindowPos2f")) == NULL) return 0;
//     if ((glWindowPos2fvProc = GetGLProcAddress("glWindowPos2fv")) == NULL) return 0;
//     if ((glWindowPos2iProc = GetGLProcAddress("glWindowPos2i")) == NULL) return 0;
//     if ((glWindowPos2ivProc = GetGLProcAddress("glWindowPos2iv")) == NULL) return 0;
//     if ((glWindowPos2sProc = GetGLProcAddress("glWindowPos2s")) == NULL) return 0;
//     if ((glWindowPos2svProc = GetGLProcAddress("glWindowPos2sv")) == NULL) return 0;
//     if ((glWindowPos3dProc = GetGLProcAddress("glWindowPos3d")) == NULL) return 0;
//     if ((glWindowPos3dvProc = GetGLProcAddress("glWindowPos3dv")) == NULL) return 0;
//     if ((glWindowPos3fProc = GetGLProcAddress("glWindowPos3f")) == NULL) return 0;
//     if ((glWindowPos3fvProc = GetGLProcAddress("glWindowPos3fv")) == NULL) return 0;
//     if ((glWindowPos3iProc = GetGLProcAddress("glWindowPos3i")) == NULL) return 0;
//     if ((glWindowPos3ivProc = GetGLProcAddress("glWindowPos3iv")) == NULL) return 0;
//     if ((glWindowPos3sProc = GetGLProcAddress("glWindowPos3s")) == NULL) return 0;
//     if ((glWindowPos3svProc = GetGLProcAddress("glWindowPos3sv")) == NULL) return 0;
//     return 1;
// }
// #endif // GL_VERSION_1_4
// #ifndef GL_VERSION_1_5
// #define GL_VERSION_1_5 1
// #include <stddef.h>
// typedef ptrdiff_t GLsizeiptr;
// #include <stddef.h>
// typedef ptrdiff_t GLintptr;
// 
// void (APIENTRYP glGenQueriesProc)(GLsizei n, GLuint* ids);
// void (APIENTRYP glDeleteQueriesProc)(GLsizei n, GLuint* ids);
// GLboolean (APIENTRYP glIsQueryProc)(GLuint id);
// void (APIENTRYP glBeginQueryProc)(GLenum target, GLuint id);
// void (APIENTRYP glEndQueryProc)(GLenum target);
// void (APIENTRYP glGetQueryivProc)(GLenum target, GLenum pname, GLint* params);
// void (APIENTRYP glGetQueryObjectivProc)(GLuint id, GLenum pname, GLint* params);
// void (APIENTRYP glGetQueryObjectuivProc)(GLuint id, GLenum pname, GLuint* params);
// void (APIENTRYP glBindBufferProc)(GLenum target, GLuint buffer);
// void (APIENTRYP glDeleteBuffersProc)(GLsizei n, GLuint* buffers);
// void (APIENTRYP glGenBuffersProc)(GLsizei n, GLuint* buffers);
// GLboolean (APIENTRYP glIsBufferProc)(GLuint buffer);
// void (APIENTRYP glBufferDataProc)(GLenum target, GLsizeiptr size, GLvoid* data, GLenum usage);
// void (APIENTRYP glBufferSubDataProc)(GLenum target, GLintptr offset, GLsizeiptr size, GLvoid* data);
// void (APIENTRYP glGetBufferSubDataProc)(GLenum target, GLintptr offset, GLsizeiptr size, GLvoid* data);
// void* (APIENTRYP glMapBufferProc)(GLenum target, GLenum access);
// GLboolean (APIENTRYP glUnmapBufferProc)(GLenum target);
// void (APIENTRYP glGetBufferParameterivProc)(GLenum target, GLenum pname, GLint* params);
// void (APIENTRYP glGetBufferPointervProc)(GLenum target, GLenum pname, GLvoid** params);
// 
// inline void glGenQueries(GLsizei n, GLuint* ids){  glGenQueriesProc(n, ids); }
// inline void glDeleteQueries(GLsizei n, GLuint* ids){  glDeleteQueriesProc(n, ids); }
// inline GLboolean glIsQuery(GLuint id){ return glIsQueryProc(id); }
// inline void glBeginQuery(GLenum target, GLuint id){  glBeginQueryProc(target, id); }
// inline void glEndQuery(GLenum target){  glEndQueryProc(target); }
// inline void glGetQueryiv(GLenum target, GLenum pname, GLint* params){  glGetQueryivProc(target, pname, params); }
// inline void glGetQueryObjectiv(GLuint id, GLenum pname, GLint* params){  glGetQueryObjectivProc(id, pname, params); }
// inline void glGetQueryObjectuiv(GLuint id, GLenum pname, GLuint* params){  glGetQueryObjectuivProc(id, pname, params); }
// inline void glBindBuffer(GLenum target, GLuint buffer){  glBindBufferProc(target, buffer); }
// inline void glDeleteBuffers(GLsizei n, GLuint* buffers){  glDeleteBuffersProc(n, buffers); }
// inline void glGenBuffers(GLsizei n, GLuint* buffers){  glGenBuffersProc(n, buffers); }
// inline GLboolean glIsBuffer(GLuint buffer){ return glIsBufferProc(buffer); }
// inline void glBufferData(GLenum target, GLsizeiptr size, GLvoid* data, GLenum usage){  glBufferDataProc(target, size, data, usage); }
// inline void glBufferSubData(GLenum target, GLintptr offset, GLsizeiptr size, GLvoid* data){  glBufferSubDataProc(target, offset, size, data); }
// inline void glGetBufferSubData(GLenum target, GLintptr offset, GLsizeiptr size, GLvoid* data){  glGetBufferSubDataProc(target, offset, size, data); }
// inline void* glMapBuffer(GLenum target, GLenum access){ return glMapBufferProc(target, access); }
// inline GLboolean glUnmapBuffer(GLenum target){ return glUnmapBufferProc(target); }
// inline void glGetBufferParameteriv(GLenum target, GLenum pname, GLint* params){  glGetBufferParameterivProc(target, pname, params); }
// inline void glGetBufferPointerv(GLenum target, GLenum pname, GLvoid** params){  glGetBufferPointervProc(target, pname, params); }
// 
// int initGL_VERSION_1_5() { 
//     if ((glGenQueriesProc = GetGLProcAddress("glGenQueries")) == NULL) return 0;
//     if ((glDeleteQueriesProc = GetGLProcAddress("glDeleteQueries")) == NULL) return 0;
//     if ((glIsQueryProc = GetGLProcAddress("glIsQuery")) == NULL) return 0;
//     if ((glBeginQueryProc = GetGLProcAddress("glBeginQuery")) == NULL) return 0;
//     if ((glEndQueryProc = GetGLProcAddress("glEndQuery")) == NULL) return 0;
//     if ((glGetQueryivProc = GetGLProcAddress("glGetQueryiv")) == NULL) return 0;
//     if ((glGetQueryObjectivProc = GetGLProcAddress("glGetQueryObjectiv")) == NULL) return 0;
//     if ((glGetQueryObjectuivProc = GetGLProcAddress("glGetQueryObjectuiv")) == NULL) return 0;
//     if ((glBindBufferProc = GetGLProcAddress("glBindBuffer")) == NULL) return 0;
//     if ((glDeleteBuffersProc = GetGLProcAddress("glDeleteBuffers")) == NULL) return 0;
//     if ((glGenBuffersProc = GetGLProcAddress("glGenBuffers")) == NULL) return 0;
//     if ((glIsBufferProc = GetGLProcAddress("glIsBuffer")) == NULL) return 0;
//     if ((glBufferDataProc = GetGLProcAddress("glBufferData")) == NULL) return 0;
//     if ((glBufferSubDataProc = GetGLProcAddress("glBufferSubData")) == NULL) return 0;
//     if ((glGetBufferSubDataProc = GetGLProcAddress("glGetBufferSubData")) == NULL) return 0;
//     if ((glMapBufferProc = GetGLProcAddress("glMapBuffer")) == NULL) return 0;
//     if ((glUnmapBufferProc = GetGLProcAddress("glUnmapBuffer")) == NULL) return 0;
//     if ((glGetBufferParameterivProc = GetGLProcAddress("glGetBufferParameteriv")) == NULL) return 0;
//     if ((glGetBufferPointervProc = GetGLProcAddress("glGetBufferPointerv")) == NULL) return 0;
//     return 1;
// }
// #endif // GL_VERSION_1_5
// #ifndef GL_VERSION_2_0
// #define GL_VERSION_2_0 1
// typedef char GLchar;
// 
// void (APIENTRYP glBlendEquationSeparateProc)(GLenum modeRGB, GLenum modeAlpha);
// void (APIENTRYP glDrawBuffersProc)(GLsizei n, GLenum* bufs);
// void (APIENTRYP glStencilOpSeparateProc)(GLenum face, GLenum sfail, GLenum dpfail, GLenum dppass);
// void (APIENTRYP glStencilFuncSeparateProc)(GLenum face, GLenum func_, GLint ref, GLuint mask);
// void (APIENTRYP glStencilMaskSeparateProc)(GLenum face, GLuint mask);
// void (APIENTRYP glAttachShaderProc)(GLuint program, GLuint shader);
// void (APIENTRYP glBindAttribLocationProc)(GLuint program, GLuint index, GLchar* name);
// void (APIENTRYP glCompileShaderProc)(GLuint shader);
// GLuint (APIENTRYP glCreateProgramProc)();
// GLuint (APIENTRYP glCreateShaderProc)(GLenum type_);
// void (APIENTRYP glDeleteProgramProc)(GLuint program);
// void (APIENTRYP glDeleteShaderProc)(GLuint shader);
// void (APIENTRYP glDetachShaderProc)(GLuint program, GLuint shader);
// void (APIENTRYP glDisableVertexAttribArrayProc)(GLuint index);
// void (APIENTRYP glEnableVertexAttribArrayProc)(GLuint index);
// void (APIENTRYP glGetActiveAttribProc)(GLuint program, GLuint index, GLsizei bufSize, GLsizei* length, GLint* size, GLenum* type_, GLchar* name);
// void (APIENTRYP glGetActiveUniformProc)(GLuint program, GLuint index, GLsizei bufSize, GLsizei* length, GLint* size, GLenum* type_, GLchar* name);
// void (APIENTRYP glGetAttachedShadersProc)(GLuint program, GLsizei maxCount, GLsizei* count, GLuint* shaders);
// GLint (APIENTRYP glGetAttribLocationProc)(GLuint program, GLchar* name);
// void (APIENTRYP glGetProgramivProc)(GLuint program, GLenum pname, GLint* params);
// void (APIENTRYP glGetProgramInfoLogProc)(GLuint program, GLsizei bufSize, GLsizei* length, GLchar* infoLog);
// void (APIENTRYP glGetShaderivProc)(GLuint shader, GLenum pname, GLint* params);
// void (APIENTRYP glGetShaderInfoLogProc)(GLuint shader, GLsizei bufSize, GLsizei* length, GLchar* infoLog);
// void (APIENTRYP glGetShaderSourceProc)(GLuint shader, GLsizei bufSize, GLsizei* length, GLchar* source);
// GLint (APIENTRYP glGetUniformLocationProc)(GLuint program, GLchar* name);
// void (APIENTRYP glGetUniformfvProc)(GLuint program, GLint location, GLfloat* params);
// void (APIENTRYP glGetUniformivProc)(GLuint program, GLint location, GLint* params);
// void (APIENTRYP glGetVertexAttribdvProc)(GLuint index, GLenum pname, GLdouble* params);
// void (APIENTRYP glGetVertexAttribfvProc)(GLuint index, GLenum pname, GLfloat* params);
// void (APIENTRYP glGetVertexAttribivProc)(GLuint index, GLenum pname, GLint* params);
// void (APIENTRYP glGetVertexAttribPointervProc)(GLuint index, GLenum pname, GLvoid** pointer);
// GLboolean (APIENTRYP glIsProgramProc)(GLuint program);
// GLboolean (APIENTRYP glIsShaderProc)(GLuint shader);
// void (APIENTRYP glLinkProgramProc)(GLuint program);
// void (APIENTRYP glShaderSourceProc)(GLuint shader, GLsizei count, GLchar** string, GLint* length);
// void (APIENTRYP glUseProgramProc)(GLuint program);
// void (APIENTRYP glUniform1fProc)(GLint location, GLfloat v0);
// void (APIENTRYP glUniform2fProc)(GLint location, GLfloat v0, GLfloat v1);
// void (APIENTRYP glUniform3fProc)(GLint location, GLfloat v0, GLfloat v1, GLfloat v2);
// void (APIENTRYP glUniform4fProc)(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);
// void (APIENTRYP glUniform1iProc)(GLint location, GLint v0);
// void (APIENTRYP glUniform2iProc)(GLint location, GLint v0, GLint v1);
// void (APIENTRYP glUniform3iProc)(GLint location, GLint v0, GLint v1, GLint v2);
// void (APIENTRYP glUniform4iProc)(GLint location, GLint v0, GLint v1, GLint v2, GLint v3);
// void (APIENTRYP glUniform1fvProc)(GLint location, GLsizei count, GLfloat* value);
// void (APIENTRYP glUniform2fvProc)(GLint location, GLsizei count, GLfloat* value);
// void (APIENTRYP glUniform3fvProc)(GLint location, GLsizei count, GLfloat* value);
// void (APIENTRYP glUniform4fvProc)(GLint location, GLsizei count, GLfloat* value);
// void (APIENTRYP glUniform1ivProc)(GLint location, GLsizei count, GLint* value);
// void (APIENTRYP glUniform2ivProc)(GLint location, GLsizei count, GLint* value);
// void (APIENTRYP glUniform3ivProc)(GLint location, GLsizei count, GLint* value);
// void (APIENTRYP glUniform4ivProc)(GLint location, GLsizei count, GLint* value);
// void (APIENTRYP glUniformMatrix2fvProc)(GLint location, GLsizei count, GLboolean transpose, GLfloat* value);
// void (APIENTRYP glUniformMatrix3fvProc)(GLint location, GLsizei count, GLboolean transpose, GLfloat* value);
// void (APIENTRYP glUniformMatrix4fvProc)(GLint location, GLsizei count, GLboolean transpose, GLfloat* value);
// void (APIENTRYP glValidateProgramProc)(GLuint program);
// void (APIENTRYP glVertexAttrib1dProc)(GLuint index, GLdouble x);
// void (APIENTRYP glVertexAttrib1dvProc)(GLuint index, GLdouble* v);
// void (APIENTRYP glVertexAttrib1fProc)(GLuint index, GLfloat x);
// void (APIENTRYP glVertexAttrib1fvProc)(GLuint index, GLfloat* v);
// void (APIENTRYP glVertexAttrib1sProc)(GLuint index, GLshort x);
// void (APIENTRYP glVertexAttrib1svProc)(GLuint index, GLshort* v);
// void (APIENTRYP glVertexAttrib2dProc)(GLuint index, GLdouble x, GLdouble y);
// void (APIENTRYP glVertexAttrib2dvProc)(GLuint index, GLdouble* v);
// void (APIENTRYP glVertexAttrib2fProc)(GLuint index, GLfloat x, GLfloat y);
// void (APIENTRYP glVertexAttrib2fvProc)(GLuint index, GLfloat* v);
// void (APIENTRYP glVertexAttrib2sProc)(GLuint index, GLshort x, GLshort y);
// void (APIENTRYP glVertexAttrib2svProc)(GLuint index, GLshort* v);
// void (APIENTRYP glVertexAttrib3dProc)(GLuint index, GLdouble x, GLdouble y, GLdouble z);
// void (APIENTRYP glVertexAttrib3dvProc)(GLuint index, GLdouble* v);
// void (APIENTRYP glVertexAttrib3fProc)(GLuint index, GLfloat x, GLfloat y, GLfloat z);
// void (APIENTRYP glVertexAttrib3fvProc)(GLuint index, GLfloat* v);
// void (APIENTRYP glVertexAttrib3sProc)(GLuint index, GLshort x, GLshort y, GLshort z);
// void (APIENTRYP glVertexAttrib3svProc)(GLuint index, GLshort* v);
// void (APIENTRYP glVertexAttrib4NbvProc)(GLuint index, GLbyte* v);
// void (APIENTRYP glVertexAttrib4NivProc)(GLuint index, GLint* v);
// void (APIENTRYP glVertexAttrib4NsvProc)(GLuint index, GLshort* v);
// void (APIENTRYP glVertexAttrib4NubProc)(GLuint index, GLubyte x, GLubyte y, GLubyte z, GLubyte w);
// void (APIENTRYP glVertexAttrib4NubvProc)(GLuint index, GLubyte* v);
// void (APIENTRYP glVertexAttrib4NuivProc)(GLuint index, GLuint* v);
// void (APIENTRYP glVertexAttrib4NusvProc)(GLuint index, GLushort* v);
// void (APIENTRYP glVertexAttrib4bvProc)(GLuint index, GLbyte* v);
// void (APIENTRYP glVertexAttrib4dProc)(GLuint index, GLdouble x, GLdouble y, GLdouble z, GLdouble w);
// void (APIENTRYP glVertexAttrib4dvProc)(GLuint index, GLdouble* v);
// void (APIENTRYP glVertexAttrib4fProc)(GLuint index, GLfloat x, GLfloat y, GLfloat z, GLfloat w);
// void (APIENTRYP glVertexAttrib4fvProc)(GLuint index, GLfloat* v);
// void (APIENTRYP glVertexAttrib4ivProc)(GLuint index, GLint* v);
// void (APIENTRYP glVertexAttrib4sProc)(GLuint index, GLshort x, GLshort y, GLshort z, GLshort w);
// void (APIENTRYP glVertexAttrib4svProc)(GLuint index, GLshort* v);
// void (APIENTRYP glVertexAttrib4ubvProc)(GLuint index, GLubyte* v);
// void (APIENTRYP glVertexAttrib4uivProc)(GLuint index, GLuint* v);
// void (APIENTRYP glVertexAttrib4usvProc)(GLuint index, GLushort* v);
// void (APIENTRYP glVertexAttribPointerProc)(GLuint index, GLint size, GLenum type_, GLboolean normalized, GLsizei stride, GLvoid* pointer);
// 
// inline void glBlendEquationSeparate(GLenum modeRGB, GLenum modeAlpha){  glBlendEquationSeparateProc(modeRGB, modeAlpha); }
// inline void glDrawBuffers(GLsizei n, GLenum* bufs){  glDrawBuffersProc(n, bufs); }
// inline void glStencilOpSeparate(GLenum face, GLenum sfail, GLenum dpfail, GLenum dppass){  glStencilOpSeparateProc(face, sfail, dpfail, dppass); }
// inline void glStencilFuncSeparate(GLenum face, GLenum func_, GLint ref, GLuint mask){  glStencilFuncSeparateProc(face, func_, ref, mask); }
// inline void glStencilMaskSeparate(GLenum face, GLuint mask){  glStencilMaskSeparateProc(face, mask); }
// inline void glAttachShader(GLuint program, GLuint shader){  glAttachShaderProc(program, shader); }
// inline void glBindAttribLocation(GLuint program, GLuint index, GLchar* name){  glBindAttribLocationProc(program, index, name); }
// inline void glCompileShader(GLuint shader){  glCompileShaderProc(shader); }
// inline GLuint glCreateProgram(){ return glCreateProgramProc(); }
// inline GLuint glCreateShader(GLenum type_){ return glCreateShaderProc(type_); }
// inline void glDeleteProgram(GLuint program){  glDeleteProgramProc(program); }
// inline void glDeleteShader(GLuint shader){  glDeleteShaderProc(shader); }
// inline void glDetachShader(GLuint program, GLuint shader){  glDetachShaderProc(program, shader); }
// inline void glDisableVertexAttribArray(GLuint index){  glDisableVertexAttribArrayProc(index); }
// inline void glEnableVertexAttribArray(GLuint index){  glEnableVertexAttribArrayProc(index); }
// inline void glGetActiveAttrib(GLuint program, GLuint index, GLsizei bufSize, GLsizei* length, GLint* size, GLenum* type_, GLchar* name){  glGetActiveAttribProc(program, index, bufSize, length, size, type_, name); }
// inline void glGetActiveUniform(GLuint program, GLuint index, GLsizei bufSize, GLsizei* length, GLint* size, GLenum* type_, GLchar* name){  glGetActiveUniformProc(program, index, bufSize, length, size, type_, name); }
// inline void glGetAttachedShaders(GLuint program, GLsizei maxCount, GLsizei* count, GLuint* shaders){  glGetAttachedShadersProc(program, maxCount, count, shaders); }
// inline GLint glGetAttribLocation(GLuint program, GLchar* name){ return glGetAttribLocationProc(program, name); }
// inline void glGetProgramiv(GLuint program, GLenum pname, GLint* params){  glGetProgramivProc(program, pname, params); }
// inline void glGetProgramInfoLog(GLuint program, GLsizei bufSize, GLsizei* length, GLchar* infoLog){  glGetProgramInfoLogProc(program, bufSize, length, infoLog); }
// inline void glGetShaderiv(GLuint shader, GLenum pname, GLint* params){  glGetShaderivProc(shader, pname, params); }
// inline void glGetShaderInfoLog(GLuint shader, GLsizei bufSize, GLsizei* length, GLchar* infoLog){  glGetShaderInfoLogProc(shader, bufSize, length, infoLog); }
// inline void glGetShaderSource(GLuint shader, GLsizei bufSize, GLsizei* length, GLchar* source){  glGetShaderSourceProc(shader, bufSize, length, source); }
// inline GLint glGetUniformLocation(GLuint program, GLchar* name){ return glGetUniformLocationProc(program, name); }
// inline void glGetUniformfv(GLuint program, GLint location, GLfloat* params){  glGetUniformfvProc(program, location, params); }
// inline void glGetUniformiv(GLuint program, GLint location, GLint* params){  glGetUniformivProc(program, location, params); }
// inline void glGetVertexAttribdv(GLuint index, GLenum pname, GLdouble* params){  glGetVertexAttribdvProc(index, pname, params); }
// inline void glGetVertexAttribfv(GLuint index, GLenum pname, GLfloat* params){  glGetVertexAttribfvProc(index, pname, params); }
// inline void glGetVertexAttribiv(GLuint index, GLenum pname, GLint* params){  glGetVertexAttribivProc(index, pname, params); }
// inline void glGetVertexAttribPointerv(GLuint index, GLenum pname, GLvoid** pointer){  glGetVertexAttribPointervProc(index, pname, pointer); }
// inline GLboolean glIsProgram(GLuint program){ return glIsProgramProc(program); }
// inline GLboolean glIsShader(GLuint shader){ return glIsShaderProc(shader); }
// inline void glLinkProgram(GLuint program){  glLinkProgramProc(program); }
// inline void glShaderSource(GLuint shader, GLsizei count, GLchar** string, GLint* length){  glShaderSourceProc(shader, count, string, length); }
// inline void glUseProgram(GLuint program){  glUseProgramProc(program); }
// inline void glUniform1f(GLint location, GLfloat v0){  glUniform1fProc(location, v0); }
// inline void glUniform2f(GLint location, GLfloat v0, GLfloat v1){  glUniform2fProc(location, v0, v1); }
// inline void glUniform3f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2){  glUniform3fProc(location, v0, v1, v2); }
// inline void glUniform4f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3){  glUniform4fProc(location, v0, v1, v2, v3); }
// inline void glUniform1i(GLint location, GLint v0){  glUniform1iProc(location, v0); }
// inline void glUniform2i(GLint location, GLint v0, GLint v1){  glUniform2iProc(location, v0, v1); }
// inline void glUniform3i(GLint location, GLint v0, GLint v1, GLint v2){  glUniform3iProc(location, v0, v1, v2); }
// inline void glUniform4i(GLint location, GLint v0, GLint v1, GLint v2, GLint v3){  glUniform4iProc(location, v0, v1, v2, v3); }
// inline void glUniform1fv(GLint location, GLsizei count, GLfloat* value){  glUniform1fvProc(location, count, value); }
// inline void glUniform2fv(GLint location, GLsizei count, GLfloat* value){  glUniform2fvProc(location, count, value); }
// inline void glUniform3fv(GLint location, GLsizei count, GLfloat* value){  glUniform3fvProc(location, count, value); }
// inline void glUniform4fv(GLint location, GLsizei count, GLfloat* value){  glUniform4fvProc(location, count, value); }
// inline void glUniform1iv(GLint location, GLsizei count, GLint* value){  glUniform1ivProc(location, count, value); }
// inline void glUniform2iv(GLint location, GLsizei count, GLint* value){  glUniform2ivProc(location, count, value); }
// inline void glUniform3iv(GLint location, GLsizei count, GLint* value){  glUniform3ivProc(location, count, value); }
// inline void glUniform4iv(GLint location, GLsizei count, GLint* value){  glUniform4ivProc(location, count, value); }
// inline void glUniformMatrix2fv(GLint location, GLsizei count, GLboolean transpose, GLfloat* value){  glUniformMatrix2fvProc(location, count, transpose, value); }
// inline void glUniformMatrix3fv(GLint location, GLsizei count, GLboolean transpose, GLfloat* value){  glUniformMatrix3fvProc(location, count, transpose, value); }
// inline void glUniformMatrix4fv(GLint location, GLsizei count, GLboolean transpose, GLfloat* value){  glUniformMatrix4fvProc(location, count, transpose, value); }
// inline void glValidateProgram(GLuint program){  glValidateProgramProc(program); }
// inline void glVertexAttrib1d(GLuint index, GLdouble x){  glVertexAttrib1dProc(index, x); }
// inline void glVertexAttrib1dv(GLuint index, GLdouble* v){  glVertexAttrib1dvProc(index, v); }
// inline void glVertexAttrib1f(GLuint index, GLfloat x){  glVertexAttrib1fProc(index, x); }
// inline void glVertexAttrib1fv(GLuint index, GLfloat* v){  glVertexAttrib1fvProc(index, v); }
// inline void glVertexAttrib1s(GLuint index, GLshort x){  glVertexAttrib1sProc(index, x); }
// inline void glVertexAttrib1sv(GLuint index, GLshort* v){  glVertexAttrib1svProc(index, v); }
// inline void glVertexAttrib2d(GLuint index, GLdouble x, GLdouble y){  glVertexAttrib2dProc(index, x, y); }
// inline void glVertexAttrib2dv(GLuint index, GLdouble* v){  glVertexAttrib2dvProc(index, v); }
// inline void glVertexAttrib2f(GLuint index, GLfloat x, GLfloat y){  glVertexAttrib2fProc(index, x, y); }
// inline void glVertexAttrib2fv(GLuint index, GLfloat* v){  glVertexAttrib2fvProc(index, v); }
// inline void glVertexAttrib2s(GLuint index, GLshort x, GLshort y){  glVertexAttrib2sProc(index, x, y); }
// inline void glVertexAttrib2sv(GLuint index, GLshort* v){  glVertexAttrib2svProc(index, v); }
// inline void glVertexAttrib3d(GLuint index, GLdouble x, GLdouble y, GLdouble z){  glVertexAttrib3dProc(index, x, y, z); }
// inline void glVertexAttrib3dv(GLuint index, GLdouble* v){  glVertexAttrib3dvProc(index, v); }
// inline void glVertexAttrib3f(GLuint index, GLfloat x, GLfloat y, GLfloat z){  glVertexAttrib3fProc(index, x, y, z); }
// inline void glVertexAttrib3fv(GLuint index, GLfloat* v){  glVertexAttrib3fvProc(index, v); }
// inline void glVertexAttrib3s(GLuint index, GLshort x, GLshort y, GLshort z){  glVertexAttrib3sProc(index, x, y, z); }
// inline void glVertexAttrib3sv(GLuint index, GLshort* v){  glVertexAttrib3svProc(index, v); }
// inline void glVertexAttrib4Nbv(GLuint index, GLbyte* v){  glVertexAttrib4NbvProc(index, v); }
// inline void glVertexAttrib4Niv(GLuint index, GLint* v){  glVertexAttrib4NivProc(index, v); }
// inline void glVertexAttrib4Nsv(GLuint index, GLshort* v){  glVertexAttrib4NsvProc(index, v); }
// inline void glVertexAttrib4Nub(GLuint index, GLubyte x, GLubyte y, GLubyte z, GLubyte w){  glVertexAttrib4NubProc(index, x, y, z, w); }
// inline void glVertexAttrib4Nubv(GLuint index, GLubyte* v){  glVertexAttrib4NubvProc(index, v); }
// inline void glVertexAttrib4Nuiv(GLuint index, GLuint* v){  glVertexAttrib4NuivProc(index, v); }
// inline void glVertexAttrib4Nusv(GLuint index, GLushort* v){  glVertexAttrib4NusvProc(index, v); }
// inline void glVertexAttrib4bv(GLuint index, GLbyte* v){  glVertexAttrib4bvProc(index, v); }
// inline void glVertexAttrib4d(GLuint index, GLdouble x, GLdouble y, GLdouble z, GLdouble w){  glVertexAttrib4dProc(index, x, y, z, w); }
// inline void glVertexAttrib4dv(GLuint index, GLdouble* v){  glVertexAttrib4dvProc(index, v); }
// inline void glVertexAttrib4f(GLuint index, GLfloat x, GLfloat y, GLfloat z, GLfloat w){  glVertexAttrib4fProc(index, x, y, z, w); }
// inline void glVertexAttrib4fv(GLuint index, GLfloat* v){  glVertexAttrib4fvProc(index, v); }
// inline void glVertexAttrib4iv(GLuint index, GLint* v){  glVertexAttrib4ivProc(index, v); }
// inline void glVertexAttrib4s(GLuint index, GLshort x, GLshort y, GLshort z, GLshort w){  glVertexAttrib4sProc(index, x, y, z, w); }
// inline void glVertexAttrib4sv(GLuint index, GLshort* v){  glVertexAttrib4svProc(index, v); }
// inline void glVertexAttrib4ubv(GLuint index, GLubyte* v){  glVertexAttrib4ubvProc(index, v); }
// inline void glVertexAttrib4uiv(GLuint index, GLuint* v){  glVertexAttrib4uivProc(index, v); }
// inline void glVertexAttrib4usv(GLuint index, GLushort* v){  glVertexAttrib4usvProc(index, v); }
// inline void glVertexAttribPointer(GLuint index, GLint size, GLenum type_, GLboolean normalized, GLsizei stride, GLvoid* pointer){  glVertexAttribPointerProc(index, size, type_, normalized, stride, pointer); }
// 
// int initGL_VERSION_2_0() { 
//     if ((glBlendEquationSeparateProc = GetGLProcAddress("glBlendEquationSeparate")) == NULL) return 0;
//     if ((glDrawBuffersProc = GetGLProcAddress("glDrawBuffers")) == NULL) return 0;
//     if ((glStencilOpSeparateProc = GetGLProcAddress("glStencilOpSeparate")) == NULL) return 0;
//     if ((glStencilFuncSeparateProc = GetGLProcAddress("glStencilFuncSeparate")) == NULL) return 0;
//     if ((glStencilMaskSeparateProc = GetGLProcAddress("glStencilMaskSeparate")) == NULL) return 0;
//     if ((glAttachShaderProc = GetGLProcAddress("glAttachShader")) == NULL) return 0;
//     if ((glBindAttribLocationProc = GetGLProcAddress("glBindAttribLocation")) == NULL) return 0;
//     if ((glCompileShaderProc = GetGLProcAddress("glCompileShader")) == NULL) return 0;
//     if ((glCreateProgramProc = GetGLProcAddress("glCreateProgram")) == NULL) return 0;
//     if ((glCreateShaderProc = GetGLProcAddress("glCreateShader")) == NULL) return 0;
//     if ((glDeleteProgramProc = GetGLProcAddress("glDeleteProgram")) == NULL) return 0;
//     if ((glDeleteShaderProc = GetGLProcAddress("glDeleteShader")) == NULL) return 0;
//     if ((glDetachShaderProc = GetGLProcAddress("glDetachShader")) == NULL) return 0;
//     if ((glDisableVertexAttribArrayProc = GetGLProcAddress("glDisableVertexAttribArray")) == NULL) return 0;
//     if ((glEnableVertexAttribArrayProc = GetGLProcAddress("glEnableVertexAttribArray")) == NULL) return 0;
//     if ((glGetActiveAttribProc = GetGLProcAddress("glGetActiveAttrib")) == NULL) return 0;
//     if ((glGetActiveUniformProc = GetGLProcAddress("glGetActiveUniform")) == NULL) return 0;
//     if ((glGetAttachedShadersProc = GetGLProcAddress("glGetAttachedShaders")) == NULL) return 0;
//     if ((glGetAttribLocationProc = GetGLProcAddress("glGetAttribLocation")) == NULL) return 0;
//     if ((glGetProgramivProc = GetGLProcAddress("glGetProgramiv")) == NULL) return 0;
//     if ((glGetProgramInfoLogProc = GetGLProcAddress("glGetProgramInfoLog")) == NULL) return 0;
//     if ((glGetShaderivProc = GetGLProcAddress("glGetShaderiv")) == NULL) return 0;
//     if ((glGetShaderInfoLogProc = GetGLProcAddress("glGetShaderInfoLog")) == NULL) return 0;
//     if ((glGetShaderSourceProc = GetGLProcAddress("glGetShaderSource")) == NULL) return 0;
//     if ((glGetUniformLocationProc = GetGLProcAddress("glGetUniformLocation")) == NULL) return 0;
//     if ((glGetUniformfvProc = GetGLProcAddress("glGetUniformfv")) == NULL) return 0;
//     if ((glGetUniformivProc = GetGLProcAddress("glGetUniformiv")) == NULL) return 0;
//     if ((glGetVertexAttribdvProc = GetGLProcAddress("glGetVertexAttribdv")) == NULL) return 0;
//     if ((glGetVertexAttribfvProc = GetGLProcAddress("glGetVertexAttribfv")) == NULL) return 0;
//     if ((glGetVertexAttribivProc = GetGLProcAddress("glGetVertexAttribiv")) == NULL) return 0;
//     if ((glGetVertexAttribPointervProc = GetGLProcAddress("glGetVertexAttribPointerv")) == NULL) return 0;
//     if ((glIsProgramProc = GetGLProcAddress("glIsProgram")) == NULL) return 0;
//     if ((glIsShaderProc = GetGLProcAddress("glIsShader")) == NULL) return 0;
//     if ((glLinkProgramProc = GetGLProcAddress("glLinkProgram")) == NULL) return 0;
//     if ((glShaderSourceProc = GetGLProcAddress("glShaderSource")) == NULL) return 0;
//     if ((glUseProgramProc = GetGLProcAddress("glUseProgram")) == NULL) return 0;
//     if ((glUniform1fProc = GetGLProcAddress("glUniform1f")) == NULL) return 0;
//     if ((glUniform2fProc = GetGLProcAddress("glUniform2f")) == NULL) return 0;
//     if ((glUniform3fProc = GetGLProcAddress("glUniform3f")) == NULL) return 0;
//     if ((glUniform4fProc = GetGLProcAddress("glUniform4f")) == NULL) return 0;
//     if ((glUniform1iProc = GetGLProcAddress("glUniform1i")) == NULL) return 0;
//     if ((glUniform2iProc = GetGLProcAddress("glUniform2i")) == NULL) return 0;
//     if ((glUniform3iProc = GetGLProcAddress("glUniform3i")) == NULL) return 0;
//     if ((glUniform4iProc = GetGLProcAddress("glUniform4i")) == NULL) return 0;
//     if ((glUniform1fvProc = GetGLProcAddress("glUniform1fv")) == NULL) return 0;
//     if ((glUniform2fvProc = GetGLProcAddress("glUniform2fv")) == NULL) return 0;
//     if ((glUniform3fvProc = GetGLProcAddress("glUniform3fv")) == NULL) return 0;
//     if ((glUniform4fvProc = GetGLProcAddress("glUniform4fv")) == NULL) return 0;
//     if ((glUniform1ivProc = GetGLProcAddress("glUniform1iv")) == NULL) return 0;
//     if ((glUniform2ivProc = GetGLProcAddress("glUniform2iv")) == NULL) return 0;
//     if ((glUniform3ivProc = GetGLProcAddress("glUniform3iv")) == NULL) return 0;
//     if ((glUniform4ivProc = GetGLProcAddress("glUniform4iv")) == NULL) return 0;
//     if ((glUniformMatrix2fvProc = GetGLProcAddress("glUniformMatrix2fv")) == NULL) return 0;
//     if ((glUniformMatrix3fvProc = GetGLProcAddress("glUniformMatrix3fv")) == NULL) return 0;
//     if ((glUniformMatrix4fvProc = GetGLProcAddress("glUniformMatrix4fv")) == NULL) return 0;
//     if ((glValidateProgramProc = GetGLProcAddress("glValidateProgram")) == NULL) return 0;
//     if ((glVertexAttrib1dProc = GetGLProcAddress("glVertexAttrib1d")) == NULL) return 0;
//     if ((glVertexAttrib1dvProc = GetGLProcAddress("glVertexAttrib1dv")) == NULL) return 0;
//     if ((glVertexAttrib1fProc = GetGLProcAddress("glVertexAttrib1f")) == NULL) return 0;
//     if ((glVertexAttrib1fvProc = GetGLProcAddress("glVertexAttrib1fv")) == NULL) return 0;
//     if ((glVertexAttrib1sProc = GetGLProcAddress("glVertexAttrib1s")) == NULL) return 0;
//     if ((glVertexAttrib1svProc = GetGLProcAddress("glVertexAttrib1sv")) == NULL) return 0;
//     if ((glVertexAttrib2dProc = GetGLProcAddress("glVertexAttrib2d")) == NULL) return 0;
//     if ((glVertexAttrib2dvProc = GetGLProcAddress("glVertexAttrib2dv")) == NULL) return 0;
//     if ((glVertexAttrib2fProc = GetGLProcAddress("glVertexAttrib2f")) == NULL) return 0;
//     if ((glVertexAttrib2fvProc = GetGLProcAddress("glVertexAttrib2fv")) == NULL) return 0;
//     if ((glVertexAttrib2sProc = GetGLProcAddress("glVertexAttrib2s")) == NULL) return 0;
//     if ((glVertexAttrib2svProc = GetGLProcAddress("glVertexAttrib2sv")) == NULL) return 0;
//     if ((glVertexAttrib3dProc = GetGLProcAddress("glVertexAttrib3d")) == NULL) return 0;
//     if ((glVertexAttrib3dvProc = GetGLProcAddress("glVertexAttrib3dv")) == NULL) return 0;
//     if ((glVertexAttrib3fProc = GetGLProcAddress("glVertexAttrib3f")) == NULL) return 0;
//     if ((glVertexAttrib3fvProc = GetGLProcAddress("glVertexAttrib3fv")) == NULL) return 0;
//     if ((glVertexAttrib3sProc = GetGLProcAddress("glVertexAttrib3s")) == NULL) return 0;
//     if ((glVertexAttrib3svProc = GetGLProcAddress("glVertexAttrib3sv")) == NULL) return 0;
//     if ((glVertexAttrib4NbvProc = GetGLProcAddress("glVertexAttrib4Nbv")) == NULL) return 0;
//     if ((glVertexAttrib4NivProc = GetGLProcAddress("glVertexAttrib4Niv")) == NULL) return 0;
//     if ((glVertexAttrib4NsvProc = GetGLProcAddress("glVertexAttrib4Nsv")) == NULL) return 0;
//     if ((glVertexAttrib4NubProc = GetGLProcAddress("glVertexAttrib4Nub")) == NULL) return 0;
//     if ((glVertexAttrib4NubvProc = GetGLProcAddress("glVertexAttrib4Nubv")) == NULL) return 0;
//     if ((glVertexAttrib4NuivProc = GetGLProcAddress("glVertexAttrib4Nuiv")) == NULL) return 0;
//     if ((glVertexAttrib4NusvProc = GetGLProcAddress("glVertexAttrib4Nusv")) == NULL) return 0;
//     if ((glVertexAttrib4bvProc = GetGLProcAddress("glVertexAttrib4bv")) == NULL) return 0;
//     if ((glVertexAttrib4dProc = GetGLProcAddress("glVertexAttrib4d")) == NULL) return 0;
//     if ((glVertexAttrib4dvProc = GetGLProcAddress("glVertexAttrib4dv")) == NULL) return 0;
//     if ((glVertexAttrib4fProc = GetGLProcAddress("glVertexAttrib4f")) == NULL) return 0;
//     if ((glVertexAttrib4fvProc = GetGLProcAddress("glVertexAttrib4fv")) == NULL) return 0;
//     if ((glVertexAttrib4ivProc = GetGLProcAddress("glVertexAttrib4iv")) == NULL) return 0;
//     if ((glVertexAttrib4sProc = GetGLProcAddress("glVertexAttrib4s")) == NULL) return 0;
//     if ((glVertexAttrib4svProc = GetGLProcAddress("glVertexAttrib4sv")) == NULL) return 0;
//     if ((glVertexAttrib4ubvProc = GetGLProcAddress("glVertexAttrib4ubv")) == NULL) return 0;
//     if ((glVertexAttrib4uivProc = GetGLProcAddress("glVertexAttrib4uiv")) == NULL) return 0;
//     if ((glVertexAttrib4usvProc = GetGLProcAddress("glVertexAttrib4usv")) == NULL) return 0;
//     if ((glVertexAttribPointerProc = GetGLProcAddress("glVertexAttribPointer")) == NULL) return 0;
//     return 1;
// }
// #endif // GL_VERSION_2_0
// #ifndef GL_VERSION_2_1
// #define GL_VERSION_2_1 1
// 
// void (APIENTRYP glUniformMatrix2x3fvProc)(GLint location, GLsizei count, GLboolean transpose, GLfloat* value);
// void (APIENTRYP glUniformMatrix3x2fvProc)(GLint location, GLsizei count, GLboolean transpose, GLfloat* value);
// void (APIENTRYP glUniformMatrix2x4fvProc)(GLint location, GLsizei count, GLboolean transpose, GLfloat* value);
// void (APIENTRYP glUniformMatrix4x2fvProc)(GLint location, GLsizei count, GLboolean transpose, GLfloat* value);
// void (APIENTRYP glUniformMatrix3x4fvProc)(GLint location, GLsizei count, GLboolean transpose, GLfloat* value);
// void (APIENTRYP glUniformMatrix4x3fvProc)(GLint location, GLsizei count, GLboolean transpose, GLfloat* value);
// 
// inline void glUniformMatrix2x3fv(GLint location, GLsizei count, GLboolean transpose, GLfloat* value){  glUniformMatrix2x3fvProc(location, count, transpose, value); }
// inline void glUniformMatrix3x2fv(GLint location, GLsizei count, GLboolean transpose, GLfloat* value){  glUniformMatrix3x2fvProc(location, count, transpose, value); }
// inline void glUniformMatrix2x4fv(GLint location, GLsizei count, GLboolean transpose, GLfloat* value){  glUniformMatrix2x4fvProc(location, count, transpose, value); }
// inline void glUniformMatrix4x2fv(GLint location, GLsizei count, GLboolean transpose, GLfloat* value){  glUniformMatrix4x2fvProc(location, count, transpose, value); }
// inline void glUniformMatrix3x4fv(GLint location, GLsizei count, GLboolean transpose, GLfloat* value){  glUniformMatrix3x4fvProc(location, count, transpose, value); }
// inline void glUniformMatrix4x3fv(GLint location, GLsizei count, GLboolean transpose, GLfloat* value){  glUniformMatrix4x3fvProc(location, count, transpose, value); }
// 
// int initGL_VERSION_2_1() { 
//     if ((glUniformMatrix2x3fvProc = GetGLProcAddress("glUniformMatrix2x3fv")) == NULL) return 0;
//     if ((glUniformMatrix3x2fvProc = GetGLProcAddress("glUniformMatrix3x2fv")) == NULL) return 0;
//     if ((glUniformMatrix2x4fvProc = GetGLProcAddress("glUniformMatrix2x4fv")) == NULL) return 0;
//     if ((glUniformMatrix4x2fvProc = GetGLProcAddress("glUniformMatrix4x2fv")) == NULL) return 0;
//     if ((glUniformMatrix3x4fvProc = GetGLProcAddress("glUniformMatrix3x4fv")) == NULL) return 0;
//     if ((glUniformMatrix4x3fvProc = GetGLProcAddress("glUniformMatrix4x3fv")) == NULL) return 0;
//     return 1;
// }
// #endif // GL_VERSION_2_1
//
import "C"
import "unsafe"
import "errors"

type Pointer unsafe.Pointer

// ----------------------------------------------------------------------------
// GL_VERSION_1_0

type (
	Enum C.GLenum
	Float C.GLfloat
	Int C.GLint
	Sizei C.GLsizei
	Void C.GLvoid
	Bitfield C.GLbitfield
	Double C.GLdouble
	Uint C.GLuint
	Boolean C.GLboolean
	Ubyte C.GLubyte
	Byte C.GLbyte
	Short C.GLshort
	Ushort C.GLushort
)


// https://www.opengl.org/sdk/docs/man/xhtml/glCullFace
func CullFace(mode Enum){
	C.glCullFace((C.GLenum)(mode))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glFrontFace
func FrontFace(mode Enum){
	C.glFrontFace((C.GLenum)(mode))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glHint
func Hint(target Enum, mode Enum){
	C.glHint((C.GLenum)(target), (C.GLenum)(mode))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glLineWidth
func LineWidth(width Float){
	C.glLineWidth((C.GLfloat)(width))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPointSize
func PointSize(size Float){
	C.glPointSize((C.GLfloat)(size))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPolygonMode
func PolygonMode(face Enum, mode Enum){
	C.glPolygonMode((C.GLenum)(face), (C.GLenum)(mode))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glScissor
func Scissor(x Int, y Int, width Sizei, height Sizei){
	C.glScissor((C.GLint)(x), (C.GLint)(y), (C.GLsizei)(width), (C.GLsizei)(height))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexParameterf
func TexParameterf(target Enum, pname Enum, param Float){
	C.glTexParameterf((C.GLenum)(target), (C.GLenum)(pname), (C.GLfloat)(param))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexParameterfv
func TexParameterfv(target Enum, pname Enum, params *Float){
	C.glTexParameterfv((C.GLenum)(target), (C.GLenum)(pname), (*C.GLfloat)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexParameteri
func TexParameteri(target Enum, pname Enum, param Int){
	C.glTexParameteri((C.GLenum)(target), (C.GLenum)(pname), (C.GLint)(param))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexParameteriv
func TexParameteriv(target Enum, pname Enum, params *Int){
	C.glTexParameteriv((C.GLenum)(target), (C.GLenum)(pname), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexImage1D
func TexImage1D(target Enum, level Int, internalformat Int, width Sizei, border Int, format Enum, type_ Enum, pixels Pointer){
	C.glTexImage1D((C.GLenum)(target), (C.GLint)(level), (C.GLint)(internalformat), (C.GLsizei)(width), (C.GLint)(border), (C.GLenum)(format), (C.GLenum)(type_), (unsafe.Pointer)(pixels))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexImage2D
func TexImage2D(target Enum, level Int, internalformat Int, width Sizei, height Sizei, border Int, format Enum, type_ Enum, pixels Pointer){
	C.glTexImage2D((C.GLenum)(target), (C.GLint)(level), (C.GLint)(internalformat), (C.GLsizei)(width), (C.GLsizei)(height), (C.GLint)(border), (C.GLenum)(format), (C.GLenum)(type_), (unsafe.Pointer)(pixels))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glDrawBuffer
func DrawBuffer(mode Enum){
	C.glDrawBuffer((C.GLenum)(mode))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glClear
func Clear(mask Bitfield){
	C.glClear((C.GLbitfield)(mask))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glClearColor
func ClearColor(red Float, green Float, blue Float, alpha Float){
	C.glClearColor((C.GLfloat)(red), (C.GLfloat)(green), (C.GLfloat)(blue), (C.GLfloat)(alpha))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glClearStencil
func ClearStencil(s Int){
	C.glClearStencil((C.GLint)(s))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glClearDepth
func ClearDepth(depth Double){
	C.glClearDepth((C.GLdouble)(depth))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glStencilMask
func StencilMask(mask Uint){
	C.glStencilMask((C.GLuint)(mask))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColorMask
func ColorMask(red Boolean, green Boolean, blue Boolean, alpha Boolean){
	C.glColorMask((C.GLboolean)(red), (C.GLboolean)(green), (C.GLboolean)(blue), (C.GLboolean)(alpha))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glDepthMask
func DepthMask(flag Boolean){
	C.glDepthMask((C.GLboolean)(flag))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glDisable
func Disable(cap Enum){
	C.glDisable((C.GLenum)(cap))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glEnable
func Enable(cap Enum){
	C.glEnable((C.GLenum)(cap))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glFinish
func Finish(){
	C.glFinish()
}

// https://www.opengl.org/sdk/docs/man/xhtml/glFlush
func Flush(){
	C.glFlush()
}

// https://www.opengl.org/sdk/docs/man/xhtml/glBlendFunc
func BlendFunc(sfactor Enum, dfactor Enum){
	C.glBlendFunc((C.GLenum)(sfactor), (C.GLenum)(dfactor))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glLogicOp
func LogicOp(opcode Enum){
	C.glLogicOp((C.GLenum)(opcode))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glStencilFunc
func StencilFunc(func_ Enum, ref Int, mask Uint){
	C.glStencilFunc((C.GLenum)(func_), (C.GLint)(ref), (C.GLuint)(mask))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glStencilOp
func StencilOp(fail Enum, zfail Enum, zpass Enum){
	C.glStencilOp((C.GLenum)(fail), (C.GLenum)(zfail), (C.GLenum)(zpass))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glDepthFunc
func DepthFunc(func_ Enum){
	C.glDepthFunc((C.GLenum)(func_))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPixelStoref
func PixelStoref(pname Enum, param Float){
	C.glPixelStoref((C.GLenum)(pname), (C.GLfloat)(param))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPixelStorei
func PixelStorei(pname Enum, param Int){
	C.glPixelStorei((C.GLenum)(pname), (C.GLint)(param))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glReadBuffer
func ReadBuffer(mode Enum){
	C.glReadBuffer((C.GLenum)(mode))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glReadPixels
func ReadPixels(x Int, y Int, width Sizei, height Sizei, format Enum, type_ Enum, pixels Pointer){
	C.glReadPixels((C.GLint)(x), (C.GLint)(y), (C.GLsizei)(width), (C.GLsizei)(height), (C.GLenum)(format), (C.GLenum)(type_), (unsafe.Pointer)(pixels))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetBooleanv
func GetBooleanv(pname Enum, params *Boolean){
	C.glGetBooleanv((C.GLenum)(pname), (*C.GLboolean)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetDoublev
func GetDoublev(pname Enum, params *Double){
	C.glGetDoublev((C.GLenum)(pname), (*C.GLdouble)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetError
func GetError() Enum {
	return (Enum)(C.glGetError())
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetFloatv
func GetFloatv(pname Enum, params *Float){
	C.glGetFloatv((C.GLenum)(pname), (*C.GLfloat)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetIntegerv
func GetIntegerv(pname Enum, params *Int){
	C.glGetIntegerv((C.GLenum)(pname), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetString
func GetString(name Enum) *Ubyte {
	return (*Ubyte)(C.glGetString((C.GLenum)(name)))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetTexImage
func GetTexImage(target Enum, level Int, format Enum, type_ Enum, pixels Pointer){
	C.glGetTexImage((C.GLenum)(target), (C.GLint)(level), (C.GLenum)(format), (C.GLenum)(type_), (unsafe.Pointer)(pixels))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetTexParameterfv
func GetTexParameterfv(target Enum, pname Enum, params *Float){
	C.glGetTexParameterfv((C.GLenum)(target), (C.GLenum)(pname), (*C.GLfloat)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetTexParameteriv
func GetTexParameteriv(target Enum, pname Enum, params *Int){
	C.glGetTexParameteriv((C.GLenum)(target), (C.GLenum)(pname), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetTexLevelParameterfv
func GetTexLevelParameterfv(target Enum, level Int, pname Enum, params *Float){
	C.glGetTexLevelParameterfv((C.GLenum)(target), (C.GLint)(level), (C.GLenum)(pname), (*C.GLfloat)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetTexLevelParameteriv
func GetTexLevelParameteriv(target Enum, level Int, pname Enum, params *Int){
	C.glGetTexLevelParameteriv((C.GLenum)(target), (C.GLint)(level), (C.GLenum)(pname), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glIsEnabled
func IsEnabled(cap Enum) Boolean {
	return (Boolean)(C.glIsEnabled((C.GLenum)(cap)))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glDepthRange
func DepthRange(near_ Double, far_ Double){
	C.glDepthRange((C.GLdouble)(near_), (C.GLdouble)(far_))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glViewport
func Viewport(x Int, y Int, width Sizei, height Sizei){
	C.glViewport((C.GLint)(x), (C.GLint)(y), (C.GLsizei)(width), (C.GLsizei)(height))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glNewList
func NewList(list Uint, mode Enum){
	C.glNewList((C.GLuint)(list), (C.GLenum)(mode))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glEndList
func EndList(){
	C.glEndList()
}

// https://www.opengl.org/sdk/docs/man/xhtml/glCallList
func CallList(list Uint){
	C.glCallList((C.GLuint)(list))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glCallLists
func CallLists(n Sizei, type_ Enum, lists Pointer){
	C.glCallLists((C.GLsizei)(n), (C.GLenum)(type_), (unsafe.Pointer)(lists))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glDeleteLists
func DeleteLists(list Uint, range_ Sizei){
	C.glDeleteLists((C.GLuint)(list), (C.GLsizei)(range_))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGenLists
func GenLists(range_ Sizei) Uint {
	return (Uint)(C.glGenLists((C.GLsizei)(range_)))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glListBase
func ListBase(base Uint){
	C.glListBase((C.GLuint)(base))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glBegin
func Begin(mode Enum){
	C.glBegin((C.GLenum)(mode))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glBitmap
func Bitmap(width Sizei, height Sizei, xorig Float, yorig Float, xmove Float, ymove Float, bitmap *Ubyte){
	C.glBitmap((C.GLsizei)(width), (C.GLsizei)(height), (C.GLfloat)(xorig), (C.GLfloat)(yorig), (C.GLfloat)(xmove), (C.GLfloat)(ymove), (*C.GLubyte)(bitmap))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor3b
func Color3b(red Byte, green Byte, blue Byte){
	C.glColor3b((C.GLbyte)(red), (C.GLbyte)(green), (C.GLbyte)(blue))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor3bv
func Color3bv(v *Byte){
	C.glColor3bv((*C.GLbyte)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor3d
func Color3d(red Double, green Double, blue Double){
	C.glColor3d((C.GLdouble)(red), (C.GLdouble)(green), (C.GLdouble)(blue))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor3dv
func Color3dv(v *Double){
	C.glColor3dv((*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor3f
func Color3f(red Float, green Float, blue Float){
	C.glColor3f((C.GLfloat)(red), (C.GLfloat)(green), (C.GLfloat)(blue))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor3fv
func Color3fv(v *Float){
	C.glColor3fv((*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor3i
func Color3i(red Int, green Int, blue Int){
	C.glColor3i((C.GLint)(red), (C.GLint)(green), (C.GLint)(blue))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor3iv
func Color3iv(v *Int){
	C.glColor3iv((*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor3s
func Color3s(red Short, green Short, blue Short){
	C.glColor3s((C.GLshort)(red), (C.GLshort)(green), (C.GLshort)(blue))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor3sv
func Color3sv(v *Short){
	C.glColor3sv((*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor3ub
func Color3ub(red Ubyte, green Ubyte, blue Ubyte){
	C.glColor3ub((C.GLubyte)(red), (C.GLubyte)(green), (C.GLubyte)(blue))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor3ubv
func Color3ubv(v *Ubyte){
	C.glColor3ubv((*C.GLubyte)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor3ui
func Color3ui(red Uint, green Uint, blue Uint){
	C.glColor3ui((C.GLuint)(red), (C.GLuint)(green), (C.GLuint)(blue))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor3uiv
func Color3uiv(v *Uint){
	C.glColor3uiv((*C.GLuint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor3us
func Color3us(red Ushort, green Ushort, blue Ushort){
	C.glColor3us((C.GLushort)(red), (C.GLushort)(green), (C.GLushort)(blue))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor3usv
func Color3usv(v *Ushort){
	C.glColor3usv((*C.GLushort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor4b
func Color4b(red Byte, green Byte, blue Byte, alpha Byte){
	C.glColor4b((C.GLbyte)(red), (C.GLbyte)(green), (C.GLbyte)(blue), (C.GLbyte)(alpha))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor4bv
func Color4bv(v *Byte){
	C.glColor4bv((*C.GLbyte)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor4d
func Color4d(red Double, green Double, blue Double, alpha Double){
	C.glColor4d((C.GLdouble)(red), (C.GLdouble)(green), (C.GLdouble)(blue), (C.GLdouble)(alpha))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor4dv
func Color4dv(v *Double){
	C.glColor4dv((*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor4f
func Color4f(red Float, green Float, blue Float, alpha Float){
	C.glColor4f((C.GLfloat)(red), (C.GLfloat)(green), (C.GLfloat)(blue), (C.GLfloat)(alpha))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor4fv
func Color4fv(v *Float){
	C.glColor4fv((*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor4i
func Color4i(red Int, green Int, blue Int, alpha Int){
	C.glColor4i((C.GLint)(red), (C.GLint)(green), (C.GLint)(blue), (C.GLint)(alpha))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor4iv
func Color4iv(v *Int){
	C.glColor4iv((*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor4s
func Color4s(red Short, green Short, blue Short, alpha Short){
	C.glColor4s((C.GLshort)(red), (C.GLshort)(green), (C.GLshort)(blue), (C.GLshort)(alpha))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor4sv
func Color4sv(v *Short){
	C.glColor4sv((*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor4ub
func Color4ub(red Ubyte, green Ubyte, blue Ubyte, alpha Ubyte){
	C.glColor4ub((C.GLubyte)(red), (C.GLubyte)(green), (C.GLubyte)(blue), (C.GLubyte)(alpha))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor4ubv
func Color4ubv(v *Ubyte){
	C.glColor4ubv((*C.GLubyte)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor4ui
func Color4ui(red Uint, green Uint, blue Uint, alpha Uint){
	C.glColor4ui((C.GLuint)(red), (C.GLuint)(green), (C.GLuint)(blue), (C.GLuint)(alpha))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor4uiv
func Color4uiv(v *Uint){
	C.glColor4uiv((*C.GLuint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor4us
func Color4us(red Ushort, green Ushort, blue Ushort, alpha Ushort){
	C.glColor4us((C.GLushort)(red), (C.GLushort)(green), (C.GLushort)(blue), (C.GLushort)(alpha))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColor4usv
func Color4usv(v *Ushort){
	C.glColor4usv((*C.GLushort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glEdgeFlag
func EdgeFlag(flag Boolean){
	C.glEdgeFlag((C.GLboolean)(flag))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glEdgeFlagv
func EdgeFlagv(flag *Boolean){
	C.glEdgeFlagv((*C.GLboolean)(flag))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glEnd
func End(){
	C.glEnd()
}

// https://www.opengl.org/sdk/docs/man/xhtml/glIndexd
func Indexd(c Double){
	C.glIndexd((C.GLdouble)(c))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glIndexdv
func Indexdv(c *Double){
	C.glIndexdv((*C.GLdouble)(c))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glIndexf
func Indexf(c Float){
	C.glIndexf((C.GLfloat)(c))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glIndexfv
func Indexfv(c *Float){
	C.glIndexfv((*C.GLfloat)(c))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glIndexi
func Indexi(c Int){
	C.glIndexi((C.GLint)(c))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glIndexiv
func Indexiv(c *Int){
	C.glIndexiv((*C.GLint)(c))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glIndexs
func Indexs(c Short){
	C.glIndexs((C.GLshort)(c))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glIndexsv
func Indexsv(c *Short){
	C.glIndexsv((*C.GLshort)(c))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glNormal3b
func Normal3b(nx Byte, ny Byte, nz Byte){
	C.glNormal3b((C.GLbyte)(nx), (C.GLbyte)(ny), (C.GLbyte)(nz))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glNormal3bv
func Normal3bv(v *Byte){
	C.glNormal3bv((*C.GLbyte)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glNormal3d
func Normal3d(nx Double, ny Double, nz Double){
	C.glNormal3d((C.GLdouble)(nx), (C.GLdouble)(ny), (C.GLdouble)(nz))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glNormal3dv
func Normal3dv(v *Double){
	C.glNormal3dv((*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glNormal3f
func Normal3f(nx Float, ny Float, nz Float){
	C.glNormal3f((C.GLfloat)(nx), (C.GLfloat)(ny), (C.GLfloat)(nz))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glNormal3fv
func Normal3fv(v *Float){
	C.glNormal3fv((*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glNormal3i
func Normal3i(nx Int, ny Int, nz Int){
	C.glNormal3i((C.GLint)(nx), (C.GLint)(ny), (C.GLint)(nz))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glNormal3iv
func Normal3iv(v *Int){
	C.glNormal3iv((*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glNormal3s
func Normal3s(nx Short, ny Short, nz Short){
	C.glNormal3s((C.GLshort)(nx), (C.GLshort)(ny), (C.GLshort)(nz))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glNormal3sv
func Normal3sv(v *Short){
	C.glNormal3sv((*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos2d
func RasterPos2d(x Double, y Double){
	C.glRasterPos2d((C.GLdouble)(x), (C.GLdouble)(y))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos2dv
func RasterPos2dv(v *Double){
	C.glRasterPos2dv((*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos2f
func RasterPos2f(x Float, y Float){
	C.glRasterPos2f((C.GLfloat)(x), (C.GLfloat)(y))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos2fv
func RasterPos2fv(v *Float){
	C.glRasterPos2fv((*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos2i
func RasterPos2i(x Int, y Int){
	C.glRasterPos2i((C.GLint)(x), (C.GLint)(y))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos2iv
func RasterPos2iv(v *Int){
	C.glRasterPos2iv((*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos2s
func RasterPos2s(x Short, y Short){
	C.glRasterPos2s((C.GLshort)(x), (C.GLshort)(y))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos2sv
func RasterPos2sv(v *Short){
	C.glRasterPos2sv((*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos3d
func RasterPos3d(x Double, y Double, z Double){
	C.glRasterPos3d((C.GLdouble)(x), (C.GLdouble)(y), (C.GLdouble)(z))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos3dv
func RasterPos3dv(v *Double){
	C.glRasterPos3dv((*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos3f
func RasterPos3f(x Float, y Float, z Float){
	C.glRasterPos3f((C.GLfloat)(x), (C.GLfloat)(y), (C.GLfloat)(z))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos3fv
func RasterPos3fv(v *Float){
	C.glRasterPos3fv((*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos3i
func RasterPos3i(x Int, y Int, z Int){
	C.glRasterPos3i((C.GLint)(x), (C.GLint)(y), (C.GLint)(z))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos3iv
func RasterPos3iv(v *Int){
	C.glRasterPos3iv((*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos3s
func RasterPos3s(x Short, y Short, z Short){
	C.glRasterPos3s((C.GLshort)(x), (C.GLshort)(y), (C.GLshort)(z))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos3sv
func RasterPos3sv(v *Short){
	C.glRasterPos3sv((*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos4d
func RasterPos4d(x Double, y Double, z Double, w Double){
	C.glRasterPos4d((C.GLdouble)(x), (C.GLdouble)(y), (C.GLdouble)(z), (C.GLdouble)(w))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos4dv
func RasterPos4dv(v *Double){
	C.glRasterPos4dv((*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos4f
func RasterPos4f(x Float, y Float, z Float, w Float){
	C.glRasterPos4f((C.GLfloat)(x), (C.GLfloat)(y), (C.GLfloat)(z), (C.GLfloat)(w))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos4fv
func RasterPos4fv(v *Float){
	C.glRasterPos4fv((*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos4i
func RasterPos4i(x Int, y Int, z Int, w Int){
	C.glRasterPos4i((C.GLint)(x), (C.GLint)(y), (C.GLint)(z), (C.GLint)(w))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos4iv
func RasterPos4iv(v *Int){
	C.glRasterPos4iv((*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos4s
func RasterPos4s(x Short, y Short, z Short, w Short){
	C.glRasterPos4s((C.GLshort)(x), (C.GLshort)(y), (C.GLshort)(z), (C.GLshort)(w))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRasterPos4sv
func RasterPos4sv(v *Short){
	C.glRasterPos4sv((*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRectd
func Rectd(x1 Double, y1 Double, x2 Double, y2 Double){
	C.glRectd((C.GLdouble)(x1), (C.GLdouble)(y1), (C.GLdouble)(x2), (C.GLdouble)(y2))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRectdv
func Rectdv(v1 *Double, v2 *Double){
	C.glRectdv((*C.GLdouble)(v1), (*C.GLdouble)(v2))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRectf
func Rectf(x1 Float, y1 Float, x2 Float, y2 Float){
	C.glRectf((C.GLfloat)(x1), (C.GLfloat)(y1), (C.GLfloat)(x2), (C.GLfloat)(y2))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRectfv
func Rectfv(v1 *Float, v2 *Float){
	C.glRectfv((*C.GLfloat)(v1), (*C.GLfloat)(v2))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRecti
func Recti(x1 Int, y1 Int, x2 Int, y2 Int){
	C.glRecti((C.GLint)(x1), (C.GLint)(y1), (C.GLint)(x2), (C.GLint)(y2))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRectiv
func Rectiv(v1 *Int, v2 *Int){
	C.glRectiv((*C.GLint)(v1), (*C.GLint)(v2))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRects
func Rects(x1 Short, y1 Short, x2 Short, y2 Short){
	C.glRects((C.GLshort)(x1), (C.GLshort)(y1), (C.GLshort)(x2), (C.GLshort)(y2))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRectsv
func Rectsv(v1 *Short, v2 *Short){
	C.glRectsv((*C.GLshort)(v1), (*C.GLshort)(v2))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord1d
func TexCoord1d(s Double){
	C.glTexCoord1d((C.GLdouble)(s))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord1dv
func TexCoord1dv(v *Double){
	C.glTexCoord1dv((*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord1f
func TexCoord1f(s Float){
	C.glTexCoord1f((C.GLfloat)(s))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord1fv
func TexCoord1fv(v *Float){
	C.glTexCoord1fv((*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord1i
func TexCoord1i(s Int){
	C.glTexCoord1i((C.GLint)(s))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord1iv
func TexCoord1iv(v *Int){
	C.glTexCoord1iv((*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord1s
func TexCoord1s(s Short){
	C.glTexCoord1s((C.GLshort)(s))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord1sv
func TexCoord1sv(v *Short){
	C.glTexCoord1sv((*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord2d
func TexCoord2d(s Double, t Double){
	C.glTexCoord2d((C.GLdouble)(s), (C.GLdouble)(t))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord2dv
func TexCoord2dv(v *Double){
	C.glTexCoord2dv((*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord2f
func TexCoord2f(s Float, t Float){
	C.glTexCoord2f((C.GLfloat)(s), (C.GLfloat)(t))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord2fv
func TexCoord2fv(v *Float){
	C.glTexCoord2fv((*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord2i
func TexCoord2i(s Int, t Int){
	C.glTexCoord2i((C.GLint)(s), (C.GLint)(t))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord2iv
func TexCoord2iv(v *Int){
	C.glTexCoord2iv((*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord2s
func TexCoord2s(s Short, t Short){
	C.glTexCoord2s((C.GLshort)(s), (C.GLshort)(t))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord2sv
func TexCoord2sv(v *Short){
	C.glTexCoord2sv((*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord3d
func TexCoord3d(s Double, t Double, r Double){
	C.glTexCoord3d((C.GLdouble)(s), (C.GLdouble)(t), (C.GLdouble)(r))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord3dv
func TexCoord3dv(v *Double){
	C.glTexCoord3dv((*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord3f
func TexCoord3f(s Float, t Float, r Float){
	C.glTexCoord3f((C.GLfloat)(s), (C.GLfloat)(t), (C.GLfloat)(r))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord3fv
func TexCoord3fv(v *Float){
	C.glTexCoord3fv((*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord3i
func TexCoord3i(s Int, t Int, r Int){
	C.glTexCoord3i((C.GLint)(s), (C.GLint)(t), (C.GLint)(r))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord3iv
func TexCoord3iv(v *Int){
	C.glTexCoord3iv((*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord3s
func TexCoord3s(s Short, t Short, r Short){
	C.glTexCoord3s((C.GLshort)(s), (C.GLshort)(t), (C.GLshort)(r))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord3sv
func TexCoord3sv(v *Short){
	C.glTexCoord3sv((*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord4d
func TexCoord4d(s Double, t Double, r Double, q Double){
	C.glTexCoord4d((C.GLdouble)(s), (C.GLdouble)(t), (C.GLdouble)(r), (C.GLdouble)(q))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord4dv
func TexCoord4dv(v *Double){
	C.glTexCoord4dv((*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord4f
func TexCoord4f(s Float, t Float, r Float, q Float){
	C.glTexCoord4f((C.GLfloat)(s), (C.GLfloat)(t), (C.GLfloat)(r), (C.GLfloat)(q))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord4fv
func TexCoord4fv(v *Float){
	C.glTexCoord4fv((*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord4i
func TexCoord4i(s Int, t Int, r Int, q Int){
	C.glTexCoord4i((C.GLint)(s), (C.GLint)(t), (C.GLint)(r), (C.GLint)(q))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord4iv
func TexCoord4iv(v *Int){
	C.glTexCoord4iv((*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord4s
func TexCoord4s(s Short, t Short, r Short, q Short){
	C.glTexCoord4s((C.GLshort)(s), (C.GLshort)(t), (C.GLshort)(r), (C.GLshort)(q))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoord4sv
func TexCoord4sv(v *Short){
	C.glTexCoord4sv((*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex2d
func Vertex2d(x Double, y Double){
	C.glVertex2d((C.GLdouble)(x), (C.GLdouble)(y))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex2dv
func Vertex2dv(v *Double){
	C.glVertex2dv((*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex2f
func Vertex2f(x Float, y Float){
	C.glVertex2f((C.GLfloat)(x), (C.GLfloat)(y))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex2fv
func Vertex2fv(v *Float){
	C.glVertex2fv((*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex2i
func Vertex2i(x Int, y Int){
	C.glVertex2i((C.GLint)(x), (C.GLint)(y))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex2iv
func Vertex2iv(v *Int){
	C.glVertex2iv((*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex2s
func Vertex2s(x Short, y Short){
	C.glVertex2s((C.GLshort)(x), (C.GLshort)(y))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex2sv
func Vertex2sv(v *Short){
	C.glVertex2sv((*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex3d
func Vertex3d(x Double, y Double, z Double){
	C.glVertex3d((C.GLdouble)(x), (C.GLdouble)(y), (C.GLdouble)(z))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex3dv
func Vertex3dv(v *Double){
	C.glVertex3dv((*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex3f
func Vertex3f(x Float, y Float, z Float){
	C.glVertex3f((C.GLfloat)(x), (C.GLfloat)(y), (C.GLfloat)(z))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex3fv
func Vertex3fv(v *Float){
	C.glVertex3fv((*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex3i
func Vertex3i(x Int, y Int, z Int){
	C.glVertex3i((C.GLint)(x), (C.GLint)(y), (C.GLint)(z))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex3iv
func Vertex3iv(v *Int){
	C.glVertex3iv((*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex3s
func Vertex3s(x Short, y Short, z Short){
	C.glVertex3s((C.GLshort)(x), (C.GLshort)(y), (C.GLshort)(z))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex3sv
func Vertex3sv(v *Short){
	C.glVertex3sv((*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex4d
func Vertex4d(x Double, y Double, z Double, w Double){
	C.glVertex4d((C.GLdouble)(x), (C.GLdouble)(y), (C.GLdouble)(z), (C.GLdouble)(w))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex4dv
func Vertex4dv(v *Double){
	C.glVertex4dv((*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex4f
func Vertex4f(x Float, y Float, z Float, w Float){
	C.glVertex4f((C.GLfloat)(x), (C.GLfloat)(y), (C.GLfloat)(z), (C.GLfloat)(w))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex4fv
func Vertex4fv(v *Float){
	C.glVertex4fv((*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex4i
func Vertex4i(x Int, y Int, z Int, w Int){
	C.glVertex4i((C.GLint)(x), (C.GLint)(y), (C.GLint)(z), (C.GLint)(w))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex4iv
func Vertex4iv(v *Int){
	C.glVertex4iv((*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex4s
func Vertex4s(x Short, y Short, z Short, w Short){
	C.glVertex4s((C.GLshort)(x), (C.GLshort)(y), (C.GLshort)(z), (C.GLshort)(w))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertex4sv
func Vertex4sv(v *Short){
	C.glVertex4sv((*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glClipPlane
func ClipPlane(plane Enum, equation *Double){
	C.glClipPlane((C.GLenum)(plane), (*C.GLdouble)(equation))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColorMaterial
func ColorMaterial(face Enum, mode Enum){
	C.glColorMaterial((C.GLenum)(face), (C.GLenum)(mode))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glFogf
func Fogf(pname Enum, param Float){
	C.glFogf((C.GLenum)(pname), (C.GLfloat)(param))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glFogfv
func Fogfv(pname Enum, params *Float){
	C.glFogfv((C.GLenum)(pname), (*C.GLfloat)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glFogi
func Fogi(pname Enum, param Int){
	C.glFogi((C.GLenum)(pname), (C.GLint)(param))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glFogiv
func Fogiv(pname Enum, params *Int){
	C.glFogiv((C.GLenum)(pname), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glLightf
func Lightf(light Enum, pname Enum, param Float){
	C.glLightf((C.GLenum)(light), (C.GLenum)(pname), (C.GLfloat)(param))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glLightfv
func Lightfv(light Enum, pname Enum, params *Float){
	C.glLightfv((C.GLenum)(light), (C.GLenum)(pname), (*C.GLfloat)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glLighti
func Lighti(light Enum, pname Enum, param Int){
	C.glLighti((C.GLenum)(light), (C.GLenum)(pname), (C.GLint)(param))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glLightiv
func Lightiv(light Enum, pname Enum, params *Int){
	C.glLightiv((C.GLenum)(light), (C.GLenum)(pname), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glLightModelf
func LightModelf(pname Enum, param Float){
	C.glLightModelf((C.GLenum)(pname), (C.GLfloat)(param))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glLightModelfv
func LightModelfv(pname Enum, params *Float){
	C.glLightModelfv((C.GLenum)(pname), (*C.GLfloat)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glLightModeli
func LightModeli(pname Enum, param Int){
	C.glLightModeli((C.GLenum)(pname), (C.GLint)(param))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glLightModeliv
func LightModeliv(pname Enum, params *Int){
	C.glLightModeliv((C.GLenum)(pname), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glLineStipple
func LineStipple(factor Int, pattern Ushort){
	C.glLineStipple((C.GLint)(factor), (C.GLushort)(pattern))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMaterialf
func Materialf(face Enum, pname Enum, param Float){
	C.glMaterialf((C.GLenum)(face), (C.GLenum)(pname), (C.GLfloat)(param))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMaterialfv
func Materialfv(face Enum, pname Enum, params *Float){
	C.glMaterialfv((C.GLenum)(face), (C.GLenum)(pname), (*C.GLfloat)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMateriali
func Materiali(face Enum, pname Enum, param Int){
	C.glMateriali((C.GLenum)(face), (C.GLenum)(pname), (C.GLint)(param))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMaterialiv
func Materialiv(face Enum, pname Enum, params *Int){
	C.glMaterialiv((C.GLenum)(face), (C.GLenum)(pname), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPolygonStipple
func PolygonStipple(mask *Ubyte){
	C.glPolygonStipple((*C.GLubyte)(mask))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glShadeModel
func ShadeModel(mode Enum){
	C.glShadeModel((C.GLenum)(mode))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexEnvf
func TexEnvf(target Enum, pname Enum, param Float){
	C.glTexEnvf((C.GLenum)(target), (C.GLenum)(pname), (C.GLfloat)(param))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexEnvfv
func TexEnvfv(target Enum, pname Enum, params *Float){
	C.glTexEnvfv((C.GLenum)(target), (C.GLenum)(pname), (*C.GLfloat)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexEnvi
func TexEnvi(target Enum, pname Enum, param Int){
	C.glTexEnvi((C.GLenum)(target), (C.GLenum)(pname), (C.GLint)(param))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexEnviv
func TexEnviv(target Enum, pname Enum, params *Int){
	C.glTexEnviv((C.GLenum)(target), (C.GLenum)(pname), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexGend
func TexGend(coord Enum, pname Enum, param Double){
	C.glTexGend((C.GLenum)(coord), (C.GLenum)(pname), (C.GLdouble)(param))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexGendv
func TexGendv(coord Enum, pname Enum, params *Double){
	C.glTexGendv((C.GLenum)(coord), (C.GLenum)(pname), (*C.GLdouble)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexGenf
func TexGenf(coord Enum, pname Enum, param Float){
	C.glTexGenf((C.GLenum)(coord), (C.GLenum)(pname), (C.GLfloat)(param))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexGenfv
func TexGenfv(coord Enum, pname Enum, params *Float){
	C.glTexGenfv((C.GLenum)(coord), (C.GLenum)(pname), (*C.GLfloat)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexGeni
func TexGeni(coord Enum, pname Enum, param Int){
	C.glTexGeni((C.GLenum)(coord), (C.GLenum)(pname), (C.GLint)(param))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexGeniv
func TexGeniv(coord Enum, pname Enum, params *Int){
	C.glTexGeniv((C.GLenum)(coord), (C.GLenum)(pname), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glFeedbackBuffer
func FeedbackBuffer(size Sizei, type_ Enum, buffer *Float){
	C.glFeedbackBuffer((C.GLsizei)(size), (C.GLenum)(type_), (*C.GLfloat)(buffer))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glSelectBuffer
func SelectBuffer(size Sizei, buffer *Uint){
	C.glSelectBuffer((C.GLsizei)(size), (*C.GLuint)(buffer))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRenderMode
func RenderMode(mode Enum) Int {
	return (Int)(C.glRenderMode((C.GLenum)(mode)))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glInitNames
func InitNames(){
	C.glInitNames()
}

// https://www.opengl.org/sdk/docs/man/xhtml/glLoadName
func LoadName(name Uint){
	C.glLoadName((C.GLuint)(name))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPassThrough
func PassThrough(token Float){
	C.glPassThrough((C.GLfloat)(token))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPopName
func PopName(){
	C.glPopName()
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPushName
func PushName(name Uint){
	C.glPushName((C.GLuint)(name))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glClearAccum
func ClearAccum(red Float, green Float, blue Float, alpha Float){
	C.glClearAccum((C.GLfloat)(red), (C.GLfloat)(green), (C.GLfloat)(blue), (C.GLfloat)(alpha))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glClearIndex
func ClearIndex(c Float){
	C.glClearIndex((C.GLfloat)(c))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glIndexMask
func IndexMask(mask Uint){
	C.glIndexMask((C.GLuint)(mask))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glAccum
func Accum(op Enum, value Float){
	C.glAccum((C.GLenum)(op), (C.GLfloat)(value))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPopAttrib
func PopAttrib(){
	C.glPopAttrib()
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPushAttrib
func PushAttrib(mask Bitfield){
	C.glPushAttrib((C.GLbitfield)(mask))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMap1d
func Map1d(target Enum, u1 Double, u2 Double, stride Int, order Int, points *Double){
	C.glMap1d((C.GLenum)(target), (C.GLdouble)(u1), (C.GLdouble)(u2), (C.GLint)(stride), (C.GLint)(order), (*C.GLdouble)(points))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMap1f
func Map1f(target Enum, u1 Float, u2 Float, stride Int, order Int, points *Float){
	C.glMap1f((C.GLenum)(target), (C.GLfloat)(u1), (C.GLfloat)(u2), (C.GLint)(stride), (C.GLint)(order), (*C.GLfloat)(points))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMap2d
func Map2d(target Enum, u1 Double, u2 Double, ustride Int, uorder Int, v1 Double, v2 Double, vstride Int, vorder Int, points *Double){
	C.glMap2d((C.GLenum)(target), (C.GLdouble)(u1), (C.GLdouble)(u2), (C.GLint)(ustride), (C.GLint)(uorder), (C.GLdouble)(v1), (C.GLdouble)(v2), (C.GLint)(vstride), (C.GLint)(vorder), (*C.GLdouble)(points))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMap2f
func Map2f(target Enum, u1 Float, u2 Float, ustride Int, uorder Int, v1 Float, v2 Float, vstride Int, vorder Int, points *Float){
	C.glMap2f((C.GLenum)(target), (C.GLfloat)(u1), (C.GLfloat)(u2), (C.GLint)(ustride), (C.GLint)(uorder), (C.GLfloat)(v1), (C.GLfloat)(v2), (C.GLint)(vstride), (C.GLint)(vorder), (*C.GLfloat)(points))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMapGrid1d
func MapGrid1d(un Int, u1 Double, u2 Double){
	C.glMapGrid1d((C.GLint)(un), (C.GLdouble)(u1), (C.GLdouble)(u2))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMapGrid1f
func MapGrid1f(un Int, u1 Float, u2 Float){
	C.glMapGrid1f((C.GLint)(un), (C.GLfloat)(u1), (C.GLfloat)(u2))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMapGrid2d
func MapGrid2d(un Int, u1 Double, u2 Double, vn Int, v1 Double, v2 Double){
	C.glMapGrid2d((C.GLint)(un), (C.GLdouble)(u1), (C.GLdouble)(u2), (C.GLint)(vn), (C.GLdouble)(v1), (C.GLdouble)(v2))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMapGrid2f
func MapGrid2f(un Int, u1 Float, u2 Float, vn Int, v1 Float, v2 Float){
	C.glMapGrid2f((C.GLint)(un), (C.GLfloat)(u1), (C.GLfloat)(u2), (C.GLint)(vn), (C.GLfloat)(v1), (C.GLfloat)(v2))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glEvalCoord1d
func EvalCoord1d(u Double){
	C.glEvalCoord1d((C.GLdouble)(u))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glEvalCoord1dv
func EvalCoord1dv(u *Double){
	C.glEvalCoord1dv((*C.GLdouble)(u))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glEvalCoord1f
func EvalCoord1f(u Float){
	C.glEvalCoord1f((C.GLfloat)(u))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glEvalCoord1fv
func EvalCoord1fv(u *Float){
	C.glEvalCoord1fv((*C.GLfloat)(u))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glEvalCoord2d
func EvalCoord2d(u Double, v Double){
	C.glEvalCoord2d((C.GLdouble)(u), (C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glEvalCoord2dv
func EvalCoord2dv(u *Double){
	C.glEvalCoord2dv((*C.GLdouble)(u))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glEvalCoord2f
func EvalCoord2f(u Float, v Float){
	C.glEvalCoord2f((C.GLfloat)(u), (C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glEvalCoord2fv
func EvalCoord2fv(u *Float){
	C.glEvalCoord2fv((*C.GLfloat)(u))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glEvalMesh1
func EvalMesh1(mode Enum, i1 Int, i2 Int){
	C.glEvalMesh1((C.GLenum)(mode), (C.GLint)(i1), (C.GLint)(i2))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glEvalPoint1
func EvalPoint1(i Int){
	C.glEvalPoint1((C.GLint)(i))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glEvalMesh2
func EvalMesh2(mode Enum, i1 Int, i2 Int, j1 Int, j2 Int){
	C.glEvalMesh2((C.GLenum)(mode), (C.GLint)(i1), (C.GLint)(i2), (C.GLint)(j1), (C.GLint)(j2))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glEvalPoint2
func EvalPoint2(i Int, j Int){
	C.glEvalPoint2((C.GLint)(i), (C.GLint)(j))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glAlphaFunc
func AlphaFunc(func_ Enum, ref Float){
	C.glAlphaFunc((C.GLenum)(func_), (C.GLfloat)(ref))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPixelZoom
func PixelZoom(xfactor Float, yfactor Float){
	C.glPixelZoom((C.GLfloat)(xfactor), (C.GLfloat)(yfactor))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPixelTransferf
func PixelTransferf(pname Enum, param Float){
	C.glPixelTransferf((C.GLenum)(pname), (C.GLfloat)(param))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPixelTransferi
func PixelTransferi(pname Enum, param Int){
	C.glPixelTransferi((C.GLenum)(pname), (C.GLint)(param))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPixelMapfv
func PixelMapfv(map_ Enum, mapsize Sizei, values *Float){
	C.glPixelMapfv((C.GLenum)(map_), (C.GLsizei)(mapsize), (*C.GLfloat)(values))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPixelMapuiv
func PixelMapuiv(map_ Enum, mapsize Sizei, values *Uint){
	C.glPixelMapuiv((C.GLenum)(map_), (C.GLsizei)(mapsize), (*C.GLuint)(values))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPixelMapusv
func PixelMapusv(map_ Enum, mapsize Sizei, values *Ushort){
	C.glPixelMapusv((C.GLenum)(map_), (C.GLsizei)(mapsize), (*C.GLushort)(values))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glCopyPixels
func CopyPixels(x Int, y Int, width Sizei, height Sizei, type_ Enum){
	C.glCopyPixels((C.GLint)(x), (C.GLint)(y), (C.GLsizei)(width), (C.GLsizei)(height), (C.GLenum)(type_))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glDrawPixels
func DrawPixels(width Sizei, height Sizei, format Enum, type_ Enum, pixels Pointer){
	C.glDrawPixels((C.GLsizei)(width), (C.GLsizei)(height), (C.GLenum)(format), (C.GLenum)(type_), (unsafe.Pointer)(pixels))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetClipPlane
func GetClipPlane(plane Enum, equation *Double){
	C.glGetClipPlane((C.GLenum)(plane), (*C.GLdouble)(equation))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetLightfv
func GetLightfv(light Enum, pname Enum, params *Float){
	C.glGetLightfv((C.GLenum)(light), (C.GLenum)(pname), (*C.GLfloat)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetLightiv
func GetLightiv(light Enum, pname Enum, params *Int){
	C.glGetLightiv((C.GLenum)(light), (C.GLenum)(pname), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetMapdv
func GetMapdv(target Enum, query Enum, v *Double){
	C.glGetMapdv((C.GLenum)(target), (C.GLenum)(query), (*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetMapfv
func GetMapfv(target Enum, query Enum, v *Float){
	C.glGetMapfv((C.GLenum)(target), (C.GLenum)(query), (*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetMapiv
func GetMapiv(target Enum, query Enum, v *Int){
	C.glGetMapiv((C.GLenum)(target), (C.GLenum)(query), (*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetMaterialfv
func GetMaterialfv(face Enum, pname Enum, params *Float){
	C.glGetMaterialfv((C.GLenum)(face), (C.GLenum)(pname), (*C.GLfloat)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetMaterialiv
func GetMaterialiv(face Enum, pname Enum, params *Int){
	C.glGetMaterialiv((C.GLenum)(face), (C.GLenum)(pname), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetPixelMapfv
func GetPixelMapfv(map_ Enum, values *Float){
	C.glGetPixelMapfv((C.GLenum)(map_), (*C.GLfloat)(values))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetPixelMapuiv
func GetPixelMapuiv(map_ Enum, values *Uint){
	C.glGetPixelMapuiv((C.GLenum)(map_), (*C.GLuint)(values))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetPixelMapusv
func GetPixelMapusv(map_ Enum, values *Ushort){
	C.glGetPixelMapusv((C.GLenum)(map_), (*C.GLushort)(values))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetPolygonStipple
func GetPolygonStipple(mask *Ubyte){
	C.glGetPolygonStipple((*C.GLubyte)(mask))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetTexEnvfv
func GetTexEnvfv(target Enum, pname Enum, params *Float){
	C.glGetTexEnvfv((C.GLenum)(target), (C.GLenum)(pname), (*C.GLfloat)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetTexEnviv
func GetTexEnviv(target Enum, pname Enum, params *Int){
	C.glGetTexEnviv((C.GLenum)(target), (C.GLenum)(pname), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetTexGendv
func GetTexGendv(coord Enum, pname Enum, params *Double){
	C.glGetTexGendv((C.GLenum)(coord), (C.GLenum)(pname), (*C.GLdouble)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetTexGenfv
func GetTexGenfv(coord Enum, pname Enum, params *Float){
	C.glGetTexGenfv((C.GLenum)(coord), (C.GLenum)(pname), (*C.GLfloat)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetTexGeniv
func GetTexGeniv(coord Enum, pname Enum, params *Int){
	C.glGetTexGeniv((C.GLenum)(coord), (C.GLenum)(pname), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glIsList
func IsList(list Uint) Boolean {
	return (Boolean)(C.glIsList((C.GLuint)(list)))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glFrustum
func Frustum(left Double, right Double, bottom Double, top Double, zNear Double, zFar Double){
	C.glFrustum((C.GLdouble)(left), (C.GLdouble)(right), (C.GLdouble)(bottom), (C.GLdouble)(top), (C.GLdouble)(zNear), (C.GLdouble)(zFar))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glLoadIdentity
func LoadIdentity(){
	C.glLoadIdentity()
}

// https://www.opengl.org/sdk/docs/man/xhtml/glLoadMatrixf
func LoadMatrixf(m *Float){
	C.glLoadMatrixf((*C.GLfloat)(m))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glLoadMatrixd
func LoadMatrixd(m *Double){
	C.glLoadMatrixd((*C.GLdouble)(m))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMatrixMode
func MatrixMode(mode Enum){
	C.glMatrixMode((C.GLenum)(mode))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultMatrixf
func MultMatrixf(m *Float){
	C.glMultMatrixf((*C.GLfloat)(m))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultMatrixd
func MultMatrixd(m *Double){
	C.glMultMatrixd((*C.GLdouble)(m))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glOrtho
func Ortho(left Double, right Double, bottom Double, top Double, zNear Double, zFar Double){
	C.glOrtho((C.GLdouble)(left), (C.GLdouble)(right), (C.GLdouble)(bottom), (C.GLdouble)(top), (C.GLdouble)(zNear), (C.GLdouble)(zFar))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPopMatrix
func PopMatrix(){
	C.glPopMatrix()
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPushMatrix
func PushMatrix(){
	C.glPushMatrix()
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRotated
func Rotated(angle Double, x Double, y Double, z Double){
	C.glRotated((C.GLdouble)(angle), (C.GLdouble)(x), (C.GLdouble)(y), (C.GLdouble)(z))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glRotatef
func Rotatef(angle Float, x Float, y Float, z Float){
	C.glRotatef((C.GLfloat)(angle), (C.GLfloat)(x), (C.GLfloat)(y), (C.GLfloat)(z))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glScaled
func Scaled(x Double, y Double, z Double){
	C.glScaled((C.GLdouble)(x), (C.GLdouble)(y), (C.GLdouble)(z))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glScalef
func Scalef(x Float, y Float, z Float){
	C.glScalef((C.GLfloat)(x), (C.GLfloat)(y), (C.GLfloat)(z))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTranslated
func Translated(x Double, y Double, z Double){
	C.glTranslated((C.GLdouble)(x), (C.GLdouble)(y), (C.GLdouble)(z))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTranslatef
func Translatef(x Float, y Float, z Float){
	C.glTranslatef((C.GLfloat)(x), (C.GLfloat)(y), (C.GLfloat)(z))
}

func initGlVersion10() error {
	if ret := C.initGL_VERSION_1_0(); ret == 0 {
		return errors.New("unable to initialize GL_VERSION_1_0")
	}
	return nil
}

// ----------------------------------------------------------------------------
// GL_VERSION_1_1

type (
	Clampf C.GLclampf
	Clampd C.GLclampd
)
const (
	DEPTH_BUFFER_BIT = 0x00000100
	STENCIL_BUFFER_BIT = 0x00000400
	COLOR_BUFFER_BIT = 0x00004000
	FALSE = 0
	TRUE = 1
	POINTS = 0x0000
	LINES = 0x0001
	LINE_LOOP = 0x0002
	LINE_STRIP = 0x0003
	TRIANGLES = 0x0004
	TRIANGLE_STRIP = 0x0005
	TRIANGLE_FAN = 0x0006
	QUADS = 0x0007
	NEVER = 0x0200
	LESS = 0x0201
	EQUAL = 0x0202
	LEQUAL = 0x0203
	GREATER = 0x0204
	NOTEQUAL = 0x0205
	GEQUAL = 0x0206
	ALWAYS = 0x0207
	ZERO = 0
	ONE = 1
	SRC_COLOR = 0x0300
	ONE_MINUS_SRC_COLOR = 0x0301
	SRC_ALPHA = 0x0302
	ONE_MINUS_SRC_ALPHA = 0x0303
	DST_ALPHA = 0x0304
	ONE_MINUS_DST_ALPHA = 0x0305
	DST_COLOR = 0x0306
	ONE_MINUS_DST_COLOR = 0x0307
	SRC_ALPHA_SATURATE = 0x0308
	NONE = 0
	FRONT_LEFT = 0x0400
	FRONT_RIGHT = 0x0401
	BACK_LEFT = 0x0402
	BACK_RIGHT = 0x0403
	FRONT = 0x0404
	BACK = 0x0405
	LEFT = 0x0406
	RIGHT = 0x0407
	FRONT_AND_BACK = 0x0408
	NO_ERROR = 0
	INVALID_ENUM = 0x0500
	INVALID_VALUE = 0x0501
	INVALID_OPERATION = 0x0502
	OUT_OF_MEMORY = 0x0505
	CW = 0x0900
	CCW = 0x0901
	POINT_SIZE = 0x0B11
	POINT_SIZE_RANGE = 0x0B12
	POINT_SIZE_GRANULARITY = 0x0B13
	LINE_SMOOTH = 0x0B20
	LINE_WIDTH = 0x0B21
	LINE_WIDTH_RANGE = 0x0B22
	LINE_WIDTH_GRANULARITY = 0x0B23
	POLYGON_MODE = 0x0B40
	POLYGON_SMOOTH = 0x0B41
	CULL_FACE = 0x0B44
	CULL_FACE_MODE = 0x0B45
	FRONT_FACE = 0x0B46
	DEPTH_RANGE = 0x0B70
	DEPTH_TEST = 0x0B71
	DEPTH_WRITEMASK = 0x0B72
	DEPTH_CLEAR_VALUE = 0x0B73
	DEPTH_FUNC = 0x0B74
	STENCIL_TEST = 0x0B90
	STENCIL_CLEAR_VALUE = 0x0B91
	STENCIL_FUNC = 0x0B92
	STENCIL_VALUE_MASK = 0x0B93
	STENCIL_FAIL = 0x0B94
	STENCIL_PASS_DEPTH_FAIL = 0x0B95
	STENCIL_PASS_DEPTH_PASS = 0x0B96
	STENCIL_REF = 0x0B97
	STENCIL_WRITEMASK = 0x0B98
	VIEWPORT = 0x0BA2
	DITHER = 0x0BD0
	BLEND_DST = 0x0BE0
	BLEND_SRC = 0x0BE1
	BLEND = 0x0BE2
	LOGIC_OP_MODE = 0x0BF0
	COLOR_LOGIC_OP = 0x0BF2
	DRAW_BUFFER = 0x0C01
	READ_BUFFER = 0x0C02
	SCISSOR_BOX = 0x0C10
	SCISSOR_TEST = 0x0C11
	COLOR_CLEAR_VALUE = 0x0C22
	COLOR_WRITEMASK = 0x0C23
	DOUBLEBUFFER = 0x0C32
	STEREO = 0x0C33
	LINE_SMOOTH_HINT = 0x0C52
	POLYGON_SMOOTH_HINT = 0x0C53
	UNPACK_SWAP_BYTES = 0x0CF0
	UNPACK_LSB_FIRST = 0x0CF1
	UNPACK_ROW_LENGTH = 0x0CF2
	UNPACK_SKIP_ROWS = 0x0CF3
	UNPACK_SKIP_PIXELS = 0x0CF4
	UNPACK_ALIGNMENT = 0x0CF5
	PACK_SWAP_BYTES = 0x0D00
	PACK_LSB_FIRST = 0x0D01
	PACK_ROW_LENGTH = 0x0D02
	PACK_SKIP_ROWS = 0x0D03
	PACK_SKIP_PIXELS = 0x0D04
	PACK_ALIGNMENT = 0x0D05
	MAX_TEXTURE_SIZE = 0x0D33
	MAX_VIEWPORT_DIMS = 0x0D3A
	SUBPIXEL_BITS = 0x0D50
	TEXTURE_1D = 0x0DE0
	TEXTURE_2D = 0x0DE1
	POLYGON_OFFSET_UNITS = 0x2A00
	POLYGON_OFFSET_POINT = 0x2A01
	POLYGON_OFFSET_LINE = 0x2A02
	POLYGON_OFFSET_FILL = 0x8037
	POLYGON_OFFSET_FACTOR = 0x8038
	TEXTURE_BINDING_1D = 0x8068
	TEXTURE_BINDING_2D = 0x8069
	TEXTURE_WIDTH = 0x1000
	TEXTURE_HEIGHT = 0x1001
	TEXTURE_INTERNAL_FORMAT = 0x1003
	TEXTURE_BORDER_COLOR = 0x1004
	TEXTURE_RED_SIZE = 0x805C
	TEXTURE_GREEN_SIZE = 0x805D
	TEXTURE_BLUE_SIZE = 0x805E
	TEXTURE_ALPHA_SIZE = 0x805F
	DONT_CARE = 0x1100
	FASTEST = 0x1101
	NICEST = 0x1102
	BYTE = 0x1400
	UNSIGNED_BYTE = 0x1401
	SHORT = 0x1402
	UNSIGNED_SHORT = 0x1403
	INT = 0x1404
	UNSIGNED_INT = 0x1405
	FLOAT = 0x1406
	DOUBLE = 0x140A
	STACK_OVERFLOW = 0x0503
	STACK_UNDERFLOW = 0x0504
	CLEAR = 0x1500
	AND = 0x1501
	AND_REVERSE = 0x1502
	COPY = 0x1503
	AND_INVERTED = 0x1504
	NOOP = 0x1505
	XOR = 0x1506
	OR = 0x1507
	NOR = 0x1508
	EQUIV = 0x1509
	INVERT = 0x150A
	OR_REVERSE = 0x150B
	COPY_INVERTED = 0x150C
	OR_INVERTED = 0x150D
	NAND = 0x150E
	SET = 0x150F
	TEXTURE = 0x1702
	COLOR = 0x1800
	DEPTH = 0x1801
	STENCIL = 0x1802
	STENCIL_INDEX = 0x1901
	DEPTH_COMPONENT = 0x1902
	RED = 0x1903
	GREEN = 0x1904
	BLUE = 0x1905
	ALPHA = 0x1906
	RGB = 0x1907
	RGBA = 0x1908
	POINT = 0x1B00
	LINE = 0x1B01
	FILL = 0x1B02
	KEEP = 0x1E00
	REPLACE = 0x1E01
	INCR = 0x1E02
	DECR = 0x1E03
	VENDOR = 0x1F00
	RENDERER = 0x1F01
	VERSION = 0x1F02
	EXTENSIONS = 0x1F03
	NEAREST = 0x2600
	LINEAR = 0x2601
	NEAREST_MIPMAP_NEAREST = 0x2700
	LINEAR_MIPMAP_NEAREST = 0x2701
	NEAREST_MIPMAP_LINEAR = 0x2702
	LINEAR_MIPMAP_LINEAR = 0x2703
	TEXTURE_MAG_FILTER = 0x2800
	TEXTURE_MIN_FILTER = 0x2801
	TEXTURE_WRAP_S = 0x2802
	TEXTURE_WRAP_T = 0x2803
	PROXY_TEXTURE_1D = 0x8063
	PROXY_TEXTURE_2D = 0x8064
	REPEAT = 0x2901
	R3_G3_B2 = 0x2A10
	RGB4 = 0x804F
	RGB5 = 0x8050
	RGB8 = 0x8051
	RGB10 = 0x8052
	RGB12 = 0x8053
	RGB16 = 0x8054
	RGBA2 = 0x8055
	RGBA4 = 0x8056
	RGB5_A1 = 0x8057
	RGBA8 = 0x8058
	RGB10_A2 = 0x8059
	RGBA12 = 0x805A
	RGBA16 = 0x805B
	CURRENT_BIT = 0x00000001
	POINT_BIT = 0x00000002
	LINE_BIT = 0x00000004
	POLYGON_BIT = 0x00000008
	POLYGON_STIPPLE_BIT = 0x00000010
	PIXEL_MODE_BIT = 0x00000020
	LIGHTING_BIT = 0x00000040
	FOG_BIT = 0x00000080
	ACCUM_BUFFER_BIT = 0x00000200
	VIEWPORT_BIT = 0x00000800
	TRANSFORM_BIT = 0x00001000
	ENABLE_BIT = 0x00002000
	HINT_BIT = 0x00008000
	EVAL_BIT = 0x00010000
	LIST_BIT = 0x00020000
	TEXTURE_BIT = 0x00040000
	SCISSOR_BIT = 0x00080000
	ALL_ATTRIB_BITS = 0xFFFFFFFF
	CLIENT_PIXEL_STORE_BIT = 0x00000001
	CLIENT_VERTEX_ARRAY_BIT = 0x00000002
	CLIENT_ALL_ATTRIB_BITS = 0xFFFFFFFF
	QUAD_STRIP = 0x0008
	POLYGON = 0x0009
	ACCUM = 0x0100
	LOAD = 0x0101
	RETURN = 0x0102
	MULT = 0x0103
	ADD = 0x0104
	AUX0 = 0x0409
	AUX1 = 0x040A
	AUX2 = 0x040B
	AUX3 = 0x040C
	X2D = 0x0600
	X3D = 0x0601
	X3D_COLOR = 0x0602
	X3D_COLOR_TEXTURE = 0x0603
	X4D_COLOR_TEXTURE = 0x0604
	PASS_THROUGH_TOKEN = 0x0700
	POINT_TOKEN = 0x0701
	LINE_TOKEN = 0x0702
	POLYGON_TOKEN = 0x0703
	BITMAP_TOKEN = 0x0704
	DRAW_PIXEL_TOKEN = 0x0705
	COPY_PIXEL_TOKEN = 0x0706
	LINE_RESET_TOKEN = 0x0707
	EXP = 0x0800
	EXP2 = 0x0801
	COEFF = 0x0A00
	ORDER = 0x0A01
	DOMAIN = 0x0A02
	PIXEL_MAP_I_TO_I = 0x0C70
	PIXEL_MAP_S_TO_S = 0x0C71
	PIXEL_MAP_I_TO_R = 0x0C72
	PIXEL_MAP_I_TO_G = 0x0C73
	PIXEL_MAP_I_TO_B = 0x0C74
	PIXEL_MAP_I_TO_A = 0x0C75
	PIXEL_MAP_R_TO_R = 0x0C76
	PIXEL_MAP_G_TO_G = 0x0C77
	PIXEL_MAP_B_TO_B = 0x0C78
	PIXEL_MAP_A_TO_A = 0x0C79
	VERTEX_ARRAY_POINTER = 0x808E
	NORMAL_ARRAY_POINTER = 0x808F
	COLOR_ARRAY_POINTER = 0x8090
	INDEX_ARRAY_POINTER = 0x8091
	TEXTURE_COORD_ARRAY_POINTER = 0x8092
	EDGE_FLAG_ARRAY_POINTER = 0x8093
	FEEDBACK_BUFFER_POINTER = 0x0DF0
	SELECTION_BUFFER_POINTER = 0x0DF3
	CURRENT_COLOR = 0x0B00
	CURRENT_INDEX = 0x0B01
	CURRENT_NORMAL = 0x0B02
	CURRENT_TEXTURE_COORDS = 0x0B03
	CURRENT_RASTER_COLOR = 0x0B04
	CURRENT_RASTER_INDEX = 0x0B05
	CURRENT_RASTER_TEXTURE_COORDS = 0x0B06
	CURRENT_RASTER_POSITION = 0x0B07
	CURRENT_RASTER_POSITION_VALID = 0x0B08
	CURRENT_RASTER_DISTANCE = 0x0B09
	POINT_SMOOTH = 0x0B10
	LINE_STIPPLE = 0x0B24
	LINE_STIPPLE_PATTERN = 0x0B25
	LINE_STIPPLE_REPEAT = 0x0B26
	LIST_MODE = 0x0B30
	MAX_LIST_NESTING = 0x0B31
	LIST_BASE = 0x0B32
	LIST_INDEX = 0x0B33
	POLYGON_STIPPLE = 0x0B42
	EDGE_FLAG = 0x0B43
	LIGHTING = 0x0B50
	LIGHT_MODEL_LOCAL_VIEWER = 0x0B51
	LIGHT_MODEL_TWO_SIDE = 0x0B52
	LIGHT_MODEL_AMBIENT = 0x0B53
	SHADE_MODEL = 0x0B54
	COLOR_MATERIAL_FACE = 0x0B55
	COLOR_MATERIAL_PARAMETER = 0x0B56
	COLOR_MATERIAL = 0x0B57
	FOG = 0x0B60
	FOG_INDEX = 0x0B61
	FOG_DENSITY = 0x0B62
	FOG_START = 0x0B63
	FOG_END = 0x0B64
	FOG_MODE = 0x0B65
	FOG_COLOR = 0x0B66
	ACCUM_CLEAR_VALUE = 0x0B80
	MATRIX_MODE = 0x0BA0
	NORMALIZE = 0x0BA1
	MODELVIEW_STACK_DEPTH = 0x0BA3
	PROJECTION_STACK_DEPTH = 0x0BA4
	TEXTURE_STACK_DEPTH = 0x0BA5
	MODELVIEW_MATRIX = 0x0BA6
	PROJECTION_MATRIX = 0x0BA7
	TEXTURE_MATRIX = 0x0BA8
	ATTRIB_STACK_DEPTH = 0x0BB0
	CLIENT_ATTRIB_STACK_DEPTH = 0x0BB1
	ALPHA_TEST = 0x0BC0
	ALPHA_TEST_FUNC = 0x0BC1
	ALPHA_TEST_REF = 0x0BC2
	INDEX_LOGIC_OP = 0x0BF1
	LOGIC_OP = 0x0BF1
	AUX_BUFFERS = 0x0C00
	INDEX_CLEAR_VALUE = 0x0C20
	INDEX_WRITEMASK = 0x0C21
	INDEX_MODE = 0x0C30
	RGBA_MODE = 0x0C31
	RENDER_MODE = 0x0C40
	PERSPECTIVE_CORRECTION_HINT = 0x0C50
	POINT_SMOOTH_HINT = 0x0C51
	FOG_HINT = 0x0C54
	TEXTURE_GEN_S = 0x0C60
	TEXTURE_GEN_T = 0x0C61
	TEXTURE_GEN_R = 0x0C62
	TEXTURE_GEN_Q = 0x0C63
	PIXEL_MAP_I_TO_I_SIZE = 0x0CB0
	PIXEL_MAP_S_TO_S_SIZE = 0x0CB1
	PIXEL_MAP_I_TO_R_SIZE = 0x0CB2
	PIXEL_MAP_I_TO_G_SIZE = 0x0CB3
	PIXEL_MAP_I_TO_B_SIZE = 0x0CB4
	PIXEL_MAP_I_TO_A_SIZE = 0x0CB5
	PIXEL_MAP_R_TO_R_SIZE = 0x0CB6
	PIXEL_MAP_G_TO_G_SIZE = 0x0CB7
	PIXEL_MAP_B_TO_B_SIZE = 0x0CB8
	PIXEL_MAP_A_TO_A_SIZE = 0x0CB9
	MAP_COLOR = 0x0D10
	MAP_STENCIL = 0x0D11
	INDEX_SHIFT = 0x0D12
	INDEX_OFFSET = 0x0D13
	RED_SCALE = 0x0D14
	RED_BIAS = 0x0D15
	ZOOM_X = 0x0D16
	ZOOM_Y = 0x0D17
	GREEN_SCALE = 0x0D18
	GREEN_BIAS = 0x0D19
	BLUE_SCALE = 0x0D1A
	BLUE_BIAS = 0x0D1B
	ALPHA_SCALE = 0x0D1C
	ALPHA_BIAS = 0x0D1D
	DEPTH_SCALE = 0x0D1E
	DEPTH_BIAS = 0x0D1F
	MAX_EVAL_ORDER = 0x0D30
	MAX_LIGHTS = 0x0D31
	MAX_CLIP_PLANES = 0x0D32
	MAX_PIXEL_MAP_TABLE = 0x0D34
	MAX_ATTRIB_STACK_DEPTH = 0x0D35
	MAX_MODELVIEW_STACK_DEPTH = 0x0D36
	MAX_NAME_STACK_DEPTH = 0x0D37
	MAX_PROJECTION_STACK_DEPTH = 0x0D38
	MAX_TEXTURE_STACK_DEPTH = 0x0D39
	MAX_CLIENT_ATTRIB_STACK_DEPTH = 0x0D3B
	INDEX_BITS = 0x0D51
	RED_BITS = 0x0D52
	GREEN_BITS = 0x0D53
	BLUE_BITS = 0x0D54
	ALPHA_BITS = 0x0D55
	DEPTH_BITS = 0x0D56
	STENCIL_BITS = 0x0D57
	ACCUM_RED_BITS = 0x0D58
	ACCUM_GREEN_BITS = 0x0D59
	ACCUM_BLUE_BITS = 0x0D5A
	ACCUM_ALPHA_BITS = 0x0D5B
	NAME_STACK_DEPTH = 0x0D70
	AUTO_NORMAL = 0x0D80
	MAP1_COLOR_4 = 0x0D90
	MAP1_INDEX = 0x0D91
	MAP1_NORMAL = 0x0D92
	MAP1_TEXTURE_COORD_1 = 0x0D93
	MAP1_TEXTURE_COORD_2 = 0x0D94
	MAP1_TEXTURE_COORD_3 = 0x0D95
	MAP1_TEXTURE_COORD_4 = 0x0D96
	MAP1_VERTEX_3 = 0x0D97
	MAP1_VERTEX_4 = 0x0D98
	MAP2_COLOR_4 = 0x0DB0
	MAP2_INDEX = 0x0DB1
	MAP2_NORMAL = 0x0DB2
	MAP2_TEXTURE_COORD_1 = 0x0DB3
	MAP2_TEXTURE_COORD_2 = 0x0DB4
	MAP2_TEXTURE_COORD_3 = 0x0DB5
	MAP2_TEXTURE_COORD_4 = 0x0DB6
	MAP2_VERTEX_3 = 0x0DB7
	MAP2_VERTEX_4 = 0x0DB8
	MAP1_GRID_DOMAIN = 0x0DD0
	MAP1_GRID_SEGMENTS = 0x0DD1
	MAP2_GRID_DOMAIN = 0x0DD2
	MAP2_GRID_SEGMENTS = 0x0DD3
	FEEDBACK_BUFFER_SIZE = 0x0DF1
	FEEDBACK_BUFFER_TYPE = 0x0DF2
	SELECTION_BUFFER_SIZE = 0x0DF4
	VERTEX_ARRAY = 0x8074
	NORMAL_ARRAY = 0x8075
	COLOR_ARRAY = 0x8076
	INDEX_ARRAY = 0x8077
	TEXTURE_COORD_ARRAY = 0x8078
	EDGE_FLAG_ARRAY = 0x8079
	VERTEX_ARRAY_SIZE = 0x807A
	VERTEX_ARRAY_TYPE = 0x807B
	VERTEX_ARRAY_STRIDE = 0x807C
	NORMAL_ARRAY_TYPE = 0x807E
	NORMAL_ARRAY_STRIDE = 0x807F
	COLOR_ARRAY_SIZE = 0x8081
	COLOR_ARRAY_TYPE = 0x8082
	COLOR_ARRAY_STRIDE = 0x8083
	INDEX_ARRAY_TYPE = 0x8085
	INDEX_ARRAY_STRIDE = 0x8086
	TEXTURE_COORD_ARRAY_SIZE = 0x8088
	TEXTURE_COORD_ARRAY_TYPE = 0x8089
	TEXTURE_COORD_ARRAY_STRIDE = 0x808A
	EDGE_FLAG_ARRAY_STRIDE = 0x808C
	TEXTURE_COMPONENTS = 0x1003
	TEXTURE_BORDER = 0x1005
	TEXTURE_LUMINANCE_SIZE = 0x8060
	TEXTURE_INTENSITY_SIZE = 0x8061
	TEXTURE_PRIORITY = 0x8066
	TEXTURE_RESIDENT = 0x8067
	AMBIENT = 0x1200
	DIFFUSE = 0x1201
	SPECULAR = 0x1202
	POSITION = 0x1203
	SPOT_DIRECTION = 0x1204
	SPOT_EXPONENT = 0x1205
	SPOT_CUTOFF = 0x1206
	CONSTANT_ATTENUATION = 0x1207
	LINEAR_ATTENUATION = 0x1208
	QUADRATIC_ATTENUATION = 0x1209
	COMPILE = 0x1300
	COMPILE_AND_EXECUTE = 0x1301
	X2_BYTES = 0x1407
	X3_BYTES = 0x1408
	X4_BYTES = 0x1409
	EMISSION = 0x1600
	SHININESS = 0x1601
	AMBIENT_AND_DIFFUSE = 0x1602
	COLOR_INDEXES = 0x1603
	MODELVIEW = 0x1700
	PROJECTION = 0x1701
	COLOR_INDEX = 0x1900
	LUMINANCE = 0x1909
	LUMINANCE_ALPHA = 0x190A
	BITMAP = 0x1A00
	RENDER = 0x1C00
	FEEDBACK = 0x1C01
	SELECT = 0x1C02
	FLAT = 0x1D00
	SMOOTH = 0x1D01
	S = 0x2000
	T = 0x2001
	R = 0x2002
	Q = 0x2003
	MODULATE = 0x2100
	DECAL = 0x2101
	TEXTURE_ENV_MODE = 0x2200
	TEXTURE_ENV_COLOR = 0x2201
	TEXTURE_ENV = 0x2300
	EYE_LINEAR = 0x2400
	OBJECT_LINEAR = 0x2401
	SPHERE_MAP = 0x2402
	TEXTURE_GEN_MODE = 0x2500
	OBJECT_PLANE = 0x2501
	EYE_PLANE = 0x2502
	CLAMP = 0x2900
	ALPHA4 = 0x803B
	ALPHA8 = 0x803C
	ALPHA12 = 0x803D
	ALPHA16 = 0x803E
	LUMINANCE4 = 0x803F
	LUMINANCE8 = 0x8040
	LUMINANCE12 = 0x8041
	LUMINANCE16 = 0x8042
	LUMINANCE4_ALPHA4 = 0x8043
	LUMINANCE6_ALPHA2 = 0x8044
	LUMINANCE8_ALPHA8 = 0x8045
	LUMINANCE12_ALPHA4 = 0x8046
	LUMINANCE12_ALPHA12 = 0x8047
	LUMINANCE16_ALPHA16 = 0x8048
	INTENSITY = 0x8049
	INTENSITY4 = 0x804A
	INTENSITY8 = 0x804B
	INTENSITY12 = 0x804C
	INTENSITY16 = 0x804D
	V2F = 0x2A20
	V3F = 0x2A21
	C4UB_V2F = 0x2A22
	C4UB_V3F = 0x2A23
	C3F_V3F = 0x2A24
	N3F_V3F = 0x2A25
	C4F_N3F_V3F = 0x2A26
	T2F_V3F = 0x2A27
	T4F_V4F = 0x2A28
	T2F_C4UB_V3F = 0x2A29
	T2F_C3F_V3F = 0x2A2A
	T2F_N3F_V3F = 0x2A2B
	T2F_C4F_N3F_V3F = 0x2A2C
	T4F_C4F_N3F_V4F = 0x2A2D
	CLIP_PLANE0 = 0x3000
	CLIP_PLANE1 = 0x3001
	CLIP_PLANE2 = 0x3002
	CLIP_PLANE3 = 0x3003
	CLIP_PLANE4 = 0x3004
	CLIP_PLANE5 = 0x3005
	LIGHT0 = 0x4000
	LIGHT1 = 0x4001
	LIGHT2 = 0x4002
	LIGHT3 = 0x4003
	LIGHT4 = 0x4004
	LIGHT5 = 0x4005
	LIGHT6 = 0x4006
	LIGHT7 = 0x4007
)

// https://www.opengl.org/sdk/docs/man/xhtml/glDrawArrays
func DrawArrays(mode Enum, first Int, count Sizei){
	C.glDrawArrays((C.GLenum)(mode), (C.GLint)(first), (C.GLsizei)(count))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glDrawElements
func DrawElements(mode Enum, count Sizei, type_ Enum, indices Pointer){
	C.glDrawElements((C.GLenum)(mode), (C.GLsizei)(count), (C.GLenum)(type_), (unsafe.Pointer)(indices))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetPointerv
func GetPointerv(pname Enum, params *Pointer){
	C.glGetPointerv((C.GLenum)(pname), (*unsafe.Pointer)(unsafe.Pointer(params)))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPolygonOffset
func PolygonOffset(factor Float, units Float){
	C.glPolygonOffset((C.GLfloat)(factor), (C.GLfloat)(units))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glCopyTexImage1D
func CopyTexImage1D(target Enum, level Int, internalformat Enum, x Int, y Int, width Sizei, border Int){
	C.glCopyTexImage1D((C.GLenum)(target), (C.GLint)(level), (C.GLenum)(internalformat), (C.GLint)(x), (C.GLint)(y), (C.GLsizei)(width), (C.GLint)(border))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glCopyTexImage2D
func CopyTexImage2D(target Enum, level Int, internalformat Enum, x Int, y Int, width Sizei, height Sizei, border Int){
	C.glCopyTexImage2D((C.GLenum)(target), (C.GLint)(level), (C.GLenum)(internalformat), (C.GLint)(x), (C.GLint)(y), (C.GLsizei)(width), (C.GLsizei)(height), (C.GLint)(border))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glCopyTexSubImage1D
func CopyTexSubImage1D(target Enum, level Int, xoffset Int, x Int, y Int, width Sizei){
	C.glCopyTexSubImage1D((C.GLenum)(target), (C.GLint)(level), (C.GLint)(xoffset), (C.GLint)(x), (C.GLint)(y), (C.GLsizei)(width))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glCopyTexSubImage2D
func CopyTexSubImage2D(target Enum, level Int, xoffset Int, yoffset Int, x Int, y Int, width Sizei, height Sizei){
	C.glCopyTexSubImage2D((C.GLenum)(target), (C.GLint)(level), (C.GLint)(xoffset), (C.GLint)(yoffset), (C.GLint)(x), (C.GLint)(y), (C.GLsizei)(width), (C.GLsizei)(height))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexSubImage1D
func TexSubImage1D(target Enum, level Int, xoffset Int, width Sizei, format Enum, type_ Enum, pixels Pointer){
	C.glTexSubImage1D((C.GLenum)(target), (C.GLint)(level), (C.GLint)(xoffset), (C.GLsizei)(width), (C.GLenum)(format), (C.GLenum)(type_), (unsafe.Pointer)(pixels))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexSubImage2D
func TexSubImage2D(target Enum, level Int, xoffset Int, yoffset Int, width Sizei, height Sizei, format Enum, type_ Enum, pixels Pointer){
	C.glTexSubImage2D((C.GLenum)(target), (C.GLint)(level), (C.GLint)(xoffset), (C.GLint)(yoffset), (C.GLsizei)(width), (C.GLsizei)(height), (C.GLenum)(format), (C.GLenum)(type_), (unsafe.Pointer)(pixels))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glBindTexture
func BindTexture(target Enum, texture Uint){
	C.glBindTexture((C.GLenum)(target), (C.GLuint)(texture))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glDeleteTextures
func DeleteTextures(n Sizei, textures *Uint){
	C.glDeleteTextures((C.GLsizei)(n), (*C.GLuint)(textures))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGenTextures
func GenTextures(n Sizei, textures *Uint){
	C.glGenTextures((C.GLsizei)(n), (*C.GLuint)(textures))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glIsTexture
func IsTexture(texture Uint) Boolean {
	return (Boolean)(C.glIsTexture((C.GLuint)(texture)))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glArrayElement
func ArrayElement(i Int){
	C.glArrayElement((C.GLint)(i))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glColorPointer
func ColorPointer(size Int, type_ Enum, stride Sizei, pointer Pointer){
	C.glColorPointer((C.GLint)(size), (C.GLenum)(type_), (C.GLsizei)(stride), (unsafe.Pointer)(pointer))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glDisableClientState
func DisableClientState(array Enum){
	C.glDisableClientState((C.GLenum)(array))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glEdgeFlagPointer
func EdgeFlagPointer(stride Sizei, pointer Pointer){
	C.glEdgeFlagPointer((C.GLsizei)(stride), (unsafe.Pointer)(pointer))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glEnableClientState
func EnableClientState(array Enum){
	C.glEnableClientState((C.GLenum)(array))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glIndexPointer
func IndexPointer(type_ Enum, stride Sizei, pointer Pointer){
	C.glIndexPointer((C.GLenum)(type_), (C.GLsizei)(stride), (unsafe.Pointer)(pointer))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glInterleavedArrays
func InterleavedArrays(format Enum, stride Sizei, pointer Pointer){
	C.glInterleavedArrays((C.GLenum)(format), (C.GLsizei)(stride), (unsafe.Pointer)(pointer))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glNormalPointer
func NormalPointer(type_ Enum, stride Sizei, pointer Pointer){
	C.glNormalPointer((C.GLenum)(type_), (C.GLsizei)(stride), (unsafe.Pointer)(pointer))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexCoordPointer
func TexCoordPointer(size Int, type_ Enum, stride Sizei, pointer Pointer){
	C.glTexCoordPointer((C.GLint)(size), (C.GLenum)(type_), (C.GLsizei)(stride), (unsafe.Pointer)(pointer))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexPointer
func VertexPointer(size Int, type_ Enum, stride Sizei, pointer Pointer){
	C.glVertexPointer((C.GLint)(size), (C.GLenum)(type_), (C.GLsizei)(stride), (unsafe.Pointer)(pointer))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glAreTexturesResident
func AreTexturesResident(n Sizei, textures *Uint, residences *Boolean) Boolean {
	return (Boolean)(C.glAreTexturesResident((C.GLsizei)(n), (*C.GLuint)(textures), (*C.GLboolean)(residences)))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPrioritizeTextures
func PrioritizeTextures(n Sizei, textures *Uint, priorities *Float){
	C.glPrioritizeTextures((C.GLsizei)(n), (*C.GLuint)(textures), (*C.GLfloat)(priorities))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glIndexub
func Indexub(c Ubyte){
	C.glIndexub((C.GLubyte)(c))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glIndexubv
func Indexubv(c *Ubyte){
	C.glIndexubv((*C.GLubyte)(c))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPopClientAttrib
func PopClientAttrib(){
	C.glPopClientAttrib()
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPushClientAttrib
func PushClientAttrib(mask Bitfield){
	C.glPushClientAttrib((C.GLbitfield)(mask))
}

func initGlVersion11() error {
	if ret := C.initGL_VERSION_1_1(); ret == 0 {
		return errors.New("unable to initialize GL_VERSION_1_1")
	}
	return nil
}

// ----------------------------------------------------------------------------
// GL_VERSION_1_2


const (
	UNSIGNED_BYTE_3_3_2 = 0x8032
	UNSIGNED_SHORT_4_4_4_4 = 0x8033
	UNSIGNED_SHORT_5_5_5_1 = 0x8034
	UNSIGNED_INT_8_8_8_8 = 0x8035
	UNSIGNED_INT_10_10_10_2 = 0x8036
	TEXTURE_BINDING_3D = 0x806A
	PACK_SKIP_IMAGES = 0x806B
	PACK_IMAGE_HEIGHT = 0x806C
	UNPACK_SKIP_IMAGES = 0x806D
	UNPACK_IMAGE_HEIGHT = 0x806E
	TEXTURE_3D = 0x806F
	PROXY_TEXTURE_3D = 0x8070
	TEXTURE_DEPTH = 0x8071
	TEXTURE_WRAP_R = 0x8072
	MAX_3D_TEXTURE_SIZE = 0x8073
	UNSIGNED_BYTE_2_3_3_REV = 0x8362
	UNSIGNED_SHORT_5_6_5 = 0x8363
	UNSIGNED_SHORT_5_6_5_REV = 0x8364
	UNSIGNED_SHORT_4_4_4_4_REV = 0x8365
	UNSIGNED_SHORT_1_5_5_5_REV = 0x8366
	UNSIGNED_INT_8_8_8_8_REV = 0x8367
	UNSIGNED_INT_2_10_10_10_REV = 0x8368
	BGR = 0x80E0
	BGRA = 0x80E1
	MAX_ELEMENTS_VERTICES = 0x80E8
	MAX_ELEMENTS_INDICES = 0x80E9
	CLAMP_TO_EDGE = 0x812F
	TEXTURE_MIN_LOD = 0x813A
	TEXTURE_MAX_LOD = 0x813B
	TEXTURE_BASE_LEVEL = 0x813C
	TEXTURE_MAX_LEVEL = 0x813D
	SMOOTH_POINT_SIZE_RANGE = 0x0B12
	SMOOTH_POINT_SIZE_GRANULARITY = 0x0B13
	SMOOTH_LINE_WIDTH_RANGE = 0x0B22
	SMOOTH_LINE_WIDTH_GRANULARITY = 0x0B23
	ALIASED_LINE_WIDTH_RANGE = 0x846E
	RESCALE_NORMAL = 0x803A
	LIGHT_MODEL_COLOR_CONTROL = 0x81F8
	SINGLE_COLOR = 0x81F9
	SEPARATE_SPECULAR_COLOR = 0x81FA
	ALIASED_POINT_SIZE_RANGE = 0x846D
)

// https://www.opengl.org/sdk/docs/man/xhtml/glBlendColor
func BlendColor(red Float, green Float, blue Float, alpha Float){
	C.glBlendColor((C.GLfloat)(red), (C.GLfloat)(green), (C.GLfloat)(blue), (C.GLfloat)(alpha))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glBlendEquation
func BlendEquation(mode Enum){
	C.glBlendEquation((C.GLenum)(mode))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glDrawRangeElements
func DrawRangeElements(mode Enum, start Uint, end Uint, count Sizei, type_ Enum, indices Pointer){
	C.glDrawRangeElements((C.GLenum)(mode), (C.GLuint)(start), (C.GLuint)(end), (C.GLsizei)(count), (C.GLenum)(type_), (unsafe.Pointer)(indices))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexImage3D
func TexImage3D(target Enum, level Int, internalformat Int, width Sizei, height Sizei, depth Sizei, border Int, format Enum, type_ Enum, pixels Pointer){
	C.glTexImage3D((C.GLenum)(target), (C.GLint)(level), (C.GLint)(internalformat), (C.GLsizei)(width), (C.GLsizei)(height), (C.GLsizei)(depth), (C.GLint)(border), (C.GLenum)(format), (C.GLenum)(type_), (unsafe.Pointer)(pixels))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glTexSubImage3D
func TexSubImage3D(target Enum, level Int, xoffset Int, yoffset Int, zoffset Int, width Sizei, height Sizei, depth Sizei, format Enum, type_ Enum, pixels Pointer){
	C.glTexSubImage3D((C.GLenum)(target), (C.GLint)(level), (C.GLint)(xoffset), (C.GLint)(yoffset), (C.GLint)(zoffset), (C.GLsizei)(width), (C.GLsizei)(height), (C.GLsizei)(depth), (C.GLenum)(format), (C.GLenum)(type_), (unsafe.Pointer)(pixels))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glCopyTexSubImage3D
func CopyTexSubImage3D(target Enum, level Int, xoffset Int, yoffset Int, zoffset Int, x Int, y Int, width Sizei, height Sizei){
	C.glCopyTexSubImage3D((C.GLenum)(target), (C.GLint)(level), (C.GLint)(xoffset), (C.GLint)(yoffset), (C.GLint)(zoffset), (C.GLint)(x), (C.GLint)(y), (C.GLsizei)(width), (C.GLsizei)(height))
}

func initGlVersion12() error {
	if ret := C.initGL_VERSION_1_2(); ret == 0 {
		return errors.New("unable to initialize GL_VERSION_1_2")
	}
	return nil
}

// ----------------------------------------------------------------------------
// GL_VERSION_1_3


const (
	TEXTURE0 = 0x84C0
	TEXTURE1 = 0x84C1
	TEXTURE2 = 0x84C2
	TEXTURE3 = 0x84C3
	TEXTURE4 = 0x84C4
	TEXTURE5 = 0x84C5
	TEXTURE6 = 0x84C6
	TEXTURE7 = 0x84C7
	TEXTURE8 = 0x84C8
	TEXTURE9 = 0x84C9
	TEXTURE10 = 0x84CA
	TEXTURE11 = 0x84CB
	TEXTURE12 = 0x84CC
	TEXTURE13 = 0x84CD
	TEXTURE14 = 0x84CE
	TEXTURE15 = 0x84CF
	TEXTURE16 = 0x84D0
	TEXTURE17 = 0x84D1
	TEXTURE18 = 0x84D2
	TEXTURE19 = 0x84D3
	TEXTURE20 = 0x84D4
	TEXTURE21 = 0x84D5
	TEXTURE22 = 0x84D6
	TEXTURE23 = 0x84D7
	TEXTURE24 = 0x84D8
	TEXTURE25 = 0x84D9
	TEXTURE26 = 0x84DA
	TEXTURE27 = 0x84DB
	TEXTURE28 = 0x84DC
	TEXTURE29 = 0x84DD
	TEXTURE30 = 0x84DE
	TEXTURE31 = 0x84DF
	ACTIVE_TEXTURE = 0x84E0
	MULTISAMPLE = 0x809D
	SAMPLE_ALPHA_TO_COVERAGE = 0x809E
	SAMPLE_ALPHA_TO_ONE = 0x809F
	SAMPLE_COVERAGE = 0x80A0
	SAMPLE_BUFFERS = 0x80A8
	SAMPLES = 0x80A9
	SAMPLE_COVERAGE_VALUE = 0x80AA
	SAMPLE_COVERAGE_INVERT = 0x80AB
	TEXTURE_CUBE_MAP = 0x8513
	TEXTURE_BINDING_CUBE_MAP = 0x8514
	TEXTURE_CUBE_MAP_POSITIVE_X = 0x8515
	TEXTURE_CUBE_MAP_NEGATIVE_X = 0x8516
	TEXTURE_CUBE_MAP_POSITIVE_Y = 0x8517
	TEXTURE_CUBE_MAP_NEGATIVE_Y = 0x8518
	TEXTURE_CUBE_MAP_POSITIVE_Z = 0x8519
	TEXTURE_CUBE_MAP_NEGATIVE_Z = 0x851A
	PROXY_TEXTURE_CUBE_MAP = 0x851B
	MAX_CUBE_MAP_TEXTURE_SIZE = 0x851C
	COMPRESSED_RGB = 0x84ED
	COMPRESSED_RGBA = 0x84EE
	TEXTURE_COMPRESSION_HINT = 0x84EF
	TEXTURE_COMPRESSED_IMAGE_SIZE = 0x86A0
	TEXTURE_COMPRESSED = 0x86A1
	NUM_COMPRESSED_TEXTURE_FORMATS = 0x86A2
	COMPRESSED_TEXTURE_FORMATS = 0x86A3
	CLAMP_TO_BORDER = 0x812D
	CLIENT_ACTIVE_TEXTURE = 0x84E1
	MAX_TEXTURE_UNITS = 0x84E2
	TRANSPOSE_MODELVIEW_MATRIX = 0x84E3
	TRANSPOSE_PROJECTION_MATRIX = 0x84E4
	TRANSPOSE_TEXTURE_MATRIX = 0x84E5
	TRANSPOSE_COLOR_MATRIX = 0x84E6
	MULTISAMPLE_BIT = 0x20000000
	NORMAL_MAP = 0x8511
	REFLECTION_MAP = 0x8512
	COMPRESSED_ALPHA = 0x84E9
	COMPRESSED_LUMINANCE = 0x84EA
	COMPRESSED_LUMINANCE_ALPHA = 0x84EB
	COMPRESSED_INTENSITY = 0x84EC
	COMBINE = 0x8570
	COMBINE_RGB = 0x8571
	COMBINE_ALPHA = 0x8572
	SOURCE0_RGB = 0x8580
	SOURCE1_RGB = 0x8581
	SOURCE2_RGB = 0x8582
	SOURCE0_ALPHA = 0x8588
	SOURCE1_ALPHA = 0x8589
	SOURCE2_ALPHA = 0x858A
	OPERAND0_RGB = 0x8590
	OPERAND1_RGB = 0x8591
	OPERAND2_RGB = 0x8592
	OPERAND0_ALPHA = 0x8598
	OPERAND1_ALPHA = 0x8599
	OPERAND2_ALPHA = 0x859A
	RGB_SCALE = 0x8573
	ADD_SIGNED = 0x8574
	INTERPOLATE = 0x8575
	SUBTRACT = 0x84E7
	CONSTANT = 0x8576
	PRIMARY_COLOR = 0x8577
	PREVIOUS = 0x8578
	DOT3_RGB = 0x86AE
	DOT3_RGBA = 0x86AF
)

// https://www.opengl.org/sdk/docs/man/xhtml/glActiveTexture
func ActiveTexture(texture Enum){
	C.glActiveTexture((C.GLenum)(texture))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glSampleCoverage
func SampleCoverage(value Float, invert Boolean){
	C.glSampleCoverage((C.GLfloat)(value), (C.GLboolean)(invert))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glCompressedTexImage3D
func CompressedTexImage3D(target Enum, level Int, internalformat Enum, width Sizei, height Sizei, depth Sizei, border Int, imageSize Sizei, data Pointer){
	C.glCompressedTexImage3D((C.GLenum)(target), (C.GLint)(level), (C.GLenum)(internalformat), (C.GLsizei)(width), (C.GLsizei)(height), (C.GLsizei)(depth), (C.GLint)(border), (C.GLsizei)(imageSize), (unsafe.Pointer)(data))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glCompressedTexImage2D
func CompressedTexImage2D(target Enum, level Int, internalformat Enum, width Sizei, height Sizei, border Int, imageSize Sizei, data Pointer){
	C.glCompressedTexImage2D((C.GLenum)(target), (C.GLint)(level), (C.GLenum)(internalformat), (C.GLsizei)(width), (C.GLsizei)(height), (C.GLint)(border), (C.GLsizei)(imageSize), (unsafe.Pointer)(data))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glCompressedTexImage1D
func CompressedTexImage1D(target Enum, level Int, internalformat Enum, width Sizei, border Int, imageSize Sizei, data Pointer){
	C.glCompressedTexImage1D((C.GLenum)(target), (C.GLint)(level), (C.GLenum)(internalformat), (C.GLsizei)(width), (C.GLint)(border), (C.GLsizei)(imageSize), (unsafe.Pointer)(data))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glCompressedTexSubImage3D
func CompressedTexSubImage3D(target Enum, level Int, xoffset Int, yoffset Int, zoffset Int, width Sizei, height Sizei, depth Sizei, format Enum, imageSize Sizei, data Pointer){
	C.glCompressedTexSubImage3D((C.GLenum)(target), (C.GLint)(level), (C.GLint)(xoffset), (C.GLint)(yoffset), (C.GLint)(zoffset), (C.GLsizei)(width), (C.GLsizei)(height), (C.GLsizei)(depth), (C.GLenum)(format), (C.GLsizei)(imageSize), (unsafe.Pointer)(data))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glCompressedTexSubImage2D
func CompressedTexSubImage2D(target Enum, level Int, xoffset Int, yoffset Int, width Sizei, height Sizei, format Enum, imageSize Sizei, data Pointer){
	C.glCompressedTexSubImage2D((C.GLenum)(target), (C.GLint)(level), (C.GLint)(xoffset), (C.GLint)(yoffset), (C.GLsizei)(width), (C.GLsizei)(height), (C.GLenum)(format), (C.GLsizei)(imageSize), (unsafe.Pointer)(data))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glCompressedTexSubImage1D
func CompressedTexSubImage1D(target Enum, level Int, xoffset Int, width Sizei, format Enum, imageSize Sizei, data Pointer){
	C.glCompressedTexSubImage1D((C.GLenum)(target), (C.GLint)(level), (C.GLint)(xoffset), (C.GLsizei)(width), (C.GLenum)(format), (C.GLsizei)(imageSize), (unsafe.Pointer)(data))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetCompressedTexImage
func GetCompressedTexImage(target Enum, level Int, img Pointer){
	C.glGetCompressedTexImage((C.GLenum)(target), (C.GLint)(level), (unsafe.Pointer)(img))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glClientActiveTexture
func ClientActiveTexture(texture Enum){
	C.glClientActiveTexture((C.GLenum)(texture))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord1d
func MultiTexCoord1d(target Enum, s Double){
	C.glMultiTexCoord1d((C.GLenum)(target), (C.GLdouble)(s))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord1dv
func MultiTexCoord1dv(target Enum, v *Double){
	C.glMultiTexCoord1dv((C.GLenum)(target), (*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord1f
func MultiTexCoord1f(target Enum, s Float){
	C.glMultiTexCoord1f((C.GLenum)(target), (C.GLfloat)(s))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord1fv
func MultiTexCoord1fv(target Enum, v *Float){
	C.glMultiTexCoord1fv((C.GLenum)(target), (*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord1i
func MultiTexCoord1i(target Enum, s Int){
	C.glMultiTexCoord1i((C.GLenum)(target), (C.GLint)(s))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord1iv
func MultiTexCoord1iv(target Enum, v *Int){
	C.glMultiTexCoord1iv((C.GLenum)(target), (*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord1s
func MultiTexCoord1s(target Enum, s Short){
	C.glMultiTexCoord1s((C.GLenum)(target), (C.GLshort)(s))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord1sv
func MultiTexCoord1sv(target Enum, v *Short){
	C.glMultiTexCoord1sv((C.GLenum)(target), (*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord2d
func MultiTexCoord2d(target Enum, s Double, t Double){
	C.glMultiTexCoord2d((C.GLenum)(target), (C.GLdouble)(s), (C.GLdouble)(t))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord2dv
func MultiTexCoord2dv(target Enum, v *Double){
	C.glMultiTexCoord2dv((C.GLenum)(target), (*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord2f
func MultiTexCoord2f(target Enum, s Float, t Float){
	C.glMultiTexCoord2f((C.GLenum)(target), (C.GLfloat)(s), (C.GLfloat)(t))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord2fv
func MultiTexCoord2fv(target Enum, v *Float){
	C.glMultiTexCoord2fv((C.GLenum)(target), (*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord2i
func MultiTexCoord2i(target Enum, s Int, t Int){
	C.glMultiTexCoord2i((C.GLenum)(target), (C.GLint)(s), (C.GLint)(t))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord2iv
func MultiTexCoord2iv(target Enum, v *Int){
	C.glMultiTexCoord2iv((C.GLenum)(target), (*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord2s
func MultiTexCoord2s(target Enum, s Short, t Short){
	C.glMultiTexCoord2s((C.GLenum)(target), (C.GLshort)(s), (C.GLshort)(t))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord2sv
func MultiTexCoord2sv(target Enum, v *Short){
	C.glMultiTexCoord2sv((C.GLenum)(target), (*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord3d
func MultiTexCoord3d(target Enum, s Double, t Double, r Double){
	C.glMultiTexCoord3d((C.GLenum)(target), (C.GLdouble)(s), (C.GLdouble)(t), (C.GLdouble)(r))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord3dv
func MultiTexCoord3dv(target Enum, v *Double){
	C.glMultiTexCoord3dv((C.GLenum)(target), (*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord3f
func MultiTexCoord3f(target Enum, s Float, t Float, r Float){
	C.glMultiTexCoord3f((C.GLenum)(target), (C.GLfloat)(s), (C.GLfloat)(t), (C.GLfloat)(r))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord3fv
func MultiTexCoord3fv(target Enum, v *Float){
	C.glMultiTexCoord3fv((C.GLenum)(target), (*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord3i
func MultiTexCoord3i(target Enum, s Int, t Int, r Int){
	C.glMultiTexCoord3i((C.GLenum)(target), (C.GLint)(s), (C.GLint)(t), (C.GLint)(r))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord3iv
func MultiTexCoord3iv(target Enum, v *Int){
	C.glMultiTexCoord3iv((C.GLenum)(target), (*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord3s
func MultiTexCoord3s(target Enum, s Short, t Short, r Short){
	C.glMultiTexCoord3s((C.GLenum)(target), (C.GLshort)(s), (C.GLshort)(t), (C.GLshort)(r))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord3sv
func MultiTexCoord3sv(target Enum, v *Short){
	C.glMultiTexCoord3sv((C.GLenum)(target), (*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord4d
func MultiTexCoord4d(target Enum, s Double, t Double, r Double, q Double){
	C.glMultiTexCoord4d((C.GLenum)(target), (C.GLdouble)(s), (C.GLdouble)(t), (C.GLdouble)(r), (C.GLdouble)(q))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord4dv
func MultiTexCoord4dv(target Enum, v *Double){
	C.glMultiTexCoord4dv((C.GLenum)(target), (*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord4f
func MultiTexCoord4f(target Enum, s Float, t Float, r Float, q Float){
	C.glMultiTexCoord4f((C.GLenum)(target), (C.GLfloat)(s), (C.GLfloat)(t), (C.GLfloat)(r), (C.GLfloat)(q))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord4fv
func MultiTexCoord4fv(target Enum, v *Float){
	C.glMultiTexCoord4fv((C.GLenum)(target), (*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord4i
func MultiTexCoord4i(target Enum, s Int, t Int, r Int, q Int){
	C.glMultiTexCoord4i((C.GLenum)(target), (C.GLint)(s), (C.GLint)(t), (C.GLint)(r), (C.GLint)(q))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord4iv
func MultiTexCoord4iv(target Enum, v *Int){
	C.glMultiTexCoord4iv((C.GLenum)(target), (*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord4s
func MultiTexCoord4s(target Enum, s Short, t Short, r Short, q Short){
	C.glMultiTexCoord4s((C.GLenum)(target), (C.GLshort)(s), (C.GLshort)(t), (C.GLshort)(r), (C.GLshort)(q))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiTexCoord4sv
func MultiTexCoord4sv(target Enum, v *Short){
	C.glMultiTexCoord4sv((C.GLenum)(target), (*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glLoadTransposeMatrixf
func LoadTransposeMatrixf(m *Float){
	C.glLoadTransposeMatrixf((*C.GLfloat)(m))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glLoadTransposeMatrixd
func LoadTransposeMatrixd(m *Double){
	C.glLoadTransposeMatrixd((*C.GLdouble)(m))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultTransposeMatrixf
func MultTransposeMatrixf(m *Float){
	C.glMultTransposeMatrixf((*C.GLfloat)(m))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultTransposeMatrixd
func MultTransposeMatrixd(m *Double){
	C.glMultTransposeMatrixd((*C.GLdouble)(m))
}

func initGlVersion13() error {
	if ret := C.initGL_VERSION_1_3(); ret == 0 {
		return errors.New("unable to initialize GL_VERSION_1_3")
	}
	return nil
}

// ----------------------------------------------------------------------------
// GL_VERSION_1_4


const (
	BLEND_DST_RGB = 0x80C8
	BLEND_SRC_RGB = 0x80C9
	BLEND_DST_ALPHA = 0x80CA
	BLEND_SRC_ALPHA = 0x80CB
	POINT_FADE_THRESHOLD_SIZE = 0x8128
	DEPTH_COMPONENT16 = 0x81A5
	DEPTH_COMPONENT24 = 0x81A6
	DEPTH_COMPONENT32 = 0x81A7
	MIRRORED_REPEAT = 0x8370
	MAX_TEXTURE_LOD_BIAS = 0x84FD
	TEXTURE_LOD_BIAS = 0x8501
	INCR_WRAP = 0x8507
	DECR_WRAP = 0x8508
	TEXTURE_DEPTH_SIZE = 0x884A
	TEXTURE_COMPARE_MODE = 0x884C
	TEXTURE_COMPARE_FUNC = 0x884D
	POINT_SIZE_MIN = 0x8126
	POINT_SIZE_MAX = 0x8127
	POINT_DISTANCE_ATTENUATION = 0x8129
	GENERATE_MIPMAP = 0x8191
	GENERATE_MIPMAP_HINT = 0x8192
	FOG_COORDINATE_SOURCE = 0x8450
	FOG_COORDINATE = 0x8451
	FRAGMENT_DEPTH = 0x8452
	CURRENT_FOG_COORDINATE = 0x8453
	FOG_COORDINATE_ARRAY_TYPE = 0x8454
	FOG_COORDINATE_ARRAY_STRIDE = 0x8455
	FOG_COORDINATE_ARRAY_POINTER = 0x8456
	FOG_COORDINATE_ARRAY = 0x8457
	COLOR_SUM = 0x8458
	CURRENT_SECONDARY_COLOR = 0x8459
	SECONDARY_COLOR_ARRAY_SIZE = 0x845A
	SECONDARY_COLOR_ARRAY_TYPE = 0x845B
	SECONDARY_COLOR_ARRAY_STRIDE = 0x845C
	SECONDARY_COLOR_ARRAY_POINTER = 0x845D
	SECONDARY_COLOR_ARRAY = 0x845E
	TEXTURE_FILTER_CONTROL = 0x8500
	DEPTH_TEXTURE_MODE = 0x884B
	COMPARE_R_TO_TEXTURE = 0x884E
)

// https://www.opengl.org/sdk/docs/man/xhtml/glBlendFuncSeparate
func BlendFuncSeparate(sfactorRGB Enum, dfactorRGB Enum, sfactorAlpha Enum, dfactorAlpha Enum){
	C.glBlendFuncSeparate((C.GLenum)(sfactorRGB), (C.GLenum)(dfactorRGB), (C.GLenum)(sfactorAlpha), (C.GLenum)(dfactorAlpha))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiDrawArrays
func MultiDrawArrays(mode Enum, first *Int, count *Sizei, drawcount Sizei){
	C.glMultiDrawArrays((C.GLenum)(mode), (*C.GLint)(first), (*C.GLsizei)(count), (C.GLsizei)(drawcount))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMultiDrawElements
func MultiDrawElements(mode Enum, count *Sizei, type_ Enum, indices *Pointer, drawcount Sizei){
	C.glMultiDrawElements((C.GLenum)(mode), (*C.GLsizei)(count), (C.GLenum)(type_), (*unsafe.Pointer)(unsafe.Pointer(indices)), (C.GLsizei)(drawcount))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPointParameterf
func PointParameterf(pname Enum, param Float){
	C.glPointParameterf((C.GLenum)(pname), (C.GLfloat)(param))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPointParameterfv
func PointParameterfv(pname Enum, params *Float){
	C.glPointParameterfv((C.GLenum)(pname), (*C.GLfloat)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPointParameteri
func PointParameteri(pname Enum, param Int){
	C.glPointParameteri((C.GLenum)(pname), (C.GLint)(param))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glPointParameteriv
func PointParameteriv(pname Enum, params *Int){
	C.glPointParameteriv((C.GLenum)(pname), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glFogCoordf
func FogCoordf(coord Float){
	C.glFogCoordf((C.GLfloat)(coord))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glFogCoordfv
func FogCoordfv(coord *Float){
	C.glFogCoordfv((*C.GLfloat)(coord))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glFogCoordd
func FogCoordd(coord Double){
	C.glFogCoordd((C.GLdouble)(coord))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glFogCoorddv
func FogCoorddv(coord *Double){
	C.glFogCoorddv((*C.GLdouble)(coord))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glFogCoordPointer
func FogCoordPointer(type_ Enum, stride Sizei, pointer Pointer){
	C.glFogCoordPointer((C.GLenum)(type_), (C.GLsizei)(stride), (unsafe.Pointer)(pointer))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glSecondaryColor3b
func SecondaryColor3b(red Byte, green Byte, blue Byte){
	C.glSecondaryColor3b((C.GLbyte)(red), (C.GLbyte)(green), (C.GLbyte)(blue))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glSecondaryColor3bv
func SecondaryColor3bv(v *Byte){
	C.glSecondaryColor3bv((*C.GLbyte)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glSecondaryColor3d
func SecondaryColor3d(red Double, green Double, blue Double){
	C.glSecondaryColor3d((C.GLdouble)(red), (C.GLdouble)(green), (C.GLdouble)(blue))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glSecondaryColor3dv
func SecondaryColor3dv(v *Double){
	C.glSecondaryColor3dv((*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glSecondaryColor3f
func SecondaryColor3f(red Float, green Float, blue Float){
	C.glSecondaryColor3f((C.GLfloat)(red), (C.GLfloat)(green), (C.GLfloat)(blue))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glSecondaryColor3fv
func SecondaryColor3fv(v *Float){
	C.glSecondaryColor3fv((*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glSecondaryColor3i
func SecondaryColor3i(red Int, green Int, blue Int){
	C.glSecondaryColor3i((C.GLint)(red), (C.GLint)(green), (C.GLint)(blue))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glSecondaryColor3iv
func SecondaryColor3iv(v *Int){
	C.glSecondaryColor3iv((*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glSecondaryColor3s
func SecondaryColor3s(red Short, green Short, blue Short){
	C.glSecondaryColor3s((C.GLshort)(red), (C.GLshort)(green), (C.GLshort)(blue))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glSecondaryColor3sv
func SecondaryColor3sv(v *Short){
	C.glSecondaryColor3sv((*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glSecondaryColor3ub
func SecondaryColor3ub(red Ubyte, green Ubyte, blue Ubyte){
	C.glSecondaryColor3ub((C.GLubyte)(red), (C.GLubyte)(green), (C.GLubyte)(blue))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glSecondaryColor3ubv
func SecondaryColor3ubv(v *Ubyte){
	C.glSecondaryColor3ubv((*C.GLubyte)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glSecondaryColor3ui
func SecondaryColor3ui(red Uint, green Uint, blue Uint){
	C.glSecondaryColor3ui((C.GLuint)(red), (C.GLuint)(green), (C.GLuint)(blue))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glSecondaryColor3uiv
func SecondaryColor3uiv(v *Uint){
	C.glSecondaryColor3uiv((*C.GLuint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glSecondaryColor3us
func SecondaryColor3us(red Ushort, green Ushort, blue Ushort){
	C.glSecondaryColor3us((C.GLushort)(red), (C.GLushort)(green), (C.GLushort)(blue))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glSecondaryColor3usv
func SecondaryColor3usv(v *Ushort){
	C.glSecondaryColor3usv((*C.GLushort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glSecondaryColorPointer
func SecondaryColorPointer(size Int, type_ Enum, stride Sizei, pointer Pointer){
	C.glSecondaryColorPointer((C.GLint)(size), (C.GLenum)(type_), (C.GLsizei)(stride), (unsafe.Pointer)(pointer))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glWindowPos2d
func WindowPos2d(x Double, y Double){
	C.glWindowPos2d((C.GLdouble)(x), (C.GLdouble)(y))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glWindowPos2dv
func WindowPos2dv(v *Double){
	C.glWindowPos2dv((*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glWindowPos2f
func WindowPos2f(x Float, y Float){
	C.glWindowPos2f((C.GLfloat)(x), (C.GLfloat)(y))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glWindowPos2fv
func WindowPos2fv(v *Float){
	C.glWindowPos2fv((*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glWindowPos2i
func WindowPos2i(x Int, y Int){
	C.glWindowPos2i((C.GLint)(x), (C.GLint)(y))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glWindowPos2iv
func WindowPos2iv(v *Int){
	C.glWindowPos2iv((*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glWindowPos2s
func WindowPos2s(x Short, y Short){
	C.glWindowPos2s((C.GLshort)(x), (C.GLshort)(y))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glWindowPos2sv
func WindowPos2sv(v *Short){
	C.glWindowPos2sv((*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glWindowPos3d
func WindowPos3d(x Double, y Double, z Double){
	C.glWindowPos3d((C.GLdouble)(x), (C.GLdouble)(y), (C.GLdouble)(z))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glWindowPos3dv
func WindowPos3dv(v *Double){
	C.glWindowPos3dv((*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glWindowPos3f
func WindowPos3f(x Float, y Float, z Float){
	C.glWindowPos3f((C.GLfloat)(x), (C.GLfloat)(y), (C.GLfloat)(z))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glWindowPos3fv
func WindowPos3fv(v *Float){
	C.glWindowPos3fv((*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glWindowPos3i
func WindowPos3i(x Int, y Int, z Int){
	C.glWindowPos3i((C.GLint)(x), (C.GLint)(y), (C.GLint)(z))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glWindowPos3iv
func WindowPos3iv(v *Int){
	C.glWindowPos3iv((*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glWindowPos3s
func WindowPos3s(x Short, y Short, z Short){
	C.glWindowPos3s((C.GLshort)(x), (C.GLshort)(y), (C.GLshort)(z))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glWindowPos3sv
func WindowPos3sv(v *Short){
	C.glWindowPos3sv((*C.GLshort)(v))
}

func initGlVersion14() error {
	if ret := C.initGL_VERSION_1_4(); ret == 0 {
		return errors.New("unable to initialize GL_VERSION_1_4")
	}
	return nil
}

// ----------------------------------------------------------------------------
// GL_VERSION_1_5

type (
	Sizeiptr C.GLsizeiptr
	Intptr C.GLintptr
)
const (
	BUFFER_SIZE = 0x8764
	BUFFER_USAGE = 0x8765
	QUERY_COUNTER_BITS = 0x8864
	CURRENT_QUERY = 0x8865
	QUERY_RESULT = 0x8866
	QUERY_RESULT_AVAILABLE = 0x8867
	ARRAY_BUFFER = 0x8892
	ELEMENT_ARRAY_BUFFER = 0x8893
	ARRAY_BUFFER_BINDING = 0x8894
	ELEMENT_ARRAY_BUFFER_BINDING = 0x8895
	VERTEX_ATTRIB_ARRAY_BUFFER_BINDING = 0x889F
	READ_ONLY = 0x88B8
	WRITE_ONLY = 0x88B9
	READ_WRITE = 0x88BA
	BUFFER_ACCESS = 0x88BB
	BUFFER_MAPPED = 0x88BC
	BUFFER_MAP_POINTER = 0x88BD
	STREAM_DRAW = 0x88E0
	STREAM_READ = 0x88E1
	STREAM_COPY = 0x88E2
	STATIC_DRAW = 0x88E4
	STATIC_READ = 0x88E5
	STATIC_COPY = 0x88E6
	DYNAMIC_DRAW = 0x88E8
	DYNAMIC_READ = 0x88E9
	DYNAMIC_COPY = 0x88EA
	SAMPLES_PASSED = 0x8914
	SRC1_ALPHA = 0x8589
	VERTEX_ARRAY_BUFFER_BINDING = 0x8896
	NORMAL_ARRAY_BUFFER_BINDING = 0x8897
	COLOR_ARRAY_BUFFER_BINDING = 0x8898
	INDEX_ARRAY_BUFFER_BINDING = 0x8899
	TEXTURE_COORD_ARRAY_BUFFER_BINDING = 0x889A
	EDGE_FLAG_ARRAY_BUFFER_BINDING = 0x889B
	SECONDARY_COLOR_ARRAY_BUFFER_BINDING = 0x889C
	FOG_COORDINATE_ARRAY_BUFFER_BINDING = 0x889D
	WEIGHT_ARRAY_BUFFER_BINDING = 0x889E
	FOG_COORD_SRC = 0x8450
	FOG_COORD = 0x8451
	CURRENT_FOG_COORD = 0x8453
	FOG_COORD_ARRAY_TYPE = 0x8454
	FOG_COORD_ARRAY_STRIDE = 0x8455
	FOG_COORD_ARRAY_POINTER = 0x8456
	FOG_COORD_ARRAY = 0x8457
	FOG_COORD_ARRAY_BUFFER_BINDING = 0x889D
	SRC0_RGB = 0x8580
	SRC1_RGB = 0x8581
	SRC2_RGB = 0x8582
	SRC0_ALPHA = 0x8588
	SRC2_ALPHA = 0x858A
)

// https://www.opengl.org/sdk/docs/man/xhtml/glGenQueries
func GenQueries(n Sizei, ids *Uint){
	C.glGenQueries((C.GLsizei)(n), (*C.GLuint)(ids))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glDeleteQueries
func DeleteQueries(n Sizei, ids *Uint){
	C.glDeleteQueries((C.GLsizei)(n), (*C.GLuint)(ids))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glIsQuery
func IsQuery(id Uint) Boolean {
	return (Boolean)(C.glIsQuery((C.GLuint)(id)))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glBeginQuery
func BeginQuery(target Enum, id Uint){
	C.glBeginQuery((C.GLenum)(target), (C.GLuint)(id))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glEndQuery
func EndQuery(target Enum){
	C.glEndQuery((C.GLenum)(target))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetQueryiv
func GetQueryiv(target Enum, pname Enum, params *Int){
	C.glGetQueryiv((C.GLenum)(target), (C.GLenum)(pname), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetQueryObjectiv
func GetQueryObjectiv(id Uint, pname Enum, params *Int){
	C.glGetQueryObjectiv((C.GLuint)(id), (C.GLenum)(pname), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetQueryObjectuiv
func GetQueryObjectuiv(id Uint, pname Enum, params *Uint){
	C.glGetQueryObjectuiv((C.GLuint)(id), (C.GLenum)(pname), (*C.GLuint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glBindBuffer
func BindBuffer(target Enum, buffer Uint){
	C.glBindBuffer((C.GLenum)(target), (C.GLuint)(buffer))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glDeleteBuffers
func DeleteBuffers(n Sizei, buffers *Uint){
	C.glDeleteBuffers((C.GLsizei)(n), (*C.GLuint)(buffers))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGenBuffers
func GenBuffers(n Sizei, buffers *Uint){
	C.glGenBuffers((C.GLsizei)(n), (*C.GLuint)(buffers))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glIsBuffer
func IsBuffer(buffer Uint) Boolean {
	return (Boolean)(C.glIsBuffer((C.GLuint)(buffer)))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glBufferData
func BufferData(target Enum, size Sizeiptr, data Pointer, usage Enum){
	C.glBufferData((C.GLenum)(target), (C.GLsizeiptr)(size), (unsafe.Pointer)(data), (C.GLenum)(usage))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glBufferSubData
func BufferSubData(target Enum, offset Intptr, size Sizeiptr, data Pointer){
	C.glBufferSubData((C.GLenum)(target), (C.GLintptr)(offset), (C.GLsizeiptr)(size), (unsafe.Pointer)(data))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetBufferSubData
func GetBufferSubData(target Enum, offset Intptr, size Sizeiptr, data Pointer){
	C.glGetBufferSubData((C.GLenum)(target), (C.GLintptr)(offset), (C.GLsizeiptr)(size), (unsafe.Pointer)(data))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glMapBuffer
func MapBuffer(target Enum, access Enum) Pointer {
	return (Pointer)(C.glMapBuffer((C.GLenum)(target), (C.GLenum)(access)))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUnmapBuffer
func UnmapBuffer(target Enum) Boolean {
	return (Boolean)(C.glUnmapBuffer((C.GLenum)(target)))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetBufferParameteriv
func GetBufferParameteriv(target Enum, pname Enum, params *Int){
	C.glGetBufferParameteriv((C.GLenum)(target), (C.GLenum)(pname), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetBufferPointerv
func GetBufferPointerv(target Enum, pname Enum, params *Pointer){
	C.glGetBufferPointerv((C.GLenum)(target), (C.GLenum)(pname), (*unsafe.Pointer)(unsafe.Pointer(params)))
}

func initGlVersion15() error {
	if ret := C.initGL_VERSION_1_5(); ret == 0 {
		return errors.New("unable to initialize GL_VERSION_1_5")
	}
	return nil
}

// ----------------------------------------------------------------------------
// GL_VERSION_2_0

type (
	Char C.GLchar
)
const (
	BLEND_EQUATION_RGB = 0x8009
	VERTEX_ATTRIB_ARRAY_ENABLED = 0x8622
	VERTEX_ATTRIB_ARRAY_SIZE = 0x8623
	VERTEX_ATTRIB_ARRAY_STRIDE = 0x8624
	VERTEX_ATTRIB_ARRAY_TYPE = 0x8625
	CURRENT_VERTEX_ATTRIB = 0x8626
	VERTEX_PROGRAM_POINT_SIZE = 0x8642
	VERTEX_ATTRIB_ARRAY_POINTER = 0x8645
	STENCIL_BACK_FUNC = 0x8800
	STENCIL_BACK_FAIL = 0x8801
	STENCIL_BACK_PASS_DEPTH_FAIL = 0x8802
	STENCIL_BACK_PASS_DEPTH_PASS = 0x8803
	MAX_DRAW_BUFFERS = 0x8824
	DRAW_BUFFER0 = 0x8825
	DRAW_BUFFER1 = 0x8826
	DRAW_BUFFER2 = 0x8827
	DRAW_BUFFER3 = 0x8828
	DRAW_BUFFER4 = 0x8829
	DRAW_BUFFER5 = 0x882A
	DRAW_BUFFER6 = 0x882B
	DRAW_BUFFER7 = 0x882C
	DRAW_BUFFER8 = 0x882D
	DRAW_BUFFER9 = 0x882E
	DRAW_BUFFER10 = 0x882F
	DRAW_BUFFER11 = 0x8830
	DRAW_BUFFER12 = 0x8831
	DRAW_BUFFER13 = 0x8832
	DRAW_BUFFER14 = 0x8833
	DRAW_BUFFER15 = 0x8834
	BLEND_EQUATION_ALPHA = 0x883D
	MAX_VERTEX_ATTRIBS = 0x8869
	VERTEX_ATTRIB_ARRAY_NORMALIZED = 0x886A
	MAX_TEXTURE_IMAGE_UNITS = 0x8872
	FRAGMENT_SHADER = 0x8B30
	VERTEX_SHADER = 0x8B31
	MAX_FRAGMENT_UNIFORM_COMPONENTS = 0x8B49
	MAX_VERTEX_UNIFORM_COMPONENTS = 0x8B4A
	MAX_VARYING_FLOATS = 0x8B4B
	MAX_VERTEX_TEXTURE_IMAGE_UNITS = 0x8B4C
	MAX_COMBINED_TEXTURE_IMAGE_UNITS = 0x8B4D
	SHADER_TYPE = 0x8B4F
	FLOAT_VEC2 = 0x8B50
	FLOAT_VEC3 = 0x8B51
	FLOAT_VEC4 = 0x8B52
	INT_VEC2 = 0x8B53
	INT_VEC3 = 0x8B54
	INT_VEC4 = 0x8B55
	BOOL = 0x8B56
	BOOL_VEC2 = 0x8B57
	BOOL_VEC3 = 0x8B58
	BOOL_VEC4 = 0x8B59
	FLOAT_MAT2 = 0x8B5A
	FLOAT_MAT3 = 0x8B5B
	FLOAT_MAT4 = 0x8B5C
	SAMPLER_1D = 0x8B5D
	SAMPLER_2D = 0x8B5E
	SAMPLER_3D = 0x8B5F
	SAMPLER_CUBE = 0x8B60
	SAMPLER_1D_SHADOW = 0x8B61
	SAMPLER_2D_SHADOW = 0x8B62
	DELETE_STATUS = 0x8B80
	COMPILE_STATUS = 0x8B81
	LINK_STATUS = 0x8B82
	VALIDATE_STATUS = 0x8B83
	INFO_LOG_LENGTH = 0x8B84
	ATTACHED_SHADERS = 0x8B85
	ACTIVE_UNIFORMS = 0x8B86
	ACTIVE_UNIFORM_MAX_LENGTH = 0x8B87
	SHADER_SOURCE_LENGTH = 0x8B88
	ACTIVE_ATTRIBUTES = 0x8B89
	ACTIVE_ATTRIBUTE_MAX_LENGTH = 0x8B8A
	FRAGMENT_SHADER_DERIVATIVE_HINT = 0x8B8B
	SHADING_LANGUAGE_VERSION = 0x8B8C
	CURRENT_PROGRAM = 0x8B8D
	POINT_SPRITE_COORD_ORIGIN = 0x8CA0
	LOWER_LEFT = 0x8CA1
	UPPER_LEFT = 0x8CA2
	STENCIL_BACK_REF = 0x8CA3
	STENCIL_BACK_VALUE_MASK = 0x8CA4
	STENCIL_BACK_WRITEMASK = 0x8CA5
	VERTEX_PROGRAM_TWO_SIDE = 0x8643
	POINT_SPRITE = 0x8861
	COORD_REPLACE = 0x8862
	MAX_TEXTURE_COORDS = 0x8871
)

// https://www.opengl.org/sdk/docs/man/xhtml/glBlendEquationSeparate
func BlendEquationSeparate(modeRGB Enum, modeAlpha Enum){
	C.glBlendEquationSeparate((C.GLenum)(modeRGB), (C.GLenum)(modeAlpha))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glDrawBuffers
func DrawBuffers(n Sizei, bufs *Enum){
	C.glDrawBuffers((C.GLsizei)(n), (*C.GLenum)(bufs))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glStencilOpSeparate
func StencilOpSeparate(face Enum, sfail Enum, dpfail Enum, dppass Enum){
	C.glStencilOpSeparate((C.GLenum)(face), (C.GLenum)(sfail), (C.GLenum)(dpfail), (C.GLenum)(dppass))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glStencilFuncSeparate
func StencilFuncSeparate(face Enum, func_ Enum, ref Int, mask Uint){
	C.glStencilFuncSeparate((C.GLenum)(face), (C.GLenum)(func_), (C.GLint)(ref), (C.GLuint)(mask))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glStencilMaskSeparate
func StencilMaskSeparate(face Enum, mask Uint){
	C.glStencilMaskSeparate((C.GLenum)(face), (C.GLuint)(mask))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glAttachShader
func AttachShader(program Uint, shader Uint){
	C.glAttachShader((C.GLuint)(program), (C.GLuint)(shader))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glBindAttribLocation
func BindAttribLocation(program Uint, index Uint, name *Char){
	C.glBindAttribLocation((C.GLuint)(program), (C.GLuint)(index), (*C.GLchar)(name))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glCompileShader
func CompileShader(shader Uint){
	C.glCompileShader((C.GLuint)(shader))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glCreateProgram
func CreateProgram() Uint {
	return (Uint)(C.glCreateProgram())
}

// https://www.opengl.org/sdk/docs/man/xhtml/glCreateShader
func CreateShader(type_ Enum) Uint {
	return (Uint)(C.glCreateShader((C.GLenum)(type_)))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glDeleteProgram
func DeleteProgram(program Uint){
	C.glDeleteProgram((C.GLuint)(program))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glDeleteShader
func DeleteShader(shader Uint){
	C.glDeleteShader((C.GLuint)(shader))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glDetachShader
func DetachShader(program Uint, shader Uint){
	C.glDetachShader((C.GLuint)(program), (C.GLuint)(shader))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glDisableVertexAttribArray
func DisableVertexAttribArray(index Uint){
	C.glDisableVertexAttribArray((C.GLuint)(index))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glEnableVertexAttribArray
func EnableVertexAttribArray(index Uint){
	C.glEnableVertexAttribArray((C.GLuint)(index))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetActiveAttrib
func GetActiveAttrib(program Uint, index Uint, bufSize Sizei, length *Sizei, size *Int, type_ *Enum, name *Char){
	C.glGetActiveAttrib((C.GLuint)(program), (C.GLuint)(index), (C.GLsizei)(bufSize), (*C.GLsizei)(length), (*C.GLint)(size), (*C.GLenum)(type_), (*C.GLchar)(name))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetActiveUniform
func GetActiveUniform(program Uint, index Uint, bufSize Sizei, length *Sizei, size *Int, type_ *Enum, name *Char){
	C.glGetActiveUniform((C.GLuint)(program), (C.GLuint)(index), (C.GLsizei)(bufSize), (*C.GLsizei)(length), (*C.GLint)(size), (*C.GLenum)(type_), (*C.GLchar)(name))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetAttachedShaders
func GetAttachedShaders(program Uint, maxCount Sizei, count *Sizei, shaders *Uint){
	C.glGetAttachedShaders((C.GLuint)(program), (C.GLsizei)(maxCount), (*C.GLsizei)(count), (*C.GLuint)(shaders))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetAttribLocation
func GetAttribLocation(program Uint, name *Char) Int {
	return (Int)(C.glGetAttribLocation((C.GLuint)(program), (*C.GLchar)(name)))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetProgramiv
func GetProgramiv(program Uint, pname Enum, params *Int){
	C.glGetProgramiv((C.GLuint)(program), (C.GLenum)(pname), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetProgramInfoLog
func GetProgramInfoLog(program Uint, bufSize Sizei, length *Sizei, infoLog *Char){
	C.glGetProgramInfoLog((C.GLuint)(program), (C.GLsizei)(bufSize), (*C.GLsizei)(length), (*C.GLchar)(infoLog))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetShaderiv
func GetShaderiv(shader Uint, pname Enum, params *Int){
	C.glGetShaderiv((C.GLuint)(shader), (C.GLenum)(pname), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetShaderInfoLog
func GetShaderInfoLog(shader Uint, bufSize Sizei, length *Sizei, infoLog *Char){
	C.glGetShaderInfoLog((C.GLuint)(shader), (C.GLsizei)(bufSize), (*C.GLsizei)(length), (*C.GLchar)(infoLog))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetShaderSource
func GetShaderSource(shader Uint, bufSize Sizei, length *Sizei, source *Char){
	C.glGetShaderSource((C.GLuint)(shader), (C.GLsizei)(bufSize), (*C.GLsizei)(length), (*C.GLchar)(source))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetUniformLocation
func GetUniformLocation(program Uint, name *Char) Int {
	return (Int)(C.glGetUniformLocation((C.GLuint)(program), (*C.GLchar)(name)))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetUniformfv
func GetUniformfv(program Uint, location Int, params *Float){
	C.glGetUniformfv((C.GLuint)(program), (C.GLint)(location), (*C.GLfloat)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetUniformiv
func GetUniformiv(program Uint, location Int, params *Int){
	C.glGetUniformiv((C.GLuint)(program), (C.GLint)(location), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetVertexAttribdv
func GetVertexAttribdv(index Uint, pname Enum, params *Double){
	C.glGetVertexAttribdv((C.GLuint)(index), (C.GLenum)(pname), (*C.GLdouble)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetVertexAttribfv
func GetVertexAttribfv(index Uint, pname Enum, params *Float){
	C.glGetVertexAttribfv((C.GLuint)(index), (C.GLenum)(pname), (*C.GLfloat)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetVertexAttribiv
func GetVertexAttribiv(index Uint, pname Enum, params *Int){
	C.glGetVertexAttribiv((C.GLuint)(index), (C.GLenum)(pname), (*C.GLint)(params))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glGetVertexAttribPointerv
func GetVertexAttribPointerv(index Uint, pname Enum, pointer *Pointer){
	C.glGetVertexAttribPointerv((C.GLuint)(index), (C.GLenum)(pname), (*unsafe.Pointer)(unsafe.Pointer(pointer)))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glIsProgram
func IsProgram(program Uint) Boolean {
	return (Boolean)(C.glIsProgram((C.GLuint)(program)))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glIsShader
func IsShader(shader Uint) Boolean {
	return (Boolean)(C.glIsShader((C.GLuint)(shader)))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glLinkProgram
func LinkProgram(program Uint){
	C.glLinkProgram((C.GLuint)(program))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glShaderSource
func ShaderSource(shader Uint, count Sizei, string **Char, length *Int){
	C.glShaderSource((C.GLuint)(shader), (C.GLsizei)(count), (**C.GLchar)(unsafe.Pointer(string)), (*C.GLint)(length))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUseProgram
func UseProgram(program Uint){
	C.glUseProgram((C.GLuint)(program))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniform1f
func Uniform1f(location Int, v0 Float){
	C.glUniform1f((C.GLint)(location), (C.GLfloat)(v0))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniform2f
func Uniform2f(location Int, v0 Float, v1 Float){
	C.glUniform2f((C.GLint)(location), (C.GLfloat)(v0), (C.GLfloat)(v1))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniform3f
func Uniform3f(location Int, v0 Float, v1 Float, v2 Float){
	C.glUniform3f((C.GLint)(location), (C.GLfloat)(v0), (C.GLfloat)(v1), (C.GLfloat)(v2))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniform4f
func Uniform4f(location Int, v0 Float, v1 Float, v2 Float, v3 Float){
	C.glUniform4f((C.GLint)(location), (C.GLfloat)(v0), (C.GLfloat)(v1), (C.GLfloat)(v2), (C.GLfloat)(v3))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniform1i
func Uniform1i(location Int, v0 Int){
	C.glUniform1i((C.GLint)(location), (C.GLint)(v0))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniform2i
func Uniform2i(location Int, v0 Int, v1 Int){
	C.glUniform2i((C.GLint)(location), (C.GLint)(v0), (C.GLint)(v1))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniform3i
func Uniform3i(location Int, v0 Int, v1 Int, v2 Int){
	C.glUniform3i((C.GLint)(location), (C.GLint)(v0), (C.GLint)(v1), (C.GLint)(v2))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniform4i
func Uniform4i(location Int, v0 Int, v1 Int, v2 Int, v3 Int){
	C.glUniform4i((C.GLint)(location), (C.GLint)(v0), (C.GLint)(v1), (C.GLint)(v2), (C.GLint)(v3))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniform1fv
func Uniform1fv(location Int, count Sizei, value *Float){
	C.glUniform1fv((C.GLint)(location), (C.GLsizei)(count), (*C.GLfloat)(value))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniform2fv
func Uniform2fv(location Int, count Sizei, value *Float){
	C.glUniform2fv((C.GLint)(location), (C.GLsizei)(count), (*C.GLfloat)(value))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniform3fv
func Uniform3fv(location Int, count Sizei, value *Float){
	C.glUniform3fv((C.GLint)(location), (C.GLsizei)(count), (*C.GLfloat)(value))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniform4fv
func Uniform4fv(location Int, count Sizei, value *Float){
	C.glUniform4fv((C.GLint)(location), (C.GLsizei)(count), (*C.GLfloat)(value))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniform1iv
func Uniform1iv(location Int, count Sizei, value *Int){
	C.glUniform1iv((C.GLint)(location), (C.GLsizei)(count), (*C.GLint)(value))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniform2iv
func Uniform2iv(location Int, count Sizei, value *Int){
	C.glUniform2iv((C.GLint)(location), (C.GLsizei)(count), (*C.GLint)(value))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniform3iv
func Uniform3iv(location Int, count Sizei, value *Int){
	C.glUniform3iv((C.GLint)(location), (C.GLsizei)(count), (*C.GLint)(value))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniform4iv
func Uniform4iv(location Int, count Sizei, value *Int){
	C.glUniform4iv((C.GLint)(location), (C.GLsizei)(count), (*C.GLint)(value))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniformMatrix2fv
func UniformMatrix2fv(location Int, count Sizei, transpose Boolean, value *Float){
	C.glUniformMatrix2fv((C.GLint)(location), (C.GLsizei)(count), (C.GLboolean)(transpose), (*C.GLfloat)(value))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniformMatrix3fv
func UniformMatrix3fv(location Int, count Sizei, transpose Boolean, value *Float){
	C.glUniformMatrix3fv((C.GLint)(location), (C.GLsizei)(count), (C.GLboolean)(transpose), (*C.GLfloat)(value))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniformMatrix4fv
func UniformMatrix4fv(location Int, count Sizei, transpose Boolean, value *Float){
	C.glUniformMatrix4fv((C.GLint)(location), (C.GLsizei)(count), (C.GLboolean)(transpose), (*C.GLfloat)(value))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glValidateProgram
func ValidateProgram(program Uint){
	C.glValidateProgram((C.GLuint)(program))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib1d
func VertexAttrib1d(index Uint, x Double){
	C.glVertexAttrib1d((C.GLuint)(index), (C.GLdouble)(x))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib1dv
func VertexAttrib1dv(index Uint, v *Double){
	C.glVertexAttrib1dv((C.GLuint)(index), (*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib1f
func VertexAttrib1f(index Uint, x Float){
	C.glVertexAttrib1f((C.GLuint)(index), (C.GLfloat)(x))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib1fv
func VertexAttrib1fv(index Uint, v *Float){
	C.glVertexAttrib1fv((C.GLuint)(index), (*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib1s
func VertexAttrib1s(index Uint, x Short){
	C.glVertexAttrib1s((C.GLuint)(index), (C.GLshort)(x))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib1sv
func VertexAttrib1sv(index Uint, v *Short){
	C.glVertexAttrib1sv((C.GLuint)(index), (*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib2d
func VertexAttrib2d(index Uint, x Double, y Double){
	C.glVertexAttrib2d((C.GLuint)(index), (C.GLdouble)(x), (C.GLdouble)(y))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib2dv
func VertexAttrib2dv(index Uint, v *Double){
	C.glVertexAttrib2dv((C.GLuint)(index), (*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib2f
func VertexAttrib2f(index Uint, x Float, y Float){
	C.glVertexAttrib2f((C.GLuint)(index), (C.GLfloat)(x), (C.GLfloat)(y))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib2fv
func VertexAttrib2fv(index Uint, v *Float){
	C.glVertexAttrib2fv((C.GLuint)(index), (*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib2s
func VertexAttrib2s(index Uint, x Short, y Short){
	C.glVertexAttrib2s((C.GLuint)(index), (C.GLshort)(x), (C.GLshort)(y))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib2sv
func VertexAttrib2sv(index Uint, v *Short){
	C.glVertexAttrib2sv((C.GLuint)(index), (*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib3d
func VertexAttrib3d(index Uint, x Double, y Double, z Double){
	C.glVertexAttrib3d((C.GLuint)(index), (C.GLdouble)(x), (C.GLdouble)(y), (C.GLdouble)(z))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib3dv
func VertexAttrib3dv(index Uint, v *Double){
	C.glVertexAttrib3dv((C.GLuint)(index), (*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib3f
func VertexAttrib3f(index Uint, x Float, y Float, z Float){
	C.glVertexAttrib3f((C.GLuint)(index), (C.GLfloat)(x), (C.GLfloat)(y), (C.GLfloat)(z))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib3fv
func VertexAttrib3fv(index Uint, v *Float){
	C.glVertexAttrib3fv((C.GLuint)(index), (*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib3s
func VertexAttrib3s(index Uint, x Short, y Short, z Short){
	C.glVertexAttrib3s((C.GLuint)(index), (C.GLshort)(x), (C.GLshort)(y), (C.GLshort)(z))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib3sv
func VertexAttrib3sv(index Uint, v *Short){
	C.glVertexAttrib3sv((C.GLuint)(index), (*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib4Nbv
func VertexAttrib4Nbv(index Uint, v *Byte){
	C.glVertexAttrib4Nbv((C.GLuint)(index), (*C.GLbyte)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib4Niv
func VertexAttrib4Niv(index Uint, v *Int){
	C.glVertexAttrib4Niv((C.GLuint)(index), (*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib4Nsv
func VertexAttrib4Nsv(index Uint, v *Short){
	C.glVertexAttrib4Nsv((C.GLuint)(index), (*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib4Nub
func VertexAttrib4Nub(index Uint, x Ubyte, y Ubyte, z Ubyte, w Ubyte){
	C.glVertexAttrib4Nub((C.GLuint)(index), (C.GLubyte)(x), (C.GLubyte)(y), (C.GLubyte)(z), (C.GLubyte)(w))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib4Nubv
func VertexAttrib4Nubv(index Uint, v *Ubyte){
	C.glVertexAttrib4Nubv((C.GLuint)(index), (*C.GLubyte)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib4Nuiv
func VertexAttrib4Nuiv(index Uint, v *Uint){
	C.glVertexAttrib4Nuiv((C.GLuint)(index), (*C.GLuint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib4Nusv
func VertexAttrib4Nusv(index Uint, v *Ushort){
	C.glVertexAttrib4Nusv((C.GLuint)(index), (*C.GLushort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib4bv
func VertexAttrib4bv(index Uint, v *Byte){
	C.glVertexAttrib4bv((C.GLuint)(index), (*C.GLbyte)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib4d
func VertexAttrib4d(index Uint, x Double, y Double, z Double, w Double){
	C.glVertexAttrib4d((C.GLuint)(index), (C.GLdouble)(x), (C.GLdouble)(y), (C.GLdouble)(z), (C.GLdouble)(w))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib4dv
func VertexAttrib4dv(index Uint, v *Double){
	C.glVertexAttrib4dv((C.GLuint)(index), (*C.GLdouble)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib4f
func VertexAttrib4f(index Uint, x Float, y Float, z Float, w Float){
	C.glVertexAttrib4f((C.GLuint)(index), (C.GLfloat)(x), (C.GLfloat)(y), (C.GLfloat)(z), (C.GLfloat)(w))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib4fv
func VertexAttrib4fv(index Uint, v *Float){
	C.glVertexAttrib4fv((C.GLuint)(index), (*C.GLfloat)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib4iv
func VertexAttrib4iv(index Uint, v *Int){
	C.glVertexAttrib4iv((C.GLuint)(index), (*C.GLint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib4s
func VertexAttrib4s(index Uint, x Short, y Short, z Short, w Short){
	C.glVertexAttrib4s((C.GLuint)(index), (C.GLshort)(x), (C.GLshort)(y), (C.GLshort)(z), (C.GLshort)(w))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib4sv
func VertexAttrib4sv(index Uint, v *Short){
	C.glVertexAttrib4sv((C.GLuint)(index), (*C.GLshort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib4ubv
func VertexAttrib4ubv(index Uint, v *Ubyte){
	C.glVertexAttrib4ubv((C.GLuint)(index), (*C.GLubyte)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib4uiv
func VertexAttrib4uiv(index Uint, v *Uint){
	C.glVertexAttrib4uiv((C.GLuint)(index), (*C.GLuint)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttrib4usv
func VertexAttrib4usv(index Uint, v *Ushort){
	C.glVertexAttrib4usv((C.GLuint)(index), (*C.GLushort)(v))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glVertexAttribPointer
func VertexAttribPointer(index Uint, size Int, type_ Enum, normalized Boolean, stride Sizei, pointer Pointer){
	C.glVertexAttribPointer((C.GLuint)(index), (C.GLint)(size), (C.GLenum)(type_), (C.GLboolean)(normalized), (C.GLsizei)(stride), (unsafe.Pointer)(pointer))
}

func initGlVersion20() error {
	if ret := C.initGL_VERSION_2_0(); ret == 0 {
		return errors.New("unable to initialize GL_VERSION_2_0")
	}
	return nil
}

// ----------------------------------------------------------------------------
// GL_VERSION_2_1


const (
	PIXEL_PACK_BUFFER = 0x88EB
	PIXEL_UNPACK_BUFFER = 0x88EC
	PIXEL_PACK_BUFFER_BINDING = 0x88ED
	PIXEL_UNPACK_BUFFER_BINDING = 0x88EF
	FLOAT_MAT2x3 = 0x8B65
	FLOAT_MAT2x4 = 0x8B66
	FLOAT_MAT3x2 = 0x8B67
	FLOAT_MAT3x4 = 0x8B68
	FLOAT_MAT4x2 = 0x8B69
	FLOAT_MAT4x3 = 0x8B6A
	SRGB = 0x8C40
	SRGB8 = 0x8C41
	SRGB_ALPHA = 0x8C42
	SRGB8_ALPHA8 = 0x8C43
	COMPRESSED_SRGB = 0x8C48
	COMPRESSED_SRGB_ALPHA = 0x8C49
	CURRENT_RASTER_SECONDARY_COLOR = 0x845F
	SLUMINANCE_ALPHA = 0x8C44
	SLUMINANCE8_ALPHA8 = 0x8C45
	SLUMINANCE = 0x8C46
	SLUMINANCE8 = 0x8C47
	COMPRESSED_SLUMINANCE = 0x8C4A
	COMPRESSED_SLUMINANCE_ALPHA = 0x8C4B
)

// https://www.opengl.org/sdk/docs/man/xhtml/glUniformMatrix2x3fv
func UniformMatrix2x3fv(location Int, count Sizei, transpose Boolean, value *Float){
	C.glUniformMatrix2x3fv((C.GLint)(location), (C.GLsizei)(count), (C.GLboolean)(transpose), (*C.GLfloat)(value))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniformMatrix3x2fv
func UniformMatrix3x2fv(location Int, count Sizei, transpose Boolean, value *Float){
	C.glUniformMatrix3x2fv((C.GLint)(location), (C.GLsizei)(count), (C.GLboolean)(transpose), (*C.GLfloat)(value))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniformMatrix2x4fv
func UniformMatrix2x4fv(location Int, count Sizei, transpose Boolean, value *Float){
	C.glUniformMatrix2x4fv((C.GLint)(location), (C.GLsizei)(count), (C.GLboolean)(transpose), (*C.GLfloat)(value))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniformMatrix4x2fv
func UniformMatrix4x2fv(location Int, count Sizei, transpose Boolean, value *Float){
	C.glUniformMatrix4x2fv((C.GLint)(location), (C.GLsizei)(count), (C.GLboolean)(transpose), (*C.GLfloat)(value))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniformMatrix3x4fv
func UniformMatrix3x4fv(location Int, count Sizei, transpose Boolean, value *Float){
	C.glUniformMatrix3x4fv((C.GLint)(location), (C.GLsizei)(count), (C.GLboolean)(transpose), (*C.GLfloat)(value))
}

// https://www.opengl.org/sdk/docs/man/xhtml/glUniformMatrix4x3fv
func UniformMatrix4x3fv(location Int, count Sizei, transpose Boolean, value *Float){
	C.glUniformMatrix4x3fv((C.GLint)(location), (C.GLsizei)(count), (C.GLboolean)(transpose), (*C.GLfloat)(value))
}

func initGlVersion21() error {
	if ret := C.initGL_VERSION_2_1(); ret == 0 {
		return errors.New("unable to initialize GL_VERSION_2_1")
	}
	return nil
}


func Init() error {
	if err := initGlVersion10(); err != nil {
		return err
	}
	if err := initGlVersion11(); err != nil {
		return err
	}
	if err := initGlVersion12(); err != nil {
		return err
	}
	if err := initGlVersion13(); err != nil {
		return err
	}
	if err := initGlVersion14(); err != nil {
		return err
	}
	if err := initGlVersion15(); err != nil {
		return err
	}
	if err := initGlVersion20(); err != nil {
		return err
	}
	if err := initGlVersion21(); err != nil {
		return err
	}
	
	return nil
}

// ----------------------------------------------------------------------------
// Utils

// Go bool to GL boolean.
func GLBool(b bool) Boolean {
	if b {
		return TRUE
	}
	return FALSE
}

// GL boolean to Go bool.
func GoBool(b Boolean) bool {
	return b == TRUE
}

// Go string to GL string.
func GLString(str string) *Char {
	return (*Char)(C.CString(str))
}

// Allocates a GL string.
func GLStringAlloc(length Sizei) *Char {
	return (*Char)(C.malloc(C.size_t(length)))
}

// Frees GL string.
func GLStringFree(str *Char) {
	C.free(unsafe.Pointer(str))
}

// GL string (GLchar*) to Go string.
func GoString(str *Char) string {
	return C.GoString((*C.char)(str))
}

// GL string (GLubyte*) to Go string.
func GoStringUb(str *Ubyte) string {
	return C.GoString((*C.char)(unsafe.Pointer(str)))
}

// GL string (GLchar*) with length to Go string.
func GoStringN(str *Char, length Sizei) string {
	return C.GoStringN((*C.char)(str), C.int(length))
}

// Converts a list of Go strings to a slice of GL strings.
// Usefull for ShaderSource().
func GLStringArray(strs ...string) []*Char {
	strSlice := make([]*Char, len(strs))
	for i, s := range strs {
		strSlice[i] = (*Char)(C.CString(s))
	}
	return strSlice
}

// Free GL string slice allocated by GLStringArray().
func GLStringArrayFree(strs []*Char) {
	for _, s := range strs {
		C.free(unsafe.Pointer(s))
	}
}

// Add offset to a pointer. Usefull for VertexAttribPointer,
// DrawElements, etc.
func Offset(p Pointer, o uintptr) Pointer {
	return Pointer(uintptr(p) + o)
}
