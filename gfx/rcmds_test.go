package gfx

import "testing"

func TestAllocCommands(t *testing.T) {
	var b commandBuffer

	setColorCmd := b.allocSetColorCommand()
	if setColorCmd.id != rcSetColor {
		t.Errorf("allocSetColorCommand: setColorCmd.id expected %d got %d", rcSetColor, setColorCmd.id)
	}
	if b.cmds[0] != setColorCmd.id {
		t.Errorf("allocSetColorCommand: commandBuffer.cmds[0] expected %d got %d", setColorCmd.id, b.cmds[0])
	}
	used := b.used
	drawCmd := b.allocDrawCommand()
	if drawCmd.id != rcDraw {
		t.Errorf("allocSetColorCommand: drawCmd.id expected %d got %d", rcDraw, drawCmd.id)
	}
	if b.cmds[used] != drawCmd.id {
		t.Errorf("allocSetColorCommand: commandBuffer.cmds[0] expected %d got %d", drawCmd.id, b.cmds[used])
	}
}
