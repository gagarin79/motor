package gfx

import (
	"github.com/AllenDang/w32"
	"github.com/gagarin79/motor/gfx/gl"
	"log"
	"syscall"
	"unsafe"
)

var Hwnd w32.HWND

var dc w32.HDC
var rc w32.HGLRC

func createContext() {
	dc = w32.GetDC(Hwnd)
	choosePFD(dc)
	rc = wglCreateContext(dc)
	wglMakeCurrent(dc, rc)

	gl.Init()

	config.renderer = gl.GoStringUb(gl.GetString(gl.RENDERER))
	config.version = gl.GoStringUb(gl.GetString(gl.VERSION))
}

func deleteContext() {
	wglMakeCurrent(dc, 0)
	wglDeleteContext(rc)
}

func bindContext() bool {
	return wglMakeCurrent(dc, rc)
}

func unbindContext() {
	wglMakeCurrent(dc, 0)
}

func swapBuffers() {
	wglSwapBuffers(dc)
}

func choosePFD(dc w32.HDC) {
	pfd := w32.PIXELFORMATDESCRIPTOR{
		0, // size of this pfd initialized later
		1, // version number
		w32.PFD_DRAW_TO_WINDOW | // support window
			w32.PFD_SUPPORT_OPENGL | // support OpenGL
			w32.PFD_DOUBLEBUFFER, // double buffered
		w32.PFD_TYPE_RGBA, // RGBA type
		24,                // 24-bit color depth
		0, 0, 0, 0, 0, 0,  // color bits ignored
		8,          // no alpha buffer
		0,          // shift bit ignored
		0,          // no accumulation buffer
		0, 0, 0, 0, // accum bits ignored
		24,                 // 24-bit z-buffer
		8,                  // 8-bit stencil buffer
		0,                  // no auxiliary buffer
		w32.PFD_MAIN_PLANE, // main layer
		0,                  // reserved
		0, 0, 0,            // layer masks ignored
	}
	pfd.Size = uint16(unsafe.Sizeof(pfd))
	if pf := w32.ChoosePixelFormat(dc, &pfd); pf != 0 {
		if !w32.SetPixelFormat(dc, pf, &pfd) {
			log.Fatalln("SetPixelFormat failed")
		}
	} else {
		log.Fatalln("ChoosePixelFormat failed")
	}
}

var (
	modopengl32 = syscall.NewLazyDLL("opengl32.dll")

	procwglCreateContext = modopengl32.NewProc("wglCreateContext")
	procwglDeleteContext = modopengl32.NewProc("wglDeleteContext")
	procwglMakeCurrent   = modopengl32.NewProc("wglMakeCurrent")
	procwglSwapBuffers   = modopengl32.NewProc("wglSwapBuffers")
)

func wglCreateContext(hdc w32.HDC) w32.HGLRC {
	ret, _, _ := procwglCreateContext.Call(
		uintptr(hdc),
	)

	return w32.HGLRC(ret)
}

func wglDeleteContext(hglrc w32.HGLRC) bool {
	ret, _, _ := procwglDeleteContext.Call(
		uintptr(hglrc),
	)

	return ret == w32.TRUE
}

func wglMakeCurrent(hdc w32.HDC, hglrc w32.HGLRC) bool {
	ret, _, _ := procwglMakeCurrent.Call(
		uintptr(hdc),
		uintptr(hglrc),
	)

	return ret == w32.TRUE
}

func wglSwapBuffers(hdc w32.HDC) bool {
	ret, _, _ := procwglSwapBuffers.Call(
		uintptr(hdc),
	)

	return ret == w32.TRUE
}
