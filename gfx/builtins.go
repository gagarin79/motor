package gfx

// ----------------------------------------------------------------------------
// images

const defaultPixSize = 16

func init() {
	imageCache["@default"] = NewImage("@default", defaultPix)
	imageCache["@white"] = NewImage("@white", whitePix)
	imageCache["@black"] = NewImage("@black", blackPix)
}

func defaultPix() ([]uint8, int, int) {
	pix := make([]uint8, defaultPixSize*defaultPixSize*4)
	for y := 0; y < defaultPixSize; y++ {
		for x := 0; x < defaultPixSize; x++ {
			pix[(y*defaultPixSize+x)*4+0] = 32
			pix[(y*defaultPixSize+x)*4+1] = 32
			pix[(y*defaultPixSize+x)*4+2] = 32
			pix[(y*defaultPixSize+x)*4+3] = 255
		}
	}
	b := defaultPixSize - 1
	for x := 0; x < defaultPixSize; x++ {
		pix[x*4+0] = 255
		pix[x*4+1] = 255
		pix[x*4+2] = 255
		pix[x*4+3] = 255

		pix[x*defaultPixSize*4+0] = 255
		pix[x*defaultPixSize*4+1] = 255
		pix[x*defaultPixSize*4+2] = 255
		pix[x*defaultPixSize*4+3] = 255

		pix[(b*defaultPixSize+x)*4+0] = 255
		pix[(b*defaultPixSize+x)*4+1] = 255
		pix[(b*defaultPixSize+x)*4+2] = 255
		pix[(b*defaultPixSize+x)*4+3] = 255

		pix[(x*defaultPixSize+b)*4+0] = 255
		pix[(x*defaultPixSize+b)*4+1] = 255
		pix[(x*defaultPixSize+b)*4+2] = 255
		pix[(x*defaultPixSize+b)*4+3] = 255
	}

	return pix, defaultPixSize, defaultPixSize
}

func whitePix() ([]uint8, int, int) {
	pix := make([]uint8, 2*2*4)
	for i := 0; i < 2*2*4; i++ {
		pix[i] = 255
	}
	return pix, 2, 2
}

func blackPix() ([]uint8, int, int) {
	pix := make([]uint8, 2*2*4)
	for i := 0; i < 2*2; i++ {
		pix[i*4+0] = 0
		pix[i*4+1] = 0
		pix[i*4+2] = 0
		pix[i*4+3] = 255
	}
	return pix, 2, 2
}
