package gfx

import (
	"fmt"
	"github.com/gagarin79/motor/gfx/gl"
	"log"
)

var config struct {
	w, h int

	renderer string
	version  string
}

// ----------------------------------------------------------------------------
// gfx initialization/finalization

func Init(w, h int) {
	config.w, config.h = w, h

	createContext()

	gl.Disable(gl.LIGHTING)
	gl.Enable(gl.TEXTURE_2D)
	gl.Enable(gl.BLEND)

	currentTexture = nil
	currentState = 0xffffffff
	defaultRenderState.bind()

	log.Printf("init gfx: gl v%s (%s)\n", config.version, config.renderer)
	rcache.load()
}

func Shutdown() {
	rcache.unload()
	deleteContext()

	log.Println("shutdown gfx: ok")
}

// ----------------------------------------------------------------------------
// resources

type resource interface {
	fmt.Stringer
	load() error
	unload() error
}

type resourceCache []resource

func (c *resourceCache) add(r resource) {
	q := *c
	if q == nil {
		q = make(resourceCache, 0)
	}
	q = append(q, r)
	*c = q
}

func (c *resourceCache) load() {
	for _, r := range *c {
		if err := r.load(); err != nil {
			log.Println(err)
		}
	}
}

func (c *resourceCache) unload() {
	for _, r := range *c {
		if err := r.unload(); err != nil {
			log.Println(err)
		}
	}
}

var rcache resourceCache
