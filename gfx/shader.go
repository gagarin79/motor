package gfx

import (
	"errors"
	"fmt"
	"github.com/gagarin79/motor/convar"
	"github.com/gagarin79/motor/decl"
	"github.com/gagarin79/motor/fs"
	"github.com/gagarin79/motor/gfx/gl"
	"strconv"
	"strings"
)

// ----------------------------------------------------------------------------
// shader interface

type Shader interface {
	decl.Interface
	bind()
}

func LookupShader(name string) Shader {
	return decl.Lookup(name).(Shader)
}

// ----------------------------------------------------------------------------
// texture

type texture struct {
	target, unit int
	filter       textureFilterVar
	repeat       bool
	image        *Image
}

var currentTexture *texture

func (t *texture) bind() {
	if currentTexture == t {
		return
	}

	currentTexture = t

	gl.ActiveTexture(gl.Enum(gl.TEXTURE0 + t.unit))
	gl.ClientActiveTexture(gl.Enum(gl.TEXTURE0 + t.unit)) // todo remove!

	gl.BindTexture(gl.Enum(t.target), t.image.id)
	gl.TexParameteri(gl.Enum(gl.TEXTURE_2D), gl.TEXTURE_MAG_FILTER, gl.Int(t.filter))
	gl.TexParameteri(gl.Enum(gl.TEXTURE_2D), gl.TEXTURE_MIN_FILTER, gl.Int(t.filter))
}

// ----------------------------------------------------------------------------
// shader parameters

type shaderParmTyp int

const (
	spSampler shaderParmTyp = iota
	spInt
	spFloat
	spFloat2
	spFloat3
	spFloat4
)

type shaderParm interface {
	init(prog *shaderProgram) error
	bind()
}

type shaderParmValue struct {
	name string
	loc  int
}

type intShaderParm struct {
	shaderParmValue
	val int
}

func (p *shaderParmValue) init(prog *shaderProgram) error {
	p.loc = prog.getUniformLoc(p.name)
	if p.loc == -1 {
		return fmt.Errorf("parameter %q not found in program")
	}
	return nil
}

func newIntParm(name string, val int) *intShaderParm {
	return &intShaderParm{shaderParmValue: shaderParmValue{name: name}, val: val}
}

func (p *intShaderParm) bind() {
	gl.Uniform1i(gl.Int(p.loc), gl.Int(p.val))
}

type floatShaderParm struct {
	shaderParmValue
	val float32
}

func newFloatParm(name string, val float32) *floatShaderParm {
	return &floatShaderParm{shaderParmValue: shaderParmValue{name: name}, val: val}
}

func (p *floatShaderParm) bind() {
	gl.Uniform1f(gl.Int(p.loc), gl.Float(p.val))
}

type float2ShaderParm struct {
	shaderParmValue
	val [2]float32
}

func newFloat2Parm(name string, val [2]float32) *float2ShaderParm {
	return &float2ShaderParm{shaderParmValue: shaderParmValue{name: name}, val: val}
}

func (p *float2ShaderParm) bind() {
	gl.Uniform2fv(gl.Int(p.loc), 1, (*gl.Float)(&p.val[0]))
}

type float3ShaderParm struct {
	shaderParmValue
	val [3]float32
}

func newFloat3Parm(name string, val [3]float32) *float3ShaderParm {
	return &float3ShaderParm{shaderParmValue: shaderParmValue{name: name}, val: val}
}

func (p *float3ShaderParm) bind() {
	gl.Uniform3fv(gl.Int(p.loc), 1, (*gl.Float)(&p.val[0]))
}

type float4ShaderParm struct {
	shaderParmValue
	val [4]float32
}

func newFloat4Parm(name string, val [4]float32) *float4ShaderParm {
	return &float4ShaderParm{shaderParmValue: shaderParmValue{name: name}, val: val}
}

func (p *float4ShaderParm) bind() {
	gl.Uniform4fv(gl.Int(p.loc), 1, (*gl.Float)(&p.val[0]))
}

// ----------------------------------------------------------------------------
// shader implementation

type shader struct {
	name, def string
	isValid   bool

	state    renderState
	prog     *shaderProgram
	textures []*texture
}

func init() {
	decl.RegisterDef("shader", func(name, def string) decl.Interface {
		return &shader{name: name, def: def}
	})
}

func (s *shader) bind() {
	s.state.bind()
	for _, t := range s.textures {
		t.bind()
	}
	if currentShaderProg != s.prog {
		if s.prog == nil {
			gl.UseProgram(0)
		} else {
			gl.UseProgram(s.prog.id)
			s.prog.bind()
		}
		currentShaderProg = s.prog
	}
}

func (s shader) Name() string {
	return s.name
}

func (s shader) Def() string {
	return s.def
}

func (s shader) IsValid() bool {
	return s.isValid
}

func (s *shader) MakeDefault() {
	t := &texture{
		target: gl.TEXTURE_2D,
		filter: defaultTextureFilter,
		image:  LookupImage(s.name),
	}
	s.textures = make([]*texture, 1)
	s.textures[0] = t
	s.isValid = true
}

func (s *shader) Parse(p *decl.Parser) {
	blendState := defaultBlendState
	depthFuncState := defaultDepthFuncState
	depthMaskState := defaultDepthMaskState
	colorMaskState := defaultColorMaskState
	polyModeState := defaultPolyModeState

	var (
		vsname, fsname string
		parms          []shaderParm
	)

	for tok := p.Next(); tok != decl.EOF && tok != '}'; tok = p.Next() {
		if ident, ok := p.ExpectIdent(); ok {
			switch ident {
			case "blend":
				blendState = s.parseBlendState(p)
			case "depthFunc":
				depthFuncState = s.parseDepthFuncState(p)
			case "depthMask":
				depthMaskState = s.parseDepthMaskState(p)
			case "colorMask":
				colorMaskState = s.parseColorMaskState(p)
			case "polyMode":
				polyModeState = s.parsePolyModeState(p)
			case "image":
				t := s.parseTexture(p)
				if t != nil {
					s.addTexture(t)
				}
			case "vs":
				vsname, _ = p.ParseString()
			case "fs":
				fsname, _ = p.ParseString()
			case "int", "sampler":
				name, ok := p.ParseIdent()
				if ok {
					v, ok := p.ParseInt()
					if ok {
						parms = append(parms, newIntParm(name, v))
					}
				}
			case "float":
				name, ok := p.ParseIdent()
				if ok {
					v, ok := p.ParseFloat()
					if ok {
						parms = append(parms, newFloatParm(name, v))
					}
				}
			case "float2":
				name, ok := p.ParseIdent()
				if ok {
					var v [2]float32
					v[0], v[1], ok = p.ParseFloat2()
					if ok {
						parms = append(parms, newFloat2Parm(name, v))
					}
				}
			case "float3":
				name, ok := p.ParseIdent()
				if ok {
					var v [3]float32
					v[0], v[1], v[2], ok = p.ParseFloat3()
					if ok {
						parms = append(parms, newFloat3Parm(name, v))
					}
				}
			case "float4":
				name, ok := p.ParseIdent()
				if ok {
					var v [4]float32
					v[0], v[1], v[2], v[3], ok = p.ParseFloat4()
					if ok {
						parms = append(parms, newFloat4Parm(name, v))
					}
				}
			default:
				p.Error("undefined: %s", ident)
			}
		}
	}

	if vsname != "" && fsname == "" {
		p.Error("vert without frag")
	} else if fsname != "" && vsname == "" {
		p.Error("frag without vert")
	}

	if vsname != "" && fsname != "" {
		if vsrc, fsrc, err := readSources(vsname+".vs", fsname+".fs"); err == nil {
			s.prog = newShaderProgram(s.name, vsrc, fsrc, parms)
			if err := s.prog.load(); err != nil {
				p.Error("%v", err)
			} else {
				rcache.add(s.prog)
			}
		} else {
			p.Error("%v", err)
		}
	}

	s.state = blendState |
		depthFuncState |
		depthMaskState |
		colorMaskState |
		polyModeState

	if len(s.textures) == 0 && s.prog == nil {
		s.addImage("@default")
		p.Error("shader without image")
	}

	s.isValid = !p.HadError()
}

func (s *shader) parseBlendState(p *decl.Parser) renderState {
	state := defaultBlendState

	srcid, ok := p.ParseIdent()
	if !ok {
		return state
	}

	src := srcBlendOne
	switch srcid {
	case "blend":
		return srcBlendSrcAlpha | dstBlendOneMinusSrcAlpha
	case "add":
		return srcBlendOne | dstBlendOne
	case "filter":
		return srcBlendDstColor | dstBlendZero
	case "none":
		return srcBlendZero | dstBlendOne
	case "one":
		src = srcBlendOne
	case "zero":
		src = srcBlendZero
	case "dstColor":
		src = srcBlendDstColor
	case "oneMinusDstColor":
		src = srcBlendOneMinusDstColor
	case "srcAlpha":
		src = srcBlendSrcAlpha
	case "oneMinusSrcAlpha":
		src = srcBlendOneMinusSrcAlpha
	case "dstAlpha":
		src = srcBlendDstAlpha
	case "oneMinusDstAlpha":
		src = srcBlendOneMinusDstAlpha
	case "srcAlphaSaturate":
		src = srcBlendAlphaSaturate
	default:
		p.Error("invalid src blend factor: %s", srcid)
		return state
	}

	p.Next()
	if !p.Expect(',') {
		return state
	}

	dstid, ok := p.ParseIdent()
	if !ok {
		return state
	}

	dst := dstBlendZero
	switch dstid {
	case "one":
		dst = dstBlendOne
	case "zero":
		dst = dstBlendZero
	case "srcAlpha":
		dst = dstBlendSrcAlpha
	case "oneMinusSrcAlpha":
		dst = dstBlendOneMinusSrcAlpha
	case "dstAlpha":
		dst = dstBlendDstAlpha
	case "oneMinusDstAlpha":
		dst = dstBlendOneMinusDstAlpha
	case "srcColor":
		dst = dstBlendSrcColor
	case "oneMinusSrcColor":
		dst = dstBlendOneMinusSrcColor
	default:
		p.Error("invalid dst blend factor: %s", dstid)
		return state
	}

	return src | dst
}

func (s *shader) parseDepthFuncState(p *decl.Parser) renderState {
	state := defaultDepthFuncState

	if ident, ok := p.ParseIdent(); ok {
		switch ident {
		case "always":
			return depthFuncAlways
		case "equal":
			return depthFuncEqual
		case "less":
			return depthFuncLess
		}
		p.Error("invalid depth func: %s", ident)
	}

	return state
}

func (s *shader) parseDepthMaskState(p *decl.Parser) renderState {
	state := defaultDepthMaskState

	if ident, ok := p.ParseIdent(); ok {
		switch ident {
		case "true":
			return depthMask
		case "false":
			return 0
		}
		p.Error("invalid depth mask: %s", ident)
	}

	return state
}

func (s *shader) parseColorMaskState(p *decl.Parser) renderState {
	state := defaultColorMaskState

	if ident, ok := p.ParseIdent(); ok {
		state = 0
		n := strings.IndexRune(ident, 'r')
		if n != -1 {
			state |= redMask
			ident = ident[:n] + ident[n+1:]
		}
		n = strings.IndexRune(ident, 'g')
		if n != -1 {
			state |= greenMask
			ident = ident[:n] + ident[n+1:]
		}
		n = strings.IndexRune(ident, 'b')
		if n != -1 {
			state |= blueMask
			ident = ident[:n] + ident[n+1:]
		}
		n = strings.IndexRune(ident, 'a')
		if n != -1 {
			state |= alphaMask
			ident = ident[:n] + ident[n+1:]
		}
		if len(ident) != 0 {
			p.Error("invalid color mask: %s", ident)
		}
	}

	return state
}

func (s *shader) parsePolyModeState(p *decl.Parser) renderState {
	state := defaultPolyModeState

	if ident, ok := p.ParseIdent(); ok {
		switch ident {
		case "line":
			return polyModeLine
		case "fill":
			return 0
		}
		p.Error("invalid poly mode: %s", ident)
	}
	return state
}

func (s *shader) parseTexture(p *decl.Parser) *texture {
	t := &texture{
		unit:   -1,
		target: gl.TEXTURE_2D,
		filter: defaultTextureFilter,
		repeat: false,
	}

	tok := p.Next()
	for ; tok != decl.EOF; tok = p.Next() {
		if tok == decl.Ident {
			switch p.TokenText() {
			case "tex2d":
				t.target = gl.TEXTURE_2D
				break
			case "cube_px":
				t.target = gl.TEXTURE_CUBE_MAP_POSITIVE_X
				break
			case "cube_py":
				t.target = gl.TEXTURE_CUBE_MAP_POSITIVE_Y
				break
			case "cube_pz":
				t.target = gl.TEXTURE_CUBE_MAP_POSITIVE_Z
				break
			case "cube_nx":
				t.target = gl.TEXTURE_CUBE_MAP_NEGATIVE_X
				break
			case "cube_ny":
				t.target = gl.TEXTURE_CUBE_MAP_NEGATIVE_Y
				break
			case "cube_nz":
				t.target = gl.TEXTURE_CUBE_MAP_NEGATIVE_Z
				break
			case "nearest":
				t.filter = gl.NEAREST
				continue
			case "linear":
				t.filter = gl.LINEAR
				continue
			case "clamp":
				t.repeat = false
				continue
			case "repeat":
				t.repeat = true
				continue
			}
		} else if tok == decl.Int {
			t.unit, _ = p.ExpectInt()
			if t.unit < 0 || t.unit > gl.TEXTURE9 {
				p.Error("texture unit out of range: %d", t.unit)
			}
			continue
		}
		break
	}

	if name, ok := p.ExpectString(); ok {
		t.image = LookupImage(name)
	} else {
		t.image = LookupImage("@default")
	}

	return t
}

func (s *shader) addTexture(t *texture) {
	if t.unit < 0 {
		t.unit = len(s.textures)
	}
	s.textures = append(s.textures, t)
}

func (s *shader) addImage(name string) {
	t := &texture{
		target: gl.TEXTURE_2D,
		filter: defaultTextureFilter,
		image:  LookupImage(name),
	}
	s.addTexture(t)
}

func readSources(vfilename, ffilename string) (string, string, error) {
	var (
		vsrc, fsrc []byte
		err        error
	)
	if vsrc, err = fs.ReadFile(vfilename); err != nil {
		return "", "", err
	}
	if fsrc, err = fs.ReadFile(ffilename); err != nil {
		return "", "", err
	}
	return string(vsrc), string(fsrc), err
}

// ----------------------------------------------------------------------------
// textureFilterVar

type textureFilterVar int

func (tf *textureFilterVar) Set(s string) error {
	switch s {
	case "nearest":
		*tf = gl.NEAREST
		return nil
	case "linear":
		*tf = gl.LINEAR
		return nil
	default:
		v, err := strconv.ParseInt(s, 0, 64)
		if err != nil {
			return err
		}
		if v != gl.NEAREST && v != gl.LINEAR {
			return errors.New("invalid textureFilterVar value")
		}
		*tf = textureFilterVar(v)
		return nil
	}
}

func (tf *textureFilterVar) String() string {
	switch *tf {
	case gl.NEAREST:
		return "nearest"
	case gl.LINEAR:
		return "linear"
	}
	return "invalid"
}

var (
	defaultTextureFilter textureFilterVar = gl.NEAREST
)

func init() {
	convar.Def(&defaultTextureFilter, "gfx.defaultTextureFilter")
}
