package gfx

import (
	"github.com/gagarin79/motor/cmd"
	"log"
	"sort"
)

func sortImages() []string {
	list := make(sort.StringSlice, len(imageCache))
	i := 0
	for _, m := range imageCache {
		list[i] = m.String()
		i++
	}
	list.Sort()
	return list
}

func listImagesCmd(a []string) {
	log.Println("images:")
	for _, m := range sortImages() {
		log.Printf("  %v\n", m)
	}
}

func sortResources() []string {
	list := make(sort.StringSlice, len(rcache))
	i := 0
	for _, r := range rcache {
		list[i] = r.String()
		i++
	}
	list.Sort()
	return list
}

func listResourcesCmd(a []string) {
	log.Println("resources:")
	for _, r := range sortResources() {
		log.Printf("  %v\n", r)
	}
}

func init() {
	cmd.Register("listImages", listImagesCmd)
	cmd.Register("listResources", listResourcesCmd)
}
