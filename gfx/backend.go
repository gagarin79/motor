package gfx

import (
	"fmt"
	"github.com/gagarin79/motor/gfx/gl"
	"unsafe"
)

// ----------------------------------------------------------------------------
// renderer tesselator

const maxVertexes = 1000
const maxIndexes = 6 * maxVertexes

var tess struct {
	color [4]byte

	shader Shader

	indexes   [maxIndexes]uint
	verts     [maxVertexes][4]float32
	texCoords [maxVertexes][2]float32
	colors    [maxVertexes][4]byte

	numIndexes uint
	numVerts   uint
}

func beginDraw(s Shader) {
	tess.shader = s
	tess.numIndexes, tess.numVerts = 0, 0
}

func endDraw() {
	if tess.numIndexes == 0 {
		return
	}

	tess.shader.bind()

	gl.EnableClientState(gl.COLOR_ARRAY)
	gl.EnableClientState(gl.TEXTURE_COORD_ARRAY)
	gl.EnableClientState(gl.VERTEX_ARRAY)
	gl.ColorPointer(4, gl.UNSIGNED_BYTE, 0, gl.Pointer(&tess.colors[0]))
	gl.TexCoordPointer(2, gl.FLOAT, 0, gl.Pointer(&tess.texCoords[0]))
	gl.VertexPointer(3, gl.FLOAT, 16, gl.Pointer(&tess.verts[0]))

	gl.DrawElements(gl.TRIANGLES, gl.Sizei(tess.numIndexes), gl.UNSIGNED_INT, gl.Pointer(&tess.indexes[0]))
}

func execDrawBuffer(cp uintptr) uintptr {
	cmd := (*drawBufferCommand)(unsafe.Pointer(cp))

	gl.DrawBuffer(gl.Enum(cmd.buffer))

	colorMask := [...]gl.Boolean{1, 1, 1, 1}
	gl.GetBooleanv(gl.COLOR_WRITEMASK, &colorMask[0])

	gl.ColorMask(1, 1, 1, 1)
	gl.ClearColor(0, 0.5, 0.9, 1)
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT | gl.STENCIL_BUFFER_BIT)
	gl.ColorMask(colorMask[0], colorMask[1], colorMask[2], colorMask[3])

	gl.Viewport(0, 0, gl.Sizei(config.w), gl.Sizei(config.h))
	gl.MatrixMode(gl.PROJECTION)
	gl.LoadIdentity()
	gl.Ortho(0.0, gl.Double(config.w), gl.Double(config.h), 0.0, -1.0, 1.0)
	gl.MatrixMode(gl.MODELVIEW)
	gl.LoadIdentity()

	return cp + unsafe.Sizeof(*cmd)
}

func execSetColor(cp uintptr) uintptr {
	cmd := (*setColorCommand)(unsafe.Pointer(cp))

	tess.color[0] = byte(cmd.color[0] * 255.0)
	tess.color[1] = byte(cmd.color[1] * 255.0)
	tess.color[2] = byte(cmd.color[2] * 255.0)
	tess.color[3] = byte(cmd.color[3] * 255.0)

	return cp + unsafe.Sizeof(*cmd)
}

func execDraw(cp uintptr) uintptr {
	cmd := (*drawCommand)(unsafe.Pointer(cp))

	if cmd.shader != tess.shader {
		if tess.numIndexes > 0 {
			endDraw()
		}
		beginDraw(cmd.shader)
	}

	numIndexes, numVerts := tess.numIndexes, tess.numVerts

	tess.numVerts += 4
	tess.numIndexes += 6

	tess.indexes[numIndexes+0] = numVerts + 3
	tess.indexes[numIndexes+1] = numVerts + 0
	tess.indexes[numIndexes+2] = numVerts + 2
	tess.indexes[numIndexes+3] = numVerts + 2
	tess.indexes[numIndexes+4] = numVerts + 0
	tess.indexes[numIndexes+5] = numVerts + 1

	tess.verts[numVerts][0] = cmd.x
	tess.verts[numVerts][1] = cmd.y
	tess.verts[numVerts][2] = 0

	tess.texCoords[numVerts][0] = cmd.s1
	tess.texCoords[numVerts][1] = cmd.t1

	tess.colors[numVerts] = tess.color

	tess.verts[numVerts+1][0] = cmd.x + cmd.w
	tess.verts[numVerts+1][1] = cmd.y
	tess.verts[numVerts+1][2] = 0

	tess.texCoords[numVerts+1][0] = cmd.s2
	tess.texCoords[numVerts+1][1] = cmd.t1

	tess.colors[numVerts+1] = tess.color

	tess.verts[numVerts+2][0] = cmd.x + cmd.w
	tess.verts[numVerts+2][1] = cmd.y + cmd.h
	tess.verts[numVerts+2][2] = 0

	tess.texCoords[numVerts+2][0] = cmd.s2
	tess.texCoords[numVerts+2][1] = cmd.t2

	tess.colors[numVerts+2] = tess.color

	tess.verts[numVerts+3][0] = cmd.x
	tess.verts[numVerts+3][1] = cmd.y + cmd.h
	tess.verts[numVerts+3][2] = 0

	tess.texCoords[numVerts+3][0] = cmd.s1
	tess.texCoords[numVerts+3][1] = cmd.t2

	tess.colors[numVerts+3] = tess.color

	return cp + unsafe.Sizeof(*cmd)
}

func execSwapBuffers(cp uintptr) uintptr {
	cmd := (*swapBuffersCommand)(unsafe.Pointer(cp))

	endDraw()

	tess.numVerts, tess.numIndexes = 0, 0

	gl.Finish()

	swapBuffers()

	return cp + unsafe.Sizeof(*cmd)
}

func execRenderCommands(cmdBuf *commandBuffer) {
	tess.color = [...]byte{255, 255, 255, 255}

	cp, ep := uintptr(unsafe.Pointer(&cmdBuf.cmds[0])), cmdBuf.used
	for cs := cp; cp < cs+ep; {
		id := cmdBuf.cmds[cp-cs]
		switch id {
		case rcDrawBuffer:
			cp = execDrawBuffer(cp)
		case rcSetColor:
			cp = execSetColor(cp)
		case rcDraw:
			cp = execDraw(cp)
		case rcSwapBuffers:
			cp = execSwapBuffers(cp)
		default:
			panic(fmt.Sprintf("renderer.execRenderCommands: invalid command %d", id))
		}
	}

	cmdBuf.used = 0
}

// ----------------------------------------------------------------------------
// renderer

const maxFrames = 2

type renderer struct {
	*commandBuffer
	backendBuf *commandBuffer

	bufs     [maxFrames]commandBuffer
}

var tr renderer

func init() {
	tr.commandBuffer, tr.backendBuf = &tr.bufs[0], &tr.bufs[1]
}

func (r *renderer) issueCommands() {
	if r.backendBuf == nil || r.backendBuf.used == 0 {
		return
	}

	execRenderCommands(r.backendBuf)
}

func (r *renderer) swapFrames() {
	r.backendBuf, r.commandBuffer = r.commandBuffer, r.backendBuf
}
