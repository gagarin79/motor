package gfx

import (
	"github.com/gagarin79/motor/gfx/gl"
)

type renderState uint32

const (
	srcBlendZero             renderState = 0x00000001
	srcBlendOne              renderState = 0x00000000
	srcBlendDstColor         renderState = 0x00000003
	srcBlendOneMinusDstColor renderState = 0x00000004
	srcBlendSrcAlpha         renderState = 0x00000005
	srcBlendOneMinusSrcAlpha renderState = 0x00000006
	srcBlendDstAlpha         renderState = 0x00000007
	srcBlendOneMinusDstAlpha renderState = 0x00000008
	srcBlendAlphaSaturate    renderState = 0x00000009
	srcBlendBits             renderState = 0x0000000f

	dstBlendZero             renderState = 0x00000000
	dstBlendOne              renderState = 0x00000020
	dstBlendSrcColor         renderState = 0x00000030
	dstBlendOneMinusSrcColor renderState = 0x00000040
	dstBlendSrcAlpha         renderState = 0x00000050
	dstBlendOneMinusSrcAlpha renderState = 0x00000060
	dstBlendDstAlpha         renderState = 0x00000070
	dstBlendOneMinusDstAlpha renderState = 0x00000080
	dstBlendBits             renderState = 0x000000f0

	blendStateBits    renderState = srcBlendBits | dstBlendBits
	defaultBlendState renderState = srcBlendSrcAlpha | dstBlendOneMinusSrcAlpha

	depthMask             renderState = 0x00000100
	depthMaskBits         renderState = depthMask
	defaultDepthMaskState renderState = 0x00000000

	redMask               renderState = 0x00000200
	greenMask             renderState = 0x00000400
	blueMask              renderState = 0x00000800
	alphaMask             renderState = 0x00001000
	colorMask             renderState = redMask | greenMask | blueMask
	colorMaskBits         renderState = redMask | greenMask | blueMask | alphaMask
	defaultColorMaskState renderState = 0x00000000

	polyModeLine         renderState = 0x00002000
	polyModeBits         renderState = polyModeLine
	defaultPolyModeState renderState = 0x00000000

	depthFuncAlways       renderState = 0x00010000
	depthFuncEqual        renderState = 0x00020000
	depthFuncLess         renderState = 0x00000000
	depthFuncBits         renderState = depthFuncAlways | depthFuncEqual | depthFuncLess
	defaultDepthFuncState renderState = depthFuncAlways

	defaultRenderState renderState = defaultBlendState |
		defaultDepthFuncState |
		defaultDepthMaskState |
		defaultColorMaskState |
		defaultPolyModeState
)

var renderStateNames = [...]struct {
	s    renderState
	name string
}{
	{srcBlendOne, "srcBlendOne"},
	{srcBlendDstColor, "srcBlendDstColor"},
	{srcBlendOneMinusDstColor, "srcBlendOneMinusDstColor"},
	{srcBlendSrcAlpha, "srcBlendSrcAlpha"},
	{srcBlendOneMinusSrcAlpha, "srcBlendOneMinusSrcAlpha"},
	{srcBlendDstAlpha, "srcBlendDstAlpha"},
	{srcBlendOneMinusDstAlpha, "srcBlendOneMinusDstAlpha"},
	{srcBlendAlphaSaturate, "srcBlendAlphaSaturate"},
	{srcBlendBits, "srcBlendBits"},
	{dstBlendOne, "dstBlendOne"},
	{dstBlendSrcColor, "dstBlendSrcColor"},
	{dstBlendOneMinusSrcColor, "dstBlendOneMinusSrcColor"},
	{dstBlendSrcAlpha, "dstBlendSrcAlpha"},
	{dstBlendOneMinusSrcAlpha, "dstBlendOneMinusSrcAlpha"},
	{dstBlendDstAlpha, "dstBlendDstAlpha"},
	{dstBlendOneMinusDstAlpha, "dstBlendOneMinusDstAlpha"},
	{dstBlendBits, "dstBlendBits"},
	{depthMask, "depthMask"},
	{redMask, "redMask"},
	{greenMask, "greenMask"},
	{blueMask, "blueMask"},
	{alphaMask, "alphaMask"},
	{polyModeLine, "polyModeLine"},
	{depthFuncAlways, "depthFuncAlways"},
	{depthFuncEqual, "depthFuncEqual"},
}

func (s renderState) String() string {
	str := ""
	for _, n := range renderStateNames {
		if n.s&s != 0 {
			if str != "" {
				str += "|"
			}
			str += n.name
		}
	}
	if str == "" {
		str = "0"
	}
	return str
}

func (s renderState) bind() {
	diff := s ^ currentState
	switch {
	case diff == 0:
		return
	case diff&blendStateBits != 0:
		s.bindBlendState()
	case diff&depthFuncBits != 0:
		s.bindDepthFuncState()
	case diff&depthMaskBits != 0:
		s.bindDepthMaskState()
	case diff&colorMaskBits != 0:
		s.bindColorMaskState()
	case diff&polyModeBits != 0:
		s.bindPolyModeState()
	}

	currentState = s
}

func (s renderState) bindBlendState() {
	var srcFactor, dstFactor gl.Enum

	switch s & srcBlendBits {
	case srcBlendZero:
		srcFactor = gl.ZERO
	case srcBlendOne:
		srcFactor = gl.ONE
	case srcBlendDstColor:
		srcFactor = gl.DST_COLOR
	case srcBlendOneMinusDstColor:
		srcFactor = gl.ONE_MINUS_DST_COLOR
	case srcBlendSrcAlpha:
		srcFactor = gl.SRC_ALPHA
	case srcBlendOneMinusSrcAlpha:
		srcFactor = gl.ONE_MINUS_SRC_ALPHA
	case srcBlendDstAlpha:
		srcFactor = gl.DST_ALPHA
	case srcBlendOneMinusDstAlpha:
		srcFactor = gl.ONE_MINUS_DST_ALPHA
	case srcBlendAlphaSaturate:
		srcFactor = gl.SRC_ALPHA_SATURATE
	default:
		srcFactor = gl.ONE
	}

	switch s & dstBlendBits {
	case dstBlendZero:
		dstFactor = gl.ZERO
	case dstBlendOne:
		dstFactor = gl.ONE
	case dstBlendSrcColor:
		dstFactor = gl.SRC_COLOR
	case dstBlendOneMinusSrcColor:
		dstFactor = gl.ONE_MINUS_SRC_COLOR
	case dstBlendSrcAlpha:
		dstFactor = gl.SRC_ALPHA
	case dstBlendOneMinusSrcAlpha:
		dstFactor = gl.ONE_MINUS_SRC_ALPHA
	case dstBlendDstAlpha:
		dstFactor = gl.DST_ALPHA
	case dstBlendOneMinusDstAlpha:
		dstFactor = gl.ONE_MINUS_DST_ALPHA
	default:
		dstFactor = gl.ZERO
	}

	gl.BlendFunc(srcFactor, dstFactor)
}

func (s renderState) bindDepthFuncState() {
	if s&depthFuncEqual != 0 {
		gl.DepthFunc(gl.EQUAL)
	} else if s&depthFuncAlways != 0 {
		gl.DepthFunc(gl.ALWAYS)
	} else {
		gl.DepthFunc(gl.LEQUAL)
	}
}

func (s renderState) bindDepthMaskState() {
	if s&depthMask != 0 {
		gl.DepthMask(gl.FALSE)
	} else {
		gl.DepthMask(gl.TRUE)
	}
}

func (s renderState) bindColorMaskState() {
	gl.ColorMask(
		gl.GLBool(s&redMask == 0),
		gl.GLBool(s&greenMask == 0),
		gl.GLBool(s&blueMask == 0),
		gl.GLBool(s&alphaMask == 0),
	)
}

func (s renderState) bindPolyModeState() {
	if s&polyModeLine != 0 {
		gl.PolygonMode(gl.FRONT_AND_BACK, gl.LINE)
	} else {
		gl.PolygonMode(gl.FRONT_AND_BACK, gl.FILL)
	}
}

var currentState renderState
