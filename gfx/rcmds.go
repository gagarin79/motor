package gfx

import (
	"fmt"
	"unsafe"
)

const (
	rcEndOfList = iota
	rcDrawBuffer
	rcSetColor
	rcDraw
	rcSwapBuffers
)

type drawBufferCommand struct {
	id     byte
	buffer uint32
}

type setColorCommand struct {
	id    byte
	color [4]float32
}

type drawCommand struct {
	id     byte
	shader Shader
	x, y   float32
	w, h   float32
	s1, t1 float32
	s2, t2 float32
}

type swapBuffersCommand struct {
	id byte
}

var commandsSizes = [rcSwapBuffers + 1]uintptr{}

func init() {
	var (
		drawBuffer  drawBufferCommand
		setColor    setColorCommand
		draw        drawCommand
		swapBuffers swapBuffersCommand
	)
	commandsSizes[rcDrawBuffer] = unsafe.Sizeof(drawBuffer)
	commandsSizes[rcSetColor] = unsafe.Sizeof(setColor)
	commandsSizes[rcDraw] = unsafe.Sizeof(draw)
	commandsSizes[rcSwapBuffers] = unsafe.Sizeof(swapBuffers)
}

const maxRenderCommands = 0x40000

type commandBuffer struct {
	cmds [maxRenderCommands]byte
	used uintptr
}

func (b *commandBuffer) allocCommand(id byte) unsafe.Pointer {
	size := commandsSizes[id]
	if b.used+size > maxRenderCommands {
		panic(fmt.Sprintf("gfx: command buffer overflow: used: %d, size: %d", b.used, size))
	}
	cmds := uintptr(unsafe.Pointer(&b.cmds[0]))

	b.used += size

	return unsafe.Pointer(cmds + b.used - size)
}

func (b *commandBuffer) allocDrawBufferCommand() *drawBufferCommand {
	cmd := (*drawBufferCommand)(b.allocCommand(rcDrawBuffer))
	cmd.id = rcDrawBuffer

	return cmd
}

func (b *commandBuffer) allocSetColorCommand() *setColorCommand {
	cmd := (*setColorCommand)(b.allocCommand(rcSetColor))
	cmd.id = rcSetColor

	return cmd
}

func (b *commandBuffer) allocDrawCommand() *drawCommand {
	cmd := (*drawCommand)(b.allocCommand(rcDraw))
	cmd.id = rcDraw

	return cmd
}

func (b *commandBuffer) allocSwapBuffersCommand() *swapBuffersCommand {
	cmd := (*swapBuffersCommand)(b.allocCommand(rcSwapBuffers))
	cmd.id = rcSwapBuffers

	return cmd
}

func (b *commandBuffer) reset() {
	b.used = 0
}
