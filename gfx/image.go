package gfx

import (
	"errors"
	"fmt"
	"github.com/gagarin79/motor/fs"
	"github.com/gagarin79/motor/gfx/gl"
	"image"
	"image/draw"
	_ "image/jpeg"
	_ "image/png"
	"log"
)

// Image represents OpenGL texture.
type Image struct {
	name          string
	id            gl.Uint
	width, height int

	pix func() ([]uint8, int, int)
}

func (m Image) Width() int {
	return m.width
}

func (m Image) Height() int {
	return m.height
}

func (m *Image) Upload(pix []uint8, w, h int) error {
	m.width, m.height = w, h

	if m.id == 0 {
		gl.GenTextures(1, &m.id)
	}
	if m.id == 0 {
		return errors.New("gen texture id failed")
	}

	gl.BindTexture(gl.TEXTURE_2D, m.id)
	gl.TexImage2D(gl.TEXTURE_2D, 0, 4, gl.Sizei(w), gl.Sizei(h), 0, gl.RGBA, gl.UNSIGNED_BYTE, gl.Pointer(&pix[0]))

	return nil
}

func (m Image) String() string {
	return fmt.Sprintf("image{%q, %dx%d}", m.name, m.width, m.height)
}

func NewImage(name string, pix func() ([]uint8, int, int)) *Image {
	m := &Image{name: name, pix: pix}
	imageCache[name] = m
	rcache.add(m)
	return m
}

// ----------------------------------------------------------------------------
// conversion

func nrgbaToPix(rgba *image.NRGBA) []byte {
	w, h := rgba.Rect.Max.X, rgba.Rect.Max.Y
	data := make([]byte, w*h*4)
	lineLen := w * 4
	dest := 0
	for src := 0; src < len(rgba.Pix); src += rgba.Stride {
		copy(data[dest:dest+lineLen], rgba.Pix[src:src+rgba.Stride])
		dest += lineLen
	}
	return data
}

func rgbaToPix(rgba *image.RGBA) []byte {
	w, h := rgba.Rect.Max.X, rgba.Rect.Max.Y
	data := make([]byte, w*h*4)
	lineLen := w * 4
	dest := 0
	for src := 0; src < len(rgba.Pix); src += rgba.Stride {
		copy(data[dest:dest+lineLen], rgba.Pix[src:src+rgba.Stride])
		dest += lineLen
	}
	return data
}

func imageToPix(m image.Image) []byte {
	nrgba, ok := m.(*image.NRGBA)
	if ok {
		return nrgbaToPix(nrgba)
	}
	rgba, ok := m.(*image.RGBA)
	if ok {
		return rgbaToPix(rgba)
	}
	b := m.Bounds()
	rgba = image.NewRGBA(image.Rect(0, 0, b.Dx(), b.Dy()))
	draw.Draw(rgba, rgba.Bounds(), m, b.Min, draw.Src)
	return rgbaToPix(rgba)
}

func (m *Image) load() error {
	if m.pix != nil {
		m.Upload(m.pix())
		return nil
	}
	f, err := fs.Open(m.name)
	if err != nil {
		return err
	}
	defer f.Close()
	p, _, err := image.Decode(f)
	if err != nil {
		return errors.New(fmt.Sprintf("image %q: %v", m.name, err))
	}
	b := p.Bounds()
	if err := m.Upload(imageToPix(p), b.Dx(), b.Dy()); err != nil {
		return err
	}
	return nil
}

func (m *Image) unload() error {
	if m.id > 0 {
		gl.DeleteTextures(1, &m.id)
		m.id = 0
	}
	return nil
}

// ----------------------------------------------------------------------------
// images cache

// imageCache
var imageCache = make(map[string]*Image)

func LookupImage(name string) *Image {
	if m, ok := imageCache[name]; ok {
		return m
	}

	m := &Image{name: name}
	if err := m.load(); err != nil {
		log.Println(err)
		return imageCache["@default"]
	}

	imageCache[name] = m
	rcache.add(m)

	return m
}
