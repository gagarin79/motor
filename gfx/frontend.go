package gfx

import (
	"github.com/gagarin79/motor/gfx/gl"
)

func Render() {
	tr.issueCommands()
}

func SwapFrames() {
	tr.swapFrames()
}

func BeginFrame() {
	cmd := tr.allocDrawBufferCommand()
	cmd.buffer = gl.BACK
}

func EndFrame() {
	_ = tr.allocSwapBuffersCommand()
}

func SetColor(r, g, b, a float32) {
	cmd := tr.allocSetColorCommand()
	cmd.color[0], cmd.color[1], cmd.color[2], cmd.color[3] = r, g, b, a
}

func Draw(s Shader, x, y, w, h, s1, t1, s2, t2 float32) {
	cmd := tr.allocDrawCommand()
	cmd.shader = s
	cmd.x, cmd.y, cmd.w, cmd.h = x, y, w, h
	cmd.s1, cmd.t1, cmd.s2, cmd.t2 = s1, t1, s2, t2
}
