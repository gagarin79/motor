package motor

import (
	"flag"
	"github.com/gagarin79/motor/cmd"
	"github.com/gagarin79/motor/fs"
	"github.com/gagarin79/motor/gfx"
	"github.com/gagarin79/motor/input"
	"go/build"
	"log"
	"runtime"
)

// Game interface provides access to client game.
type Game interface {
	Init()
	Shutdown()
	Update()
}

// A Config controls game paramters.
type Config struct {
	Game Game

	BaseDir string // use standard if empty
	UserDir string // use standard if empty

	Name    string
	Title   string
	Version string
}

// gameHost controls the game process running.
type gameHost struct {
	Config
	Game

	work chan bool
	wait chan bool
	done chan bool

	isrun    bool
	isactive bool
}

var host gameHost

func Run(cfg Config) {
	defer writeLog()

	runtime.GOMAXPROCS(runtime.NumCPU())
	runtime.LockOSThread()

	fs.SetDirs(cfg.BaseDir, cfg.UserDir)

	host.Config, host.Game = cfg, cfg.Game
	host.isrun = true
	host.work = make(chan bool)
	host.wait = make(chan bool)
	host.done = make(chan bool)

	log.Printf("%s v%s (%v %v-%v)\n", host.Name, host.Version,
		runtime.Version(), build.Default.GOOS, build.Default.GOARCH)

	readConfig()
	flag.Parse()

	host.init()
	host.run()
	host.shutdown()

	writeConfig()
}

// ----------------------------------------------------------------------------
// initialization and activation

func (h *gameHost) init() {
	video.init()
	input.Init()

	h.activate()

	h.Init()
	h.runClient()
}

func (h *gameHost) shutdown() {
	h.stopClient()
	h.Shutdown()

	h.deactivate()

	input.Shutdown()
	video.shutdown()
}

func (h *gameHost) restart() {
	h.deactivate()
	input.Shutdown()
	video.shutdown()
	video.init()
	input.Init()
	h.activate()
}

func (h *gameHost) activate() {
	if !h.isactive {
		h.isactive = true
		video.activate()
		input.Capture()
	}
}

func (h *gameHost) deactivate() {
	if h.isactive {
		input.Release()
		video.deactivate()
		h.isactive = false
	}
}

func (h *gameHost) quit() {
	h.isrun = false
}

// ----------------------------------------------------------------------------
// main loop

func (h *gameHost) run() {
	for h.isrun {
		pollEvents()
		input.Update()
		cmd.ExecuteCalls()

		h.workClient()
		gfx.Render()
		h.waitClient()

		gfx.SwapFrames()
	}
}

// ----------------------------------------------------------------------------
// game management

func (g *gameHost) runClient() {
	go func(){
		for {
			select {
			case <- g.work:
				gfx.BeginFrame()
				g.Update()
				gfx.EndFrame()
				g.wait <- true
			case <- g.done:
				break
			}
		}
	}()
}

func (g *gameHost) stopClient() {
	g.done <- true
}

func (g *gameHost) workClient() {
	g.work <- true
}

func (g *gameHost) waitClient() {
	<- g.wait
}
