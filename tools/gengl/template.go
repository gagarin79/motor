package main

import "text/template"

var gltmpl = template.Must(template.
	New("gl").
	Parse(`// Automatically generated OpenGL binding.
// {{.Api}}{{.Version}}-{{.Profile}}
package gl

// #cgo darwin  LDFLAGS: -framework OpenGL
// #cgo linux   LDFLAGS: -lGL
// #cgo windows LDFLAGS: -lopengl32
//
// #include <stdlib.h>
// #if defined(__APPLE__)
// #include <dlfcn.h>
// #elif defined(_WIN32)
// #define WIN32_LEAN_AND_MEAN 1
// #include <windows.h>
// #else
// #include <X11/Xlib.h>
// #include <GL/glx.h>
// #endif
//
// #ifndef APIENTRY
// #define APIENTRY
// #endif
// #ifndef APIENTRYP
// #define APIENTRYP APIENTRY *
// #endif
// #ifndef GLAPI
// #define GLAPI extern
// #endif
//
// #ifdef _WIN32
// static HMODULE hOpengl32Lib = NULL;
// #endif
//
// static void* GetGLProcAddress(const char* name) {
// #ifdef __APPLE__
//     return dlsym(RTLD_DEFAULT, name);
// #elif _WIN32
//     void* pf = wglGetProcAddress((LPCSTR)name);
// 	   if(pf) {
//         return pf;
//     }
//     if(hOpengl32Lib == NULL) {
//         hOpengl32Lib = LoadLibraryA("opengl32.dll");
//     }
//     return GetProcAddress(hOpengl32Lib, (LPCSTR)name);
// #else
//     return glXGetProcAddress((const GLubyte*)name);
// #endif
// }
// {{range .Features}}
// #ifndef {{.Name}}
// #define {{.Name}} 1
// {{range .Types}}{{.Decl}}
// {{end}}
// {{range .Commands}}{{.CType}} (APIENTRYP {{.Name}}Proc)({{range .Params}}{{.CDecl}}{{end}});
// {{end}}
// {{range .Commands}}inline {{.CType}} {{.Name}}({{range .Params}}{{.CDecl}}{{end}}){ {{if .IsFunc}}return{{end}} {{.Name}}Proc({{range .Params}}{{.CCallDecl}}{{end}}); }
// {{end}}
// int init{{.Name}}() { {{range .Commands}}
//     if (({{.Name}}Proc = GetGLProcAddress("{{.Name}}")) == NULL) return 0;{{end}}
//     return 1;
// }
// #endif // {{.Name}}{{end}}
//
import "C"
import "unsafe"
import "errors"

type Pointer unsafe.Pointer
{{range .Features}}
// ----------------------------------------------------------------------------
// {{.Name}}

{{if .GoTypes}}type (
{{range .GoTypes}}	{{.GoName}} C.{{.Name}}
{{end}}){{end}}
{{if .Consts}}const (
{{range .Consts}}	{{.Name}} = {{.Value}}
{{end}}){{end}}
{{range .Commands}}
// https://www.opengl.org/sdk/docs/man/xhtml/{{.Name}}
func {{.GoName}}({{range .Params}}{{.GoDecl}}{{end}}){{if .IsFunc}} {{.GoType}} {{end}}{
	{{if .IsFunc}}return ({{.GoType}})({{end}}C.{{.Name}}({{range .Params}}{{.CgDecl}}{{end}}){{if .IsFunc}}){{end}}
}
{{end}}
func init{{.GoName}}() error {
	if ret := C.init{{.Name}}(); ret == 0 {
		return errors.New("unable to initialize {{.Name}}")
	}
	return nil
}
{{end}}

func Init() error {
	{{range .Features}}if err := init{{.GoName}}(); err != nil {
		return err
	}
	{{end}}
	return nil
}

// ----------------------------------------------------------------------------
// Utils

// Go bool to GL boolean.
func GLBool(b bool) Boolean {
	if b {
		return TRUE
	}
	return FALSE
}

// GL boolean to Go bool.
func GoBool(b Boolean) bool {
	return b == TRUE
}

// Go string to GL string.
func GLString(str string) *Char {
	return (*Char)(C.CString(str))
}

// Allocates a GL string.
func GLStringAlloc(length Sizei) *Char {
	return (*Char)(C.malloc(C.size_t(length)))
}

// Frees GL string.
func GLStringFree(str *Char) {
	C.free(unsafe.Pointer(str))
}

// GL string (GLchar*) to Go string.
func GoString(str *Char) string {
	return C.GoString((*C.char)(str))
}

// GL string (GLubyte*) to Go string.
func GoStringUb(str *Ubyte) string {
	return C.GoString((*C.char)(unsafe.Pointer(str)))
}

// GL string (GLchar*) with length to Go string.
func GoStringN(str *Char, length Sizei) string {
	return C.GoStringN((*C.char)(str), C.int(length))
}

// Converts a list of Go strings to a slice of GL strings.
// Usefull for ShaderSource().
func GLStringArray(strs ...string) []*Char {
	strSlice := make([]*Char, len(strs))
	for i, s := range strs {
		strSlice[i] = (*Char)(C.CString(s))
	}
	return strSlice
}

// Free GL string slice allocated by GLStringArray().
func GLStringArrayFree(strs []*Char) {
	for _, s := range strs {
		C.free(unsafe.Pointer(s))
	}
}

// Add offset to a pointer. Usefull for VertexAttribPointer,
// DrawElements, etc.
func Offset(p Pointer, o uintptr) Pointer {
	return Pointer(uintptr(p) + o)
}
`))
