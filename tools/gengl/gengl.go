package main

import (
	"encoding/xml"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
)

const (
	registryBaseURL = "https://cvs.khronos.org/svn/repos/ogl/trunk/doc/registry/public/api/"
	defaultSpecsDir = "src/github.com/gagarin79/motor/gfx/gl/specs"
	defaultPkgDir   = "src/github.com/gagarin79/motor/gfx/gl"
)

var (
	root = os.Getenv("GOPATH")

	// common options
	force    = flag.Bool("force", false, "Force download specs")
	specsDir = flag.String("specsDir", fmt.Sprintf("%s/%s", root, defaultSpecsDir), "Specs output directory")
	pkgDir   = flag.String("pkgDir", fmt.Sprintf("%s/%s", root, defaultPkgDir), "Package output directory")
	spec     = flag.String("spec", "gl.xml", "OpenGL spec file name")
	pkg      = flag.String("pkg", "gl.go", "OpenGL package file name")

	// gl api specification
	api  = flag.String("api", "gl", "OpenGL APIs")
	prof = flag.String("profile", "core", "OpenGL profile")
	ver  = flag.Float64("version", 3.3, "OpenGL version")
)

func downloadSpecFile(fileName string, force bool) error {
	url := registryBaseURL + fileName
	fullName := filepath.Join(*specsDir, fileName)
	if _, err := os.Stat(fullName); !os.IsNotExist(err) && !force {
		return nil
	}
	r, err := http.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}
	if err := ioutil.WriteFile(fullName, data, 0644); err != nil {
		return err
	}
	return nil
}

func generatePackageFile(spec, pkg, api, prof string, ver float64) error {
	src := filepath.Join(*specsDir, spec)
	data, err := ioutil.ReadFile(src)
	if err != nil {
		return err
	}
	r, glr := Registry{}, GLRegistry{}
	if err := xml.Unmarshal(data, &r); err != nil {
		return err
	}

	dst := filepath.Join(*pkgDir, pkg)
	f, err := os.Create(dst)
	if err != nil {
		return err
	}
	defer f.Close()

	glr.init(&r, api, prof, ver)
	gltmpl.Execute(f, &glr)

	fmt.Println(dst)

	return nil
}

func main() {
	if root == "" {
		log.Fatalln("GOPATH environment variable not found")
	}

	flag.Parse()

	if err := os.MkdirAll(*specsDir, 0755); err != nil {
		log.Fatalln(err)
	}
	if err := os.MkdirAll(*pkgDir, 0755); err != nil {
		log.Fatalln(err)
	}

	if err := downloadSpecFile(*spec, *force); err != nil {
		log.Fatalln(err)
	}
	if err := generatePackageFile(*spec, *pkg, *api, *prof, *ver); err != nil {
		log.Fatalln(err)
	}
}
