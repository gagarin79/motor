package main

import (
	"encoding/xml"
	"fmt"
	"html"
	"strings"
	"unicode"
	"unicode/utf8"
)

// ----------------------------------------------------------------------------
// Registry

var reservedNames = [...]string{
	"break", "default", "func", "interface", "select",
	"case", "defer", "go", "map", "struct",
	"chan", "else", "goto", "package", "switch",
	"const", "fallthrough", "if", "range", "type",
	"continue", "for", "import", "return", "var",
	"near", "far",
}

type RegistryType struct {
	Api      string `xml:"api,attr"`
	Requires string `xml:"requires,attr"`
	AttrName string `xml:"name,attr"`
	Name     string `xml:"name"`
	Value    string `xml:",chardata"`
	InnerXml string `xml:",innerxml"`
}

type RegistryEnum struct {
	Name  string `xml:"name,attr"`
	Value string `xml:"value,attr"`
}

type RegistryCommandParamType struct {
	Type  string `xml:"ptype"`
	Name  string `xml:"name"`
	Inner string `xml:",chardata"`
}

type RegistryCommandProto struct {
	RegistryCommandParamType
}

type RegistryCommandParam struct {
	RegistryCommandParamType
	InnerXml string `xml:",innerxml"`
}

type RegistryCommand struct {
	Proto  RegistryCommandProto   `xml:"proto"`
	Params []RegistryCommandParam `xml:"param"`
}

type RegistryFeatureItem struct {
	Name string `xml:"name,attr"`
}

type RegistryRequire struct {
	Profile string `xml:"profile"`

	Types    []RegistryFeatureItem `xml:"type"`
	Enums    []RegistryFeatureItem `xml:"enum"`
	Commands []RegistryFeatureItem `xml:"command"`

	types map[string]*RegistryType
}

type RegistryFeature struct {
	Api    string  `xml:"api,attr"`
	Name   string  `xml:"name,attr"`
	Number float64 `xml:"number,attr"`

	Requires []RegistryRequire `xml:"require"`
	Removes  []RegistryRequire `xml:"remove"`
}

type Registry struct {
	XMLName  xml.Name          `xml:"registry"`
	Types    []RegistryType    `xml:"types>type"`
	Enums    []RegistryEnum    `xml:"enums>enum"`
	Commands []RegistryCommand `xml:"commands>command"`
	Features []RegistryFeature `xml:"feature"`

	types    map[string]*RegistryType
	enums    map[string]*RegistryEnum
	commands map[string]*RegistryCommand

	removes map[string]int

	usedTypes map[string]*RegistryType
	usedEnums map[string]*RegistryEnum
}

func (r *Registry) findType(name string) *RegistryType {
	for i := range r.Types {
		t := &r.Types[i]
		if t.AttrName == name || t.Name == name {
			return t
		}
	}
	return nil
}

// ----------------------------------------------------------------------------
// GLRegistry

type GLType struct {
	Name    string
	GoName  string
	Decl    string
	GoExp   bool
	Require string
}

func makeGLType(rt *RegistryType) GLType {
	decl := html.UnescapeString(rt.InnerXml)
	decl = strings.Replace(decl, "<name>", "", -1)
	decl = strings.Replace(decl, "</name>", "", -1)
	decl = strings.Replace(decl, "<apientry/>", "APIENTRY", -1)
	decl = strings.Replace(decl, "\n", "\n// ", -1)

	goname := strings.Title(strings.Replace(rt.Name, "GL", "", 1))

	return GLType{rt.Name, goname, decl, rt.AttrName == "", rt.Requires}
}

type GLConst struct {
	Name  string
	Value string
}

func makeGLConst(re *RegistryEnum) GLConst {
	name := re.Name
	if strings.HasPrefix(name, "GL_") {
		name = name[3:]
		if unicode.IsDigit(rune(name[0])) {
			name = "X" + name
		}
	}
	return GLConst{name, re.Value}
}

type GLParam struct {
	Name   string
	GoName string
	Type   string
	Ptr    string

	CType, CDecl   string
	CCallDecl      string
	GoType, GoDecl string
	CgType, CgDecl string
}

func makeGLParam(rp *RegistryCommandParamType) GLParam {
	p := GLParam{Name: rp.Name, GoName: rp.Name, Type: rp.Type}
	tlist := strings.Fields(rp.Inner)
	if len(tlist) > 0 {
		if tlist[0] == "const" {
			tlist = tlist[1:]
		}
		if tlist[0] == "**" || tlist[0] == "*const*" {
			p.Ptr = "**"
		} else if tlist[0] == "*" {
			p.Ptr = "*"
		} else if p.Type == "" {
			p.Type = tlist[0]
		}
		if len(tlist) > 1 {
			if tlist[1] == "**" || tlist[1] == "*const*" {
				p.Ptr = "**"
			} else {
				p.Ptr = "*"
			}
		}
	}
	p.CType = p.Type
	p.GoType = strings.Title(strings.Replace(p.Type, "GL", "", 1))
	p.CgType = "C." + p.Type
	if p.Ptr == "**" {
		p.CType = p.Type + "**"
		if p.GoType == "Void" {
			p.GoType = "*Pointer"
			p.CgType = "*unsafe.Pointer"
		} else {
			p.GoType = "**" + p.GoType
			p.CgType = "**C." + p.Type
		}
	} else if p.Ptr == "*" {
		p.CType = p.Type + "*"
		if p.GoType == "Void" {
			p.GoType = "Pointer"
			p.CgType = "unsafe.Pointer"
		} else {
			p.GoType = "*" + p.GoType
			p.CgType = "*C." + p.Type
		}
	} else {
	}
	for _, r := range reservedNames {
		if p.Name == r {
			p.Name = p.Name + "_"
		}
	}
	p.CDecl = p.CType + " " + p.Name
	p.CCallDecl = p.Name
	p.GoDecl = p.Name + " " + p.GoType
	if p.Ptr == "**" {
		p.CgDecl = fmt.Sprintf("(%s)(unsafe.Pointer(%s))", p.CgType, p.Name)
	} else {
		p.CgDecl = fmt.Sprintf("(%s)(%s)", p.CgType, p.Name)
	}
	return p
}

type GLCommand struct {
	GLParam
	Params []GLParam
	IsFunc bool
}

func makeGLCommand(rc *RegistryCommand) GLCommand {
	c := GLCommand{
		GLParam: makeGLParam(&rc.Proto.RegistryCommandParamType),
		Params:  make([]GLParam, 0, len(rc.Params)),
	}

	if strings.HasPrefix(c.GoName, "gl") && !strings.HasPrefix(c.GoName, "glX") {
		c.GoName = c.GoName[2:]
	} else {
		r, _ := utf8.DecodeRuneInString(c.GoName)
		if unicode.IsUpper(r) {
			c.GoName = string([]rune{unicode.ToLower(r)}) + c.GoName[1:]
		}
	}

	for i := range rc.Params {
		p := makeGLParam(&rc.Params[i].RegistryCommandParamType)
		if i < len(rc.Params)-1 {
			p.CDecl += ", "
			p.CCallDecl += ", "
			p.GoDecl += ", "
			p.CgDecl += ", "
		}
		c.Params = append(c.Params, p)
	}

	c.IsFunc = !strings.Contains(strings.ToLower(c.Type), "void") || c.Ptr != ""

	return c
}

type GLFeature struct {
	r *GLRegistry

	Name   string
	GoName string

	Types    []GLType
	GoTypes  []GLType
	Consts   []GLConst
	Commands []GLCommand
}

func (f *GLFeature) init(rf *RegistryFeature) {
	f.Types = make([]GLType, 0)
	f.GoTypes = make([]GLType, 0)
	f.Consts = make([]GLConst, 0)
	f.Commands = make([]GLCommand, 0)
	for _, req := range rf.Requires {
		if req.Profile != "" && req.Profile != f.r.Profile {
			continue
		}
		// find types from commands
		for _, fc := range req.Commands {
			c, found := f.r.commands[fc.Name]
			if !found {
				continue
			}
			if dup := f.r.dups[c.Type]; dup {
				continue
			}
			if t, found := f.r.types[c.Type]; found {
				if t.Require != "" {
					if rt, found := f.r.types[t.Require]; found {
						f.Types = append(f.Types, rt)
						f.r.dups[rt.Name] = true
					}
				}
				f.Types = append(f.Types, t)
				f.r.dups[c.Type] = true
			}
			for _, p := range c.Params {
				if dup := f.r.dups[p.Type]; dup {
					continue
				}
				if t, found := f.r.types[p.Type]; found {
					if t.Require != "" {
						if rt, found := f.r.types[t.Require]; found {
							f.Types = append(f.Types, rt)
							f.r.dups[rt.Name] = true
						}
					}
					f.Types = append(f.Types, t)
					f.r.dups[p.Type] = true
				}
			}
		}
		// init types from registry
		for _, ft := range req.Types {
			dup := f.r.dups[ft.Name]
			if t, found := f.r.types[ft.Name]; found && !dup {
				f.Types = append(f.Types, t)
				f.r.dups[ft.Name] = true
			}
		}
		// init consts
		for _, fe := range req.Enums {
			dup := f.r.dups[fe.Name]
			if c, found := f.r.consts[fe.Name]; found && !dup {
				f.Consts = append(f.Consts, c)
				f.r.dups[fe.Name] = true
			}
		}
		// init commands
		for _, fc := range req.Commands {
			dup := f.r.dups[fc.Name]
			if c, found := f.r.commands[fc.Name]; found && !dup {
				f.Commands = append(f.Commands, c)
			}
		}
	}
	f.Name = rf.Name
	fields := strings.FieldsFunc(f.Name, func(r rune) bool { return r == '_' })
	for i := range fields {
		fields[i] = strings.Title(strings.ToLower(fields[i]))
	}
	// init go types
	for _, t := range f.Types {
		if t.GoExp {
			f.GoTypes = append(f.GoTypes, t)
		}
	}
	f.GoName = strings.Title(strings.Join(fields, ""))
}

type GLRegistry struct {
	Api     string
	Profile string
	Version float64

	Features []*GLFeature

	types    map[string]GLType
	consts   map[string]GLConst
	commands map[string]GLCommand

	dups map[string]bool
}

func (r *GLRegistry) init(reg *Registry, api, profile string, version float64) {
	r.Api = api
	r.Profile = profile
	r.Version = version

	r.Features = make([]*GLFeature, 0)

	r.types = make(map[string]GLType, len(reg.Types))
	r.consts = make(map[string]GLConst, len(reg.Enums))
	r.commands = make(map[string]GLCommand, len(reg.Commands))

	r.dups = make(map[string]bool)

	removes := make(map[string]bool)

	// add removes
	for _, f := range reg.Features {
		if f.Api != api || f.Number > version {
			continue
		}
		for _, req := range f.Removes {
			if req.Profile != "" && req.Profile != profile {
				continue
			}
			for _, t := range req.Types {
				removes[t.Name] = true
			}
			for _, e := range req.Enums {
				removes[e.Name] = true
			}
			for _, c := range req.Commands {
				removes[c.Name] = true
			}
		}
	}

	// init types
	for _, t := range reg.Types {
		if _, remove := removes[t.Name]; !remove && (t.Api == "" || t.Api == api) {
			if t.Requires != "" {
				rt := reg.findType(t.Requires)
				if rt != nil {
					if _, remove := removes[t.Name]; !remove {
						r.types[t.Requires] = makeGLType(rt)
					}
				}
			}
			r.types[t.Name] = makeGLType(&t)
		}
	}
	// init consts
	for _, e := range reg.Enums {
		if _, remove := removes[e.Name]; !remove {
			r.consts[e.Name] = makeGLConst(&e)
		}
	}
	// init commands
	for _, c := range reg.Commands {
		if _, remove := removes[c.Proto.Name]; !remove {
			r.commands[c.Proto.Name] = makeGLCommand(&c)
		}
	}
	// init features
	for i := range reg.Features {
		rf := &reg.Features[i]
		if rf.Api == api && rf.Number <= version {
			f := &GLFeature{r: r}
			f.init(rf)
			r.Features = append(r.Features, f)
		}
	}
}
