package motor

import (
	"github.com/AllenDang/w32"
	"github.com/gagarin79/motor/input"
)

func wndProc(hwnd w32.HWND, msg uint32, wparam, lparam uintptr) uintptr {
	switch msg {
	case w32.WM_CREATE:
		win32.wincreated = true
		return 0
	case w32.WM_ACTIVATE:
		if win32.wincreated {
			win32.wincreated = false
			return 0
		}
		if w32.LOWORD(uint32(wparam)) != 0 {
			host.activate()
		} else {
			host.deactivate()
		}
		return 0
	case w32.WM_MOVE:
		if !video.fullscreen {
			xpos, ypos := w32.LOWORD(uint32(lparam)), w32.HIWORD(uint32(lparam))
			r := w32.RECT{0, 0, 1, 1}
			w32.AdjustWindowRect(&r, uint(w32.GetWindowLong(hwnd, w32.GWL_STYLE)), false)
			video.x, video.y = int(int32(xpos)+r.Left), int(int32(ypos)+r.Top)
		}
	case w32.WM_SYSKEYDOWN, w32.WM_KEYDOWN:
		if msg == w32.WM_SYSKEYDOWN && wparam == 13 {
			video.fullscreen = !video.fullscreen
			host.restart()
			return 0
		}
		input.PushEvent(input.KeyEv, int(mapKey(lparam)), 1)
	case w32.WM_SYSKEYUP, w32.WM_KEYUP:
		input.PushEvent(input.KeyEv, int(mapKey(lparam)), 0)
	case w32.WM_CLOSE:
		w32.DestroyWindow(hwnd)
	}
	return w32.DefWindowProc(hwnd, msg, wparam, lparam)
}

func pollEvents() {
	var msg w32.MSG
	for w32.PeekMessage(&msg, w32.HWND(0), 0, 0, w32.PM_NOREMOVE) {
		if w32.GetMessage(&msg, w32.HWND(0), 0, 0) == 0 {
			host.quit()
		}
		w32.TranslateMessage(&msg)
		w32.DispatchMessage(&msg)
	}
}

var scantokey = [256]int{
	0, 27, '1', '2', '3', '4', '5', '6',
	'7', '8', '9', '0', '-', '=', input.Backspace, 9,
	'q', 'w', 'e', 'r', 't', 'y', 'u', 'i',
	'o', 'p', '[', ']', input.Enter, input.Ctrl, 'a', 's',
	'd', 'f', 'g', 'h', 'j', 'k', 'l', ';',
	'\'', '`', input.Shift, '\\', 'z', 'x', 'c', 'v',
	'b', 'n', 'm', ',', '.', '/', input.Shift, input.KpStar,
	input.Alt, ' ', input.Capslock, input.F1, input.F2, input.F3, input.F4, input.F5,
	input.F6, input.F7, input.F8, input.F9, input.F10, input.Pause, input.Scroll, input.Home,
	input.UpArrow, input.PgUp, input.KpMinus, input.LeftArrow, input.Kp5, input.RightArrow, input.KpPlus, input.End,
	input.DownArrow, input.PgDn, input.Ins, input.Del, 0, 0, 0, input.F11,
	input.F12, 0, 0, input.LSuper, input.RSuper, input.Menu, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	// Shifted
	0, 27, '!', '@', '#', '$', '%', '^',
	'&', '*', '(', ')', '_', '+', input.Backspace, 9,
	'q', 'w', 'e', 'r', 't', 'y', 'u', 'i',
	'o', 'p', '[', ']', input.Enter, input.Ctrl, 'a', 's',
	'd', 'f', 'g', 'h', 'j', 'k', 'l', ';',
	'\'', '~', input.Shift, '\\', 'z', 'x', 'c', 'v',
	'b', 'n', 'm', ',', '.', '/', input.Shift, input.KpStar,
	input.Alt, ' ', input.Capslock, input.F1, input.F2, input.F3, input.F4, input.F5,
	input.F6, input.F7, input.F8, input.F9, input.F10, input.Pause, input.Scroll, input.Home,
	input.UpArrow, input.PgUp, input.KpMinus, input.LeftArrow, input.Kp5, input.RightArrow, input.KpPlus, input.End,
	input.DownArrow, input.PgDn, input.Ins, input.Del, 0, 0, 0, input.F11,
	input.F12, 0, 0, input.LSuper, input.RSuper, input.Menu, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
}

func mapKey(key uintptr) (res input.Key) {
	mod := (key >> 16) & 255
	if mod > 127 {
		return 0
	}
	ext := key & (1 << 24)
	res = input.Key(scantokey[mod])
	if ext != 0 {
		switch res {
		case input.Home:
			return input.KpHome
		case input.UpArrow:
			return input.KpUpArrow
		case input.PgUp:
			return input.KpPgUp
		case input.LeftArrow:
			return input.KpLeftArrow
		case input.RightArrow:
			return input.KpRightArrow
		case input.End:
			return input.KpEnd
		case input.DownArrow:
			return input.KpDownArrow
		case input.PgDn:
			return input.KpPgDn
		case input.Ins:
			return input.KpIns
		case input.Del:
			return input.KpDel
		}
	} else {
		switch res {
		case 0x0D:
			return input.KpEnter
		case 0x2F:
			return input.KpSlash
		case 0xAF:
			return input.KpPlus
		}
	}
	return
}
