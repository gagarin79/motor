package motor

import (
	"github.com/gagarin79/motor/cmd"
	"github.com/gagarin79/motor/convar"
	"log"
)

func quitCmd(a []string) {
	host.quit()
}

func restartCmd(a []string) {
	host.restart()
}

func writeConfigCmd(a []string) {
	writeConfig()
}

func listCmds(a []string) {
	log.Println("commands:")
	cmd.Visit(func(name string) {
		log.Printf("  %s\n", name)
	})
}

func listVars(a []string) {
	log.Println("vars:")
	convar.Visit(func(name string, v convar.Var) {
		log.Printf("  %s = %v\n", name, v)
	})
}

func init() {
	cmd.Register("quit", quitCmd)
	cmd.Register("restart", restartCmd)
	cmd.Register("writeConfig", writeConfigCmd)
	cmd.Register("listCmds", listCmds)
	cmd.Register("listVars", listVars)
}
