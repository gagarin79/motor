package motor

import (
	"github.com/AllenDang/w32"
	"github.com/gagarin79/motor/gfx"
	"github.com/gagarin79/motor/input"
	"log"
	"syscall"
	"unsafe"
)

var win32 struct {
	hwnd       w32.HWND
	registered bool
	created    bool
	fullscreen bool
	wincreated bool
}

func enumModes() {
	var (
		dm  w32.DEVMODE
		vms = make([]videoMode, 0)
		mn  uint32
	)
	dm.DmSize = uint16(unsafe.Sizeof(dm))
	for w32.EnumDisplaySettingsEx(nil, mn, &dm, 0) {
		vms = append(vms, videoMode{
			int(dm.DmPelsWidth),
			int(dm.DmPelsHeight), true,
		})
		mn++
	}
	for i := range modes {
		m := &modes[i]
		m.valid = false
		for j := range vms {
			vm := &vms[j]
			if m.w == vm.w && m.h == vm.h {
				m.valid = true
			}
		}
	}
}

func (v *videoSys) activate() {
	if !win32.fullscreen && v.fullscreen {
		var dm w32.DEVMODE
		dm.DmSize = uint16(unsafe.Sizeof(dm))

		if !w32.EnumDisplaySettingsEx(nil, w32.ENUM_CURRENT_SETTINGS, &dm, 0) {
			log.Println("failed get current display settings")
			return
		}

		runchange := dm.DmPelsWidth != uint32(v.w) || dm.DmPelsHeight != uint32(v.h)

		dm.DmPelsWidth = uint32(v.w)
		dm.DmPelsHeight = uint32(v.h)
		dm.DmBitsPerPel = 32
		dm.DmFields = w32.DM_PELSWIDTH | w32.DM_PELSHEIGHT | w32.DM_BITSPERPEL
		w32.ShowWindow(win32.hwnd, w32.SW_RESTORE)
		if runchange {
			if w32.ChangeDisplaySettingsEx(nil, &dm, w32.HWND(0), w32.CDS_FULLSCREEN, 0) != w32.DISP_CHANGE_SUCCESSFUL {
				log.Println("failed change display settings")
				return
			}
		}
		w32.UpdateWindow(win32.hwnd)
		win32.fullscreen = v.fullscreen
	}
	win32.fullscreen = v.fullscreen
}

func (v *videoSys) deactivate() {
	if win32.fullscreen {
		win32.wincreated = true
		w32.ShowWindow(win32.hwnd, w32.SW_MINIMIZE)
		w32.ChangeDisplaySettingsEx(nil, nil, w32.HWND(0), 0, 0)
		win32.fullscreen = false
	}
}

func (v *videoSys) createWindow() {
	wcname := syscall.StringToUTF16Ptr("@" + host.Name + "WindowClassEx@")
	var (
		wc       w32.WNDCLASSEX
		exst, st uint
	)

	if !win32.registered {
		wc.Size = uint32(unsafe.Sizeof(wc))
		wc.WndProc = syscall.NewCallback(wndProc)
		wc.Instance = w32.GetModuleHandle("")
		wc.Icon = w32.LoadIcon(0, (*uint16)(unsafe.Pointer(uintptr(w32.IDI_APPLICATION))))
		wc.Cursor = w32.LoadCursor(0, (*uint16)(unsafe.Pointer(uintptr(w32.IDC_ARROW))))
		wc.Background = w32.COLOR_BTNFACE + 1
		wc.MenuName = nil
		wc.ClassName = wcname
		wc.IconSm = w32.LoadIcon(0, (*uint16)(unsafe.Pointer(uintptr(w32.IDI_APPLICATION))))

		if res := w32.RegisterClassEx(&wc); res == 0 {
			panic("RegisterClassEx")
		}
		win32.registered = true
	}
	x, y := v.x, v.y
	w, h := v.w, v.h

	if v.fullscreen {
		exst = w32.WS_EX_TOPMOST
		st = w32.WS_POPUP | w32.WS_VISIBLE | w32.WS_SYSMENU
		x, y = 0, 0
	} else {
		exst = 0
		st = w32.WS_OVERLAPPED | w32.WS_BORDER | w32.WS_CAPTION | w32.WS_VISIBLE | w32.WS_SYSMENU
		if x < 0 {
			x = w32.GetSystemMetrics(w32.SM_CXFULLSCREEN)/2 - w/2
		}
		if y < 0 {
			y = w32.GetSystemMetrics(w32.SM_CYFULLSCREEN)/2 - h/2
		}
		v.x, v.y = x, y
		r := w32.RECT{0, 0, int32(w), int32(h)}
		w32.AdjustWindowRect(&r, st, false)
		w, h = int(r.Right-r.Left), int(r.Bottom-r.Top)
	}

	if !win32.created {
		if hwnd := w32.CreateWindowEx(
			exst,
			wcname,
			syscall.StringToUTF16Ptr(host.Title),
			st,
			x, y, w, h,
			w32.HWND(0), w32.HMENU(0), w32.GetModuleHandle(""), unsafe.Pointer(nil)); hwnd == 0 {
			panic("CreateWindowEx")
		} else {
			win32.hwnd, input.Hwnd, gfx.Hwnd = hwnd, hwnd, hwnd
		}
		win32.created = true
	} else {
		w32.SetWindowLong(win32.hwnd, w32.GWL_EXSTYLE, uint32(exst))
		w32.SetWindowLong(win32.hwnd, w32.GWL_STYLE, uint32(st))
		if v.fullscreen {
			w32.SetWindowPos(win32.hwnd, w32.HWND_TOPMOST, 0, 0, w, h, w32.SWP_NOACTIVATE)
		} else {
			w32.SetWindowPos(win32.hwnd, w32.HWND_NOTOPMOST, x, y, w, h, w32.SWP_NOACTIVATE)
		}
	}
	w32.ShowWindow(win32.hwnd, w32.SW_SHOWDEFAULT)
	w32.UpdateWindow(win32.hwnd)
}
