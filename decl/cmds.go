package decl

import (
	"fmt"
	"github.com/gagarin79/motor/cmd"
	"log"
	"sort"
)

func sortDecls() []string {
	list := make(sort.StringSlice, len(decls))
	i := 0
	for k, v := range decls {
		list[i] = fmt.Sprintf("%s %q", v.Def(), k)
		i++
	}
	sort.Sort(list)
	return list
}

func listDeclsCmd(a []string) {
	log.Printf("decls:")
	for _, d := range sortDecls() {
		log.Printf("  %s\n", d)
	}
}

func reloadDeclsCmd(a []string) {
	Reload()
}

func init() {
	cmd.Register("listDecls", listDeclsCmd)
	cmd.Register("reloadDecls", reloadDeclsCmd)
}
