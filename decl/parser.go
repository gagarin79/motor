package decl

import (
	"fmt"
	"io"
	"log"
	"strconv"
	"strings"
	"text/scanner"
)

const (
	EOF       = scanner.EOF
	Ident     = scanner.Ident
	Int       = scanner.Int
	Float     = scanner.Float
	Char      = scanner.Char
	String    = scanner.String
	RawString = scanner.RawString
	Comment   = scanner.Comment
)

type Parser struct {
	filename string
	scanner  scanner.Scanner
	tok      rune
	hadError bool
}

func (p *Parser) Init(filename string, src io.Reader) {
	p.filename = filename
	p.scanner.Init(src)
	p.scanner.Error = func(s *scanner.Scanner, msg string) {
		log.Printf("error in %s:%d: %s", p.filename, s.Position.Line, msg)
	}
}

func (p *Parser) Error(format string, a ...interface{}) {
	p.scanner.Error(&p.scanner, fmt.Sprintf(format, a...))
}

func (p *Parser) HadError() bool {
	return p.hadError
}

func (p *Parser) Next() rune {
	p.tok = p.scanner.Scan()
	return p.tok
}

func (p *Parser) TokenText() string {
	return p.scanner.TokenText()
}

func (p *Parser) Expect(expect rune) bool {
	if p.tok != expect {
		p.Error("expected %s but found %s", scanner.TokenString(expect), scanner.TokenString(p.tok))
		return false
	}
	return true
}

func (p *Parser) ExpectIdent() (string, bool) {
	if p.tok != Ident {
		p.Error("expected Ident but found %s (%s)", scanner.TokenString(p.tok), p.TokenText())
		return "", false
	}
	return p.TokenText(), true
}

func (p *Parser) ExpectString() (string, bool) {
	if p.tok != String {
		p.Error("expected String but found %s (%s)", scanner.TokenString(p.tok), p.TokenText())
		return "", false
	}
	return strings.Trim(p.TokenText(), "\""), true
}

func (p *Parser) ExpectInt() (int, bool) {
	if p.tok != Int {
		p.Error("expected Int but found %s (%s)", scanner.TokenString(p.tok), p.TokenText())
		return 0, false
	}
	v, _ := strconv.ParseInt(p.TokenText(), 0, 64)
	return int(v), true
}

func (p *Parser) ExpectFloat() (float32, bool) {
	if p.tok != Int && p.tok != Float {
		p.Error("expected Int or Float but found %s (%s)", scanner.TokenString(p.tok), p.TokenText())
		return 0, false
	}
	if p.tok == Int {
		v, _ := strconv.ParseInt(p.TokenText(), 0, 64)
		return float32(v), true
	}
	v, _ := strconv.ParseFloat(p.TokenText(), 64)
	return float32(v), true
}

func (p *Parser) ParseIdent() (string, bool) {
	p.Next()
	return p.ExpectIdent()
}

func (p *Parser) ParseString() (string, bool) {
	p.Next()
	return p.ExpectString()
}

func (p *Parser) ParseInt() (int, bool) {
	p.Next()
	return p.ExpectInt()
}

func (p *Parser) ParseFloat() (float32, bool) {
	p.Next()
	return p.ExpectFloat()
}

func (p *Parser) ParseFloat2() (x float32, y float32, ok bool) {
	if x, ok = p.ParseFloat(); !ok {
		return
	}
	p.Next()
	if !p.Expect(',') {
		return
	}
	if y, ok = p.ParseFloat(); !ok {
		return
	}
	return
}

func (p *Parser) ParseFloat3() (x float32, y float32, z float32, ok bool) {
	if x, ok = p.ParseFloat(); !ok {
		return
	}
	p.Next()
	if !p.Expect(',') {
		return
	}
	if y, ok = p.ParseFloat(); !ok {
		return
	}
	p.Next()
	if !p.Expect(',') {
		return
	}
	if z, ok = p.ParseFloat(); !ok {
		return
	}
	return
}

func (p *Parser) ParseFloat4() (x float32, y float32, z float32, w float32, ok bool) {
	if x, ok = p.ParseFloat(); !ok {
		return
	}
	p.Next()
	if !p.Expect(',') {
		return
	}
	if y, ok = p.ParseFloat(); !ok {
		return
	}
	p.Next()
	if !p.Expect(',') {
		return
	}
	if z, ok = p.ParseFloat(); !ok {
		return
	}
	p.Next()
	if !p.Expect(',') {
		return
	}
	if w, ok = p.ParseFloat(); !ok {
		return
	}
	return
}

func (p *Parser) parseDecls() {
	for tok := p.Next(); tok != EOF; tok = p.Next() {
		if def, ok := p.ExpectIdent(); ok {
			p.parseDecl(def)
		} else {
			break
		}
	}
}

func (p *Parser) parseDecl(def string) {
	newDecl, ok := defs[def]
	if ok {
		if name, ok := p.ParseString(); ok {
			decl, ok := decls[name]
			if !ok {
				decl = newDecl(name, def)
				decls[name] = decl
			}
			p.Next()
			if p.Expect('{') {
				decl.Parse(p)
			}
		}
	} else {
		p.Error("unexpected decl type %q", def)
	}
}
