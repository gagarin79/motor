package decl

import (
	"bytes"
	"github.com/gagarin79/motor/fs"
	"errors"
	"io"
	"log"
)

type Interface interface {
	Name() string
	Def() string
	IsValid() bool

	Parse(p *Parser)
	MakeDefault()
}

type pack struct {
	path, pattern string
}

func (p pack) load() {
	matches, err := fs.Glob(p.path + p.pattern)
	if err != nil {
		log.Println(err)
		return
	}
	for _, m := range matches {
		parseFile(m)
	}
}

var (
	defs  = make(map[string]func(string, string) Interface)
	packs = make(map[string]pack)
	decls = make(map[string]Interface)
)

func RegisterDef(def string, new func(string, string) Interface) {
	if _, ok := defs[def]; ok {
		log.Printf("decl definition %q already registerd\n", def)
		return
	}
	defs[def] = new
}

func RegisterPack(path, ext string) {
	name := path + "/*." + ext
	if _, ok := packs[name]; !ok {
		p := pack{path, "/*." + ext}
		p.load()
		packs[name] = p
	}
}

func Lookup(name string) Interface {
	return decls[name]
}

func LookupOrMake(name, def string) Interface {
	decl, ok := decls[name]
	if !ok {
		if newDecl, ok := defs[def]; ok {
			decl = newDecl(name, def)
			decl.MakeDefault()
			return decl
		}
		return nil
	}
	return decl
}

func Reload() {
	for _, p := range packs {
		p.load()
	}
}

func readSource(filename string, src interface{}) ([]byte, error) {
	if src != nil {
		switch s := src.(type) {
		case string:
			return []byte(s), nil
		case []byte:
			return s, nil
		case *bytes.Buffer:
			if s != nil {
				return s.Bytes(), nil
			}
		case io.Reader:
			var buf bytes.Buffer
			if _, err := io.Copy(&buf, s); err != nil {
				return nil, err
			}
			return buf.Bytes(), nil
		}
		return nil, errors.New("invalid source")
	}
	return fs.ReadFile(filename)
}

func parseFile(filename string) {
	text, err := readSource(filename, nil)
	if err != nil {
		log.Println(err)
		return
	}
	var p Parser
	p.Init(filename, bytes.NewBuffer(text))
	p.parseDecls()
}
