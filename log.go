package motor

import (
	"bytes"
	"github.com/gagarin79/motor/fs"
	"io"
	"log"
	"os"
)

var logbuf = new(bytes.Buffer)

func writeLog() {
	f, err := fs.Create(fs.Base, "log.txt")
	if err != nil {
		return
	}
	defer f.Close()
	f.Write(logbuf.Bytes())
}

func init() {
	log.SetFlags(0)
	log.SetOutput(io.MultiWriter(logbuf, os.Stdout))
}
