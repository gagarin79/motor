package main

import (
	"github.com/gagarin79/motor/decl"
	"strconv"
	"strings"
	"unicode"
)

type Pattern struct {
	name, def string
	isValid   bool

	matches []match
}

func (pat Pattern) Name() string {
	return pat.name
}

func (pat Pattern) Def() string {
	return pat.def
}

func (pat Pattern) IsValid() bool {
	return pat.isValid
}

func (pat *Pattern) Parse(p *decl.Parser) {
	pat.matches = make([]match, 0)
	for tok := p.Next(); tok != decl.EOF && tok != '}'; tok = p.Next() {
		if uv, ok := parseCoords(p); ok {
			p.Next()
			if m, ok := parseMatch(p, uv); ok {
				m.uv = uv
				pat.matches = append(pat.matches, m)
			}
		}
	}
	pat.isValid = !p.HadError()
}

func (pat *Pattern) MakeDefault() {
}

func parseCoords(p *decl.Parser) ([3][2]byte, bool) {
	uv := [3][2]byte{}
	text, ok := p.ExpectString()
	if !ok {
		return uv, false
	}
	if strings.Contains(text, "-") {
		coords := strings.Split(text, "-")
		if len(coords) == 2 {
			uv[0], ok = parseCoord(p, coords[0])
			uv[2], ok = parseCoord(p, coords[1])
			if uv[0][0] == uv[2][0] {
				uv[1][0] = uv[0][0]
				uv[1][1] = uv[0][1] + 1
			} else if uv[0][1] == uv[2][1] {
				uv[1][0] = uv[0][0] + 1
				uv[1][1] = uv[0][1]
			} else {
				p.Error("invalid coords: %q", text)
				return uv, false
			}
		} else {
			p.Error("invalid coords: %q", text)
			return uv, false
		}
	} else {
		coords := strings.Split(text, ",")
		if len(coords) == 3 {
			uv[0], ok = parseCoord(p, coords[0])
			uv[1], ok = parseCoord(p, coords[1])
			uv[2], ok = parseCoord(p, coords[2])
		} else {
			p.Error("invalid coords: %q", text)
			return uv, false
		}
	}
	return uv, ok
}

func parseCoord(p *decl.Parser, text string) ([2]byte, bool) {
	uv := [2]byte{0, 0}
	text = strings.TrimSpace(text)
	if text != "" {
		l := unicode.ToUpper(rune(text[0]))
		if l >= 'A' && l <= 'Z' {
			if n, err := strconv.Atoi(text[1:]); err == nil {
				uv[0] = byte(n)
				uv[1] = byte(l - 'A')
			} else {
				p.Error("invalid coords: %q", text)
				return uv, false
			}
		} else {
			p.Error("invalid coords: %q", text)
			return uv, false
		}
	} else {
		p.Error("invalid coords: %q", text)
		return uv, false
	}

	return uv, true
}

type match struct {
	uv [3][2]byte

	mask, val           uint
	blendMask, blendVal uint
}

var matchStringValue = map[string]uint{
	"top":    0x80,
	"bottom": 0x40,
	"left":   0x20,
	"right":  0x10,
	"tl":     0x08,
	"tr":     0x04,
	"bl":     0x02,
	"br":     0x01,
}

func parseMatch(p *decl.Parser, uv [3][2]byte) (match, bool) {
	match := match{}
	text, ok := p.ExpectString()
	if !ok {
		return match, false
	}
	match.mask, match.val = 0xf0, 0x00
	var exclMask uint = 0x00
	matches := strings.Split(text, ",")
	for i := 0; i < len(matches); i++ {
		m := strings.ToLower(strings.TrimSpace(matches[i]))
		if len(m) < 2 {
			ok = false
			p.Error("invalid pattern match value %q", m)
			continue
		}
		excl := false
		if m[0] == '!' {
			excl = true
			m = m[1:]
			v, ok := parseMatchVal(p, m)
			if !ok {
				return match, false
			}
			if excl {
				exclMask |= v
			}
			match.mask |= v
			match.val |= v
		}
	}
	match.val &^= exclMask
	return match, true
}

func parseMatchVal(p *decl.Parser, text string) (uint, bool) {
	switch {
	case text == "none":
		return 0x00, true
	case text == "all":
		return 0xff, true
	case text == "default":
		return 0xf0, true
	}
	val, ok := matchStringValue[text]
	if !ok {
		p.Error("invalid pattern match value %q", text)
	}
	return val, ok
}
