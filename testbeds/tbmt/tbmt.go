package main

import (
	"fmt"
	"github.com/gagarin79/motor"
	"github.com/gagarin79/motor/cmd"
	"github.com/gagarin79/motor/decl"
	"github.com/gagarin79/motor/gfx"
	"github.com/gagarin79/motor/input"
	_ "time"
)

type Material struct {
	name, def string
	isValid   bool
}

func (m Material) Name() string {
	return m.name
}

func (m Material) Def() string {
	return m.def
}

func (m Material) IsValid() bool {
	return m.isValid
}

func (m *Material) Parse(p *decl.Parser) {
	for tok := p.Next(); tok != decl.EOF; tok = p.Next() {
		if tok == '}' {
			break
		}
	}
	m.isValid = !p.HadError()
}

func (m *Material) MakeDefault() {
	m.isValid = true
	fmt.Printf("make default mtl: %q\n", m.name)
}

type Game struct {
	s, s0, s1 gfx.Shader
	x         float32

	state int
}

func (g *Game) Init() {
	decl.RegisterPack("test/shaders", "shader")
	decl.RegisterPack("test/mtls", "mtl")

	g.s0 = gfx.LookupShader("test1")
	g.s1 = gfx.LookupShader("test3")
	g.s = gfx.LookupShader("test2")

	// time.Sleep(500 * time.Millisecond)

	g.state = 1
}

func (g *Game) Shutdown() {
}

func (g *Game) Update() {
	if g.state == 0 {
		return
	}

	gfx.Draw(
		g.s, g.x, 200, 88*3, 107*3,
		0, 0, 1, 1,
	)
	gfx.Draw(
		g.s0, 500, 200, 88*3, 107*3,
		0, 0, 1, 1,
	)
	gfx.Draw(
		g.s1, 600, 200, 88*3, 107*3,
		0, 0, 1, 1,
	)
	g.x++
	if g.x > 400 {
		g.x = 0
	}
}

func handleF10(e input.Event) bool {
	if e.Kind == input.KeyEv && e.Val0 == input.F10 {
		fmt.Println("h.Handle: F10")
		return true
	}
	return false
}

func f1Cmd(args []string) {
	fmt.Println("f1Cmd")
}

func f2Cmd(args []string) {
	fmt.Println("f1Cmd")
}

func init() {
	decl.RegisterDef("pattern", func(name, def string) decl.Interface {
		return &Pattern{name: name, def: def}
	})
	decl.RegisterDef("material", func(name, def string) decl.Interface {
		return &Material{name: name, def: def}
	})

	input.BindKey(input.Escape, "quit")

	cmd.Register("+listCmds", func(a []string) {
		cmd.Execute("listCmds")
	})
	cmd.Register("+listVars", func(a []string) {
		cmd.Execute("listVars")
	})
	cmd.Register("+listDecls", func(a []string) {
		cmd.Execute("listDecls")
	})
	cmd.Register("+restart", func(a []string) {
		cmd.Execute("restart")
	})
	cmd.Register("+listImages", func(a []string) {
		cmd.Execute("listImages")
	})
	cmd.Register("+listResources", func(a []string) {
		cmd.Execute("listResources")
	})

	cmd.Register("f10", f1Cmd)
	cmd.Register("+f11", func(a []string) {
		fmt.Println("f11 down")
	})
	cmd.Register("-f11", func(a []string) {
		fmt.Println("f11 up")
	})

	input.BindKey(input.F1, "+listCmds")
	input.BindKey(input.F2, "+listVars")
	input.BindKey(input.F3, "+listDecls")
	input.BindKey(input.F4, "+listImages")
	input.BindKey(input.F5, "+listResources")

	input.BindKey(input.F10, "f10")
	input.BindKey(input.F11, "+f11")
	input.BindKey(input.F12, "restart")

	input.AddHandler(input.HandlerFunc(handleF10))
}

func main() {
	motor.Run(motor.Config{
		Game:    new(Game),
		Name:    "motortb",
		Title:   "MotorTestbed1",
		Version: "0.0.1",
	})
}
