// Testbed: Voronoi Diagram

package main

import (
	"flag"
	"fmt"
	"github.com/gagarin79/motor/math/geom/voronoi"
	"log"
	"math/rand"
	"os"
	"text/template"
	"time"
)

const SvgTemplate = `<?xml version="1.0" ?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
  "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg width="{{.Width}}px" height="{{.Height}}px" viewBox="0 0 {{.Width}} {{.Height}}"
     xmlns="http://www.w3.org/2000/svg" version="1.1">
  <title>{{.Title}}</title>
  <desc>{{.Description}}</desc>
  <!-- Edges -->
    {{range .Polygons}}
	 	<g stroke="none" stroke-width="{{$.StrokeWidth}}" fill="{{.Color}}">
    		<path d="M{{.Start.X}},{{.Start.Y}} {{range .Points}}L{{.X}},{{.Y}} {{end}}"/>
    	</g>
    {{end}}
 <g fill="red" >
    {{range .Sites}}
    <circle cx="{{.X}}" cy="{{.Y}}" r="{{$.PointRadius}}" />
    {{end}}</g>
</svg>`

var colors = [...]string{
	"slategrey",
	"bisque",
	"blueviolet",
	"darkseagreen",
	"gold",
	"purple",
	"firebrick",
	"darkorange",
	"blanchedalmond",
	"indigo",
	"brown",
	"slategray",
	"lightpink",
}
var (
	pts    = flag.Int("pts", 1000, "points count")
	width  = flag.Int("w", 1365, "width")
	height = flag.Int("h", 768, "height")
)

type Polygon struct {
	Color  string
	Start  *voronoi.Point
	Points []*voronoi.Point
}

type Edge struct {
	Start, End *voronoi.Point
}

type SVG struct {
	Width       float64
	Height      float64
	Sites       []*voronoi.Site
	Polygons    []Polygon
	Title       string
	Description string
	StrokeWidth float64
	PointRadius float64
}

func main() {
	flag.Parse()
	var (
		rnd    = rand.New(rand.NewSource(2934857))
		d      = voronoi.NewDiagram()
		points = make([]*voronoi.Point, *pts)
		dx     = float64(*width)
		dy     = float64(*height)
	)
	for i := 0; i < len(points); i++ {
		points[i] = voronoi.Pt(rnd.Float64()*dx, rnd.Float64()*dy)
	}
	t := time.Now()
	d.Compute(points)
	d.CloseRect(voronoi.Rect(0, 0, dx, dy))
	fmt.Printf("exec time: %v\n", time.Since(t))

	fmt.Println(d)

	svg := SVG{
		Title:       "Voronoi diagram",
		Description: "Edges and points",
		Width:       dx,
		Height:      dy,
		StrokeWidth: 1,
		PointRadius: 1,
		Sites:       d.Sites,
		Polygons:    make([]Polygon, 0, len(points)),
	}
	for i, s := range d.Sites {
		if len(s.Halfedges) == 0 {
			continue
		}
		p := Polygon{Color: colors[i%len(colors)], Start: s.Halfedges[0].Start()}
		for _, h := range s.Halfedges {
			p.Points = append(p.Points, h.End())
		}
		svg.Polygons = append(svg.Polygons, p)
	}

	f, err := os.Create("exp1.svg")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	tmpl := template.Must(template.New("svg").Parse(SvgTemplate))
	if err := tmpl.Execute(f, svg); err != nil {
		log.Fatal(err)
	}
}
