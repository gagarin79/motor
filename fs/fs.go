/*
	Package fs provides access to the virtual file system.

	Virtual file system implements two locations: base and user.

	Base location contains game assets and initial configuration.
	User location contains user data, i. e. generated worlds, game saves, etc.
*/
package fs

import (
	"io/ioutil"
	"log"
	"os"
	"path"
	"path/filepath"
)

// Location represents a location of virtual file system.
type Location int

const (
	Base Location = iota
	User
)

// Directories of locations.
var baseDir, userDir string

// SetDirs sets the directories of locations.
func SetDirs(base, user string) {
	if base != "" {
		baseDir = base
	}
	if user != "" {
		userDir = user
	}
}

// Open opens the named file for reading. First look
// file in base location, then look in user location.
func Open(name string) (*os.File, error) {
	return os.Open(osname(name))
}

// Ceate creates the named file in loc location, truncating
// it if it already exists.
func Create(loc Location, name string) (*os.File, error) {
	return os.Create(locname(loc, name))
}

// ReadDir reads the directory named by dirname and returns
// a list of sorted directory entries. First look
// directory in base location, then look in user location.
func ReadDir(dirname string) ([]os.FileInfo, error) {
	return ioutil.ReadDir(osname(dirname))
}

// ReadFile reads the file named by filename and returns the contents.
// First look file in base location, then look in user location.
func ReadFile(filename string) ([]byte, error) {
	return ioutil.ReadFile(osname(filename))
}

// WriteFile writes data to a file named by filename.
// First look file in base location, then look in user location.
func WriteFile(loc Location, filename string, data []byte, perm os.FileMode) error {
	return ioutil.WriteFile(locname(loc, filename), data, perm)
}

// Glob returns the names of all files matching pattern or nil
// if there is no matching file. First look pattern directory
// in base location, then look in user location.
func Glob(pattern string) ([]string, error) {
	path := filepath.Dir(pattern)
	matches, err := filepath.Glob(osname(pattern))
	if err != nil {
		return nil, err
	}
	for i := 0; i < len(matches); i++ {
		match, err := filepath.Rel(osname(path), matches[i])
		if err != nil {
			return nil, err
		}
		matches[i] = filepath.ToSlash(filepath.Join(path, match))
	}
	return matches, nil
}

// osname convert the name to os-specified name. First look
// in base location, then in user location.
func osname(name string) string {
	osname := path.Join(baseDir, name)
	if _, err := os.Stat(osname); os.IsNotExist(err) {
		osname = path.Join(userDir, name)
	}
	return osname
}

// locname join the loc directory name and the name.
func locname(loc Location, name string) string {
	if loc == User {
		return path.Join(userDir, name)
	}
	return path.Join(baseDir, name)
}

func init() {
	baseDir = os.Getenv("MOTOR_BASE_PATH")
	if baseDir == "" {
		exepath, err := lookupExecutable()
		if err != nil {
			panic(err)
		}
		baseDir = path.Join(path.Dir(exepath), "data")
	}
	if _, err := os.Stat(baseDir); os.IsNotExist(err) {
		log.Fatalf("base %q dir not found", baseDir)
	}

	userDir = os.Getenv("MOTOR_USER_PATH")
	if userDir == "" {
		userDir = baseDir
	}
	os.Mkdir(userDir, 0755)
}
