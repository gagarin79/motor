package fs

import "syscall"

func lookupExecutable() (string, error) {
	f, err := Open("/proc/" + itoa(Getpid()) + "/text")
	if err != nil {
		return "", err
	}
	defer f.Close()
	return syscall.Fd2path(int(f.Fd()))
}
