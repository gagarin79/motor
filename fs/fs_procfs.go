// +build linux netbsd openbsd

package fs

import (
	"errors"
	"os"
	"runtime"
)

func lookupExecutable() (string, error) {
	switch runtime.GOOS {
	case "linux":
		return os.Readlink("/proc/self/exe")
	case "netbsd":
		return os.Readlink("/proc/curproc/exe")
	case "openbsd":
		return os.Readlink("/proc/curproc/file")
	}
	return "", errors.New("lookupExecutable not implemented for " + runtime.GOOS)
}
