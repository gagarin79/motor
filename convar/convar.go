/*
   Package convar implements variables binding to configuration file.

   Usage:

   Bind variables using convar.String(), Bool(), Int(), etc.
*/
package convar

import (
	"encoding/json"
	"fmt"
	"io"
	"sort"
	"strconv"
)

// bool Var
type boolVar bool

func newBoolVar(val bool, p *bool) *boolVar {
	*p = val
	return (*boolVar)(p)
}

func (b *boolVar) Set(s string) error {
	v, err := strconv.ParseBool(s)
	*b = boolVar(v)
	return err
}

func (b *boolVar) String() string { return fmt.Sprintf("%v", *b) }

// int Var
type intVar int

func newIntVar(val int, p *int) *intVar {
	*p = val
	return (*intVar)(p)
}

func (i *intVar) Set(s string) error {
	v, err := strconv.ParseInt(s, 0, 64)
	*i = intVar(v)
	return err
}

func (i *intVar) String() string { return fmt.Sprintf("%v", *i) }

// int64 Var
type int64Var int64

func newInt64Var(val int64, p *int64) *int64Var {
	*p = val
	return (*int64Var)(p)
}

func (i *int64Var) Set(s string) error {
	v, err := strconv.ParseInt(s, 0, 64)
	*i = int64Var(v)
	return err
}

func (i *int64Var) String() string { return fmt.Sprintf("%v", *i) }

// uint Var
type uintVar uint

func newUintVar(val uint, p *uint) *uintVar {
	*p = val
	return (*uintVar)(p)
}

func (i *uintVar) Set(s string) error {
	v, err := strconv.ParseUint(s, 0, 64)
	*i = uintVar(v)
	return err
}

func (i *uintVar) String() string { return fmt.Sprintf("%v", *i) }

// uint64 Var
type uint64Var uint64

func newUint64Var(val uint64, p *uint64) *uint64Var {
	*p = val
	return (*uint64Var)(p)
}

func (i *uint64Var) Set(s string) error {
	v, err := strconv.ParseInt(s, 0, 64)
	*i = uint64Var(v)
	return err
}

func (i *uint64Var) String() string { return fmt.Sprintf("%v", *i) }

// float32 Var
type float32Var float32

func newFloat32Var(val float32, p *float32) *float32Var {
	*p = val
	return (*float32Var)(p)
}

func (f *float32Var) Set(s string) error {
	v, err := strconv.ParseFloat(s, 32)
	*f = float32Var(v)
	return err
}

func (f *float32Var) String() string { return fmt.Sprintf("%v", *f) }

// float64 Var
type float64Var float64

func newFloat64Var(val float64, p *float64) *float64Var {
	*p = val
	return (*float64Var)(p)
}

func (f *float64Var) Set(s string) error {
	v, err := strconv.ParseFloat(s, 64)
	*f = float64Var(v)
	return err
}

func (f *float64Var) String() string { return fmt.Sprintf("%v", *f) }

// string Var
type stringVar string

func newStringVar(val string, p *string) *stringVar {
	*p = val
	return (*stringVar)(p)
}

func (s *stringVar) Set(val string) error {
	*s = stringVar(val)
	return nil
}

func (s *stringVar) String() string { return fmt.Sprintf("%s", *s) }

// Var is the interface to the dynamic variable stored in configuration.
type Var interface {
	String() string
	Set(string) error
}

// A Config represents a set of defined vars.
type Config struct {
	name string
	vars map[string]Var
}

// sortVars returns the vars names as a slice in lexicographical order.
func sortVars(vars map[string]Var) []string {
	list := make(sort.StringSlice, len(vars))
	i := 0
	for k := range vars {
		list[i] = k
		i++
	}
	list.Sort()
	return list
}

// Visit visits the vars in lexicographical order, calling fn for each.
func (c *Config) Visit(fn func(string, Var)) {
	for _, k := range sortVars(c.vars) {
		fn(k, c.vars[k])
	}
}

// Visit visits the vars in lexicographical order, calling fn for each.
func Visit(fn func(string, Var)) {
	defaultConfig.Visit(fn)
}

// Lookup returns the Var interface of the named var, returning nil if none exists.
func (c *Config) Lookup(name string) Var {
	return c.vars[name]
}

// Lookup returns the Var interface of the named var, returning nil if none exists.
func Lookup(name string) Var {
	return defaultConfig.Lookup(name)
}

// Set sets the value of the named var.
func (c *Config) Set(name, value string) error {
	cvar, ok := c.vars[name]
	if !ok {
		return fmt.Errorf("no such var %v", name)
	}
	err := cvar.Set(value)
	if err != nil {
		return err
	}
	return nil
}

// Set sets the value of the named var.
func Set(name, value string) error {
	return defaultConfig.Set(name, value)
}

// Bool defines a bool var with specified name and default value.
// The argument p points to a bool variable in which to store the value of the var.
func (c *Config) Bool(p *bool, name string, value bool) {
	c.Def(newBoolVar(value, p), name)
}

// Bool defines a bool var with specified name and default value.
// The argument p points to a bool variable in which to store the value of the var.
func Bool(p *bool, name string, value bool) {
	defaultConfig.Bool(p, name, value)
}

// Int defines a int var with specified name and default value.
// The argument p points to a int variable in which to store the value of the var.
func (c *Config) Int(p *int, name string, value int) {
	c.Def(newIntVar(value, p), name)
}

// Int defines a int var with specified name and default value.
// The argument p points to a int variable in which to store the value of the var.
func Int(p *int, name string, value int) {
	defaultConfig.Int(p, name, value)
}

// Int64 defines a int64 var with specified name and default value.
// The argument p points to a int64 variable in which to store the value of the var.
func (c *Config) Int64(p *int64, name string, value int64) {
	c.Def(newInt64Var(value, p), name)
}

// Int64 defines a int64 var with specified name and default value.
// The argument p points to a int64 variable in which to store the value of the var.
func Int64(p *int64, name string, value int64) {
	defaultConfig.Int64(p, name, value)
}

// Uint defines a uint var with specified name and default value.
// The argument p points to a uint variable in which to store the value of the var.
func (c *Config) Uint(p *uint, name string, value uint) {
	c.Def(newUintVar(value, p), name)
}

// Uint defines a uint var with specified name and default value.
// The argument p points to a uint variable in which to store the value of the var.
func Uint(p *uint, name string, value uint) {
	defaultConfig.Uint(p, name, value)
}

// Uint64 defines a uint64 var with specified name and default value.
// The argument p points to a uint64 variable in which to store the value of the var.
func (c *Config) Uint64(p *uint64, name string, value uint64) {
	c.Def(newUint64Var(value, p), name)
}

// Uint64 defines a uint64 var with specified name and default value.
// The argument p points to a uint64 variable in which to store the value of the var.
func Uint64(p *uint64, name string, value uint64) {
	defaultConfig.Uint64(p, name, value)
}

// Float32 defines a float32 var with specified name and default value.
// The argument p points to a float32 variable in which to store the value of the var.
func (c *Config) Float32(p *float32, name string, value float32) {
	c.Def(newFloat32Var(value, p), name)
}

// Float32 defines a float32 var with specified name and default value.
// The argument p points to a float32 variable in which to store the value of the var.
func Float32(p *float32, name string, value float32) {
	defaultConfig.Float32(p, name, value)
}

// Float64 defines a float64 var with specified name and default value.
// The argument p points to a float64 variable in which to store the value of the var.
func (c *Config) Float64(p *float64, name string, value float64) {
	c.Def(newFloat64Var(value, p), name)
}

// Float64 defines a float64 var with specified name and default value.
// The argument p points to a float64 variable in which to store the value of the var.
func Float64(p *float64, name string, value float64) {
	defaultConfig.Float64(p, name, value)
}

// String defines a string var with specified name and default value.
// The argument p points to a string variable in which to store the value of the var.
func (c *Config) String(p *string, name string, value string) {
	c.Def(newStringVar(value, p), name)
}

// String defines a string var with specified name and default value.
// The argument p points to a string variable in which to store the value of the var.
func String(p *string, name string, value string) {
	defaultConfig.String(p, name, value)
}

// Def defines a flag with the specified name.
func (c *Config) Def(v Var, name string) {
	_, alreadythere := c.vars[name]
	if alreadythere {
		panic(fmt.Sprintf("%s var redefined: %s", c.name, name))
	}
	if c.vars == nil {
		c.vars = make(map[string]Var)
	}
	c.vars[name] = v
}

// Def defines a flag with the specified name.
func Def(v Var, name string) {
	defaultConfig.Def(v, name)
}

// ReadFrom read from reads vars from r.
// The data in r must be in JSON format.
func (c *Config) Read(r io.Reader) error {
	vars := make(map[string]string)
	for k, v := range c.vars {
		vars[k] = v.String()
	}
	dec := json.NewDecoder(r)
	if err := dec.Decode(&vars); err == io.EOF {
		return nil
	} else if err != nil {
		return err
	}
	var firstErr error
	for k, v := range vars {
		if cvar, ok := c.vars[k]; ok {
			if err := cvar.Set(v); err != nil {
				firstErr = err
			}
		}
	}
	return firstErr
}

// ReadFrom read from reads vars from r.
// The data in r must be in JSON format.
func Read(r io.Reader) error {
	return defaultConfig.Read(r)
}

// Write writes all vars to w in JSON format.
func (c *Config) Write(w io.Writer) error {
	vars := make(map[string]string)
    c.Visit(func (name string, cvar Var) {
        vars[name] = cvar.String()
    })
	b, err := json.MarshalIndent(vars, "", "\t")
	if err != nil {
		return err
	}
	_, err = w.Write(b)
	return err
}

// Write writes all vars to w in JSON format.
func Write(w io.Writer) error {
	return defaultConfig.Write(w)
}

// The default set of command-line flags, parsed from os.Args.
var defaultConfig = NewConfig("default")

// NewConfig returns a new, empty config with specified name.
func NewConfig(name string) *Config {
	return &Config{name: name}
}
