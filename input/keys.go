package input

type Key int

const (
	Tab    = iota + 9
	Enter  = 13
	Escape = iota + 27 - 2
	Space  = iota + 32 - 3

	Backspace = iota + 127 - 4

	Command = iota + 128 - 5
	Capslock
	Scroll
	Power
	Pause

	UpArrow = iota + 133 - 10
	DownArrow
	LeftArrow
	RightArrow

	LSuper = iota + 137 - 14
	RSuper
	Menu

	Alt = iota + 140 - 17
	Ctrl
	Shift
	Ins
	Del
	PgDn
	PgUp
	Home
	End

	F1 = iota + 149 - 26
	F2
	F3
	F4
	F5
	F6
	F7
	F8
	F9
	F10
	F11
	F12
	F13
	F14
	F15

	KpHome
	KpUpArrow
	KpPgUp
	KpLeftArrow
	Kp5
	KpRightArrow
	KpEnd
	KpDownArrow
	KpPgDn
	KpEnter
	KpIns
	KpDel
	KpSlash
	KpMinus
	KpPlus
	KpNumlock
	KpStar
	KpEquals

	Mouse1
	Mouse2
	Mouse3

	MWheelDown
	MWheelUp

	PrintScr = iota + 252 - 65
	RightAlt = iota + 253 - 66

	LastKey = iota + 255 - 67
)
