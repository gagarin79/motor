package input

const (
	maxEvents = 256
	eventMask = maxEvents - 1
)

const (
	NoneEv = iota
	KeyEv
	CharEv
	MouseEv
)

type EventKind int

type Event struct {
	Kind       EventKind
	Val0, Val1 int
}

type Handler interface {
	Handle(e Event) bool
}

type HandlerFunc func(e Event) bool

func (f HandlerFunc) Handle(e Event) bool {
	return f(e)
}

var (
	events       [maxEvents]Event
	ehead, etail int
)

func PushEvent(kind EventKind, val0, val1 int) {
	e := &events[ehead&eventMask]
	if ehead-etail >= maxEvents {
		etail++
	}
	ehead++
	e.Kind = kind
	e.Val0, e.Val1 = val0, val1
}

func getEvent() Event {
	if ehead > etail {
		etail++
		return events[(etail-1)&eventMask]
	}
	return Event{}
}

var handlers = make([]Handler, 0)

func AddHandler(h Handler) {
	if findHandler(h) != -1 {
		return
	}
	handlers = append(handlers, h)
}

func RemoveHandler(h Handler) {
	i := findHandler(h)
	if i != -1 {
		handlers = append(handlers[:i], handlers[i+1:]...)
	}
}

func findHandler(h Handler) int {
	for i := 0; i < len(handlers); i++ {
		if handlers[i] == h {
			return i
		}
	}
	return -1
}
