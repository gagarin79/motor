package input

import (
	"github.com/gagarin79/motor/cmd"
	"log"
)

var X, Y int

var captured bool

func Init() {
	log.Println("init input: ok")
}

func Shutdown() {
	log.Println("shutdown input: ok")
}

func Capture() {
	if !captured {
		captureInput()
		captured = true
	}
}

func Release() {
	if captured {
		releaseInput()
		captured = false
	}
}

func Update() {
	pollEvents()

	for i := 0; i < 1024; i++ {
		e := getEvent()
		if e.Kind == NoneEv {
			break
		}
		if e.Kind == MouseEv {
			X += e.Val0
			Y += e.Val1
			if X < 0 {
				X = 0
			}
			if Y < 0 {
				Y = 0
			}
		}
		if e.Kind == KeyEv {
			k := &keys[e.Val0]
			k.wasdown = k.down
			k.down = e.Val1 != 0
		}
		done := false
		for _, h := range handlers {
			if h.Handle(e) {
				done = true
				break
			}
		}
		if !done && e.Kind == KeyEv {
			runKeyCmd(Key(e.Val0))
		}
	}
}

type keyState struct {
	down, wasdown bool
	trigger       bool
	cmd, altcmd   string
}

var keys [LastKey]keyState

func BindKey(key Key, cmd string) bool {
	if key < 0 || key >= LastKey || len(cmd) < 1 {
		return false
	}
	k := &keys[key]
	k.cmd = cmd
	k.trigger = cmd[0] == '+'
	if k.trigger {
		k.altcmd = "-" + cmd[1:]
	}
	return true
}

func UnbindKey(key Key) bool {
	if key < 0 || key >= LastKey {
		return false
	}
	k := &keys[key]
	if k.cmd == "" {
		return false
	}
	k.cmd = ""
	k.trigger = false
	return true
}

func runKeyCmd(key Key) {
	k := &keys[key]
	if k.cmd != "" {
		if k.trigger {
			if k.down && !k.wasdown {
				cmd.Execute(k.cmd)
			} else if !k.down && k.wasdown {
				cmd.Execute(k.altcmd)
			}
		} else {
			cmd.Execute(k.cmd)
		}
	}
}
