package input

import (
	"syscall"
	"unsafe"

	"github.com/AllenDang/w32"
)

var (
	Hwnd w32.HWND

	cx, cy int
)

func pollEvents() {
	if captured {
		x, y, _ := w32.GetCursorPos()
		w32.SetCursorPos(cx, cy)
		dx, dy := x-cx, y-cy
		if dx != 0 || dy != 0 {
			PushEvent(MouseEv, dx, dy)
		}
	}
}

func captureInput() {
	r := w32.GetWindowRect(Hwnd)

	w, h := w32.GetSystemMetrics(w32.SM_CXSCREEN), w32.GetSystemMetrics(w32.SM_CYSCREEN)

	if r.Left < 0 {
		r.Left = 0
	}
	if r.Top < 0 {
		r.Top = 0
	}
	if r.Right >= int32(w) {
		r.Right = int32(w - 1)
	}
	if r.Bottom >= int32(h) {
		r.Bottom = int32(h - 1)
	}
	cx, cy = int(r.Left + (r.Right-r.Left)/2), int(r.Top + (r.Bottom-r.Top)/2)

	w32.SetCursorPos(cx, cy)

	w32.SetCapture(Hwnd)
	clipCursor(r)
	for showCursor(false) >= 0 {
	}
}

func releaseInput() {
	for showCursor(true) < 0 {
	}
	clipCursor(nil)
	w32.ReleaseCapture()
}

var (
	moduser32 = syscall.NewLazyDLL("user32.dll")

	procClipCursor = moduser32.NewProc("ClipCursor")
	procShowCursor = moduser32.NewProc("ShowCursor")
)

func clipCursor(r *w32.RECT) bool {
	ret, _, _ := procClipCursor.Call(uintptr(unsafe.Pointer(r)))
	return ret != 0
}

func showCursor(show bool) int {
	var f int32
	if show {
		f = 1
	} else {
		f = 0
	}
	ret, _, _ := procShowCursor.Call(uintptr(f))
	return int(ret)
}
